import {DataTranslateInterface} from '../app/interfaces/data-translate.interface';

const data: DataTranslateInterface = {
    T: {
        LANG: 'en',
        roles: {
            ADMINISTRATOR: 'Administrator',
            CREATOR: 'Purchases creator',
            SELLER: 'Seller',
            CURATOR: 'Curator',
            AUDITOR: 'Auditor',
        },
        footer: {
            copyright: 'All Rights Reserved',
            copyrightBy: 'Copyrights by GPC Azerbaijan',
            address: {
                london: {
                    street: '43 Bedford str., office 11',
                    country: 'London, United Kingdom',
                },
                baku: {
                    street: '69 Nizami str., Isr Plaza,',
                    country: 'AZ1005, Azerbaijan, Baku',
                }
            }
        },
        pages: {
            COMPANY: 'COMPANY PROFILE',
            SERVICES: 'SERVICES',
            PARTNERS: 'PARTNERS',
            NEWS: 'NEWS',
            USEFUL_INFORMATION: 'USEFUL INFORMATION',
            CONTACTS: 'CONTACTS',
            PURCHASES: 'PURCHASES'
        },
        buttons: {
            save: 'Save',
            saveAndPrint: 'Save and Print',
            saveAndPublish: 'Save and Publish',
            publish: 'Publish',
            send: 'Send',
            cancel: 'Cancel',
            sendAll: 'Send all',
            add: 'Add',
            print: 'Print',
            delete: 'Delete',
            signin: 'Sign in',
            file: 'Selected file',
            seeMore: 'Show more',
        },
        actions: {
            edit: 'Edit',
            back: 'Go back',
            open: 'Open',
            reply: 'Reply',
            instruction: '1) Fill out the fields 2) Click the Save button 3) Upload files 4) Click the button ',
        },
        files: {
            documents: 'Documentation',
            limit: 'file size not more than',
            prepare: 'Documents to download',
            name: 'Name',
            size: 'Size',
            status: 'Status',
            actions: 'Actions',
            upload: 'Upload',
            cancel: 'Cancel',
            remove: 'Remove',
            uploadAll: 'Upload all',
            cancelAll: 'Cancel all',
            removeAll: 'Remove all',
            queueProgress: 'Queue Progress',
            loaded: 'Documents loaded',
            error: 'file upload Error',
            sizeError: 'file size exceeds',
            maxFiles: 'no more than',
            extentionError: 'invalid file extension'
        },
        tenders: {
            id: '№ purchase',
            forAdd: 'purchase',
            forNumber: 'Purchase',
            name: 'Name',
            period: 'Period display',
            sphere: 'Sphere',
            viewdocs: 'view documentation',
            currency: 'Currency',
            additional: 'Additional information',
            documents: 'Documentation',
            groupSellers: 'Suppliers Group',
            emptyList: 'Public of purchases is currently not',
            creator: 'Performer',
            number: '№',
            closed: 'Purchase closed',
            chatFiles: 'Additional files from curator',
            sellerFiles: 'Additional files from seller',
            operatingCompany: 'Operating company',
            curator: 'Curator',
            status: 'My status',
            startDate: 'start date / time',
            endDate: 'end date / time',
            printError: 'Tenders for printing are not selected',
            periodType: 'Urgency of tender',
        },
        goffers: {
            id: '№ offer',
            date: 'Date of offer',
            companyName: 'Company',
            question: 'Questions on purchase',
            contactName: 'Contact person',
            contactPhone: 'Contact phone',
            contactPosition: 'Contact person position',
            additional: 'Additional information',
            documents: 'Documentation',
            viewdocs: 'view documentation',
            proposalStateFalse: 'NO',
            proposalStateFalseText: 'offer',
            proposalStateTrue: 'YES',
            proposalStateTrueText: 'offer',
        },
        header: {
            signin: 'Sign in',
            signout: 'Sign out',
            changePassword: 'Change password',
            rootPage: 'Home page',
        },
        openTenders: {
            pageHeader: 'Published company purchases',
            createTender: 'Create purchase',
            changeTenders: 'Edit purchase',
            changeTender: 'Edit purchas',
            publishedTenders: 'waiting to start',
            activeTenders: 'active',
            finishedTenders: 'finished',
            adminPage: 'Admin page',
            closedTenders: 'Closed purchases',
            myOffers: 'Supplier Cabinet'
        },
        closedTenders: {
            pageHeader: 'Closed purchases',
            emptyList: 'Purchases is currently not',
        },
        sellerGroup: {
            pageHeader: 'Seller Groups',
        },
        groupEmails: {
            pageHeader: 'Email for group mailing',
        },
        curators: {
            pageHeader: 'List of curators',
            editBoss: 'Change email boss',
            addCurator: 'Add curator'
        },
        newTender: {
            pageHeader: 'Create purchase',
            nameOfTender: 'Name of purchase',
            startDate: 'Submit request',
            endDate: 'Deadline for com. proposals',
            time: 'Time',
            curator: 'Curator',
            emailcur: 'Email curator',
            namecur: 'Name and position of curator',
            example: 'example',
            fileLock: 'To upload files, you need to save the purchase',
            period: 'Urgency of application',
            urgentNumber: 'Urgent request Number',
            urgentDate: 'Urgent request creation date',
            quarter: 'Quarter',
            fastSearch: 'Quick search...',
        },
        newTenderDialogs: {
            noEditPublish: 'Purchase published, cannot be edited',
            noForgetPublish: 'Don\'t forget to post purchase',
            successSaved: 'Purchase saved successfully',
            notSaved: 'The Purchase was not saved',
            successPublished: 'Purchase successfully published',
            notPublished: 'Purchase failed to publish',
            deleteSelected: 'Delete the selected purchase?',
            successDeleted: 'Purchases successfully removed',
            isPublishTender: 'Publish Purchase?',
            afterPublishNoChanges: 'once published, the offer cannot be changed ...',
        },
        tendersEdit: {
            pageHeader: 'Edit unpublish purchases',
        },
        checkSeller: {
            pageHeader: 'Member registration',
            phankyou: 'Thank you for your confidence in our company!',
            askform: 'Fill out the form and get access to full information on purchasing',
            fio: 'Full Name',
            your: 'Your',
            yourPassword: 'Your password to login',
            phone: 'Phone number',
            company: 'Company name',
            position: 'Position'
        },
        login: {
            pageHeader: 'Login for users',
            login: 'Login',
            password: 'Password',
            forgot: 'Forgot your password?',
        },
        recovery: {
            successMessageStart: 'To Your e-mail',
            successMessageEnd: 'sent a letter with instructions to reset',
        },
        changepwd: {
            pageHeader: 'Set a new password',
            password2: 'Enter password again',
        },
        admin: {
            pageHeader: 'Admin page',
            save: 'Save',
        },
        tender: {
            pageHeader: 'The documentation for the purchase',
            toOffer: 'Post offer',
            toChat: 'Ask the curator',
        },
        newOffer: {
            pageHeader: 'Offer for participation in the purchase',
            fileLock: 'To upload files, you need to save the offer',
            publish: 'Publish offer',
            sendAsk: 'Send a question',
            saveAndPublish: 'Save and publish offer',
            saveAndSendAsk: 'Save and send a question',
        },
        chat: {
            pageHeader: 'Questions and answers',
            seller: 'Seller',
            curator: 'Curator',
            header: 'Header of curator',
            boss: 'Boss',
            myFiles: 'My files',
        },
        tenderGroupUnlock: {
            pageHeader: 'Open the offer for the purchase',
            inputKeys: 'Enter the access keys',
            key: 'key',
            updateTender: 'Extend the period',
            unlock: 'Open results',
        },
        offers: {
            pageHeader: 'Purchase requisition',
            list: 'List of proposals received',
            chat: 'Questions to the curator',
            dirty: 'Drafts',
            files: 'Files from provider',
            chatFiles: 'Files from curator'
        },
        offer: {
            pageHeader: 'Documentation on request'
        },
        offerDialogs: {
            successSaveTitle: 'Don\'t forget to publish your offer',
            successSaveContent: 'Offer successfully saved',
            successPublished: 'The offer was successfully published',
            notPublished: 'The offer has failed to publish',
            successPublishedToChat: 'Offer was successfully sent to the chat',
            notPublishedToChat: 'The offer was unable to send to chat',
            noEditPublish: 'The offer has been sent and cannot be edited',
            notSaved: 'Offer not saved',
            isPublishOffer: 'To publish the offer?',
            isPublishOfferToChat: 'Send a chat request?',
            afterPublishNoChanges: 'The offer cannot be changed ...',
        },
        sellerPage: {
            complete: 'My sent offers',
            chat: 'My sent questions to curator',
            dirtyComplete: 'My unsent offers',
            dirtyChat: 'My pending questions to the curator',
        },
        curatorPage: {
            pageHeader: 'Open chat with the vendors',
            endDate: 'Purchase term',
            messageText: 'Last message',
            messageTime: 'Message date',
            answerTime: 'Curator response',
            chatLink: 'Open chat',
            answerEnd: 'Need to answer before the deadline',
            yourAnswer: 'You answered',
            curatorAnswer: 'Curator answered',
            quarter: 'quarter',
            urgently: 'urgent',
        },
        periodType: {
            urgent: 'urgent request',
            quarter: 'quarterly request',
        },
        quarterType: {
            first: '1st quarter',
            second: '2nd quarter',
            third: '3rd quarter',
            fourth: '4th quarter'
        },
        form: {
            headers: {
                invite: 'Register'
            },
            errors: {
                required: 'This field is required',
                email: 'Incorrect email',
                minLength: 'Minimum length',
                userRegistered: 'User registered',
            },
            help: {
                password: 'Minimum password length 8 characters',
                lessPassword: 'Weak password',
            }
        },
        cabinetPages: {
            seller: {
                openTenders: 'Open the purchase of the company',
                offers: 'My requests',
                offersActive: 'Published for the part',
                offersChat: 'Sent to chat only',
                offersDraft: 'Not sent',
                offerChat: 'chat only',
                offerActive: 'active for tender',
            }
        },
        dialogs: {
            outPageQuestion: 'Leave the page?',
            haveUnSaveChanges: 'you have unsaved changes ...',
            updateTenderQuestion: 'Are you sure you want to extend the purchase?',
            unlockTenderQuestion: 'Are you sure you want to open the purchase?',
            yes: 'Yes',
            no: 'No',
            repeatAfterError: 'Error! Try&nbsp;again',
            successCreated: 'Successfully created',
            successUpdated: 'Successfully updated',
            successDeleted: 'Successfully removed',
            successDone: 'Successful',
            error: 'Error',
            warning: 'Warning',
        },
        authErrors: {
            error: 'Error',
            email: {
                invalidValue: 'Invalid email',
            },
            user: {
                userBanned: 'User is blocked',
            },
            sendEmail: {
                error: 'The email was not sent for some reason, please contact technical support',
            },
            nonFieldErrors: {
                captchaInvalid: 'Incorrect code',
            },
            auth: {
                invalidValue: 'Invalid login or password',
                passwordValue: 'Invalid password',
            },
            uuid: {
                invalidValue: 'Invalid invitation code',
            },
        },
        networkError: 'Network error',
        notFoundError: 'Page does not exist',
        forbiddenError: 'Access denied',
        formErrors: {
            invalidValue: 'Invalid value',
        },
        auditorPages: {
            invite: {
                warning: 'Tender extended, wait for closing',
                opened: 'The Results have already been opened earlier',
            }
        },
        statistics: {
            invites: 'Number of contractors invited to the tender',
            proposals: 'Number of commercial offers received',
            questions: 'The number of questions to the curator'
        },
        settingsPage: {
            pageHeader: 'Change password',
            oldPassword: 'Old password',
            newPassword: 'New password',
            repeatNewPassword: 'Repeat new password',
            repeatError: 'Password Repeat Error',
            saveError: 'Password not saved',
        },
        emptyList: 'List is empty',
        noMessages: 'No messages',
    }
};

export default data;
