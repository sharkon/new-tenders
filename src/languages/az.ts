import {DataTranslateInterface} from '../app/interfaces/data-translate.interface';

const data: DataTranslateInterface = {
    T: {
        LANG: 'az',
        roles: {
            ADMINISTRATOR: 'Administrator',
            CREATOR: 'Alışın icraçısı',
            SELLER: 'Satici',
            CURATOR: 'Kurator',
            AUDITOR: 'Auditor',
        },
        footer: {
            copyright: 'Bütün hüquqlar qorunur',
            copyrightBy: 'GPC Azərbaycan',
            address: {
                london: {
                    street: 'Bedford küçəsi 43, ofis 11',
                    country: 'London, Birləşmiş Krallıq',
                },
                baku: {
                    street: 'Nizami kücəsi 69, Isr Plaza,',
                    country: 'AZ1005, Azərbaycan, Bakı',
                }
            }
        },
        pages: {
            COMPANY: 'ŞİRKƏTİMİZ',
            SERVICES: 'XİDMƏTLƏR',
            PARTNERS: 'PARTNYORLAR',
            NEWS: 'XƏBƏRLƏR',
            USEFUL_INFORMATION: 'FAYDALI MƏLUMAT',
            CONTACTS: 'ƏLAQƏ',
            PURCHASES: 'Alış'
        },
        buttons: {
            save: 'Yadda saxlamaq',
            add: 'Əlavə etmək',
            saveAndPrint: 'Saxlaya və çap',
            saveAndPublish: 'Saxlaya və dərc',
            publish: 'Dərc etmək',
            send: 'Göndərmək',
            cancel: 'Ləğv etmək',
            sendAll: 'Hamıya göndərmək ',
            print: 'Çap',
            delete: 'Sil',
            signin: 'Daxil ol',
            file: 'Faylları seçin',
            seeMore: 'Daha göstərmək',
        },
        actions: {
            edit: 'redaktəetmək',
            back: 'Geriyə qayıtmaq',
            open: 'Açmak',
            reply: 'Cavab vermək',
            instruction: '1) Sətirləri doldurun 2) Yadda saxla düyməsini sıxın 3) Fayılları yükləyin 4) Mətbuat ',
        },
        files: {
            documents: 'Əlavə ediləcək sənədlər',
            limit: 'faylın ölçüsü artıq olmamalıdır',
            prepare: 'Yükləniləcək sənədlər',
            name: 'Ad',
            size: 'Ölçü',
            status: 'Status',
            actions: 'Hərəkət',
            upload: 'Yükləmək',
            cancel: 'Ləğv etmək',
            remove: 'Silmək',
            uploadAll: 'Hamsını yükləmək',
            cancelAll: 'Hamsını ləğv etmək',
            removeAll: 'Hamsını silmək',
            queueProgress: 'Yükləmə procesi',
            loaded: 'Yüklənilmiş sənədlər',
            error: 'Səhv upload fayl',
            sizeError: 'fayl ölçüsü çoxdur',
            maxFiles: 'faylları daha',
            extentionError: 'yolverilməz genişləndirilməsi fayl'
        },
        tenders: {
            id: '№ alış',
            forAdd: 'alış',
            forNumber: 'Alış',
            name: 'Ad',
            period: 'Sifariş müddəti',
            sphere: 'Sahə',
            viewdocs: 'Sənədə bax',
            currency: 'Məzənnə',
            additional: 'Əlavə məlumat',
            documents: 'Sənədlər',
            groupSellers: 'Təhcizatçı qruupu',
            emptyList: 'Hal-hazırda açıq alış yoxdur',
            creator: 'İcraçı',
            number: '№',
            closed: 'Alış bağlı',
            chatFiles: 'kuratordan əlavə fayllar',
            sellerFiles: 'satıcıdan əlavə fayllar',
            operatingCompany: 'Əməliyyat şirkəti',
            curator: 'Küratör',
            status: 'Mənim status',
            startDate: 'Tarix/vaxt başlamaq',
            endDate: 'Tarix/vaxt başa çatdıqdan',
            printError: 'Tender üçün mətbuat seçilib',
            periodType: 'Təcili tətbiqi',
        },
        goffers: {
            id: '№ Alış',
            date: 'Alış tarixi',
            companyName: 'Şirkətin adı',
            question: 'Alış üzrə suallar',
            contactName: 'Əlaqəli şəxs',
            contactPhone: 'Telefon',
            contactPosition: 'Vəzifə',
            additional: 'Əlavə məlumat',
            documents: 'Sənədlər',
            viewdocs: 'Sənədlərə bax',
            proposalStateFalse: 'YOXDUR',
            proposalStateFalseText: 'Alış',
            proposalStateTrue: 'VAR',
            proposalStateTrueText: 'Alış',
        },
        header: {
            signin: 'Daxil ol',
            signout: 'Çıxmaq',
            changePassword: 'Parolu dəyişdir',
            rootPage: 'şəxsi kabinet',
        },
        openTenders: {
            pageHeader: 'Yayınlanan şirkət satınalmaları',
            createTender: 'Alışı yaradmaq',
            changeTenders: 'Alışı dəyişmək',
            changeTender: 'Alışı dəyişmək',
            publishedTenders: 'göndərməyə başlamaq üçün gözləyir',
            activeTenders: 'açıq',
            finishedTenders: 'bağlanıb',
            adminPage: 'Administratorun səhifəsi',
            closedTenders: 'Alışı bağlı',
            myOffers: 'Təchizatçı Cabinet'
        },
        closedTenders: {
            pageHeader: 'Alışı bağlı',
            emptyList: 'Hal-hazırda alış yoxdur',
        },
        sellerGroup: {
            pageHeader: 'Qrupunun satıcılar',
        },
        groupEmails: {
            pageHeader: 'E-poçt göndərmək üçün qrup',
        },
        curators: {
            pageHeader: 'Siyahısı kurator',
            editBoss: 'Dəyişə email boss',
            addCurator: 'Et kurator'
        },
        newTender: {
            pageHeader: 'Alışı əlavə etmək',
            nameOfTender: 'Alışın adı',
            startDate: 'Sorğunun göndərilmə tarixi',
            endDate: 'Komersiya təklifinin son verilmə tarixi',
            time: 'vaxt',
            curator: 'Kuratorun',
            emailcur: 'Kuratorun Email-i',
            namecur: 'Kuratorun adi və vəsifəsi',
            example: 'Misal',
            fileLock: 'Fayılları yükləmək üçün alışı yaddaşa vermək ',
            period: 'Aktuallıq tətbiqi',
            urgentNumber: 'Nömrəli təcili ərizə',
            urgentDate: 'yaranma Tarixi müddətli tətbiqi',
            quarter: 'Məhəllə',
            fastSearch: 'Tez axtarış...',
        },
        newTenderDialogs: {
            noEditPublish: 'Alış nəşr, onu redaktə etmək mümkün deyil',
            noForgetPublish: 'unutmayın dərc Alış',
            successSaved: 'Alış uğurla xilas',
            notSaved: 'Alış deyil qorunub saxlanmışdır',
            successPublished: 'Alış uğurla dərc edilmişdir',
            notPublished: 'Alış uğursuz dərc',
            deleteSelected: 'Aradan qaldırılması üçün seçilmiş Alış?',
            successDeleted: 'Alış uğurla silindi',
            isPublishTender: 'Dərc Alış?',
            afterPublishNoChanges: 'dərc sonra təklif olmaz dəyişəcək ...',
        },
        tendersEdit: {
            pageHeader: 'Dərc olunmamış alışları redəktə etmək',
        },
        checkSeller: {
            pageHeader: 'İştirakçının qydə alınması',
            phankyou: 'Şirkətimizə inandığınız üçün Sizə təşəkkür edirik!',
            askform: 'Formanı doldurun və alış üzrə bütün məlumata buraxılışı əldə edin',
            fio: 'Soyad Ad Atasının adı',
            your: 'Sizin',
            yourPassword: 'Daxil olmaq üçün Sizin şifrəniz',
            phone: 'Telefon nömrəsi',
            company: 'Şirkətin adı',
            position: 'Vəzifəsi'
        },
        login: {
            pageHeader: 'İstifadəçilər üçün daxil olma',
            login: 'Login',
            password: 'Şifrə',
            forgot: 'şifrəni Unutmusunuz?',
        },
        recovery: {
            successMessageStart: 'Sizin e-mail',
            successMessageEnd: 'sürgün məktub göstərişi ilə bərpası',
        },
        changepwd: {
            pageHeader: 'Set a new password',
            password2: 'Enter password again',
        },
        admin: {
            pageHeader: 'Administratorun səhifəsi',
            save: 'Yaddaşa vermək',
        },
        tender: {
            pageHeader: 'Alış üzrə sənədlər',
            toOffer: 'Post təklif',
            toChat: 'Kuratoru Ask',
        },
        newOffer: {
            pageHeader: 'Alışda iştirak etmək üçün komersiya təklifi ',
            fileLock: 'Fayılları yükləmək üçün komersiya təklifini yaddaşa vermək lazımdır',
            publish: 'Dərc cümlə',
            sendAsk: 'Göndərmək məsələsi',
            saveAndPublish: 'Saxlamaq və göndərmək təklifini',
            saveAndSendAsk: 'Saxlamaq və göndərmək suallar',
        },
        chat: {
            pageHeader: 'Suallar və cavablar',
            seller: 'Satıcı',
            curator: 'Kurator',
            header: 'kuratorun başlığı',
            boss: 'Boss',
            myFiles: 'Mənim files',
        },
        tenderGroupUnlock: {
            pageHeader: 'Komersiya təkliflərini açmaq',
            inputKeys: 'Daxil olma açarlarını daxil etmək',
            key: 'Açar',
            updateTender: 'Uzatmaq',
            unlock: 'Nəticələrinə bax',
        },
        offers: {
            pageHeader: 'Alış üçün sifarişlər',
            list: 'Komersiya təkliflərinin siyahısı',
            chat: 'Yalnız suallar hazırlanmasında böyük əməkləri olan kurator',
            dirty: 'Qaralamalar',
            files: 'Faylları satıcı',
            chatFiles: 'Faylları kurator'
        },
        offer: {
            pageHeader: 'Sifariş üzrə sənədlər'
        },
        offerDialogs: {
            successSaveTitle: 'unutmayın dərc ərizə',
            successSaveContent: 'Sifarişi uğurla saxlanılır',
            successPublished: 'Ərizə uğurla nəşr',
            notPublished: 'Ərizə uğursuz dərc',
            successPublishedToChat: 'Sifarişi uğurla göndərilib chat',
            notPublishedToChat: 'Ərizə uğursuz göndərmək chat',
            noEditPublish: 'Ərizə göndərilib, redaktə etmək, onu olmaz',
            notSaved: 'Ərizə saxlanılır deyil',
            isPublishOffer: 'Dərc Ərizə?',
            isPublishOfferToChat: 'Sorğu Göndərə chat?',
            afterPublishNoChanges: 'təklifi olmaz dəyişəcək ...',
        },
        sellerPage: {
            complete: 'Mənim göndərilən təkliflər',
            chat: 'Mənim göndərilən məsələləri hazırlanmasında böyük əməkləri olan kurator',
            dirtyComplete: 'Mənim sizin təkliflər',
            dirtyChat: 'Mənim sizin məsələlər hazırlanmasında böyük əməkləri olan kurator',
        },
        curatorPage: {
            pageHeader: 'Açıq sohbetler təchizatçıları ilə',
            endDate: 'Müddəti Alış',
            messageText: 'Son mesaj',
            messageTime: 'mesaj Tarixi',
            answerTime: 'Cavab kurator',
            chatLink: 'Açmaq chat',
            answerEnd: 'Bir müddət əvvəl cavab vermək lazımdır',
            yourAnswer: 'Siz cavab',
            curatorAnswer: 'Kurator cavab verdi',
            quarter: 'rüblük',
            urgently: 'təcili',
        },
        periodType: {
            urgent: 'təcili sifarişi',
            quarter: 'rüblük ərizə',
        },
        quarterType: {
            first: '1 rüb',
            second: '2 rüb',
            third: '3 rüb',
            fourth: '4 rüb'
        },
        form: {
            headers: {
                invite: 'Qeydiyyat'
            },
            errors: {
                required: 'Bu sahə icbari',
                email: 'Heç düzgün tərcüməsinə email',
                minLength: 'Minimum uzunluğu',
                userRegistered: 'İstifadəçi qeydə',
            },
            help: {
                password: ', Minimum parol uzunluğu 8 simvol',
                lessPassword: 'Zəif parol',
            }
        },
        cabinetPages: {
            seller: {
                openTenders: 'Açıq alış şirkətin',
                offers: 'Mənim ərizə',
                offersActive: 'Dərc olunmuş iştirak etmək',
                offersChat: 'Göndərilmiş, yalnız chat',
                offersDraft: 'göndərilənlər',
                offerChat: 'yalnız chat',
                offerActive: 'aktiv üçün açıq',
            }
        },
        dialogs: {
            outPageQuestion: 'Səhifəni tərk?',
            haveUnSaveChanges: 'var saxlanılan dəyişikliklər ...',
            updateTenderQuestion: 'Satınalma müddətini uzatmaq istəyirsiniz?',
            unlockTenderQuestion: 'Satınalma prosesini açmaq istədiyinizə əminsinizmi?',
            yes: 'Bəli',
            no: 'Xeyr',
            repeatAfterError: 'Səhv! Keçir&nbsp;yenidən',
            successCreated: 'Uğurla yaradıldı',
            successUpdated: 'Uğurla yenilənib',
            successDeleted: 'Müvəffəqiyyətlə silindi',
            successDone: 'Uğurla yerinə yetirildi',
            error: 'Səhv',
            warning: 'Diqqət',
        },
        authErrors: {
            error: 'Səhv',
            email: {
                invalidValue: 'Heç düzgün tərcüməsinə e-poçt',
            },
            user: {
                userBanned: 'İstifadəçi kilidli',
            },
            sendEmail: {
                error: 'bir Məktub yox idi göndərilib hansısa səbəblərdən əlaqə saxlayın texniki dəstək',
            },
            nonFieldErrors: {
                captchaInvalid: 'Heç düzgün tərcüməsinə kodu ilə şəkil',
            },
            auth: {
                invalidValue: 'Heç düzgün tərcüməsinə istifadəçi adı və ya parol',
                passwordValue: 'Yanlış parol',
            },
            uuid: {
                invalidValue: 'Heç düzgün tərcüməsinə kodu dəvətnamə',
            },
        },
        networkError: 'Şəbəkə səhvi',
        notFoundError: 'Səhifə yoxdur',
        forbiddenError: 'Erişim rədd edildi',
        formErrors: {
            invalidValue: 'Yaramaz əhəmiyyəti',
        },
        auditorPages: {
            invite: {
                warning: 'Tender uzadılıb, bağlanmağı gözləyin',
                opened: 'Nəticələr artıq əvvəllər açıqdır',
            }
        },
        statistics: {
            invites: 'Tenderə dəvət edilən podratçıların sayı',
            proposals: 'Ticarət təkliflərinin sayı',
            questions: 'Kuratora sualların sayı'
        },
        settingsPage: {
            pageHeader: 'Şifrəni dəyişin',
            oldPassword: 'Köhnə şifrə',
            newPassword: 'Yeni parol',
            repeatNewPassword: 'Yeni parol təkrarlayın',
            repeatError: 'Şifrəni təkrarlayın',
            saveError: 'Şifrə yadda saxlanmadı',
        },
        emptyList: 'Siyahısı boş',
        noMessages: 'Mesaj yoxdur',
    }
};

export default data;
