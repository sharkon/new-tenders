import {DataTranslateInterface} from '../app/interfaces/data-translate.interface';

const data: DataTranslateInterface = {
    T: {
        LANG: 'ru',
        roles: {
            ADMINISTRATOR: 'Администратор',
            CREATOR: 'Создатель закупов',
            SELLER: 'Продавец',
            CURATOR: 'Куратор',
            AUDITOR: 'Аудитор',
        },
        footer: {
            copyright: 'Все права защищены',
            copyrightBy: 'GPC Aзербайджан',
            address: {
                london: {
                    street: 'улица Бедфорд 43, офис 11',
                    country: 'Лондон, Объединенное Королевство',
                },
                baku: {
                    street: 'улица Низами 69, Isr Plaza,',
                    country: 'AZ1005, Азербайджан, Баку',
                }
            }
        },
        pages: {
            COMPANY: 'КОМПАНИЯ',
            SERVICES: 'УСЛУГИ',
            PARTNERS: 'ПАРТНЁРЫ',
            NEWS: 'НОВОСТИ',
            USEFUL_INFORMATION: 'ПОЛЕЗНОЕ',
            CONTACTS: 'КОНТАКТЫ',
            PURCHASES: 'ЗАКУПЫ'
        },
        buttons: {
            save: 'Сохранить',
            saveAndPrint: 'Сохранить и распечатать',
            saveAndPublish: 'Сохранить и опубликовать',
            publish: 'Опубликовать',
            send: 'Отправить',
            cancel: 'Отменить',
            sendAll: 'Отправить всем',
            add: 'Добавить',
            print: 'Печать',
            delete: 'Удалить',
            signin: 'Войти',
            file: 'Выбрать файлы',
            seeMore: 'Показать ещё',
        },
        actions: {
            edit: 'Редактировать',
            back: 'Вернуться назад',
            open: 'Открыть',
            reply: 'Ответить',
            instruction: '1) Заполните поля 2) Нажмите кнопку Сохранить 3) Загрузите файлы 4) Нажмите кнопку ',
        },
        files: {
            documents: 'Прилагаемая документация',
            limit: 'размер файла не более',
            prepare: 'Документы к загрузке',
            name: 'Имя',
            size: 'Размер',
            status: 'Статус',
            actions: 'Действия',
            upload: 'Загрузить',
            cancel: 'Отменить',
            remove: 'Удалить',
            uploadAll: 'Загрузить все',
            cancelAll: 'Отменить все',
            removeAll: 'Удалить все',
            queueProgress: 'Процесс загрузки',
            loaded: 'Загруженные документы',
            error: 'Ошибка загрузки файла',
            sizeError: 'размер файла превышает',
            maxFiles: 'файлов не более',
            extentionError: 'недопустимое расширение файла'
        },
        tenders: {
            id: '№ закупа',
            forAdd: 'закупа',
            forNumber: 'Закуп',
            name: 'Название',
            period: 'Период показа',
            sphere: 'Отрасль',
            viewdocs: 'см. документацию',
            currency: 'Валюта',
            additional: 'Дополнительная информация',
            documents: 'Документация',
            groupSellers: 'Группа поставщиков',
            emptyList: 'Открытых закупов сейчас нет',
            creator: 'Исполнитель',
            number: '№',
            closed: 'Закуп закрыт',
            chatFiles: 'Дополнительные файлы от куратора',
            sellerFiles: 'Дополнительные файлы от поставщика',
            operatingCompany: 'Операционная компания',
            curator: 'Куратор',
            status: 'Мой статус',
            startDate: 'Дата/время начала',
            endDate: 'Дата/время окончания',
            printError: 'Тендеры для печати не выбраны',
            periodType: 'Срочность заявки',
        },
        goffers: {
            id: '№ заявки',
            date: 'Дата заявки',
            companyName: 'Наименование компании',
            question: 'Вопросы по закупу',
            contactName: 'Контактное лицо',
            contactPhone: 'Телефон',
            contactPosition: 'Должность',
            additional: 'Дополнительная информация',
            documents: 'Документация',
            viewdocs: 'см. документацию',
            proposalStateFalse: 'НЕТ',
            proposalStateFalseText: 'заявки',
            proposalStateTrue: 'ЕСТЬ',
            proposalStateTrueText: 'заявка',
        },
        header: {
            signin: 'Войти',
            signout: 'Выйти',
            changePassword: 'Изменить пароль',
            rootPage: 'Личный кабинет',
        },
        openTenders: {
            pageHeader: 'Опубликованные закупы компании',
            createTender: 'Создать закуп',
            changeTenders: 'Редактировать закупы',
            changeTender: 'Редактировать закуп',
            publishedTenders: 'ожидающие начала рассылки',
            activeTenders: 'открытые',
            finishedTenders: 'закрытые',
            adminPage: 'Страница администратора',
            closedTenders: 'Закрытые закупы',
            myOffers: 'Кабинет поставщика'
        },
        closedTenders: {
            pageHeader: 'Закрытые закупы',
            emptyList: 'Закупов сейчас нет',
        },
        sellerGroup: {
            pageHeader: 'Группы продавцов',
        },
        groupEmails: {
            pageHeader: 'Email для рассылки по группе',
        },
        curators: {
            pageHeader: 'Список кураторов',
            editBoss: 'Изменить email boss',
            addCurator: 'Добавить куратора'
        },
        newTender: {
            pageHeader: 'Добавить закуп',
            nameOfTender: 'Наименование закупа',
            startDate: 'Дата отправки запроса',
            endDate: 'Конечный срок подачи ком. предложений',
            time: 'Время',
            curator: 'Куратор',
            emailcur: 'Email куратора',
            namecur: 'Имя и должность куратора',
            example: 'пример',
            fileLock: 'Чтобы загрузить файлы, нужно сохранить закуп',
            period: 'Срочность заявки',
            urgentNumber: 'Номер срочной заявки',
            urgentDate: 'Дата создания срочной заявки',
            quarter: 'Квартал',
            fastSearch: 'Быстрый поиск...',
        },
        newTenderDialogs: {
            noEditPublish: 'Закуп опубликован, редактировать его нельзя',
            noForgetPublish: 'Не забудьте опубликовать закуп',
            successSaved: 'Закуп успешно сохранен',
            notSaved: 'Закуп не сохранился',
            successPublished: 'Закуп успешно опубликован',
            notPublished: 'Закуп не удалось опубликовать',
            deleteSelected: 'Удалить выбранные Закупы?',
            successDeleted: 'Закупы успешно удалены',
            isPublishTender: 'Опубликовать Закуп?',
            afterPublishNoChanges: 'после публикации предложение нельзя будет изменить ...',
        },
        tendersEdit: {
            pageHeader: 'Редактировать неопубликованные закупы',
        },
        checkSeller: {
            pageHeader: 'Регистрация участника',
            phankyou: 'Спасибо за доверие к нашей компании!',
            askform: 'Заполните форму и получите доступ к полной информации по закупу',
            fio: 'Фамилия Имя Отчество',
            your: 'Ваш',
            yourPassword: 'Ваш пароль для входа',
            phone: 'Номер телефона',
            company: 'Название компании',
            position: 'Должность'
        },
        login: {
            pageHeader: 'Вход для пользователей',
            login: 'Логин',
            password: 'Пароль',
            forgot: 'Забыли пароль?',
        },
        recovery: {
            successMessageStart: 'На Ваш e-mail',
            successMessageEnd: 'выслано письмо с инструкцией по восстановлению',
        },
        changepwd: {
            pageHeader: 'Установка нового пароля',
            password2: 'Введите пароль повторно',
        },
        admin: {
            pageHeader: 'Страница администратора',
            save: 'Сохранить',
        },
        tender: {
            pageHeader: 'Документация по закупу',
            toOffer: 'Подать предложение',
            toChat: 'Задать вопрос куратору',
        },
        newOffer: {
            pageHeader: 'Заявка на участие в закупе',
            fileLock: 'Чтобы загрузить файлы, нужно сохранить заявку',
            publish: 'Опубликовать предложение',
            sendAsk: 'Отправить вопрос',
            saveAndPublish: 'Сохранить и опубликовать предложение',
            saveAndSendAsk: 'Сохранить и отправить вопрос',
        },
        chat: {
            pageHeader: 'Вопросы и ответы',
            seller: 'Продавец',
            curator: 'Куратор',
            header: 'Руководитель',
            boss: 'Шеф',
            myFiles: 'Мои файлы',
        },
        tenderGroupUnlock: {
            pageHeader: 'Открыть заявки по закупу',
            inputKeys: 'Ввести ключи доступа',
            key: 'ключ',
            updateTender: 'Продлить срок',
            unlock: 'Открыть результаты',
        },
        offers: {
            pageHeader: 'Заявки на закуп',
            list: 'Список поступивших предложений',
            chat: 'Только вопросы куратору',
            dirty: 'Черновики',
            files: 'Файлы от поставщика',
            chatFiles: 'Файлы от куратора'
        },
        offer: {
            pageHeader: 'Документация по заявке'
        },
        offerDialogs: {
            successSaveTitle: 'Не забудьте опубликовать заявку',
            successSaveContent: 'Заявка успешно сохранена',
            successPublished: 'Заявка успешно опубликована',
            notPublished: 'Заявку не удалось опубликовать',
            successPublishedToChat: 'Заявка успешно отправлена в чат',
            notPublishedToChat: 'Заявку не удалось отправить в чат',
            noEditPublish: 'Заявка отправлена, редактировать её нельзя',
            notSaved: 'Заявка не сохранилась',
            isPublishOffer: 'Опубликовать Заявку?',
            isPublishOfferToChat: 'Отправить Заявку в чат?',
            afterPublishNoChanges: 'предложение нельзя будет изменить ...',
        },
        sellerPage: {
            complete: 'Мои отправленные предложения',
            chat: 'Мои отправленные вопросы куратору',
            dirtyComplete: 'Мои неотправленные предложения',
            dirtyChat: 'Мои неотправленные вопросы куратору',
        },
        curatorPage: {
            pageHeader: 'Открытые чаты с поставщиками',
            endDate: 'Срок закупа',
            messageText: 'Последнее сообщение',
            messageTime: 'Дата сообщения',
            answerTime: 'Ответ куратора',
            chatLink: 'Открыть чат',
            answerEnd: 'Нужно ответить до срока',
            yourAnswer: 'Вы ответили',
            curatorAnswer: 'Куратор ответил',
            quarter: 'квартал',
            urgently: 'срочная',
        },
        periodType: {
            urgent: 'срочная заявка',
            quarter: 'квартальная заявка',
        },
        quarterType: {
            first: '1 квартал',
            second: '2 квартал',
            third: '3 квартал',
            fourth: '4 квартал'
        },
        form: {
            headers: {
                invite: 'Регистрация'
            },
            errors: {
                required: 'Это поле обязательное',
                email: 'Некорректный email',
                minLength: 'Минимальная длина',
                userRegistered: 'Пользователь зарегистрирован',
            },
            help: {
                password: 'Минимальная длина пароля 8 символов',
                lessPassword: 'Слабый пароль',
            }
        },
        cabinetPages: {
            seller: {
                openTenders: 'Открытые закупы компании',
                offers: 'Мои заявки',
                offersActive: 'Опубликованные на участие',
                offersChat: 'Отправленные только в чат',
                offersDraft: 'НЕ отправленные',
                offerChat: 'только чат',
                offerActive: 'участие в закупе',
            }
        },
        dialogs: {
            outPageQuestion: 'Покинуть страницу?',
            haveUnSaveChanges: 'у вас есть не сохраненные изменения ...',
            updateTenderQuestion: 'Вы уверены, что хотите продлить закуп?',
            unlockTenderQuestion: 'Вы уверены, что хотите открыть закуп?',
            yes: 'Да',
            no: 'Нет',
            repeatAfterError: 'Ошибка! Попробуйте&nbsp;снова',
            successCreated: 'Успешно создано',
            successUpdated: 'Успешно обновлено',
            successDeleted: 'Успешно удалено',
            successDone: 'Успешно выполнено',
            error: 'Ошибка',
            warning: 'Внимание',
        },
        authErrors: {
            error: 'Ошибка',
            email: {
                invalidValue: 'Некорректный email',
            },
            user: {
                userBanned: 'Пользователь заблокирован',
            },
            sendEmail: {
                error: 'Письмо не было отправленно по каким-то причинам, обратитесь в техническую поддержку',
            },
            nonFieldErrors: {
                captchaInvalid: 'Некорректный код с картинки',
            },
            auth: {
                invalidValue: 'Некорректный логин или пароль',
                passwordValue: 'Некорректный пароль',
            },
            uuid: {
                invalidValue: 'Некорректный код приглашения',
            },
        },
        networkError: 'Ошибка сети',
        notFoundError: 'Страница не существует',
        forbiddenError: 'Доступ запрещен',
        formErrors: {
            invalidValue: 'Недопустимое значение',
        },
        auditorPages: {
            invite: {
                warning: 'Тендер продлен, ожидайте закрытия',
                opened: 'Результаты уже открыты ранее',
            }
        },
        statistics: {
            invites: 'Количество приглашаемых на тендер контрагентов',
            proposals: 'Количество поступивших коммерческих предложений',
            questions: 'Количество поступивших вопросов куратору'
        },
        settingsPage: {
            pageHeader: 'Изменить пароль',
            oldPassword: 'Старый пароль',
            newPassword: 'Новый пароль',
            repeatNewPassword: 'Повторить новый пароль',
            repeatError: 'Ошибка повторения пароля',
            saveError: 'Пароль не сохранился',
        },
        emptyList: 'Список пуст',
        noMessages: 'Нет сообщений',
    }
};

export default data;
