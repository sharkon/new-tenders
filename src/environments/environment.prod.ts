export const environment = {
    production: true,
    rest: {
        url: 'https://api.geam.az',
        lang: 'ca'
    },
    adminPage: 'https://api.geam.az/dj1_admin/',
};
