import { Pipe, PipeTransform } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppStore } from '../../store/app.store';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Pipe({
    name: 'translate',
    pure: false
})
export class TranslatePipe implements PipeTransform {

    constructor(private store$: Store<AppStore.IState>) {}

    transform(key: any): Observable<string> {

        return this.store$.pipe(
            select(AppStore.Selects.appTranslate.data),
            map(data => key.split('.').reduce((obj, i) => obj[i], data) || key)
        );
    }

}
