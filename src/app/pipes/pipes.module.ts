import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslatePipe } from './translate/translate.pipe';
import {MultilangPipe} from './multilang/multilang.pipe';
import {OfferDatePipe} from './offer-date/offer-date.pipe';

const pipes = [
    TranslatePipe,
    MultilangPipe,
    OfferDatePipe,
];

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: pipes,
    exports: pipes,
    providers: pipes,
})
export class PipesModule {
}
