import {Pipe, PipeTransform} from '@angular/core';
import {IOffer} from '../../interfaces/offer.interface';

@Pipe({
    name: 'offerDate'
})
export class OfferDatePipe implements PipeTransform {

    transform(offer: IOffer.ITimeOfModel, args?: any): string {

        return offer.publishedTime ? offer.publishedTime : offer.createTime;
    }

}
