import {Pipe, PipeTransform} from '@angular/core';
import {IMultilang} from '../../interfaces/multilang.interface';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../store/app.store';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';

@Pipe({
    name: 'multilang'
})
export class MultilangPipe implements PipeTransform {

    constructor(private store$: Store<AppStore.IState>) {
    }

    transform(multilang: IMultilang.IMultilangString, args?: any): Observable<string> {

        if (!multilang) {
            return of('');
        }

        return this.store$.pipe(
            select(AppStore.Selects.appTranslate.lang),
            map(lang => {

                const hasText = Object.keys(multilang).find(key => {
                    return !!multilang[key];
                });

                return multilang[lang] || multilang[hasText];
            }),
        );
    }

}
