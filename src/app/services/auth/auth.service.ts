import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { IAuth } from '../../interfaces/auth.interface';
import { DataObservableService } from '../data-observable/data-observable.service';
import { StorageService } from '../storage/storage.service';
import { IUser } from '../../interfaces/user.interface';
import { Store } from '@ngrx/store';
import { AppStore } from '../../store/app.store';
import { AppAuthActions } from '../../store/auth/app-auth.actions';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private dataObservableService: DataObservableService,
        private storageService: StorageService,
        private store$: Store<AppStore.IState>) {
    }

    private setupUserSubscription: Subscription;

    getToken(): string {
        return this.storageService.local.getItem('token');
    }

    getAuthorizationHeader(): string {
        const token = this.getToken();
        return token ? `Token ${token}` : null;
    }

    setToken(token: string, time: number = null): void {
        return this.storageService.local.setItem('token', token, time);
    }

    getCaptcha(): Observable<IAuth.ICaptchaResponse> {
        return this.dataObservableService.get<IAuth.ICaptchaResponse>('auth');
    }

    logIn(loginData: IAuth.ISignInRequest): Observable<IAuth.ISignInResponse> {
        return this.dataObservableService.post<IAuth.ISignInResponse>('auth', loginData, false);
    }

    logOut(): void {
        this.store$.dispatch(new AppAuthActions.LogOut());
    }

    getUser(): Observable<IUser.IUserResponse> {
        return this.dataObservableService.get<IUser.IUserResponse>('auth/user');
    }

    recovery(recoveryData: IAuth.IRecoveryRequest): Observable<any> {
        return this.dataObservableService.post<IAuth.ISignInResponse>('auth/reset', recoveryData, false);
    }

    setupUser(): Promise<any> {

        return new Promise((resolve, reject) => {

            if (this.getToken()) {

                this.setupUserSubscription = this.getUser()
                    .subscribe(
                        (response: IUser.IUserResponse) => {

                            this.store$.dispatch(new AppAuthActions.SetUser(response));
                            this.setupUserSubscription.unsubscribe();

                            resolve();
                        },
                        (error: any) => {

                            this.store$.dispatch(new AppAuthActions.LogOut());
                            this.setupUserSubscription.unsubscribe();

                            resolve();
                        }
                    );

            } else {

                resolve();
            }
        });
    }
}
