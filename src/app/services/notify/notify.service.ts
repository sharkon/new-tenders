import {Inject, Injectable} from '@angular/core';
import {TranslateService} from '../translate/translate.service';
import {INotify} from '../../interfaces/notify.interface';
import {WINDOW} from '../../providers/window.providers';

@Injectable({
    providedIn: 'root'
})
export class NotifyService {

    constructor(private ts: TranslateService,
                @Inject(WINDOW) private window: Window) {
    }

    success(customOptions: INotify.ISuccess = {}) {

        const defaultOptions: INotify.ISuccess = {
            dialogTitle: this.ts.data.T.dialogs.successDone,
            dialogContent: '',
        };

        const options = {
            ...defaultOptions,
            ...customOptions,
        };

        return this.window.alert(`${options.dialogTitle}\n${options.dialogContent}`);
    }

    confirm(customOptions: INotify.IConfirm = {}) {

        const defaultOptions: INotify.ISuccess = {
            dialogTitle: this.ts.data.T.dialogs.successDone,
            dialogContent: '',
        };

        const options = {
            ...defaultOptions,
            ...customOptions,
        };

        return this.window.confirm(`${options.dialogTitle}\n${options.dialogContent}`);
    }

    error(customOptions: INotify.IError = {}) {

        const defaultOptions: INotify.ISuccess = {
            dialogTitle: this.ts.data.T.dialogs.error,
            dialogContent: '',
        };

        const options = {
            ...defaultOptions,
            ...customOptions,
        };

        return this.window.alert(`${options.dialogTitle}\n${options.dialogContent}`);
    }

    warning(customOptions: INotify.IWarning = {}) {

        const defaultOptions: INotify.ISuccess = {
            dialogTitle: this.ts.data.T.dialogs.warning,
            dialogContent: '',
        };

        const options = {
            ...defaultOptions,
            ...customOptions,
        };

        return this.window.alert(`${options.dialogTitle}\n${options.dialogContent}`);
    }

}
