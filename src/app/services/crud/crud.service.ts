import {Observable, throwError as observableThrowError} from 'rxjs';
import {Injectable} from '@angular/core';
import {DataObservableService} from '../data-observable/data-observable.service';
import {catchError, map} from 'rxjs/operators';
import {ICrud} from '../../interfaces/crud.interface';
import {NotifyService} from '../notify/notify.service';
import {TranslateService} from '../translate/translate.service';

@Injectable({
    providedIn: 'root'
})
export class CrudService {

    static SERVICE_LOCK = 'CrudService';

    constructor(private dataService: DataObservableService,
                private notifyService: NotifyService,
                private ts: TranslateService) {
    }

    create(request: ICrud.ICRUDCreateRequest): Observable<ICrud.ICRUDCreateResponse> {

        return this.dataService
            .post(request.url, request.item, request.notify, request.mapMessage, request.options)
            .pipe(
                map((response: ICrud.IModel) => {

                    if (!request.deniedNotifySuccess) {

                        this.notifyService.success({
                            dialogTitle: this.ts.data.T.dialogs.successCreated,
                        });
                    }

                    return {
                        item: response,
                        uid: request.uid,
                    };
                })
                , catchError(e => {
                    return observableThrowError({
                        error: e,
                        uid: request.uid,
                    });
                })
            );
    }

    patch(request: ICrud.ICRUDUpdateRequest): Observable<ICrud.ICRUDUpdateResponse> {

        return this.dataService
            .patch(request.url, request.item, request.notify, request.mapMessage, request.options)
            .pipe(
                map((response: ICrud.IModel) => {

                    if (!request.deniedNotifySuccess) {

                        this.notifyService.success({
                            dialogTitle: this.ts.data.T.dialogs.successUpdated,
                        });
                    }

                    return {
                        item: response,
                        uid: request.uid,
                    };
                })
                , catchError(e => {
                    return observableThrowError({
                        error: e,
                        uid: request.uid,
                    });
                })
            );
    }

    update(request: ICrud.ICRUDUpdateRequest): Observable<ICrud.ICRUDUpdateResponse> {

        return this.dataService
            .put(request.url, request.item, request.notify, request.mapMessage, request.options)
            .pipe(
                map((response: ICrud.IModel) => {

                    if (!request.deniedNotifySuccess) {

                        this.notifyService.success({
                            dialogTitle: this.ts.data.T.dialogs.successUpdated,
                        });
                    }

                    return {
                        item: response,
                        uid: request.uid,
                    };
                })
                , catchError(e => {
                    return observableThrowError({
                        error: e,
                        uid: request.uid,
                    });
                })
            );
    }

    remove(request: ICrud.ICRUDRequest): Observable<ICrud.ICRUDDeleteResponse> {

        return this.dataService
            .delete<ICrud.ICRUDDeleteResponse>(request.url, request.notify, request.mapMessage, request.options)
            .pipe(
                map((response: ICrud.ICRUDDeleteResponse) => {

                    if (!request.deniedNotifySuccess) {

                        this.notifyService.success({
                            dialogTitle: this.ts.data.T.dialogs.successDeleted,
                        });
                    }

                    return {
                        ...response,
                        uid: request.uid,
                    };
                })
                , catchError(e => {
                    return observableThrowError({
                        error: e,
                        uid: request.uid,
                    });
                })
            );

    }

    getItem(request: ICrud.ICRUDRequest): Observable<ICrud.ICRUDGetItemResponse> {

        return this.dataService
            .get<ICrud.IModel>(request.url, request.notify, request.mapMessage, request.options)
            .pipe(
                map((response: ICrud.IModel) => {

                    return {
                        item: response,
                        uid: request.uid,
                    };
                })
                , catchError(e => {
                    return observableThrowError({
                        error: e,
                        uid: request.uid,
                    });
                })
            );
    }

    getPostItem(request: ICrud.ICRUDPostRequest): Observable<ICrud.ICRUDGetItemResponse> {

        return this.dataService
            .post<ICrud.IModel>(request.url, request.item, request.notify, request.mapMessage, request.options)
            .pipe(
                map((response: ICrud.IModel) => {

                    return {
                        item: response,
                        uid: request.uid,
                    };
                })
                , catchError(e => {
                    return observableThrowError({
                        error: e,
                        uid: request.uid,
                    });
                })
            );
    }

    getList(request: ICrud.ICRUDRequest): Observable<ICrud.ICRUDGetListResponse> {

        return this.dataService
            .get<ICrud.IGetListResponse>(request.url, request.notify, request.mapMessage, request.options)
            .pipe(
                map((response: ICrud.IGetListResponse) => {

                    return {
                        ...response,
                        uid: request.uid,
                    };
                })
                , catchError(e => {
                    return observableThrowError({
                        error: e,
                        uid: request.uid,
                    });
                })
            );
    }

    getPostList(request: ICrud.ICRUDPostRequest): Observable<ICrud.ICRUDGetListResponse> {

        return this.dataService
            .post<ICrud.IGetListResponse>(request.url, request.item, request.notify, request.mapMessage, request.options)
            .pipe(
                map((response: ICrud.IGetListResponse) => {

                    return {
                        ...response,
                        uid: request.uid,
                    };
                })
                , catchError(e => {
                    return observableThrowError({
                        error: e,
                        uid: request.uid,
                    });
                })
            );
    }
}
