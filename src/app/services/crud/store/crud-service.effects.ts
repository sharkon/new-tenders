import {Actions, Effect, ofType} from '@ngrx/effects';
import {CrudService} from '../crud.service';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Action} from '@ngrx/store';
import {CrudServiceActions, CrudServiceActionTypes} from './crud-service.actions';
import {AppBlockActions} from '../../../store/block/app-block.actions';
import {catchError, map, mergeMap} from 'rxjs/operators';
import { IMap } from '../../../interfaces/map.interface';

@Injectable()
export class CrudServiceEffects {

    private requests: IMap.IMapOfNumber = {};

    @Effect()
    create$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CREATE)
            , map((action: CrudServiceActions.Create) => action.payload)
            , mergeMap(payload => {

                return this.crudService.create(payload).pipe(
                    map(response => {
                        return new CrudServiceActions.CreateSuccess(response);
                    }),
                    catchError(e => {
                        return of(new CrudServiceActions.CreateFailed(e));
                    })
                );
            })
        );

    @Effect()
    patch$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.PATCH)
            , map((action: CrudServiceActions.Patch) => action.payload)
            , mergeMap(payload => {

                return this.crudService.patch(payload).pipe(
                    map(response => {
                        return new CrudServiceActions.PatchSuccess(response);
                    }),
                    catchError(e => {
                        return of(new CrudServiceActions.PatchFailed(e));
                    })
                );
            })
        );

    @Effect()
    update$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.UPDATE)
            , map((action: CrudServiceActions.Update) => action.payload)
            , mergeMap(payload => {

                return this.crudService.update(payload).pipe(
                    map(response => {
                        return new CrudServiceActions.UpdateSuccess(response);
                    }),
                    catchError(e => {
                        return of(new CrudServiceActions.UpdateFailed(e));
                    })
                );
            })
        );

    @Effect()
    delete$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.DELETE)
            , map((action: CrudServiceActions.Delete) => action.payload)
            , mergeMap(payload => {

                return this.crudService.remove(payload).pipe(
                    map(response => {
                        return new CrudServiceActions.DeleteSuccess(response);
                    }),
                    catchError(e => {
                        return of(new CrudServiceActions.DeleteFailed(e));
                    })
                );
            })
        );

    @Effect()
    getItem$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM)
            , map((action: CrudServiceActions.GetItem) => action.payload)
            , mergeMap(payload => {

                const number = this.getNextRequestNumber(payload.uid);

                return this.crudService.getItem(payload).pipe(
                    map(response => {
                        return number === this.requests[payload.uid] ?
                            new CrudServiceActions.GetItemSuccess(response) :
                            new CrudServiceActions.GetItemCanceled(payload.uid);
                    }),
                    catchError(e => {
                        return of(new CrudServiceActions.GetItemFailed(e));
                    })
                );
            })
        );

    @Effect()
    getPostItem$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_POST_ITEM)
            , map((action: CrudServiceActions.GetPostItem) => action.payload)
            , mergeMap(payload => {

                const number = this.getNextRequestNumber(payload.uid);

                return this.crudService.getPostItem(payload).pipe(
                    map(response => {
                        return number === this.requests[payload.uid] ?
                            new CrudServiceActions.GetPostItemSuccess(response) :
                            new CrudServiceActions.GetPostItemCanceled(payload.uid);
                    }),
                    catchError(e => {
                        return of(new CrudServiceActions.GetPostItemFailed(e));
                    })
                );
            })
        );

    @Effect()
    getList$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST)
            , map((action: CrudServiceActions.GetList) => action.payload)
            , mergeMap(payload => {

                const number = this.getNextRequestNumber(payload.uid);

                return this.crudService.getList(payload).pipe(
                    map(response => {
                        return number === this.requests[payload.uid] ?
                            new CrudServiceActions.GetListSuccess(response) :
                            new CrudServiceActions.GetListCanceled(payload.uid);
                    }),
                    catchError(e => {
                        return of(new CrudServiceActions.GetListFailed(e));
                    })
                );
            })
        );

    @Effect()
    getPostList$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_POST_LIST)
            , map((action: CrudServiceActions.GetPostList) => action.payload)
            , mergeMap(payload => {

                const number = this.getNextRequestNumber(payload.uid);

                return this.crudService.getPostList(payload).pipe(
                    map(response => {
                        return number === this.requests[payload.uid] ?
                            new CrudServiceActions.GetPostListSuccess(response) :
                            new CrudServiceActions.GetPostListCanceled(payload.uid);
                    }),
                    catchError(e => {
                        return of(new CrudServiceActions.GetPostListFailed(e));
                    })
                );
            })
        );

    @Effect()
    changeLock$: Observable<Action> =
        this.actions$.pipe(
            ofType(
                CrudServiceActionTypes.CREATE,
                CrudServiceActionTypes.PATCH,
                CrudServiceActionTypes.UPDATE,
                CrudServiceActionTypes.DELETE
            )
            , map(action => {
                return new AppBlockActions.Lock(CrudService.SERVICE_LOCK);
            })
        );

    @Effect()
    changeUnLock$: Observable<Action> =
        this.actions$.pipe(
            ofType(
                CrudServiceActionTypes.CREATE_SUCCESS,
                CrudServiceActionTypes.CREATE_FAILED,
                CrudServiceActionTypes.PATCH_SUCCESS,
                CrudServiceActionTypes.PATCH_FAILED,
                CrudServiceActionTypes.UPDATE_SUCCESS,
                CrudServiceActionTypes.UPDATE_FAILED,
                CrudServiceActionTypes.DELETE_SUCCESS,
                CrudServiceActionTypes.DELETE_FAILED,
            )
            , map(action => {
                return new AppBlockActions.UnLock(CrudService.SERVICE_LOCK);
            })
        );

    @Effect()
    changeLockByUid$: Observable<Action> =
        this.actions$.pipe(
            ofType(
                CrudServiceActionTypes.CREATE,
                CrudServiceActionTypes.PATCH,
                CrudServiceActionTypes.UPDATE,
                CrudServiceActionTypes.DELETE,
                CrudServiceActionTypes.GET_ITEM,
                CrudServiceActionTypes.GET_LIST,
                CrudServiceActionTypes.GET_POST_ITEM,
                CrudServiceActionTypes.GET_POST_LIST,
            )
            , map((action: CrudServiceActions.CallActions) => {
                return new AppBlockActions.Lock(action.payload.uid);
            })
        );

    @Effect()
    changeUnLockByUidForSuccessAndFailed$: Observable<Action> =
        this.actions$.pipe(
            ofType(
                CrudServiceActionTypes.CREATE_SUCCESS,
                CrudServiceActionTypes.CREATE_FAILED,
                CrudServiceActionTypes.PATCH_SUCCESS,
                CrudServiceActionTypes.PATCH_FAILED,
                CrudServiceActionTypes.UPDATE_SUCCESS,
                CrudServiceActionTypes.UPDATE_FAILED,
                CrudServiceActionTypes.DELETE_SUCCESS,
                CrudServiceActionTypes.DELETE_FAILED,
                CrudServiceActionTypes.GET_ITEM_SUCCESS,
                CrudServiceActionTypes.GET_ITEM_FAILED,
                CrudServiceActionTypes.GET_LIST_SUCCESS,
                CrudServiceActionTypes.GET_LIST_FAILED,
                CrudServiceActionTypes.GET_POST_ITEM_SUCCESS,
                CrudServiceActionTypes.GET_POST_ITEM_FAILED,
                CrudServiceActionTypes.GET_POST_LIST_SUCCESS,
                CrudServiceActionTypes.GET_POST_LIST_FAILED,
            )
            , map((action: CrudServiceActions.SuccessActions | CrudServiceActions.FailedActions) => {
                return new AppBlockActions.UnLock(action.payload.uid);
            })
        );

    @Effect()
    changeUnLockByUidForCanceled$: Observable<Action> =
        this.actions$.pipe(
            ofType(
                CrudServiceActionTypes.GET_ITEM_CANCELED,
                CrudServiceActionTypes.GET_LIST_CANCELED,
                CrudServiceActionTypes.GET_POST_ITEM_CANCELED,
                CrudServiceActionTypes.GET_POST_LIST_CANCELED,
            )
            , map((action: CrudServiceActions.CanceledActions) => {
                return new AppBlockActions.UnLock(action.payload);
            })
        );

    @Effect()
    changeUnLockByUidForCancelRequest$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CANCEL_REQUESTS)
            , map((action: CrudServiceActions.CancelRequests) => {

                return new AppBlockActions.GroupUnLock(action.payload);
            })
        );

    @Effect()
    changeUnLockByUidForCancelRequestLike$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CANCEL_REQUESTS_LIKE)
            , map((action: CrudServiceActions.CancelRequestsLike) => {

                const uids = this.getUidsByLikes(action.payload);
                return new AppBlockActions.GroupUnLock(uids);
            })
        );

    private getNextRequestNumber(uid: string) {

        this.requests[uid] = this.requests[uid] ? this.requests[uid] + 1 : 1;
        return this.requests[uid];
    }

    private getUidsByLikes(likeUids: string[]): string[] {

        const uids: string[] = [];

        Object.keys(this.requests)
            .forEach(uid => {

                (likeUids || []).forEach(likeUid => {

                    if (uid.indexOf(likeUid) === 0) {
                        uids.push(uid);
                    }
                });
            });

        return uids;
    }

    constructor(private actions$: Actions,
                private crudService: CrudService) {

        this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.CANCEL_REQUESTS)
                , map((action: CrudServiceActions.CancelRequests) => action.payload)
            )
            .subscribe(payload => {

                (payload || []).forEach(uid => {
                    this.getNextRequestNumber(uid);
                });
            });

        this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.CANCEL_REQUESTS_LIKE)
                , map((action: CrudServiceActions.CancelRequestsLike) => action.payload)
            )
            .subscribe(payload => {

                const uids = this.getUidsByLikes(payload);

                uids.forEach(uid => {
                    this.getNextRequestNumber(uid);
                });
            });
    }
}
