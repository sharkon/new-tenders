import { Action } from '@ngrx/store';
import { ICrud } from '../../../interfaces/crud.interface';

export namespace CrudServiceActionTypes {

    export const CREATE = '[CrudService] CREATE';
    export const CREATE_SUCCESS = '[CrudService] CREATE_SUCCESS';
    export const CREATE_FAILED = '[CrudService] CREATE_FAILED';

    export const PATCH = '[CrudService] PATCH';
    export const PATCH_SUCCESS = '[CrudService] PATCH_SUCCESS';
    export const PATCH_FAILED = '[CrudService] PATCH_FAILED';

    export const UPDATE = '[CrudService] UPDATE';
    export const UPDATE_SUCCESS = '[CrudService] UPDATE_SUCCESS';
    export const UPDATE_FAILED = '[CrudService] UPDATE_FAILED';

    export const DELETE = '[CrudService] DELETE';
    export const DELETE_SUCCESS = '[CrudService] DELETE_SUCCESS';
    export const DELETE_FAILED = '[CrudService] DELETE_FAILED';

    export const GET_ITEM = '[CrudService] GET_ITEM';
    export const GET_ITEM_SUCCESS = '[CrudService] GET_ITEM_SUCCESS';
    export const GET_ITEM_FAILED = '[CrudService] GET_ITEM_FAILED';
    export const GET_ITEM_CANCELED = '[CrudService] GET_ITEM_CANCELED';

    export const GET_POST_ITEM = '[CrudService] GET_POST_ITEM';
    export const GET_POST_ITEM_SUCCESS = '[CrudService] GET_POST_ITEM_SUCCESS';
    export const GET_POST_ITEM_FAILED = '[CrudService] GET_POST_ITEM_FAILED';
    export const GET_POST_ITEM_CANCELED = '[CrudService] GET_POST_ITEM_CANCELED';

    export const GET_LIST = '[CrudService] GET_LIST';
    export const GET_LIST_SUCCESS = '[CrudService] GET_LIST_SUCCESS';
    export const GET_LIST_FAILED = '[CrudService] GET_LIST_FAILED';
    export const GET_LIST_CANCELED = '[CrudService] GET_LIST_CANCELED';

    export const GET_POST_LIST = '[CrudService] GET_POST_LIST';
    export const GET_POST_LIST_SUCCESS = '[CrudService] GET_POST_LIST_SUCCESS';
    export const GET_POST_LIST_FAILED = '[CrudService] GET_POST_LIST_FAILED';
    export const GET_POST_LIST_CANCELED = '[CrudService] GET_POST_LIST_CANCELED';

    export const CANCEL_REQUESTS = '[CrudService] CANCEL_REQUESTS';
    export const CANCEL_REQUESTS_LIKE = '[CrudService] CANCEL_REQUESTS_LIKE';
}

export namespace CrudServiceActions {

    export class Create implements Action {

        readonly type = CrudServiceActionTypes.CREATE;

        constructor(public payload: ICrud.ICRUDCreateRequest) {
        }
    }

    export class CreateSuccess implements Action {

        readonly type = CrudServiceActionTypes.CREATE_SUCCESS;

        constructor(public payload: ICrud.ICRUDCreateResponse) {
        }
    }

    export class CreateFailed implements Action {

        readonly type = CrudServiceActionTypes.CREATE_FAILED;

        constructor(public payload: ICrud.ICRUDError) {
        }
    }

    export class Patch implements Action {

        readonly type = CrudServiceActionTypes.PATCH;

        constructor(public payload: ICrud.ICRUDUpdateRequest) {
        }
    }

    export class PatchSuccess implements Action {

        readonly type = CrudServiceActionTypes.PATCH_SUCCESS;

        constructor(public payload: ICrud.ICRUDUpdateResponse) {
        }
    }

    export class PatchFailed implements Action {

        readonly type = CrudServiceActionTypes.PATCH_FAILED;

        constructor(public payload: ICrud.ICRUDError) {
        }
    }

    export class Update implements Action {

        readonly type = CrudServiceActionTypes.UPDATE;

        constructor(public payload: ICrud.ICRUDUpdateRequest) {
        }
    }

    export class UpdateSuccess implements Action {

        readonly type = CrudServiceActionTypes.UPDATE_SUCCESS;

        constructor(public payload: ICrud.ICRUDUpdateResponse) {
        }
    }

    export class UpdateFailed implements Action {

        readonly type = CrudServiceActionTypes.UPDATE_FAILED;

        constructor(public payload: ICrud.ICRUDError) {
        }
    }

    export class Delete implements Action {

        readonly type = CrudServiceActionTypes.DELETE;

        constructor(public payload: ICrud.ICRUDRequest) {
        }
    }

    export class DeleteSuccess implements Action {

        readonly type = CrudServiceActionTypes.DELETE_SUCCESS;

        constructor(public payload: ICrud.ICRUDDeleteResponse) {
        }
    }

    export class DeleteFailed implements Action {

        readonly type = CrudServiceActionTypes.DELETE_FAILED;

        constructor(public payload: ICrud.ICRUDError) {
        }
    }

    export class GetItem implements Action {

        readonly type = CrudServiceActionTypes.GET_ITEM;

        constructor(public payload: ICrud.ICRUDRequest) {
        }
    }

    export class GetItemSuccess implements Action {

        readonly type = CrudServiceActionTypes.GET_ITEM_SUCCESS;

        constructor(public payload: ICrud.ICRUDGetItemResponse) {
        }
    }

    export class GetItemFailed implements Action {

        readonly type = CrudServiceActionTypes.GET_ITEM_FAILED;

        constructor(public payload: ICrud.ICRUDError) {
        }
    }

    export class GetItemCanceled implements Action {

        readonly type = CrudServiceActionTypes.GET_ITEM_CANCELED;

        constructor(public payload: string = null) {
            console.log(`Service Canceled: ${payload}`);
        }
    }

    export class GetPostItem implements Action {

        readonly type = CrudServiceActionTypes.GET_POST_ITEM;

        constructor(public payload: ICrud.ICRUDPostRequest) {
        }
    }

    export class GetPostItemSuccess implements Action {

        readonly type = CrudServiceActionTypes.GET_POST_ITEM_SUCCESS;

        constructor(public payload: ICrud.ICRUDGetItemResponse) {
        }
    }

    export class GetPostItemFailed implements Action {

        readonly type = CrudServiceActionTypes.GET_POST_ITEM_FAILED;

        constructor(public payload: ICrud.ICRUDError) {
        }
    }

    export class GetPostItemCanceled implements Action {

        readonly type = CrudServiceActionTypes.GET_POST_ITEM_CANCELED;

        constructor(public payload: string = null) {
            console.log(`Service Canceled: ${payload}`);
        }
    }

    export class GetList implements Action {

        readonly type = CrudServiceActionTypes.GET_LIST;

        constructor(public payload: ICrud.ICRUDRequest) {
        }
    }

    export class GetListSuccess implements Action {

        readonly type = CrudServiceActionTypes.GET_LIST_SUCCESS;

        constructor(public payload: ICrud.ICRUDGetListResponse) {
        }
    }

    export class GetListFailed implements Action {

        readonly type = CrudServiceActionTypes.GET_LIST_FAILED;

        constructor(public payload: ICrud.ICRUDError) {
        }
    }

    export class GetListCanceled implements Action {

        readonly type = CrudServiceActionTypes.GET_LIST_CANCELED;

        constructor(public payload: string = null) {
            console.log(`Service Canceled: ${payload}`);
        }
    }

    export class GetPostList implements Action {

        readonly type = CrudServiceActionTypes.GET_POST_LIST;

        constructor(public payload: ICrud.ICRUDPostRequest) {
        }
    }

    export class GetPostListSuccess implements Action {

        readonly type = CrudServiceActionTypes.GET_POST_LIST_SUCCESS;

        constructor(public payload: ICrud.ICRUDGetListResponse) {
        }
    }

    export class GetPostListFailed implements Action {

        readonly type = CrudServiceActionTypes.GET_POST_LIST_FAILED;

        constructor(public payload: ICrud.ICRUDError) {
        }
    }

    export class GetPostListCanceled implements Action {

        readonly type = CrudServiceActionTypes.GET_POST_LIST_CANCELED;

        constructor(public payload: string = null) {
            console.log(`Service Canceled: ${payload}`);
        }
    }

    export class CancelRequests implements Action {

        readonly type = CrudServiceActionTypes.CANCEL_REQUESTS;

        constructor(public payload: string[]) {
        }
    }

    export class CancelRequestsLike implements Action {

        readonly type = CrudServiceActionTypes.CANCEL_REQUESTS_LIKE;

        constructor(public payload: string[]) {
        }
    }

    export type CallActions =
        Create
        | Patch
        | Update
        | Delete
        | GetItem
        | GetPostItem
        | GetList
        | GetPostList;

    export type SuccessActions =
        CreateSuccess
        | PatchSuccess
        | UpdateSuccess
        | DeleteSuccess
        | GetItemSuccess
        | GetPostItemSuccess
        | GetListSuccess
        | GetPostListSuccess;

    export type FailedActions =
        CreateFailed
        | PatchFailed
        | UpdateFailed
        | DeleteFailed
        | GetItemFailed
        | GetPostItemFailed
        | GetListFailed
        | GetPostListFailed;

    export type CanceledActions =
        GetItemCanceled
        | GetPostItemCanceled
        | GetListCanceled
        | GetPostListCanceled;

    export type All =
        Create
        | CreateSuccess
        | CreateFailed
        | Patch
        | PatchSuccess
        | PatchFailed
        | Update
        | UpdateSuccess
        | UpdateFailed
        | Delete
        | DeleteSuccess
        | DeleteFailed
        | GetItem
        | GetItemSuccess
        | GetItemFailed
        | GetItemCanceled
        | GetPostItem
        | GetPostItemSuccess
        | GetPostItemFailed
        | GetPostItemCanceled
        | GetList
        | GetListSuccess
        | GetListCanceled
        | GetListFailed
        | GetPostList
        | GetPostListSuccess
        | GetPostListFailed
        | GetPostListCanceled
        | CancelRequests
        | CancelRequestsLike;
}
