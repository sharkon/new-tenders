import {Injectable} from '@angular/core';
import {ILang} from '../../interfaces/lang.interface';
import az from '../../../languages/az';
import en from '../../../languages/en';
import ru from '../../../languages/ru';
import {Store} from '@ngrx/store';
import {AppStore} from '../../store/app.store';
import {AppTranslateActions} from '../../store/translate/app-translate.actions';
import {DataTranslateInterface} from '../../interfaces/data-translate.interface';
import {AppLangConst} from '../../consts/app-lang.const';

@Injectable({
    providedIn: 'root'
})
export class TranslateService {

    private _data: DataTranslateInterface;
    private _lang: ILang.LangType;

    get data(): DataTranslateInterface {
        return this._data;
    }

    get lang(): ILang.LangType {
        return this._lang;
    }

    private _translations: any = {
        'az': az,
        'en': en,
        'ru': ru,
    };

    constructor(private store$: Store<AppStore.IState>) {
    }

    use(lang: ILang.LangType): Promise<{}> {

        return new Promise<{}>((resolve, reject) => {

            this._lang = lang;

            this._data = this._translations[lang]
                ? this._translations[lang]
                : this._translations[AppLangConst.DEFAULT_LANG];

            this.store$.dispatch(new AppTranslateActions.SetLang(this._lang));
            this.store$.dispatch(new AppTranslateActions.SetData(this._data));

            resolve();
        });
    }

}
