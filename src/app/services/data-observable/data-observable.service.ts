import {Observable, throwError as observableThrowError} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {IError} from '../../interfaces/error.interface';
import {NotifyService} from '../notify/notify.service';
import {TranslateService} from '../translate/translate.service';

@Injectable({
    providedIn: 'root'
})
export class DataObservableService {

    /**
     * Init methods
     */

    constructor(private http: HttpClient,
                private notifyService: NotifyService,
                private ts: TranslateService) {
    }

    private notifyStatus404(err, notify: boolean, mapMessage: IError.IBackendTranslateError) {

        if (err && err instanceof HttpErrorResponse && notify) {

            if (err.status === 404) {

                const dialogTitle: string = mapMessage && mapMessage['404'] && mapMessage['404']['dialogTitle']
                    ? mapMessage['404']['dialogTitle']
                    : this.ts.data.T.notFoundError;

                const dialogContent: string = mapMessage && mapMessage['404'] && mapMessage['404']['dialogContent']
                    ? mapMessage['404']['dialogContent']
                    : '';

                this.notifyService.error({
                    dialogTitle,
                    dialogContent,
                });

            } else if (err.status === 403) {

                const dialogTitle: string = mapMessage && mapMessage['403'] && mapMessage['403']['dialogTitle']
                    ? mapMessage['403']['dialogTitle']
                    : this.ts.data.T.forbiddenError;

                const dialogContent: string = mapMessage && mapMessage['403'] && mapMessage['403']['dialogContent']
                    ? mapMessage['403']['dialogContent']
                    : '';

                this.notifyService.warning({
                    dialogTitle,
                    dialogContent,
                });

            } else if (err.status === 400) {

                if (err.error && err.error.length) {

                    const errors = <IError.IBackendError[]>err.error;

                    errors.forEach(error => {

                        if (typeof error === 'string') {

                            this.notifyService.error();

                        } else {

                            Object.keys(error).forEach(key => {

                                const errorTypes = error[key];
                                let message: string;

                                const translate = mapMessage[key] || {};

                                if (errorTypes.length) {
                                    message = `${translate[key] || key}: ${errorTypes.map(val => translate[val] || val).join(',')}`;
                                } else {
                                    message = `${translate[key] || key}`;
                                }

                                this.notifyService.error({
                                    dialogContent: message,
                                });
                            });
                        }
                    });

                } else {

                    this.notifyService.error();
                }
            }
        }
    }

    /**
     * CRUD methods
     */

    get<T>(url: string, notify = true, mapMessage: IError.IBackendTranslateError = {}, options = {}): Observable<T> {

        return this.http
            .get<T>(`${url}`, options)
            .pipe(
                catchError(err => this.handleError(err, notify, mapMessage))
            );
    }

    post<T>(url: string, data: any, notify = true, mapMessage: IError.IBackendTranslateError = {}, options = {}): Observable<T> {

        return this.http
            .post<T>(`${url}`, data, options)
            .pipe(
                catchError(err => this.handleError(err, notify, mapMessage))
            );
    }

    patch<T>(url: string, data: any, notify = true, mapMessage: IError.IBackendTranslateError = {}, options = {}): Observable<T> {

        return this.http
            .patch<T>(`${url}`, data, options)
            .pipe(
                catchError(err => this.handleError(err, notify, mapMessage))
            );
    }

    put<T>(url: string, data: any, notify = true, mapMessage: IError.IBackendTranslateError = {}, options = {}): Observable<T> {

        return this.http
            .put<T>(`${url}`, data, options)
            .pipe(
                catchError(err => this.handleError(err, notify, mapMessage))
            );
    }

    delete<T>(url: string, notify = true, mapMessage: IError.IBackendTranslateError = {}, options = {}): Observable<T> {

        return this.http
            .delete<T>(`${url}`, options)
            .pipe(
                catchError(err => this.handleError(err, notify, mapMessage))
            );
    }

    private handleError(err, notify: boolean, mapMessage: IError.IBackendTranslateError) {

        this.notifyStatus404(err, notify, mapMessage);
        return observableThrowError(err);
    }

}
