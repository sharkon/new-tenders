import {IError} from '../interfaces/error.interface';
import {TranslateService} from '../services/translate/translate.service';

export namespace AppErrorsConst {

    // TODO MOVE OR RENAME FOR AUTH ONLY
    export function mapErrorMessage(ts: TranslateService): IError.IBackendTranslateError {

        const authErrors = ts.data.T.authErrors;

        const mapEM: IError.IBackendTranslateError = {
            'email': {
                'email': authErrors.error,
            },
            'user': {
                'user': authErrors.error,
            },
            'send_mail': {
                'send_mail': authErrors.error,
            },
            'non_field_errors': {
                'non_field_errors': authErrors.error,
            },
            'auth': {
                'auth': authErrors.error,
            },
            'uuid': {
                'uuid': authErrors.error,
            }
        };

        mapEM['email'][IError.BACKEND_ERROR_TYPES.INVALID_VALUE] =
            authErrors.email.invalidValue;

        mapEM['user'][IError.BACKEND_ERROR_TYPES.USER_BANNED] =
            authErrors.user.userBanned;

        mapEM['send_mail'][IError.BACKEND_ERROR_TYPES.ERROR] =
            authErrors.sendEmail.error;

        mapEM['non_field_errors'][IError.BACKEND_ERROR_TYPES.CAPTCHA_INVALID] =
            authErrors.nonFieldErrors.captchaInvalid;

        mapEM['auth'][IError.BACKEND_ERROR_TYPES.INVALID_VALUE] =
            authErrors.auth.invalidValue;

        mapEM['uuid'][IError.BACKEND_ERROR_TYPES.INVALID_VALUE] =
            authErrors.uuid.invalidValue;

        return mapEM;
    }


    export type IBackendErrorType =
        // Ошибка уникального значения
        // Поле с датой уникально
        'DUPLICATED' |
        // Обязательное поле
        'REQUIRED' |
        // Поле не может быть пустым
        'IS_NULL' |
        // Любое не корректное значение
        // Не верное значение для поля (Есть список одобренных констант)
        // Не верное значение
        // Ожидался список
        // Ожидался объект
        'INVALID_VALUE' |
        // Поле не может быть не заполненным ("" - тоже не пройдет)
        'IS_BLANK' |
        // Превышена макс длина
        'MAX_LENGTH' |
        // Наоборот
        'MIN_LENGTH' |
        // Ожидается время и дата, пришло что-то другое
        // Пришло время без таймзоны
        // Ожидается Дата, а пришло datetime
        'NOT_DATE' |
        // Ошибка имени файла
        'NO_NAME' |
        // Для забаненного пользователя
        'USER_BANNED' |
        // Возможные значения ошибок captchaValue
        // Гугл не ответил в течении отведенного времени
        'TIMEOUT' |
        // Любая другая ошибка при запросе к гуглу
        'THIRD_PARTY_SERVICE_ERRORS' |
        // поьзователь не найден
        'NOT_FOUND' |
        // лимит запросов к dadata
        'LIMIT_EXCEEDED';

    export const BACKEND_ERROR_TYPES = {
        DUPLICATED: <IBackendErrorType>'DUPLICATED',
        REQUIRED: <IBackendErrorType>'REQUIRED',
        IS_NULL: <IBackendErrorType>'IS_NULL',
        INVALID_VALUE: <IBackendErrorType>'INVALID_VALUE',
        IS_BLANK: <IBackendErrorType>'IS_BLANK',
        MAX_LENGTH: <IBackendErrorType>'MAX_LENGTH',
        MIN_LENGTH: <IBackendErrorType>'MIN_LENGTH',
        NOT_DATE: <IBackendErrorType>'NOT_DATE',
        NO_NAME: <IBackendErrorType>'NO_NAME',
        USER_BANNED: <IBackendErrorType>'USER_BANNED',
        TIMEOUT: <IBackendErrorType>'TIMEOUT',
        THIRD_PARTY_SERVICE_ERRORS: <IBackendErrorType>'THIRD_PARTY_SERVICE_ERRORS',
        NOT_FOUND: <IBackendErrorType>'NOT_FOUND',
        LIMIT_EXCEEDED: <IBackendErrorType>'LIMIT_EXCEEDED',
        AGREEMENT_NOT_ACCEPTED: <IBackendErrorType>'AGREEMENT_NOT_ACCEPTED',
    };

}
