export namespace AppConst {

    export const CRUD_NEW_ID = 'new';
    export const OPERATING_COMPANIES = ['SOC', 'KAOC', 'NOC', 'BOC', 'AOC'];
    export const CURRENCIES = ['USD', 'EUR', 'AZN', 'RUR'];

    // See the Moment.js docs for the meaning of these formats:
    // https://momentjs.com/docs/#/displaying/format/

    export const MY_MOMENT_FORMATS = {
        parseInput: 'YYYY-MM-DD HH:mm',
        fullPickerInput: 'YYYY-MM-DD HH:mm',
        datePickerInput: 'YYYY-MM-DD',
        timePickerInput: 'HH:mm',
        monthYearLabel: 'MMMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    };

    export const DATE_PIPE_FORMAT = {
        withoutTime: 'yyyy-MM-dd',
        withoutSeconds: 'yyyy-MM-dd HH:mm',
        withSeconds: 'yyyy-MM-dd HH:mm:ss',
    };

    export const RELOAD_LIST_INTERVAL = 3 * 60 * 1000;
    export const AUTH_SERVICES_UID = 'AUTH_SERVICES_UID';

    export const VALID_STRENGTH_PASSWORD = 2;
}
