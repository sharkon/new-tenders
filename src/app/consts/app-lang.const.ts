import {ILang} from '../interfaces/lang.interface';

export namespace AppLangConst {

    export const DEFAULT_LANG: ILang.LangType = 'ru';
    export const DEFAULT_CALENDAR_LANG: ILang.LangType = 'en';
    export const LANGS: ILang.LangType[] = ['az', 'en', 'ru'];
}
