import { Action } from '@ngrx/store';

export namespace AppBlockActionTypes {

    export const LOCK = '[AppBlock] LOCK';
    export const UN_LOCK = '[AppBlock] UN_LOCK';
    export const GROUP_UN_LOCK = '[AppBlock] GROUP_UN_LOCK';
}

export namespace AppBlockActions {

    export class Lock implements Action {

        readonly type = AppBlockActionTypes.LOCK;

        constructor(public payload: string) {
        }
    }

    export class UnLock implements Action {

        readonly type = AppBlockActionTypes.UN_LOCK;

        constructor(public payload: string) {
        }
    }

    export class GroupUnLock implements Action {

        readonly type = AppBlockActionTypes.GROUP_UN_LOCK;

        constructor(public payload: string[]) {
        }
    }

    export type All =
        Lock
        | UnLock
        | GroupUnLock;

}
