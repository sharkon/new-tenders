import { AppBlockActions, AppBlockActionTypes } from './app-block.actions';
import { IMap } from '../../interfaces/map.interface';
import { MemoizedSelector, createSelector } from '@ngrx/store';

export namespace AppBlockStore {

    export interface IState {
        locks: IMap.IMapOfBoolean;
    }

    const initialState: IState = {
        locks: {},
    };

    export interface ISelects {
        locks: (state: IState) => IMap.IMapOfBoolean;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class AppBlockSelector implements ISelects {

            locks = createSelector(
                selector,
                ((state: IState) => state.locks),
            );
        }

        return new AppBlockSelector();
    }

    export function reducer(state = initialState, action: AppBlockActions.All): IState {

        switch (action.type) {

            case AppBlockActionTypes.LOCK: {

                const locks = {
                    ...state.locks
                };

                locks[action.payload] = true;

                return {
                    ...state,
                    locks: locks,
                };
            }

            case AppBlockActionTypes.UN_LOCK: {

                const locks = {
                    ...state.locks
                };

                locks[action.payload] = false;

                return {
                    ...state,
                    locks: locks,
                };
            }

            case AppBlockActionTypes.GROUP_UN_LOCK: {

                const locks = {
                    ...state.locks
                };

                (action.payload || [])
                    .forEach(uid => {

                        locks[uid] = false;
                    });

                return {
                    ...state,
                    locks: locks,
                };
            }

            default: {
                return state;
            }

        }
    }

    export const getLocks = (state: IState) => state.locks;
}
