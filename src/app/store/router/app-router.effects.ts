import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AppRouterActions, AppRouterActionTypes } from './app-router.actions';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class AppRouterEffects {

    @Effect({ dispatch: false })
    navigate$ =
        this.actions$.pipe(
            ofType(AppRouterActionTypes.GO)
            , map((action: AppRouterActions.Go) => action.payload)
            , tap(({ path, query: queryParams, extras }) => {
                return this.router.navigate(path, { queryParams, ...extras });
            })
        );

    @Effect({ dispatch: false })
    navigateBack$ =
        this.actions$.pipe(
            ofType(AppRouterActionTypes.BACK)
            , tap(() => this.location.back())
        );

    @Effect({ dispatch: false })
    navigateForward$ =
        this.actions$.pipe(
            ofType(AppRouterActionTypes.FORWARD)
            , tap(() => this.location.forward())
        );

    constructor(private actions$: Actions,
        private router: Router,
        private location: Location) {
    }
}
