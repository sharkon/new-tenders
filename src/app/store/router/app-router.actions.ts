import { Action } from '@ngrx/store';
import { NavigationExtras } from '@angular/router';

export namespace AppRouterActionTypes {

    export const GO = '[AppRouter] GO';
    export const BACK = '[AppRouter] BACK';
    export const FORWARD = '[AppRouter] FORWARD';
}

export namespace AppRouterActions {

    export interface IGo {
        path: any[];
        query?: object;
        extras?: NavigationExtras;
    }

    export class Go implements Action {

        readonly type = AppRouterActionTypes.GO;

        constructor(public payload: IGo) {
        }
    }

    export class Back implements Action {

        readonly type = AppRouterActionTypes.BACK;
    }

    export class Forward implements Action {

        readonly type = AppRouterActionTypes.FORWARD;
    }

    export type All =
        Go
        | Back
        | Forward;

}
