import { Action } from '@ngrx/store';
import { ILang } from '../../interfaces/lang.interface';
import { IMap } from '../../interfaces/map.interface';
import {DataTranslateInterface} from '../../interfaces/data-translate.interface';

export namespace AppTranslateActionTypes {

    export const SET_LANG = '[AppTranslate] SET_LANG';
    export const SET_DATA = '[AppTranslate] SET_DATA';
}

export namespace AppTranslateActions {

    export class SetLang implements Action {

        readonly type = AppTranslateActionTypes.SET_LANG;

        constructor(public payload: ILang.LangType) {
        }
    }

    export class SetData implements Action {

        readonly type = AppTranslateActionTypes.SET_DATA;

        constructor(public payload: DataTranslateInterface) {
        }
    }

    export type All =
        SetLang
        | SetData;

}
