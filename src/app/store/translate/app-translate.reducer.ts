import { AppTranslateActions, AppTranslateActionTypes } from './app-translate.actions';
import { MemoizedSelector, createSelector } from '@ngrx/store';
import { ILang } from '../../interfaces/lang.interface';
import { IMap } from '../../interfaces/map.interface';
import {DataTranslateInterface} from '../../interfaces/data-translate.interface';

export namespace AppTranslateStore {

    export interface IState {
        lang: ILang.LangType;
        data: DataTranslateInterface;
    }

    const initialState: IState = {
        lang: null,
        data: null,
    };

    export interface ISelects {
        lang: (state: IState) => ILang.LangType;
        data: (state: IState) => DataTranslateInterface;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class AppTranslateSelector implements ISelects {

            lang = createSelector(
                selector,
                ((state: IState) => state.lang),
            );

            data = createSelector(
                selector,
                ((state: IState) => state.data),
            );
        }

        return new AppTranslateSelector();
    }

    export function reducer(state = initialState, action: AppTranslateActions.All): IState {

        switch (action.type) {

            case AppTranslateActionTypes.SET_LANG: {

                const lang = action.payload;

                return {
                    ...state,
                    lang,
                };
            }

            case AppTranslateActionTypes.SET_DATA: {

                const data = action.payload ? {...action.payload} : null;

                return {
                    ...state,
                    data,
                };
            }

            default: {
                return state;
            }

        }
    }
}
