import { Action } from '@ngrx/store';

export namespace AppErrorActionTypes {

    export const ADD_ERROR = '[AppError] ADD_ERROR';
}

export namespace AppErrorActions {

    export class AddError implements Action {

        readonly type = AppErrorActionTypes.ADD_ERROR;

        constructor(public payload: any) {
        }
    }

    export type All =
        AddError;

}
