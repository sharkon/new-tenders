import { AppErrorActions, AppErrorActionTypes } from './app-error.actions';
import { MemoizedSelector, createSelector } from '@ngrx/store';

export namespace AppErrorStore {

    export interface IState {
        errors: any[];
    }

    const initialState: IState = {
        errors: [],
    };

    export interface ISelects {
        errors: (state: IState) => any[];
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class AppErrorSelector implements ISelects {

            errors = createSelector(
                selector,
                ((state: IState) => state.errors),
            );
        }

        return new AppErrorSelector();
    }

    export function reducer(state = initialState, action: AppErrorActions.All): IState {

        switch (action.type) {

            case AppErrorActionTypes.ADD_ERROR: {

                const errors = state.errors.slice();
                errors.push(action.payload);

                return {
                    ...state,
                    errors,
                };
            }

            default: {
                return state;
            }

        }
    }

    export const getErrors = (state: IState) => state.errors;
}
