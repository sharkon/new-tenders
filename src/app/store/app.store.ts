import { ActionReducerMap, createSelector, createFeatureSelector } from '@ngrx/store';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { AppAuthStore } from './auth/app-auth.reducer';
import { AppBlockStore } from './block/app-block.reducer';
import { AppErrorStore } from './error/app-error.reducer';
import { AppTranslateStore } from './translate/app-translate.reducer';

export namespace AppStore {

    export interface IState {
        appRouter: RouterReducerState;
        appAuth: AppAuthStore.IState;
        appBlock: AppBlockStore.IState;
        appError: AppErrorStore.IState;
        appTranslate: AppTranslateStore.IState;
    }

    export const mapReducers: ActionReducerMap<IState> = {
        appRouter: routerReducer,
        appAuth: AppAuthStore.reducer,
        appBlock: AppBlockStore.reducer,
        appError: AppErrorStore.reducer,
        appTranslate: AppTranslateStore.reducer,
    };

    export namespace Selects {

        // router
        export const getAppRouter = (state: IState) => state.appRouter;

        export namespace AppRouter {

            export const getState = createSelector(
                getAppRouter,
                (state: RouterReducerState) => state ? state.state : null,
            );
        }

        // auth
        const authSelector = createFeatureSelector<AppAuthStore.IState>('appAuth');

        const selfAuthSelector = createSelector(
            authSelector,
            (state: AppAuthStore.IState) => state,
        );

        export const appAuth = AppAuthStore.createStoreSelector(selfAuthSelector);

        // block
        const blockSelector = createFeatureSelector<AppBlockStore.IState>('appBlock');

        const selfBlockSelector = createSelector(
            blockSelector,
            (state: AppBlockStore.IState) => state,
        );

        export const appBlock = AppBlockStore.createStoreSelector(selfBlockSelector);

        // error
        const errorSelector = createFeatureSelector<AppErrorStore.IState>('appError');

        const selfErrorSelector = createSelector(
            errorSelector,
            (state: AppErrorStore.IState) => state,
        );

        export const appError = AppErrorStore.createStoreSelector(selfErrorSelector);

        // translate
        const translateSelector = createFeatureSelector<AppTranslateStore.IState>('appTranslate');

        const selfTranslateSelector = createSelector(
            translateSelector,
            (state: AppTranslateStore.IState) => state,
        );

        export const appTranslate = AppTranslateStore.createStoreSelector(selfTranslateSelector);
    }
}

