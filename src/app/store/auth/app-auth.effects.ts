import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {AuthService} from '../../services/auth/auth.service';
import {Observable, of} from 'rxjs';
import {AppAuthActions, AppAuthActionTypes} from './app-auth.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import {Action} from '@ngrx/store';
import {IAuth} from '../../interfaces/auth.interface';
import {IUser} from '../../interfaces/user.interface';
import {ActivatedRoute, Router} from '@angular/router';
import {StorageService} from '../../services/storage/storage.service';
import {AppBlockActions} from '../block/app-block.actions';
import {AppConst} from '../../consts/app.const';


@Injectable()
export class AppAuthEffects {

    @Effect()
    getCaptcha$: Observable<Action> =
        this.actions$.pipe(
            ofType(AppAuthActionTypes.GET_CAPTCHA),
            switchMap((action: AppAuthActions.GetCaptcha) => {
                return this.authService.getCaptcha()
                    .pipe(
                        map((response: IAuth.ICaptchaResponse) => {
                            return new AppAuthActions.SetCaptcha(response);
                        }),
                        catchError((e: any) => {
                            return of(new AppAuthActions.GetCaptchaFailed(e.error));
                        })
                    );
            })
        );

    @Effect()
    logIn$: Observable<Action> =
        this.actions$.pipe(
            ofType(AppAuthActionTypes.LOGIN),
            map((action: AppAuthActions.LogIn) => action.payload),
            switchMap((payload: IAuth.ISignInRequest) => {
                return this.authService.logIn(payload)
                    .pipe(
                        map((response: IAuth.ISignInResponse) => {

                            this.authService.setToken(response.token.key);

                            return new AppAuthActions.LogInSuccess();
                        }),
                        catchError((e: any) => {
                            return of(new AppAuthActions.LogInFailed(e.error));
                        })
                    );
            })
        );

    @Effect()
    logInSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(AppAuthActionTypes.LOGIN_SUCCESS),
            switchMap((action: AppAuthActions.LogInSuccess) => {
                return this.authService.getUser()
                    .pipe(
                        map((response: IUser.IUserResponse) => {

                            const returnUrl = action.payload || this.route.snapshot.queryParams['returnUrl'];
                            this.router.navigateByUrl(returnUrl ? this.router.parseUrl(returnUrl) : '/');

                            return new AppAuthActions.SetUser(response);
                        }),
                        catchError((e: any) => {
                            return of(new AppAuthActions.GetUserFailed(e.error));
                        })
                    );
            })
        );

    @Effect()
    logInFailed$: Observable<Action> =
        this.actions$.pipe(
            ofType(AppAuthActionTypes.LOGIN_FAILED),
            map((action: AppAuthActions.LogInFailed) => {
                return new AppAuthActions.GetCaptcha();
            })
        );

    @Effect()
    logOut$: Observable<Action> = this.actions$.pipe(
        ofType(AppAuthActionTypes.LOGOUT),
        map((action: AppAuthActions.LogOut) => {

            this.storageService.local.removeItem('token');
            this.router.navigate([action.payload || '/']);

            return new AppAuthActions.SetUser(null);
        })
    );

    @Effect()
    recovery$: Observable<Action> =
        this.actions$.pipe(
            ofType(AppAuthActionTypes.RECOVERY),
            map((action: AppAuthActions.Recovery) => action.payload),
            switchMap((payload: IAuth.IRecoveryRequest) => {
                return this.authService.recovery(payload)
                    .pipe(
                        map((response: any) => {
                            return new AppAuthActions.RecoverySuccess();
                        }),
                        catchError((e: any) => {
                            return of(new AppAuthActions.RecoveryFailed(e.error));
                        })
                    );
            })
        );

    @Effect()
    locks$: Observable<Action> =
        this.actions$.pipe(
            ofType(
                AppAuthActionTypes.LOGIN,
                AppAuthActionTypes.RECOVERY,
                AppAuthActionTypes.GET_CAPTCHA,
            ),
            map(() => {
                return new AppBlockActions.Lock(AppConst.AUTH_SERVICES_UID);
            }),
        );

    @Effect()
    unlocks$: Observable<Action> =
        this.actions$.pipe(
            ofType(
                AppAuthActionTypes.SET_USER,
                AppAuthActionTypes.GET_USER_FAILED,
                AppAuthActionTypes.LOGIN_FAILED,
                AppAuthActionTypes.RECOVERY_SUCCESS,
                AppAuthActionTypes.RECOVERY_FAILED,
                AppAuthActionTypes.SET_CAPTCHA,
                AppAuthActionTypes.GET_CAPTCHA_FAILED,
            ),
            map(() => {
                return new AppBlockActions.UnLock(AppConst.AUTH_SERVICES_UID);
            }),
        );

    constructor(
        private actions$: Actions,
        private authService: AuthService,
        private router: Router,
        private route: ActivatedRoute,
        private storageService: StorageService,
    ) {
    }

}
