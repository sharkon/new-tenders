import {Action} from '@ngrx/store';
import {IAuth} from '../../interfaces/auth.interface';
import {IUser} from '../../interfaces/user.interface';
import {IError} from '../../interfaces/error.interface';

export namespace AppAuthActionTypes {

    export const GET_CAPTCHA = '[AppAuth] GET_CAPTCHA';
    export const SET_CAPTCHA = '[AppAuth] SET_CAPTCHA';
    export const GET_CAPTCHA_FAILED = '[AppAuth] GET_CAPTCHA_FAILED';

    export const LOGIN = '[AppAuth] LOGIN';
    export const LOGIN_SUCCESS = '[AppAuth] LOGIN_SUCCESS';
    export const LOGIN_FAILED = '[AppAuth] LOGIN_FAILED';

    export const LOGOUT = '[AppAuth] LOGOUT';

    export const RECOVERY = '[AppAuth] RECOVERY';
    export const RECOVERY_SUCCESS = '[AppAuth] RECOVERY_SUCCESS';
    export const RECOVERY_FAILED = '[AppAuth] RECOVERY_FAILED';

    export const SET_USER = '[AppAuth] SET_USER';
    export const GET_USER_FAILED = '[AppAuth] GET_USER_FAILED';
}

export namespace AppAuthActions {

    export class GetCaptcha implements Action {

        readonly type = AppAuthActionTypes.GET_CAPTCHA;
    }

    export class SetCaptcha implements Action {

        readonly type = AppAuthActionTypes.SET_CAPTCHA;

        constructor(public payload: IAuth.ICaptchaResponse) {
        }
    }

    export class GetCaptchaFailed implements Action {

        readonly type = AppAuthActionTypes.GET_CAPTCHA_FAILED;

        constructor(public payload: IError.IBackendError[]) {
        }
    }

    export class LogIn implements Action {

        readonly type = AppAuthActionTypes.LOGIN;

        constructor(public payload: IAuth.ISignInRequest) {
        }
    }

    export class LogInSuccess implements Action {

        readonly type = AppAuthActionTypes.LOGIN_SUCCESS;

        constructor(public payload?: string) {
        }
    }

    export class LogInFailed implements Action {

        readonly type = AppAuthActionTypes.LOGIN_FAILED;

        constructor(public payload: IError.IBackendError[]) {
        }
    }

    export class LogOut implements Action {

        readonly type = AppAuthActionTypes.LOGOUT;

        constructor(public payload?: string) {
        }
    }

    export class Recovery implements Action {

        readonly type = AppAuthActionTypes.RECOVERY;

        constructor(public payload: IAuth.IRecoveryRequest) {
        }
    }

    export class RecoverySuccess implements Action {

        readonly type = AppAuthActionTypes.RECOVERY_SUCCESS;
    }

    export class RecoveryFailed implements Action {

        readonly type = AppAuthActionTypes.RECOVERY_FAILED;

        constructor(public payload: IError.IBackendError[]) {
        }
    }

    export class SetUser implements Action {

        readonly type = AppAuthActionTypes.SET_USER;

        constructor(public payload: IUser.IUserResponse) {
        }
    }

    export class GetUserFailed implements Action {

        readonly type = AppAuthActionTypes.GET_USER_FAILED;

        constructor(public payload: IError.IBackendError[]) {
        }
    }

    export type All =
        GetCaptcha
        | SetCaptcha
        | GetCaptchaFailed
        | LogIn
        | LogInSuccess
        | LogInFailed
        | LogOut
        | Recovery
        | RecoverySuccess
        | RecoveryFailed
        | SetUser
        | GetUserFailed;

}
