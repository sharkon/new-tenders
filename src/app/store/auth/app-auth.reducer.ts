import {AppAuthActions, AppAuthActionTypes} from './app-auth.actions';
import {IAuth} from '../../interfaces/auth.interface';
import {IUser} from '../../interfaces/user.interface';
import {createSelector, MemoizedSelector} from '@ngrx/store';
import {IError} from '../../interfaces/error.interface';

export namespace AppAuthStore {

    export interface IState {
        captcha: IAuth.ICaptchaResponse;
        captchaErrorMessage: IError.IBackendError[];
        authenticated: boolean;
        authErrorMessage: IError.IBackendError[];
        user: IUser.IUserResponse;
        userErrorMessage: IError.IBackendError[];
    }

    const initialState: IState = {
        captcha: null,
        captchaErrorMessage: null,
        authenticated: null,
        authErrorMessage: null,
        user: null,
        userErrorMessage: null,
    };

    export interface ISelects {
        captcha: (state: IState) => IAuth.ICaptchaResponse;
        captchaErrorMessage: (state: IState) => IError.IBackendError[];
        authenticated: (state: IState) => boolean;
        authErrorMessage: (state: IState) => IError.IBackendError[];
        user: (state: IState) => IUser.IUserResponse;
        userErrorMessage: (state: IState) => IError.IBackendError[];
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class AppAuthSelector implements ISelects {

            captcha = createSelector(
                selector,
                ((state: IState) => state.captcha),
            );

            captchaErrorMessage = createSelector(
                selector,
                ((state: IState) => state.captchaErrorMessage),
            );

            authenticated = createSelector(
                selector,
                ((state: IState) => state.authenticated),
            );

            authErrorMessage = createSelector(
                selector,
                ((state: IState) => state.authErrorMessage),
            );

            user = createSelector(
                selector,
                ((state: IState) => state.user),
            );

            userErrorMessage = createSelector(
                selector,
                ((state: IState) => state.userErrorMessage),
            );
        }

        return new AppAuthSelector();
    }

    export function reducer(state = initialState, action: AppAuthActions.All): IState {

        switch (action.type) {

            case AppAuthActionTypes.SET_CAPTCHA: {

                const captcha = action.payload;
                const captchaErrorMessage = null;

                return {
                    ...state,
                    captcha,
                    captchaErrorMessage,
                };
            }

            case AppAuthActionTypes.GET_CAPTCHA_FAILED: {

                const captcha = null;
                const captchaErrorMessage = action.payload;

                return {
                    ...state,
                    captcha,
                    captchaErrorMessage,
                };
            }

            case AppAuthActionTypes.LOGIN_SUCCESS: {

                const authenticated = true;
                const authErrorMessage = null;

                return {
                    ...state,
                    authenticated,
                    authErrorMessage,
                };
            }

            case AppAuthActionTypes.LOGIN_FAILED: {

                const authenticated = false;
                const authErrorMessage = action.payload;

                return {
                    ...state,
                    authenticated,
                    authErrorMessage,
                };
            }

            case AppAuthActionTypes.LOGOUT: {

                const authenticated = false;

                return {
                    ...state,
                    authenticated,
                };
            }

            case AppAuthActionTypes.SET_USER: {

                const user = action.payload;
                const authenticated = !!user;

                return {
                    ...state,
                    authenticated,
                    user,
                };
            }

            case AppAuthActionTypes.GET_USER_FAILED: {

                const userErrorMessage = action.payload;

                return {
                    ...state,
                    userErrorMessage,
                };
            }

            default: {
                return state;
            }

        }
    }

    export const getCaptcha = (state: IState) => state.captcha;
    export const getCaptchaErrorMessage = (state: IState) => state.captchaErrorMessage;
    export const getAuthenticated = (state: IState) => state.authenticated;
    export const getAuthErrorMessage = (state: IState) => state.authErrorMessage;
    export const getUser = (state: IState) => state.user;
}
