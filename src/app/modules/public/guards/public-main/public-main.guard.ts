import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../../../store/app.store';
import {switchMap} from 'rxjs/operators';
import {IUser} from '../../../../interfaces/user.interface';

@Injectable()
export class PublicMainGuard implements CanActivate {

    constructor(private router: Router,
                private store$: Store<AppStore.IState>) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        return this.store$.pipe(
            select(AppStore.Selects.appAuth.user),
            switchMap((user: IUser.IUserResponse) => {

                if (!user) {

                    return of(true);

                } else {

                    if (user.role.indexOf(IUser.USER_ROLES.CREATOR) > -1) {
                        this.router.navigate(['/creator']);
                    }

                    if (user.role.indexOf(IUser.USER_ROLES.SELLER) > -1) {
                        this.router.navigate(['/seller/tenders']);
                    }

                    if (user.role.indexOf(IUser.USER_ROLES.CURATOR) > -1
                        || user.role.indexOf(IUser.USER_ROLES.CURATOR_BOSS) > -1
                        || user.role.indexOf(IUser.USER_ROLES.BOSS) > -1) {

                        this.router.navigate(['/curator']);
                    }

                    if (user.role.indexOf(IUser.USER_ROLES.AUDITOR) > -1) {
                        this.router.navigate(['/auditor/tender']);
                    }

                    if (user.role.indexOf(IUser.USER_ROLES.SITE_ADMIN) > -1) {
                        this.router.navigate(['/admin']);
                    }

                    return of(false);
                }
            })
        );
    }
}
