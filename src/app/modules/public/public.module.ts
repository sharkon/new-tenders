import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicComponent } from './public.component';
import { RouterModule } from '@angular/router';
import {PublicMainGuard} from './guards/public-main/public-main.guard';

@NgModule({
    declarations: [PublicComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: PublicComponent,
                canActivate: [PublicMainGuard],
            }
        ]),
    ],
    providers: [
        PublicMainGuard,
    ],
})
export class PublicModule { }
