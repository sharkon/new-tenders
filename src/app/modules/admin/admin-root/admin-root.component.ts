import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'app-admin-root',
    templateUrl: './admin-root.component.html',
    styleUrls: ['./admin-root.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminRootComponent implements OnInit {

    get url() {
        return environment.adminPage;
    }

    constructor() {
    }

    ngOnInit() {
    }

}
