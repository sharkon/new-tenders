import { TestBed, async, inject } from '@angular/core/testing';

import { SellerInviteDataGuard } from './seller-invite-data.guard';

describe('SellerInviteDataGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SellerInviteDataGuard]
    });
  });

  it('should ...', inject([SellerInviteDataGuard], (guard: SellerInviteDataGuard) => {
    expect(guard).toBeTruthy();
  }));
});
