import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../../services/crud/store/crud-service.actions';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../../../../store/app.store';
import {Actions, ofType} from '@ngrx/effects';
import {catchError, filter, switchMap, withLatestFrom} from 'rxjs/operators';
import {ISellerInvite} from '../../interfaces/seller-invite.interface';
import {SellerInviteStore} from '../../store/seller-invite.store';
import {SellerInviteActions} from '../../store/seller-invite.actions';

@Injectable({
    providedIn: 'root'
})
export class SellerInviteDataGuard implements CanActivate {

    constructor(private store$: Store<SellerInviteStore.IState>,
                private actions$: Actions,
                private router: Router) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        const inviteToken = next.params['inviteToken'] || null;

        if (!inviteToken) {
            return false;
        }

        this.store$.dispatch(new CrudServiceActions.GetItem({
            url: `${SellerInviteStore.SERVICES_URL.invite}/${inviteToken}`,
            uid: SellerInviteStore.SERVICES_UID.invite,
        }));

        const failed = () => {
            this.router.navigate(['/']);
            return of(false);
        };

        return this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS, CrudServiceActionTypes.GET_ITEM_FAILED),
                filter((action: CrudServiceActions.GetItemSuccess | CrudServiceActions.GetItemFailed) => {
                    return action.payload.uid === SellerInviteStore.SERVICES_UID.invite;
                }),
                withLatestFrom(
                    this.store$.pipe(select(AppStore.Selects.appAuth.authenticated)),
                ),
                switchMap(([action, authenticated]) => {

                    if (action.type === CrudServiceActionTypes.GET_ITEM_FAILED) {

                        return failed();
                    }

                    const inviteResponse = action.payload.item as ISellerInvite.InviteResponse;
                    const returnUrl = inviteResponse.tender_id ? `/seller/tender/${inviteResponse.tender_id}` : '/';

                    if (authenticated) {

                        this.router.navigate([returnUrl]);
                        return of(false);
                    }

                    if (inviteResponse.seller_exists) {

                        this.router.navigate(['/login'], {queryParams: {returnUrl}});
                        return of(false);
                    }

                    this.store$.dispatch(new SellerInviteActions.SetInviteResponse(inviteResponse));
                    this.store$.dispatch(new SellerInviteActions.SetReturnUrl(returnUrl));

                    return of(true);
                }),
                catchError((e) => failed())
            );
    }

}
