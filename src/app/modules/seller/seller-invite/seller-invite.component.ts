import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IAuth} from '../../../interfaces/auth.interface';
import {Observable, Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../../store/app.store';
import {Actions, ofType} from '@ngrx/effects';
import {ActivatedRoute} from '@angular/router';
import {AppAuthActions} from '../../../store/auth/app-auth.actions';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../services/crud/store/crud-service.actions';
import {filter, map} from 'rxjs/operators';
import {AppErrorsUtils} from '../../../utils/app-errors.utils';
import {AuthService} from '../../../services/auth/auth.service';
import {TranslateService} from '../../../services/translate/translate.service';
import {SellerInviteActions} from './store/seller-invite.actions';
import {SellerInviteStore} from './store/seller-invite.store';
import {ISellerInvite} from './interfaces/seller-invite.interface';
import {IError} from '../../../interfaces/error.interface';
import {AppErrorsConst} from '../../../consts/app-errors.const';
import {AppConst} from '../../../consts/app.const';

@Component({
    selector: 'app-seller-invite',
    templateUrl: './seller-invite.component.html',
    styleUrls: ['./seller-invite.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SellerInviteComponent implements OnInit, OnDestroy {

    inviteResponse: ISellerInvite.InviteResponse;
    returnUrl: string;

    inviteFormGroup: FormGroup;
    inviteFormGroupSubmitted: boolean;

    captcha: IAuth.ICaptchaResponse;
    captchaErrorMessage: string;

    signUpErrorMessage: string;
    passwordType = 'password';
    strengthPassword: number;

    get VALID_STRENGTH_PASSWORD() {
        return AppConst.VALID_STRENGTH_PASSWORD;
    }

    loading$: Observable<boolean>;

    private inviteToken: string;

    private SERVICES_UID = {
        register: '[SellerInviteComponent][SERVICES_UID] register',
    };

    private SERVICES_URL = {
        register: 'sellers/register',
    };

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get captchaImageString(): string {
        return this.captcha
            ? `data:${this.captcha.image_type};${this.captcha.image_decode},${this.captcha.captcha_image}`
            : null;
    }

    get fullNameControl(): AbstractControl {
        return this.getFormControl('fullName');
    }

    get passwordControl(): AbstractControl {
        return this.getFormControl('password');
    }

    get phoneControl(): AbstractControl {
        return this.getFormControl('phone');
    }

    get companyNameControl(): AbstractControl {
        return this.getFormControl('companyName');
    }

    get positionControl(): AbstractControl {
        return this.getFormControl('position');
    }

    get captchaControl(): AbstractControl {
        return this.getFormControl('captcha_value');
    }

    constructor(private formBuilder: FormBuilder,
                private store$: Store<AppStore.IState>,
                private actions$: Actions,
                private cdr: ChangeDetectorRef,
                private route: ActivatedRoute,
                private authService: AuthService,
                private ts: TranslateService) {
    }

    ngOnInit() {

        this.initForm();
        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new SellerInviteActions.DestroyState());
    }

    private initForm(): void {

        this.inviteFormGroup = this.formBuilder.group({
            fullName: ['', [Validators.required]],
            password: ['', [Validators.minLength(8), Validators.required]],
            phone: ['', [Validators.required]],
            companyName: ['', [Validators.required]],
            position: ['', [Validators.required]],
            captcha_value: ['', [Validators.required]],
        });
    }

    private initStore() {

        this.inviteToken = this.route.snapshot.params['inviteToken'] || null;

        this.loading$ = this.store$.pipe(
            select(AppStore.Selects.appBlock.locks),
            map(locks => {
                return locks[this.SERVICES_UID.register];
            }),
        );

        this.subs = this.store$.pipe(select(SellerInviteStore.Selects.self.inviteResponse))
            .subscribe(inviteResponse => {

                this.inviteResponse = inviteResponse;
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(SellerInviteStore.Selects.self.returnUrl))
            .subscribe(returnUrl => {

                this.returnUrl = returnUrl;
                this.cdr.detectChanges();
            });

        // get captcha
        this.store$.dispatch(new CrudServiceActions.GetItem({
            url: this.SERVICES_URL.register,
            uid: this.SERVICES_UID.register,
        }));

        this.subs = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
                map((action: CrudServiceActions.GetItemSuccess) => action.payload),
                filter(payload => payload.uid === this.SERVICES_UID.register),
            )
            .subscribe((payload: any) => {

                this.captcha = payload.item;

                if (this.getFormControl('captcha_value')) {
                    this.getFormControl('captcha_value').setValue('');
                }

                this.cdr.detectChanges();
            });

        this.subs = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_FAILED),
                map((action: CrudServiceActions.GetItemFailed) => action.payload),
                filter(payload => payload.uid === this.SERVICES_UID.register),
            )
            .subscribe((payload: any) => {

                this.captchaErrorMessage = AppErrorsUtils.parseErrors(payload.error, this.ts);
                this.cdr.detectChanges();
            });

        // sign up
        this.subs = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.CREATE_SUCCESS),
                map((action: CrudServiceActions.CreateSuccess) => action.payload),
                filter(payload => payload.uid === this.SERVICES_UID.register),
            )
            .subscribe((payload: any) => {

                const response: IAuth.ISignInResponse = payload.item;
                this.authService.setToken(response.token.key);

                this.store$.dispatch(new AppAuthActions.LogInSuccess(this.returnUrl));

                this.cdr.detectChanges();
            });

        this.subs = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.CREATE_FAILED),
                map((action: CrudServiceActions.CreateFailed) => action.payload),
                filter(payload => payload.uid === this.SERVICES_UID.register),
            )
            .subscribe((payload: any) => {

                this.signUpErrorMessage = AppErrorsUtils.parseErrors(payload.error, this.ts);
                this.reloadCaptcha();

                this.cdr.detectChanges();
            });
    }

    private getFormControl(key: string): AbstractControl {
        return this.inviteFormGroup && this.inviteFormGroup.controls[key]
            ? this.inviteFormGroup.controls[key]
            : null;
    }

    onSubmit(event: Event) {

        event.preventDefault();

        this.inviteFormGroupSubmitted = true;

        if (this.inviteFormGroup.valid && this.strengthPassword >= this.VALID_STRENGTH_PASSWORD) {

            const formValues = this.inviteFormGroup.value;

            const request: IAuth.IInviteRequest = {
                invite: this.inviteToken,
                fullName: formValues.fullName || null,
                password: formValues.password || null,
                extra: {
                    phone: formValues.phone || null,
                    companyName: formValues.companyName || null,
                    position: formValues.position || null,
                },
                captcha_key: this.captcha ? this.captcha.captcha_key : null,
                captcha_value: formValues.captcha_value || null,
            };

            this.store$.dispatch(new CrudServiceActions.Create({
                url: this.SERVICES_URL.register,
                uid: this.SERVICES_UID.register,
                item: {
                    id: null,
                    ...request,
                },
                mapMessage: this.getMapMessage(),
            }));
        }
    }

    onStrengthChange(strength: number): void {
        this.strengthPassword = strength;
    }

    reloadCaptcha(): void {

        this.store$.dispatch(new CrudServiceActions.GetItem({
            url: this.SERVICES_URL.register,
            uid: this.SERVICES_UID.register,
        }));
    }

    toggleViewPassword() {
        this.passwordType = this.passwordType === 'password' ? 'text' : 'password';
    }

    private getMapMessage(): IError.IBackendTranslateError {

        const mapMessage: IError.IBackendTranslateError = {
            'captcha_value': {
                'captcha_value': this.ts.data.T.authErrors.error,
            },
            'non_field_errors': {
                'non_field_errors': this.ts.data.T.authErrors.error,
            },
        };

        mapMessage['captcha_value'][AppErrorsConst.BACKEND_ERROR_TYPES.MAX_LENGTH] = this.ts.data.T.authErrors.nonFieldErrors.captchaInvalid;
        mapMessage['non_field_errors']['CAPTCHA_INVALID'] = this.ts.data.T.authErrors.nonFieldErrors.captchaInvalid;
        mapMessage['non_field_errors']['CAPTCHA_INVALID_OR_EXPIRED'] = this.ts.data.T.authErrors.nonFieldErrors.captchaInvalid;

        return mapMessage;
    }
}
