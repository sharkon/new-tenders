import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SellerInviteComponent} from './seller-invite.component';
import {PipesModule} from '../../../pipes/pipes.module';
import {ReactiveFormsModule} from '@angular/forms';
import {PasswordStrengthMeterModule} from 'angular-password-strength-meter';
import {DesignPreloaderModule} from '../../design/design-preloader/design-preloader.module';
import {RouterModule} from '@angular/router';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {SellerInviteStore} from './store/seller-invite.store';
import {SellerInviteEffects} from './store/seller-invite.effects';
import {FontAwesomeModule} from 'ngx-icons';

@NgModule({
    declarations: [SellerInviteComponent],
    imports: [
        CommonModule,
        PipesModule,
        ReactiveFormsModule,
        PasswordStrengthMeterModule,
        DesignPreloaderModule,
        RouterModule.forChild([
            {
                path: '',
                component: SellerInviteComponent,
                data: {
                    module: SellerInviteStore.MODULE_UID,
                },
            }
        ]),
        StoreModule.forFeature(SellerInviteStore.FEATURE_UID, SellerInviteStore.mapReducers),
        EffectsModule.forFeature([
            SellerInviteEffects,
        ]),
        FontAwesomeModule,
    ]
})
export class SellerInviteModule {
}
