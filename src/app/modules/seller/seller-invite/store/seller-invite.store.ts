import {SellerInviteReducer} from './seller-invite.reducer';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {AppStore} from '../../../../store/app.store';

export namespace SellerInviteStore {

    export const FEATURE_UID = '[SellerInviteStore] FEATURE_UID';
    export const MODULE_UID = '[SellerInviteStore] MODULE_UID';

    export const SERVICES_UID = {
        invite: '[SellerInviteStore][SERVICES_UID] invite',
    };

    export const SERVICES_URL = {
        invite: 'sellers/invite',
    };

    interface IStore {
        selfStore: SellerInviteReducer.IState;
    }

    export interface IState extends AppStore.IState {
        sellerInviteStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: SellerInviteReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = SellerInviteReducer.createStoreSelector(selfSelector);

    }

}
