import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {SellerInviteActionTypes} from './seller-invite.actions';
import {CrudServiceActions} from '../../../../services/crud/store/crud-service.actions';
import {SellerInviteStore} from './seller-invite.store';

@Injectable()
export class SellerInviteEffects {

    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(SellerInviteActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        SellerInviteStore.SERVICES_UID.invite,
                    ]);
                })
            );

    constructor(private actions$: Actions) {
    }
}
