import {Action} from '@ngrx/store';
import {ISellerInvite} from '../interfaces/seller-invite.interface';

export namespace SellerInviteActionTypes {

    export const SET_INVITE_RESPONSE = '[SellerInvite] SET_INVITE_RESPONSE';
    export const SET_RETURN_URL = '[SellerInvite] SET_RETURN_URL';
    export const DESTROY_STATE = '[SellerInvite] DESTROY_STATE';
}

export namespace SellerInviteActions {

    export class SetInviteResponse implements Action {

        readonly type = SellerInviteActionTypes.SET_INVITE_RESPONSE;

        constructor(public payload: ISellerInvite.InviteResponse) {
        }
    }

    export class SetReturnUrl implements Action {

        readonly type = SellerInviteActionTypes.SET_RETURN_URL;

        constructor(public payload: string) {
        }
    }

    export class DestroyState implements Action {

        readonly type = SellerInviteActionTypes.DESTROY_STATE;
    }

    export type All =
        SetInviteResponse
        | SetReturnUrl
        | DestroyState;

}
