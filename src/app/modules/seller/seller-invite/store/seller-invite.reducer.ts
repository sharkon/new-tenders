import {createSelector, MemoizedSelector} from '@ngrx/store';
import {SellerInviteActions, SellerInviteActionTypes} from './seller-invite.actions';
import {ISellerInvite} from '../interfaces/seller-invite.interface';

export namespace SellerInviteReducer {

    export interface IState {
        inviteResponse: ISellerInvite.InviteResponse;
        returnUrl: string;
    }

    const initialState: IState = {
        inviteResponse: null,
        returnUrl: null,
    };

    export interface ISelects {
        inviteResponse: (state: IState) => ISellerInvite.InviteResponse;
        returnUrl: (state: IState) => string;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            inviteResponse = createSelector(
                selector,
                ((state: IState) => state.inviteResponse),
            );

            returnUrl = createSelector(
                selector,
                ((state: IState) => state.returnUrl),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = {...initialState}, action: SellerInviteActions.All): IState {

        switch (action.type) {

            case SellerInviteActionTypes.SET_INVITE_RESPONSE: {

                return {
                    ...state,
                    inviteResponse: action.payload ? {...action.payload} : null,
                };
            }

            case SellerInviteActionTypes.SET_RETURN_URL: {

                return {
                    ...state,
                    returnUrl: action.payload,
                };
            }

            case SellerInviteActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
