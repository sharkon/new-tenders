import {ICrud} from '../../../../interfaces/crud.interface';

export namespace ISellerInvite {

    export interface InviteResponse extends ICrud.IModel {
        email: string;
        tender_id: number;
        seller_exists: boolean;
    }
}
