import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SellerOfferComponent} from './seller-offer.component';
import {RouterModule} from '@angular/router';
import {PipesModule} from '../../../pipes/pipes.module';
import {StoreModule} from '@ngrx/store';
import {SellerOfferStore} from './store/seller-offer.store';
import {EffectsModule} from '@ngrx/effects';
import {SellerOfferEffects} from './store/seller-offer.effects';
import {DesignBackModule} from '../../design/design-back/design-back.module';
import {DesignPreloaderModule} from '../../design/design-preloader/design-preloader.module';
import {DesignFileUploaderModule} from '../../design/design-file-uploader/design-file-uploader.module';
import {DesignDateTimeModule} from '../../design/design-date-time/design-date-time.module';
import {ComponentDeactivateGuard} from '../../../guards/component-deactivate/component-deactivate.guard';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
    declarations: [SellerOfferComponent],
    imports: [
        CommonModule,
        PipesModule,
        RouterModule.forChild([
            {
                path: '',
                component: SellerOfferComponent,
                canDeactivate: [ComponentDeactivateGuard],
                data: {
                    module: SellerOfferStore.MODULE_UID,
                }
            }
        ]),
        StoreModule.forFeature(SellerOfferStore.FEATURE_UID, SellerOfferStore.mapReducers),
        EffectsModule.forFeature([
            SellerOfferEffects,
        ]),
        DesignBackModule,
        DesignFileUploaderModule,
        DesignPreloaderModule,
        ReactiveFormsModule,
    ]
})
export class SellerOfferModule {
}
