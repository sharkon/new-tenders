import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {SellerOfferActions, SellerOfferActionTypes} from './store/seller-offer.actions';
import {IFile} from '../../../interfaces/file.interface';
import {ActivatedRoute} from '@angular/router';
import {AppStore} from '../../../store/app.store';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Actions, ofType} from '@ngrx/effects';
import {NotifyService} from '../../../services/notify/notify.service';
import {TranslateService} from '../../../services/translate/translate.service';
import {filter, map} from 'rxjs/operators';
import {SellerOfferStore} from './store/seller-offer.store';
import {AppFormUtils} from '../../../utils/app-form.utils';
import {UploadFile} from 'ngx-uploader';
import {IOffer} from '../../../interfaces/offer.interface';

@Component({
    selector: 'app-seller-offer',
    templateUrl: './seller-offer.component.html',
    styleUrls: ['./seller-offer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SellerOfferComponent implements OnInit, OnDestroy {

    offerId: number;
    formGroup: FormGroup;

    toChat: boolean;

    canDeactivateComponent = true;
    serviceLoading: boolean;

    initFiles: IFile.IFileResponse[] = [];
    filesLoading: boolean;

    maxFilesLength = 10;
    maxFileSizeMB = 16;

    get filesFormArray(): FormArray {
        return this.formGroup ? this.formGroup.controls['files'] as FormArray : null;
    }

    set filesFormArray(formArray: FormArray) {
        this.formGroup.controls['files'] = formArray;
    }

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    constructor(private store$: Store<SellerOfferStore.IState>,
                private actions$: Actions,
                private route: ActivatedRoute,
                private cdr: ChangeDetectorRef,
                private fb: FormBuilder,
                private notifyService: NotifyService,
                private ts: TranslateService) {
    }

    ngOnInit() {

        this.toChat = this.route.snapshot.queryParams['chat'] || null;

        this.initForm();
        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new SellerOfferActions.DestroyState());
    }

    canDeactivate() {

        if (!this.canDeactivateComponent) {

            return this.notifyService.confirm({
                dialogTitle: this.ts.data.T.dialogs.outPageQuestion,
                dialogContent: this.ts.data.T.dialogs.haveUnSaveChanges,
            });
        }

        return true;
    }

    private initForm() {

        this.formGroup = this.fb.group({
            id: [null],
            tenderId: [null],
            contactCompanyName: [null, [Validators.required, Validators.maxLength(256)]],
            question: [null],
            contactName: [null, [Validators.required, Validators.maxLength(256)]],
            contactPhone: [null, [Validators.required, Validators.maxLength(30)]],
            contactPosition: [null, [Validators.required, Validators.maxLength(256)]],
            description: [null],
            files: this.fb.array([]),
        });

        this.subs = this.formGroup.valueChanges
            .pipe(
                filter(() => this.canDeactivateComponent),
            )
            .subscribe(value => {

                this.store$.dispatch(new SellerOfferActions.SetCanDeactivate(false));
            });
    }

    private initStore() {

        this.subs = this.store$.pipe(select(SellerOfferStore.Selects.self.offerId))
            .subscribe(offerId => {

                this.offerId = offerId;
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(SellerOfferStore.Selects.self.canDeactivate))
            .subscribe(canDeactivate => {

                this.canDeactivateComponent = canDeactivate;
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(AppStore.Selects.appBlock.locks))
            .subscribe(locks => {

                this.serviceLoading =
                    locks[SellerOfferStore.SERVICES_UID.crud]
                    || locks[SellerOfferStore.SERVICES_UID.publish]
                    || locks[SellerOfferStore.SERVICES_UID.chat];

                this.cdr.detectChanges();
            });

        this.subs = this.actions$
            .pipe(
                ofType(SellerOfferActionTypes.IMPORT_OFFER),
                map((action: SellerOfferActions.ImportOffer) => action.payload),
            )
            .subscribe(offer => {

                this.formGroup.reset({
                    emitEvent: false,
                });

                this.saveFilesToForm(offer ? offer.files : null);

                if (offer) {

                    this.formGroup.patchValue(offer, {
                        emitEvent: false,
                    });
                }

                this.initFiles = this.filesFormArray.getRawValue();
                this.cdr.detectChanges();
            });

    }

    private saveFilesToForm(files: IFile.IFileResponse[]) {

        AppFormUtils.clearFormArray(this.filesFormArray);

        if (files) {

            files.forEach((file) => {
                this.filesFormArray.push(this.fb.control(file));
            });
        }
    }

    onFilesUpdate(files: UploadFile[]): void {

        const formFiles = files ? files.map(f => <IFile.IFileResponse>(f.response || f)) : [];
        this.saveFilesToForm(formFiles);
    }

    onUpdateLoadStatus(loading: boolean): void {

        this.filesLoading = loading;
    }

    private getOfferValue(): IOffer.IModel {

        const form = this.formGroup.value;

        const offer: IOffer.IModel = {
            ...this.formGroup.value,
        };

        return offer;
    }

    save(): void {
        this.store$.dispatch(new SellerOfferActions.SaveOffer(this.getOfferValue()));
    }

    publish(): void {

        const confirm = this.notifyService.confirm({
            dialogTitle: this.toChat ? this.ts.data.T.offerDialogs.isPublishOfferToChat : this.ts.data.T.offerDialogs.isPublishOffer,
            dialogContent: this.ts.data.T.offerDialogs.afterPublishNoChanges,
        });

        if (confirm) {

            if (this.toChat) {

                this.store$.dispatch(new SellerOfferActions.SendOfferToChat(this.getOfferValue()));
            } else {

                this.store$.dispatch(new SellerOfferActions.PublishOffer(this.getOfferValue()));
            }

        }
    }

}
