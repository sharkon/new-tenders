import { AppStore } from '../../../../store/app.store';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import { SellerOfferReducer } from './seller-offer.reducer';

export namespace SellerOfferStore {

    export const FEATURE_UID = '[SellerOfferStore] FEATURE_UID';
    export const MODULE_UID = '[SellerOfferStore] MODULE_UID';

    export const SERVICES_UID = {
        crud: '[SellerOfferStore][SERVICES_UID] crud',
        chat: '[SellerOfferStore][SERVICES_UID] chat',
        publish: '[SellerOfferStore][SERVICES_UID] publish',
    };

    export const SERVICES_URL = {
        crud: (offerId: string | number): string => {
            const url = 'cabinet/seller/proposal';
            return offerId ? `${url}/${offerId}` : url;
        },
        chat: (offerId: string | number): string => {
            return `cabinet/seller/proposal/${offerId}/publish/chat/`;
        },
        publish: (offerId: string | number): string => {
            return `cabinet/seller/proposal/${offerId}/publish/offer/`;
        },
    };

    interface IStore {
        selfStore: SellerOfferReducer.IState;
    }

    export interface IState extends AppStore.IState {
        sellerOfferStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: SellerOfferReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = SellerOfferReducer.createStoreSelector(selfSelector);

    }
}
