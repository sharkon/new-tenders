import {createSelector, MemoizedSelector} from '@ngrx/store';
import {SellerOfferActions, SellerOfferActionTypes} from './seller-offer.actions';

export namespace SellerOfferReducer {

    export interface IState {
        offerId: number;
        canDeactivate: boolean;
    }

    const initialState: IState = {
        offerId: null,
        canDeactivate: null,
    };

    export interface ISelects {
        offerId: (state: IState) => number;
        canDeactivate: (state: IState) => boolean;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            offerId = createSelector(
                selector,
                ((state: IState) => state.offerId),
            );

            canDeactivate = createSelector(
                selector,
                ((state: IState) => state.canDeactivate),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = {...initialState}, action: SellerOfferActions.All): IState {

        switch (action.type) {

            case SellerOfferActionTypes.IMPORT_OFFER: {

                return {
                    ...state,
                    offerId: action.payload ? action.payload.id : null,
                };
            }

            case SellerOfferActionTypes.SET_CAN_DEACTIVATE: {

                return {
                    ...state,
                    canDeactivate: action.payload,
                };
            }

            case SellerOfferActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
