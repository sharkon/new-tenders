import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable} from 'rxjs';
import {Action, select, Store} from '@ngrx/store';
import {ROUTER_NAVIGATED, RouterNavigationAction} from '@ngrx/router-store';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';
import {delay, filter, map, mergeMap, tap, withLatestFrom} from 'rxjs/operators';
import {SellerOfferStore} from './seller-offer.store';
import {SellerOfferActions, SellerOfferActionTypes} from './seller-offer.actions';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../services/crud/store/crud-service.actions';
import {IOffer} from '../../../../interfaces/offer.interface';
import {ICrud} from '../../../../interfaces/crud.interface';
import {AppRouterActions} from '../../../../store/router/app-router.actions';
import {AppConst} from '../../../../consts/app.const';
import {TranslateService} from '../../../../services/translate/translate.service';
import {NotifyService} from '../../../../services/notify/notify.service';
import {SellerConst} from '../../consts/seller.const';
import {IError} from '../../../../interfaces/error.interface';
import {AppErrorsConst} from '../../../../consts/app-errors.const';
import {AppStore} from '../../../../store/app.store';

@Injectable()
export class SellerOfferEffects {

    get offerMapMessage(): IError.IBackendTranslateError {

        const mapMessage: IError.IBackendTranslateError = {};

        Object.keys(mapMessage).forEach(key => {
            mapMessage[key][AppErrorsConst.BACKEND_ERROR_TYPES.INVALID_VALUE] = this.ts.data.T.formErrors.invalidValue;
            mapMessage[key][AppErrorsConst.BACKEND_ERROR_TYPES.IS_NULL] = this.ts.data.T.formErrors.invalidValue;
        });

        return mapMessage;
    }

    @Effect()
    onRoute$: Observable<Action> =
        this.actions$.pipe(
            ofType(ROUTER_NAVIGATED),
            map((action: RouterNavigationAction<RouterStateUrl>) => action.payload),
            filter(payload => {
                return payload.routerState.data.module === SellerOfferStore.MODULE_UID;
            }),
            withLatestFrom(
                this.store$.pipe(select(AppStore.Selects.appAuth.user)),
            ),
            delay(0),
            map(([payload, user]) => {

                const offerId: string = payload.routerState.params['offerId'];
                const tenderId: string = payload.routerState.params['tenderId'];

                if (offerId === AppConst.CRUD_NEW_ID) {

                    return new SellerOfferActions.ImportOffer({
                        id: null,
                        contactCompanyName: user && user.extraData ? user.extraData.companyName : null,
                        contactName: user && user.extraData ? user.data.fullName : null,
                        contactPhone: user && user.extraData ? user.extraData.phone : null,
                        contactPosition: user && user.extraData ? user.extraData.position : null,
                        description: null,
                        files: [],
                        question: null,
                        state: IOffer.STATE_TYPES.DRAFT,
                        tenderId: +tenderId,
                    });

                } else {

                    return new CrudServiceActions.GetItem({
                        uid: SellerOfferStore.SERVICES_UID.crud,
                        url: SellerOfferStore.SERVICES_URL.crud(offerId),
                    });
                }
            }),
        );

    @Effect()
    importOffer$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
            map((action: CrudServiceActions.GetItemSuccess) => action.payload),
            filter(payload => {
                return payload.uid === SellerOfferStore.SERVICES_UID.crud;
            }),
            filter(payload => {
                const offer = <IOffer.IModel>payload.item;
                return offer.state === IOffer.STATE_TYPES.DRAFT;
            }),
            map(payload => {

                return new SellerOfferActions.ImportOffer(<IOffer.IModel>payload.item);
            }),
        );

    @Effect()
    importOfferError$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM_FAILED),
            map((action: CrudServiceActions.GetItemFailed) => action.payload),
            filter(payload => {
                return payload.uid === SellerOfferStore.SERVICES_UID.crud;
            }),
            map(payload => {

                return new SellerOfferActions.SetCanDeactivate(true);
            }),
        );

    @Effect()
    importOfferLocked$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
            map((action: CrudServiceActions.GetItemSuccess) => action.payload),
            filter(payload => {
                return payload.uid === SellerOfferStore.SERVICES_UID.crud;
            }),
            filter(payload => {
                const offer = <IOffer.IModel>payload.item;
                return offer.state !== IOffer.STATE_TYPES.DRAFT;
            }),
            mergeMap(payload => {

                this.notifyService.warning({
                    dialogContent: this.ts.data.T.offerDialogs.noEditPublish,
                });

                return [
                    new SellerOfferActions.SetCanDeactivate(true),
                    new AppRouterActions.Go({
                        path: [SellerConst.ROOT_URL],
                        extras: {
                            replaceUrl: true,
                        }
                    }),
                ];
            }),
        );

    @Effect()
    saveOffer$: Observable<Action> =
        this.actions$.pipe(
            ofType(SellerOfferActionTypes.SAVE_OFFER),
            map((action: SellerOfferActions.SaveOffer) => action.payload),
            map(offer => {

                const request: ICrud.ICRUDCreateRequest = {
                    url: SellerOfferStore.SERVICES_URL.crud(offer.id),
                    uid: SellerOfferStore.SERVICES_UID.crud,
                    item: offer,
                    mapMessage: this.offerMapMessage,
                    deniedNotifySuccess: true,
                };

                if (offer.id) {
                    return new CrudServiceActions.Update(request);
                } else {
                    return new CrudServiceActions.Create(request);
                }
            }),
        );

    @Effect()
    saveOfferSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CREATE_SUCCESS, CrudServiceActionTypes.UPDATE_SUCCESS),
            map((action: CrudServiceActions.CreateSuccess | CrudServiceActions.UpdateSuccess) => action.payload),
            filter(payload => payload.uid === SellerOfferStore.SERVICES_UID.crud),
            mergeMap(payload => {

                this.notifyService.success({
                    dialogTitle: this.ts.data.T.offerDialogs.successSaveTitle,
                    dialogContent: this.ts.data.T.offerDialogs.successSaveContent,
                });

                return [
                    new SellerOfferActions.SetCanDeactivate(true),
                    new AppRouterActions.Go({
                        path: [`/seller/offer/${(<IOffer.IModel>payload.item).tenderId}/${payload.item.id}`],
                        extras: {
                            replaceUrl: true,
                            queryParamsHandling: 'preserve',
                        }
                    })
                ];
            })
        );

    @Effect({dispatch: false})
    saveOfferFailed$ =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CREATE_FAILED, CrudServiceActionTypes.UPDATE_FAILED),
            map((action: CrudServiceActions.CreateFailed | CrudServiceActions.UpdateFailed) => action.payload),
            filter(payload => payload.uid === SellerOfferStore.SERVICES_UID.crud),
            tap(payload => {

                this.notifyService.error({
                    dialogTitle: this.ts.data.T.dialogs.repeatAfterError,
                    dialogContent: this.ts.data.T.offerDialogs.notSaved,
                });
            })
        );

    @Effect()
    publishOffer$: Observable<Action> =
        this.actions$.pipe(
            ofType(SellerOfferActionTypes.PUBLISH_OFFER),
            map((action: SellerOfferActions.PublishOffer) => action.payload),
            map(offer => {

                const publishOffer: IOffer.IModel = {
                    ...offer,
                    state: IOffer.STATE_TYPES.PROPOSAL_PUBLISH,
                };

                const request: ICrud.ICRUDCreateRequest = {
                    url: SellerOfferStore.SERVICES_URL.publish(offer.id),
                    uid: SellerOfferStore.SERVICES_UID.publish,
                    item: publishOffer,
                    mapMessage: this.offerMapMessage,
                    deniedNotifySuccess: true,
                };

                return new CrudServiceActions.Update(request);
            })
        );

    @Effect()
    publishOfferSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.UPDATE_SUCCESS),
            map((action: CrudServiceActions.UpdateSuccess) => action.payload),
            filter(payload => payload.uid === SellerOfferStore.SERVICES_UID.publish),
            mergeMap(payload => {

                this.notifyService.success({
                    dialogTitle: this.ts.data.T.offerDialogs.successPublished,
                });

                return [
                    new SellerOfferActions.SetCanDeactivate(true),
                    new AppRouterActions.Go({
                        path: [SellerConst.ROOT_URL],
                        extras: {
                            replaceUrl: true,
                        }
                    }),
                ];
            })
        );

    @Effect({dispatch: false})
    publishOfferFailed$ =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.UPDATE_FAILED),
            map((action: CrudServiceActions.UpdateFailed) => action.payload),
            filter(payload => payload.uid === SellerOfferStore.SERVICES_UID.publish),
            tap(payload => {

                this.notifyService.error({
                    dialogTitle: this.ts.data.T.dialogs.repeatAfterError,
                    dialogContent: this.ts.data.T.offerDialogs.notPublished,
                });
            }),
        );

    @Effect()
    sendOfferToChat$: Observable<Action> =
        this.actions$.pipe(
            ofType(SellerOfferActionTypes.SEND_OFFER_TO_CHAT),
            map((action: SellerOfferActions.SendOfferToChat) => action.payload),
            map(offer => {

                const publishOffer: IOffer.IModel = {
                    ...offer,
                    state: IOffer.STATE_TYPES.CHAT_PUBLISH,
                };

                const request: ICrud.ICRUDCreateRequest = {
                    url: SellerOfferStore.SERVICES_URL.chat(offer.id),
                    uid: SellerOfferStore.SERVICES_UID.chat,
                    item: publishOffer,
                    mapMessage: this.offerMapMessage,
                    deniedNotifySuccess: true,
                };

                return new CrudServiceActions.Update(request);
            })
        );

    @Effect()
    sendOfferToChatSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.UPDATE_SUCCESS),
            map((action: CrudServiceActions.UpdateSuccess) => action.payload),
            filter(payload => payload.uid === SellerOfferStore.SERVICES_UID.chat),
            mergeMap(payload => {

                this.notifyService.success({
                    dialogTitle: this.ts.data.T.offerDialogs.successPublishedToChat,
                });

                return [
                    new SellerOfferActions.SetCanDeactivate(true),
                    new AppRouterActions.Go({
                        path: [SellerConst.ROOT_URL],
                        extras: {
                            replaceUrl: true,
                        }
                    }),
                ];
            })
        );

    @Effect({dispatch: false})
    sendOfferToChatFailed$ =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.UPDATE_FAILED),
            map((action: CrudServiceActions.UpdateFailed) => action.payload),
            filter(payload => payload.uid === SellerOfferStore.SERVICES_UID.chat),
            tap(payload => {

                this.notifyService.error({
                    dialogTitle: this.ts.data.T.dialogs.repeatAfterError,
                    dialogContent: this.ts.data.T.offerDialogs.notPublishedToChat,
                });
            }),
        );

    @Effect()
    destroyState$: Observable<Action> =
        this.actions$.pipe(
            ofType(SellerOfferActionTypes.DESTROY_STATE),
            mergeMap(() => {

                return [
                    new CrudServiceActions.CancelRequests([
                        SellerOfferStore.SERVICES_UID.crud,
                        SellerOfferStore.SERVICES_UID.publish,
                        SellerOfferStore.SERVICES_UID.chat,
                    ]),
                ];
            })
        );

    constructor(private actions$: Actions,
                private store$: Store<SellerOfferStore.IState>,
                private ts: TranslateService,
                private notifyService: NotifyService) {
    }
}
