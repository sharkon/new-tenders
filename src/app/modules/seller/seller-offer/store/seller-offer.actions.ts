import {Action} from '@ngrx/store';
import {IOffer} from '../../../../interfaces/offer.interface';

export namespace SellerOfferActionTypes {

    export const SET_CAN_DEACTIVATE = '[SellerOffer] SET_CAN_DEACTIVATE';

    export const IMPORT_OFFER = '[SellerOffer] IMPORT_OFFER';

    export const SAVE_OFFER = '[SellerOffer] SAVE_OFFER';
    export const SEND_OFFER_TO_CHAT = '[SellerOffer] SEND_OFFER_TO_CHAT';
    export const PUBLISH_OFFER = '[SellerOffer] PUBLISH_OFFER';

    export const DESTROY_STATE = '[SellerOffer] DESTROY_STATE';
}

export namespace SellerOfferActions {

    export class SetCanDeactivate implements Action {

        readonly type = SellerOfferActionTypes.SET_CAN_DEACTIVATE;

        constructor(public payload: boolean) {
        }
    }

    export class ImportOffer implements Action {

        readonly type = SellerOfferActionTypes.IMPORT_OFFER;

        constructor(public payload: IOffer.IModel) {
        }
    }

    export class SaveOffer implements Action {

        readonly type = SellerOfferActionTypes.SAVE_OFFER;

        constructor(public payload: IOffer.IModel) {
        }
    }

    export class SendOfferToChat implements Action {

        readonly type = SellerOfferActionTypes.SEND_OFFER_TO_CHAT;

        constructor(public payload: IOffer.IModel) {
        }
    }

    export class PublishOffer implements Action {

        readonly type = SellerOfferActionTypes.PUBLISH_OFFER

        constructor(public payload: IOffer.IModel) {
        }
    }

    export class DestroyState implements Action {

        readonly type = SellerOfferActionTypes.DESTROY_STATE;
    }

    export type All =
        SetCanDeactivate
        | ImportOffer
        | SaveOffer
        | SendOfferToChat
        | PublishOffer
        | DestroyState;

}
