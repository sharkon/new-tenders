import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable} from 'rxjs';
import {Action, select, Store} from '@ngrx/store';
import {filter, map, mergeMap, withLatestFrom} from 'rxjs/operators';
import {PaginationActions, PaginationActionTypes} from '../../../../reducers/pagination/pagination.actions';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../services/crud/store/crud-service.actions';
import {CrudActions} from '../../../../reducers/crud/crud.actions';
import {AppStore} from '../../../../store/app.store';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';
import {SellerOffersStore} from './seller-offers.store';
import {SellerOffersActionTypes} from './seller-offers.actions';

@Injectable()
export class SellerOffersEffects {

    @Effect()
    getList$: Observable<Action> =
        this.actions$.pipe(
            ofType(PaginationActionTypes.SET_REQUEST),
            withLatestFrom(
                this.store$.pipe(select(AppStore.Selects.AppRouter.getState)),
                this.store$.pipe(select(SellerOffersStore.Selects.pagination.request)),
            ),
            filter(([action, routerState]) => {
                return (<RouterStateUrl>routerState).data.module === SellerOffersStore.MODULE_UID;
            }),
            mergeMap(([action, routerState, paginationRequest]) => {

                return [
                    new CrudActions.DestroyState(SellerOffersStore.CRUD_STORE_UID),
                    new CrudServiceActions.GetList({
                        url: SellerOffersStore.getCrudUrl(<RouterStateUrl>routerState, paginationRequest),
                        uid: SellerOffersStore.SERVICES_UID.list,
                    }),
                ];
            })
        );

    @Effect()
    getListSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === SellerOffersStore.SERVICES_UID.list),
            mergeMap(payload => {

                return [
                    new CrudActions.ImportModels(SellerOffersStore.CRUD_STORE_UID, payload.data),
                    new PaginationActions.SetResponse(SellerOffersStore.PAGINATION_STORE_UID, payload.pageInfo),
                ];
            })
        );

    @Effect()
    cancelRequests$: Observable<Action> =
        this.actions$.pipe(
            ofType(SellerOffersActionTypes.DESTROY_STATE),
            mergeMap(() => {

                return [
                    new CrudServiceActions.CancelRequests([
                        SellerOffersStore.SERVICES_UID.list,
                    ])
                ];
            })
        );

    constructor(private actions$: Actions,
                private store$: Store<SellerOffersStore.IState>) {
    }
}
