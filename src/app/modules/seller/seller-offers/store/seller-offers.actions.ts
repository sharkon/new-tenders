import { Action } from '@ngrx/store';

export namespace SellerOffersActionTypes {

    export const DESTROY_STATE = '[SellerOffers] DESTROY_STATE';
}

export namespace SellerOffersActions {

    export class DestroyState implements Action {

        readonly type = SellerOffersActionTypes.DESTROY_STATE;
    }

    export type All =
        DestroyState;

}
