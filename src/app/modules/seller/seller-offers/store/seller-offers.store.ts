import {CrudStore} from '../../../../reducers/crud/crud.reducer';
import {PaginationStore} from '../../../../reducers/pagination/pagination.reducer';
import {AppStore} from '../../../../store/app.store';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';
import {IPagination} from '../../../../interfaces/pagination.interface';
import {Params} from '@angular/router';
import {PaginationUtils} from '../../../../utils/pagination-utils';
import {IOffer} from '../../../../interfaces/offer.interface';

export namespace SellerOffersStore {

    export const MODULE_UID = '[SellerOffersStore] MODULE_UID';
    export const FEATURE_UID = '[SellerOffersStore] FEATURE_UID';

    export const CRUD_STORE_UID = '[SellerOffersStore] CRUD_STORE_UID';

    export const PAGINATION_STORE_UID = '[SellerOffersStore] PAGINATION_STORE_UID';

    export const PAGE_LIMIT = 20;

    export const SERVICES_URL = {
        list: 'cabinet/seller/proposal',
    };

    export const SERVICES_UID = {
        list: '[SellerOffersStore][SERVICES_UID] list',
    };

    export interface IStore {
        crud: CrudStore.IState;
        pagination: PaginationStore.IState;
    }

    export interface IState extends AppStore.IState {
        sellerOffersStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        crud: CrudStore.createReducer(CRUD_STORE_UID),
        pagination: PaginationStore.createReducer(PAGINATION_STORE_UID),
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // crud

        const crudSelector = createSelector(
            featureSelector,
            (state: IStore) => state.crud,
        );

        export const crud = CrudStore.createStoreSelector(crudSelector);

        // pagination

        const paginationSelector = createSelector(
            featureSelector,
            (state: IStore) => state.pagination,
        );

        export const pagination = PaginationStore.createStoreSelector(paginationSelector);
    }

    export function getCrudUrl(routerState: RouterStateUrl, paginationRequest: IPagination.IPaginationOnlyRequest = {offset: 0, limit: PAGE_LIMIT}): string {

        const queryParams: Params = routerState
            ? routerState.queryParams : null;

        const status: IOffer.QueryStatusType = queryParams
            ? queryParams[IOffer.QUERY_STATUS_NAME] || IOffer.QUERY_STATUS.PROPOSAL_PUBLISH
            : IOffer.QUERY_STATUS.PROPOSAL_PUBLISH;

        const url = `${SERVICES_URL.list}?state=${status}`
            + `${PaginationUtils.GetPaginationUrlQueryString(paginationRequest)}`;

        return url;
    }

}
