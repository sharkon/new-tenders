import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {IOffer} from '../../../../../interfaces/offer.interface';
import {SellerConst} from '../../../consts/seller.const';

@Injectable()
export class SellerOffersStatusGuard implements CanActivate {

    constructor(private router: Router) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {


        const status: IOffer.QueryStatusType = state.root.queryParams && state.root.queryParams[IOffer.QUERY_STATUS_NAME];

        if (status && (Object.keys(IOffer.QUERY_STATUS).map(key => IOffer.QUERY_STATUS[key])).indexOf(status) < 0) {

            this.router.navigate([SellerConst.ROOT_URL], {replaceUrl: true});
            return false;
        }

        return true;
    }

}
