import { TestBed, async, inject } from '@angular/core/testing';

import { SellerOffersStatusGuard } from './seller-offers-status.guard';

describe('SellerOffersStatusGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SellerOffersStatusGuard]
    });
  });

  it('should ...', inject([SellerOffersStatusGuard], (guard: SellerOffersStatusGuard) => {
    expect(guard).toBeTruthy();
  }));
});
