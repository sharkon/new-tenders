import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {SellerOffersStore} from './store/seller-offers.store';
import {PaginationStore} from '../../../reducers/pagination/pagination.reducer';
import {select, Store} from '@ngrx/store';
import {SellerOffersActions} from './store/seller-offers.actions';
import {IOffer} from '../../../interfaces/offer.interface';
import {Params} from '@angular/router';
import {Actions} from '@ngrx/effects';
import {AppStore} from '../../../store/app.store';
import {RouterStateUrl} from '../../../providers/custom-router-state-serializer';

@Component({
    selector: 'app-seller-offers',
    templateUrl: './seller-offers.component.html',
    styleUrls: ['./seller-offers.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SellerOffersComponent implements OnInit, OnDestroy {

    models: IOffer.IListModel[];
    queryStatus: IOffer.QueryStatusType;

    queryParamsChat: Params = {
        [IOffer.QUERY_STATUS_NAME]: IOffer.QUERY_STATUS.CHAT_PUBLISH,
    };

    queryParamsDraft: Params = {
        [IOffer.QUERY_STATUS_NAME]: IOffer.QUERY_STATUS.DRAFT,
    };

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get QUERY_STATUS() {
        return IOffer.QUERY_STATUS;
    }

    get PAGINATION_STORE_UID(): string {
        return SellerOffersStore.PAGINATION_STORE_UID;
    }

    get PAGINATION_SELECT_FUNCTION(): PaginationStore.ISelects {
        return SellerOffersStore.Selects.pagination;
    }

    constructor(private store$: Store<SellerOffersStore.IState>,
                private actions$: Actions,
                private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new SellerOffersActions.DestroyState());
    }

    private initStore() {

        this.subs = this.store$.pipe(select(SellerOffersStore.Selects.crud.models))
            .subscribe((models: IOffer.IListModel[]) => {

                this.models = models;
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(AppStore.Selects.AppRouter.getState))
            .subscribe((routerState: RouterStateUrl) => {

                this.queryStatus = (routerState && routerState.queryParams) ? routerState.queryParams[IOffer.QUERY_STATUS_NAME] : null;
                this.cdr.detectChanges();
            });

    }

}
