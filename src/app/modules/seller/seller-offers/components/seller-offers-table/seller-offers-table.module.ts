import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SellerOffersTableComponent} from './seller-offers-table.component';
import {DesignPreloaderModule} from '../../../../design/design-preloader/design-preloader.module';
import {PipesModule} from '../../../../../pipes/pipes.module';
import {RouterModule} from '@angular/router';

@NgModule({
    declarations: [SellerOffersTableComponent],
    exports: [SellerOffersTableComponent],
    imports: [
        CommonModule,
        DesignPreloaderModule,
        PipesModule,
        RouterModule,
    ]
})
export class SellerOffersTableModule {
}
