import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerOffersTableComponent } from './seller-offers-table.component';

describe('SellerOffersTableComponent', () => {
  let component: SellerOffersTableComponent;
  let fixture: ComponentFixture<SellerOffersTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerOffersTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerOffersTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
