import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {IOffer} from '../../../../../interfaces/offer.interface';
import {AppConst} from '../../../../../consts/app.const';
import {ITender} from '../../../../../interfaces/tender.interface';

@Component({
    selector: 'app-seller-offers-table',
    templateUrl: './seller-offers-table.component.html',
    styleUrls: ['./seller-offers-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SellerOffersTableComponent implements OnInit {

    @Input() models: IOffer.IListModel[];

    get TENDER_STATE() {
        return ITender.STATE_TYPES;
    }

    get OFFER_STATE() {
        return IOffer.STATE_TYPES;
    }

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    constructor() {
    }

    ngOnInit() {
    }

}
