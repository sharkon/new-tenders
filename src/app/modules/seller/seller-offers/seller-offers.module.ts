import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SellerOffersComponent} from './seller-offers.component';
import {RouterModule} from '@angular/router';
import {StoreModule} from '@ngrx/store';
import {SellerOffersStore} from './store/seller-offers.store';
import {EffectsModule} from '@ngrx/effects';
import {PaginationEffects} from '../../../reducers/pagination/pagination.effects';
import {CurdEffects} from '../../../reducers/crud/crud.effects';
import {SellerOffersEffects} from './store/seller-offers.effects';
import {PaginationStoreConfig} from '../../../reducers/pagination/pagination.store.config';
import {CrudStoreConfig} from '../../../reducers/crud/crud.store.config';
import {PipesModule} from '../../../pipes/pipes.module';
import {DesignPaginationModule} from '../../design/design-pagination/design-pagination.module';
import {SellerOffersStatusGuard} from './guards/seller-offers-status/seller-offers-status.guard';
import {SellerOffersTableModule} from './components/seller-offers-table/seller-offers-table.module';

@NgModule({
    declarations: [SellerOffersComponent],
    imports: [
        CommonModule,
        PipesModule,
        DesignPaginationModule,
        RouterModule.forChild([
            {
                path: '',
                component: SellerOffersComponent,
                data: {
                    module: SellerOffersStore.MODULE_UID
                },
                canActivate: [SellerOffersStatusGuard],
            }
        ]),
        StoreModule.forFeature(SellerOffersStore.FEATURE_UID, SellerOffersStore.mapReducers),
        EffectsModule.forFeature([
            PaginationEffects,
            CurdEffects,
            SellerOffersEffects,
        ]),
        SellerOffersTableModule,
    ],
    providers: [
        SellerOffersStatusGuard,
        {
            provide: PaginationStoreConfig,
            useValue: <PaginationStoreConfig>{
                storeUid: SellerOffersStore.PAGINATION_STORE_UID,
                moduleUid: SellerOffersStore.MODULE_UID,
                config: {
                    defaultRequest: {
                        offset: 0,
                        limit: SellerOffersStore.PAGE_LIMIT,
                    },
                    limitValues: [SellerOffersStore.PAGE_LIMIT],
                    seeMoreOption: false,
                    perPageOption: false,
                },
            }
        },
        {
            provide: CrudStoreConfig,
            useValue: <CrudStoreConfig>{
                storeUid: SellerOffersStore.CRUD_STORE_UID,
                config: {
                    crudUrl: SellerOffersStore.SERVICES_URL.list,
                    serviceUid: SellerOffersStore.SERVICES_UID.list,
                }
            }
        }
    ]
})
export class SellerOffersModule {
}
