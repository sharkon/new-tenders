import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SellerRootComponent } from './seller-root.component';
import { RouterModule } from '@angular/router';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [SellerRootComponent],
    imports: [
        CommonModule,
        PipesModule,
        RouterModule.forChild([
            {
                path: '',
                component: SellerRootComponent,
                children: [
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'tenders',
                    },
                    {
                        path: 'tenders',
                        loadChildren: '../seller-tenders/seller-tenders.module#SellerTendersModule',
                    },
                    {
                        path: 'tender/:tenderId',
                        loadChildren: '../seller-tender/seller-tender.module#SellerTenderModule',
                    },
                    {
                        path: 'offers',
                        loadChildren: '../seller-offers/seller-offers.module#SellerOffersModule',
                    },
                    {
                        path: 'offer/:tenderId/:offerId',
                        loadChildren: '../seller-offer/seller-offer.module#SellerOfferModule',
                    },
                    {
                        path: 'chat/:tenderId',
                        loadChildren: '../../chat/chat.module#ChatModule',
                    },
                ]
            }
        ]),
    ]
})
export class SellerRootModule { }
