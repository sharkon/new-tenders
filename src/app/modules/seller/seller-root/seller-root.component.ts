import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {SellerConst} from '../consts/seller.const';

@Component({
    selector: 'app-seller-oot',
    templateUrl: './seller-root.component.html',
    styleUrls: ['./seller-root.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SellerRootComponent implements OnInit {

    get SellerConst() {
        return SellerConst;
    }

    constructor() {
    }

    ngOnInit() {
    }

}
