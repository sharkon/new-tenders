import {Component, OnDestroy, OnInit} from '@angular/core';
import {ITender} from '../../../interfaces/tender.interface';
import {Subscription} from 'rxjs';
import {PaginationStore} from '../../../reducers/pagination/pagination.reducer';
import {SellerTendersStore} from './store/seller-tenders.store';
import {select, Store} from '@ngrx/store';
import {SellerTendersActions} from './store/seller-tenders.actions';

@Component({
    selector: 'app-seller-tenders',
    templateUrl: './seller-tenders.component.html',
    styleUrls: ['./seller-tenders.component.scss']
})
export class SellerTendersComponent implements OnInit, OnDestroy {

    models: ITender.IListModelForSeller[];

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get PAGINATION_STORE_UID(): string {
        return SellerTendersStore.PAGINATION_STORE_UID;
    }

    get PAGINATION_SELECT_FUNCTION(): PaginationStore.ISelects {
        return SellerTendersStore.Selects.pagination;
    }

    constructor(private store$: Store<SellerTendersStore.IState>) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new SellerTendersActions.DestroyState());
    }

    private initStore() {

        this.subs = this.store$
            .pipe(
                select(SellerTendersStore.Selects.crud.models)
            )
            .subscribe((models: ITender.IListModelForSeller[]) => {

                this.models = models;
            });

    }

}
