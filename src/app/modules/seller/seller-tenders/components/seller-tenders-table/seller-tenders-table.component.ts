import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ITender} from '../../../../../interfaces/tender.interface';
import {AppConst} from '../../../../../consts/app.const';

@Component({
    selector: 'app-seller-tenders-table',
    templateUrl: './seller-tenders-table.component.html',
    styleUrls: ['./seller-tenders-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SellerTendersTableComponent implements OnInit {

    @Input() models: ITender.IListModelForSeller[];

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    constructor() {
    }

    ngOnInit() {
    }

}
