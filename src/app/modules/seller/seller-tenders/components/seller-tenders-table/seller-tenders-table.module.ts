import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SellerTendersTableComponent } from './seller-tenders-table.component';
import { PipesModule } from '../../../../../pipes/pipes.module';
import { RouterModule } from '@angular/router';
import {DesignPreloaderModule} from '../../../../design/design-preloader/design-preloader.module';

@NgModule({
    declarations: [SellerTendersTableComponent],
    imports: [
        CommonModule,
        PipesModule,
        RouterModule,
        DesignPreloaderModule,
    ],
    exports: [SellerTendersTableComponent]
})
export class SellerTendersTableModule { }
