import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SellerTendersComponent } from './seller-tenders.component';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { SellerTendersStore } from './store/seller-tenders.store';
import { EffectsModule } from '@ngrx/effects';
import { PaginationEffects } from '../../../reducers/pagination/pagination.effects';
import { CurdEffects } from '../../../reducers/crud/crud.effects';
import { SellerTendersEffects } from './store/seller-tenders.effects';
import { PaginationStoreConfig } from '../../../reducers/pagination/pagination.store.config';
import { CrudStoreConfig } from '../../../reducers/crud/crud.store.config';
import { PipesModule } from '../../../pipes/pipes.module';
import { DesignPaginationModule } from '../../design/design-pagination/design-pagination.module';
import { SellerTendersTableModule } from './components/seller-tenders-table/seller-tenders-table.module';

@NgModule({
    declarations: [SellerTendersComponent],
    imports: [
        CommonModule,
        PipesModule,
        DesignPaginationModule,
        SellerTendersTableModule,
        RouterModule.forChild([
            {
                path: '',
                component: SellerTendersComponent,
                data: {
                    module: SellerTendersStore.MODULE_UID,
                }
            }
        ]),
        StoreModule.forFeature(SellerTendersStore.FEATURE_UID, SellerTendersStore.mapReducers),
        EffectsModule.forFeature([
            PaginationEffects,
            CurdEffects,
            SellerTendersEffects,
        ]),
    ],
    providers: [
        {
            provide: PaginationStoreConfig,
            useValue: <PaginationStoreConfig>{
                storeUid: SellerTendersStore.PAGINATION_STORE_UID,
                moduleUid: SellerTendersStore.MODULE_UID,
                config: {
                    defaultRequest: {
                        offset: 0,
                        limit: SellerTendersStore.PAGE_LIMIT,
                    },
                    limitValues: [SellerTendersStore.PAGE_LIMIT],
                    seeMoreOption: false,
                    perPageOption: false,
                },
            }
        },
        {
            provide: CrudStoreConfig,
            useValue: <CrudStoreConfig>{
                storeUid: SellerTendersStore.CRUD_STORE_UID,
                config: {
                    crudUrl: SellerTendersStore.SERVICES_URL.list,
                    serviceUid: SellerTendersStore.SERVICES_UID.list,
                }
            }
        }
    ]
})
export class SellerTendersModule { }
