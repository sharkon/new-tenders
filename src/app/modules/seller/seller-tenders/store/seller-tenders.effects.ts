import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable} from 'rxjs';
import {Action, select, Store} from '@ngrx/store';
import {filter, map, mergeMap, withLatestFrom} from 'rxjs/operators';
import {PaginationActions, PaginationActionTypes} from '../../../../reducers/pagination/pagination.actions';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../services/crud/store/crud-service.actions';
import {CrudActions} from '../../../../reducers/crud/crud.actions';
import {AppStore} from '../../../../store/app.store';
import {SellerTendersStore} from './seller-tenders.store';
import {SellerTendersActionTypes} from './seller-tenders.actions';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';
import {PaginationUtils} from '../../../../utils/pagination-utils';

@Injectable()
export class SellerTendersEffects {

    @Effect()
    getList$: Observable<Action> =
        this.actions$.pipe(
            ofType(PaginationActionTypes.SET_REQUEST),
            withLatestFrom(
                this.store$.pipe(select(AppStore.Selects.AppRouter.getState)),
                this.store$.pipe(select(SellerTendersStore.Selects.pagination.request)),
            ),
            filter(([action, routerState, paginationRequest]) => {
                return (<RouterStateUrl>routerState).data.module === SellerTendersStore.MODULE_UID;
            }),
            map(([action, routerState, paginationRequest]) => {

                const url = `${SellerTendersStore.SERVICES_URL.list}?`
                    + `${PaginationUtils.GetPaginationUrlQueryString(paginationRequest)}`;

                return new CrudServiceActions.GetList({
                    url,
                    uid: SellerTendersStore.SERVICES_UID.list,
                });
            })
        );

    @Effect()
    getListSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === SellerTendersStore.SERVICES_UID.list),
            mergeMap(payload => {

                return [
                    new CrudActions.ImportModels(SellerTendersStore.CRUD_STORE_UID, payload.data),
                    new PaginationActions.SetResponse(SellerTendersStore.PAGINATION_STORE_UID, payload.pageInfo),
                ];
            })
        );

    @Effect()
    cancelRequests$: Observable<Action> =
        this.actions$.pipe(
            ofType(SellerTendersActionTypes.DESTROY_STATE),
            mergeMap(action => {

                return [
                    new CrudServiceActions.CancelRequests([
                        SellerTendersStore.SERVICES_UID.list,
                    ]),
                ];
            })
        );

    constructor(private actions$: Actions,
                private store$: Store<SellerTendersStore.IState>) {
    }
}
