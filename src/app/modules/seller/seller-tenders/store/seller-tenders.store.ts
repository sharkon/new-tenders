import { CrudStore } from '../../../../reducers/crud/crud.reducer';
import { PaginationStore } from '../../../../reducers/pagination/pagination.reducer';
import { AppStore } from '../../../../store/app.store';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

export namespace SellerTendersStore {

    export const MODULE_UID = '[SellerTendersStore] MODULE_UID';
    export const FEATURE_UID = '[SellerTendersStore] FEATURE_UID';

    export const CRUD_STORE_UID = '[SellerTendersStore] CRUD_STORE_UID';

    export const PAGINATION_STORE_UID = '[SellerTendersStore] PAGINATION_STORE_UID';

    export const PAGE_LIMIT = 20;

    export const SERVICES_URL = {
        list: 'cabinet/seller/tender',
    };

    export const SERVICES_UID = {
        list: '[SellerTendersStore][SERVICES_UID] list',
    };

    export interface IStore {
        crud: CrudStore.IState;
        pagination: PaginationStore.IState;
    }

    export interface IState extends AppStore.IState {
        sellerTendersStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        crud: CrudStore.createReducer(CRUD_STORE_UID),
        pagination: PaginationStore.createReducer(PAGINATION_STORE_UID),
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // crud

        const crudSelector = createSelector(
            featureSelector,
            (state: IStore) => state.crud,
        );

        export const crud = CrudStore.createStoreSelector(crudSelector);

        // pagination

        const paginationSelector = createSelector(
            featureSelector,
            (state: IStore) => state.pagination,
        );

        export const pagination = PaginationStore.createStoreSelector(paginationSelector);
    }

}
