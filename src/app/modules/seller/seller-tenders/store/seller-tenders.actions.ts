import { Action } from '@ngrx/store';

export namespace SellerTendersActionTypes {

    export const DESTROY_STATE = '[SellerTenders] DESTROY_STATE';
}

export namespace SellerTendersActions {

    export class DestroyState implements Action {

        readonly type = SellerTendersActionTypes.DESTROY_STATE;
    }

    export type All =
        DestroyState;

}
