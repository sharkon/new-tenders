import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SellerTenderComponent} from './seller-tender.component';
import {RouterModule} from '@angular/router';
import {PipesModule} from '../../../pipes/pipes.module';
import {DesignPreloaderModule} from '../../design/design-preloader/design-preloader.module';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {SellerTenderStore} from './store/seller-tender.store';
import {SellerTenderEffects} from './store/seller-tender.effects';
import {SellerTenderViewModule} from './components/seller-tender-view/seller-tender-view.module';
import {SellerTenderOffersModule} from './components/seller-tender-offers/seller-tender-offers.module';
import {CrudStoreConfig} from '../../../reducers/crud/crud.store.config';
import {DesignBackModule} from '../../design/design-back/design-back.module';

@NgModule({
    declarations: [SellerTenderComponent],
    imports: [
        CommonModule,
        PipesModule,
        DesignPreloaderModule,
        RouterModule.forChild([
            {
                path: '',
                component: SellerTenderComponent,
                data: {
                    module: SellerTenderStore.MODULE_UID,
                }
            }
        ]),
        StoreModule.forFeature(SellerTenderStore.FEATURE_UID, SellerTenderStore.mapReducers),
        EffectsModule.forFeature([
            SellerTenderEffects,
        ]),
        SellerTenderViewModule,
        SellerTenderOffersModule,
        DesignBackModule,
    ],
    providers: [
        {
            provide: CrudStoreConfig,
            useValue: <CrudStoreConfig>{
                storeUid: SellerTenderStore.CRUD_STORE_UID,
                config: {
                    crudUrl: '',
                    serviceUid: SellerTenderStore.SERVICES_UID.list,
                }
            }
        }
    ]
})
export class SellerTenderModule {
}
