import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {SellerTenderStore} from './store/seller-tender.store';
import {Observable, Subscription} from 'rxjs';
import {ITender} from '../../../interfaces/tender.interface';
import {IOffer} from '../../../interfaces/offer.interface';
import {SellerTenderActions} from './store/seller-tender.actions';
import {SellerConst} from '../consts/seller.const';
import {Params} from '@angular/router';

@Component({
    selector: 'app-seller-tender',
    templateUrl: './seller-tender.component.html',
    styleUrls: ['./seller-tender.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SellerTenderComponent implements OnInit, OnDestroy {

    tender: ITender.IModelForSeller;
    offers$: Observable<IOffer.IListModelForTender[]>;

    queryParamsChat: Params = {chat: 1};

    get SellerConst() {
        return SellerConst;
    }

    get TENDER_STATES() {
        return ITender.STATE_TYPES;
    }

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    constructor(private store$: Store<SellerTenderStore.IState>,
                private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new SellerTenderActions.DestroyState());
    }

    private initStore() {

        this.subs = this.store$.pipe(select(SellerTenderStore.Selects.self.tender))
            .subscribe(tender => {
                this.tender = tender;
                this.cdr.detectChanges();
            });
        this.offers$ = this.store$.pipe(select(SellerTenderStore.Selects.crud.models)) as Observable<IOffer.IListModelForTender[]>;
    }

}
