import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerTenderOffersComponent } from './seller-tender-offers.component';

describe('SellerTenderOffersComponent', () => {
  let component: SellerTenderOffersComponent;
  let fixture: ComponentFixture<SellerTenderOffersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerTenderOffersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerTenderOffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
