import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {IOffer} from '../../../../../interfaces/offer.interface';
import {AppConst} from '../../../../../consts/app.const';
import {ITender} from '../../../../../interfaces/tender.interface';

@Component({
    selector: 'app-seller-tender-offers',
    templateUrl: './seller-tender-offers.component.html',
    styleUrls: ['./seller-tender-offers.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SellerTenderOffersComponent implements OnInit {

    @Input() tender: ITender.IModelForSeller;
    @Input() offers: IOffer.IListModelForTender[];

    get TENDER_STATES() {
        return ITender.STATE_TYPES;
    }

    get OFFER_STATE() {
        return IOffer.STATE_TYPES;
    }

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    constructor() {
    }

    ngOnInit() {
    }

}

