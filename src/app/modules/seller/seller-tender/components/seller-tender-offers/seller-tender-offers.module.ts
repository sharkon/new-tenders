import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SellerTenderOffersComponent} from './seller-tender-offers.component';
import {PipesModule} from '../../../../../pipes/pipes.module';
import {RouterModule} from '@angular/router';

@NgModule({
    declarations: [SellerTenderOffersComponent],
    exports: [SellerTenderOffersComponent],
    imports: [
        CommonModule,
        PipesModule,
        RouterModule,
    ]
})
export class SellerTenderOffersModule {
}
