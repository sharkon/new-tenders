import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SellerTenderViewComponent} from './seller-tender-view.component';
import {PipesModule} from '../../../../../pipes/pipes.module';
import {DesignPreloaderModule} from '../../../../design/design-preloader/design-preloader.module';
import {DesignFileLinkModule} from '../../../../design/design-file-link/design-file-link.module';
import {RouterModule} from '@angular/router';

@NgModule({
    declarations: [SellerTenderViewComponent],
    exports: [SellerTenderViewComponent],
    imports: [
        CommonModule,
        PipesModule,
        DesignPreloaderModule,
        DesignFileLinkModule,
        RouterModule,
    ]
})
export class SellerTenderViewModule {
}
