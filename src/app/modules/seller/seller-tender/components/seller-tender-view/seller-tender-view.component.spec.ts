import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerTenderViewComponent } from './seller-tender-view.component';

describe('SellerTenderViewComponent', () => {
  let component: SellerTenderViewComponent;
  let fixture: ComponentFixture<SellerTenderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerTenderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerTenderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
