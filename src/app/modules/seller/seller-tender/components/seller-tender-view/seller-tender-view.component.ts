import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ITender} from '../../../../../interfaces/tender.interface';
import {AppConst} from '../../../../../consts/app.const';

@Component({
    selector: 'app-seller-tender-view',
    templateUrl: './seller-tender-view.component.html',
    styleUrls: ['./seller-tender-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SellerTenderViewComponent implements OnInit {

    @Input() tender: ITender.IModelForSeller;
    @Input() offersCount: number;

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    get TENDER_STATES() {
        return ITender.STATE_TYPES;
    }

    constructor() {
    }

    ngOnInit() {
    }

}
