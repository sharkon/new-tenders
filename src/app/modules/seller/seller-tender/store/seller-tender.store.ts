import {SellerTenderReducer} from './seller-tender.reducer';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {CrudStore} from '../../../../reducers/crud/crud.reducer';
import {AppStore} from '../../../../store/app.store';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';

export namespace SellerTenderStore {

    export const FEATURE_UID = '[SellerTenderStore] FEATURE_UID';
    export const MODULE_UID = '[SellerTenderStore] MODULE_UID';

    export const CRUD_STORE_UID = '[SellerTenderStore] CRUD_STORE_UID';

    export const SERVICES_URL = {
        tender: (id: string | number): string => {
            return `cabinet/seller/tender/${id}`;
        },
        list: (id: string | number): string => {
            return `cabinet/seller/tender/${id}/proposal`;
        },
    };

    export const SERVICES_UID = {
        tender: '[SellerTenderStore][SERVICES_UID] tender',
        list: '[SellerTenderStore][SERVICES_UID] list',
    };

    interface IStore {
        selfStore: SellerTenderReducer.IState;
        crud: CrudStore.IState;

    }

    export interface IState extends AppStore.IState {
        sellerTenderStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: SellerTenderReducer.reducer,
        crud: CrudStore.createReducer(CRUD_STORE_UID),
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = SellerTenderReducer.createStoreSelector(selfSelector);

        // crud

        const crudSelector = createSelector(
            featureSelector,
            (state: IStore) => state.crud,
        );

        export const crud = CrudStore.createStoreSelector(crudSelector);

    }

    export function getCrudUrl(routerState: RouterStateUrl): string {

        const tenderId: string = routerState.params['tenderId'];
        const url = `${SERVICES_URL.list(tenderId)}`;

        return url;
    }

}
