import {createSelector, MemoizedSelector} from '@ngrx/store';
import {SellerTenderActions, SellerTenderActionTypes} from './seller-tender.actions';
import {ITender} from '../../../../interfaces/tender.interface';

export namespace SellerTenderReducer {

    export interface IState {
        tender: ITender.IModelForSeller;
    }

    const initialState: IState = {
        tender: null,
    };

    export interface ISelects {
        tender: (state: IState) => ITender.IModelForSeller;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            tender = createSelector(
                selector,
                ((state: IState) => state.tender),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = {...initialState}, action: SellerTenderActions.All): IState {

        switch (action.type) {

            case SellerTenderActionTypes.SET_TENDER: {

                const tender: ITender.IModelForSeller = action.payload ? {...action.payload} : null;

                return {
                    ...state,
                    tender,
                };
            }

            case SellerTenderActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
