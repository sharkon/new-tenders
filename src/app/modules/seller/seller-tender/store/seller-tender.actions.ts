import {Action} from '@ngrx/store';
import {ITender} from '../../../../interfaces/tender.interface';

export namespace SellerTenderActionTypes {

    export const SET_TENDER = '[SellerTender] SET_TENDER';
    export const DESTROY_STATE = '[SellerTender] DESTROY_STATE';
}

export namespace SellerTenderActions {

    export class SetTender implements Action {

        readonly type = SellerTenderActionTypes.SET_TENDER;

        constructor(public payload: ITender.IModelForSeller) {
        }
    }

    export class DestroyState implements Action {

        readonly type = SellerTenderActionTypes.DESTROY_STATE;
    }

    export type All =
        DestroyState
        | SetTender;

}
