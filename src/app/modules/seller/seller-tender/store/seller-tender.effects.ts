import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs';
import {filter, map, mergeMap} from 'rxjs/operators';
import {ROUTER_NAVIGATED, RouterNavigationAction} from '@ngrx/router-store';
import {SellerTenderActions, SellerTenderActionTypes} from './seller-tender.actions';
import {SellerTenderStore} from './seller-tender.store';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../services/crud/store/crud-service.actions';
import {CrudActions} from '../../../../reducers/crud/crud.actions';
import {ITender} from '../../../../interfaces/tender.interface';

@Injectable()
export class SellerTenderEffects {

    @Effect()
    onRoute$: Observable<Action> =
        this.actions$.pipe(
            ofType(ROUTER_NAVIGATED),
            map((action: RouterNavigationAction<RouterStateUrl>) => action.payload),
            filter(payload => {
                return payload.routerState.data.module === SellerTenderStore.MODULE_UID;
            }),
            mergeMap((payload) => {

                const tenderId: string = payload.routerState.params['tenderId'];

                return [
                    new CrudServiceActions.GetItem({
                        url: `${SellerTenderStore.SERVICES_URL.tender(tenderId)}/`,
                        uid: SellerTenderStore.SERVICES_UID.tender,
                    }),
                    new CrudActions.DestroyState(SellerTenderStore.CRUD_STORE_UID),
                    new CrudServiceActions.GetList({
                        url: SellerTenderStore.getCrudUrl(<RouterStateUrl>payload.routerState),
                        uid: SellerTenderStore.SERVICES_UID.list,
                    }),
                ];
            }),
        );

    @Effect()
    loadTenderSuccess$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
                map((action: CrudServiceActions.GetItemSuccess) => action.payload),
                filter(payload => payload.uid === SellerTenderStore.SERVICES_UID.tender),
                map(payload => {

                    return new SellerTenderActions.SetTender(<ITender.IModelForSeller>payload.item);
                }),
            );

    @Effect()
    getListSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === SellerTenderStore.SERVICES_UID.list),
            map(payload => {

                return new CrudActions.ImportModels(SellerTenderStore.CRUD_STORE_UID, payload.data);
            }),
        );

    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(SellerTenderActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        SellerTenderStore.SERVICES_UID.tender,
                        SellerTenderStore.SERVICES_UID.list,
                    ]);
                })
            );

    constructor(private actions$: Actions) {
    }
}
