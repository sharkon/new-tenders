import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import * as moment from 'moment';
import {PickerType} from 'ng-pick-datetime/date-time/date-time.class';

@Component({
    selector: 'app-design-date-time',
    templateUrl: './design-date-time.component.html',
    styleUrls: ['./design-date-time.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DesignDateTimeComponent implements OnInit {

    @Input() min: moment.Moment;
    @Input() formGroupInput: FormGroup;
    @Input() formControlNameInput: string;
    @Input() pickerType: PickerType = 'both';

    constructor() {
    }

    ngOnInit() {
    }

}
