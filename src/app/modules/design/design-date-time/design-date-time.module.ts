import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DesignDateTimeComponent} from './design-date-time.component';
import {ReactiveFormsModule} from '@angular/forms';
import {OwlDateTimeModule} from 'ng-pick-datetime';

@NgModule({
    declarations: [DesignDateTimeComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        OwlDateTimeModule,
    ],
    exports: [DesignDateTimeComponent],
})
export class DesignDateTimeModule {
}
