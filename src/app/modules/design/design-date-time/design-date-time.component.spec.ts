import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignDateTimeComponent } from './design-date-time.component';

describe('DesignDateTimeComponent', () => {
  let component: DesignDateTimeComponent;
  let fixture: ComponentFixture<DesignDateTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignDateTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignDateTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
