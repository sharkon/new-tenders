import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-design-preloader',
    templateUrl: './design-preloader.component.html',
    styleUrls: ['./design-preloader.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DesignPreloaderComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
