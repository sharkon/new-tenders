import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DesignPreloaderComponent} from './design-preloader.component';

@NgModule({
    declarations: [DesignPreloaderComponent],
    exports: [DesignPreloaderComponent],
    imports: [
        CommonModule
    ]
})
export class DesignPreloaderModule {
}
