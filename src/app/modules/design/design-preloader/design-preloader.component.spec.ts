import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignPreloaderComponent } from './design-preloader.component';

describe('DesignPreloaderComponent', () => {
  let component: DesignPreloaderComponent;
  let fixture: ComponentFixture<DesignPreloaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignPreloaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignPreloaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
