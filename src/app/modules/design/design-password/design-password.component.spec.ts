import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignPasswordComponent } from './design-password.component';

describe('DesignPasswordComponent', () => {
    let component: DesignPasswordComponent;
    let fixture: ComponentFixture<DesignPasswordComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DesignPasswordComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DesignPasswordComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
