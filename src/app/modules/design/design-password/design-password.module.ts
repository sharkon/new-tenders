import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesignPasswordComponent } from './design-password.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from 'ngx-icons';
import { PasswordStrengthMeterModule } from 'angular-password-strength-meter';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
    declarations: [DesignPasswordComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        PasswordStrengthMeterModule,
        PipesModule,
    ],
    exports: [DesignPasswordComponent]
})
export class DesignPasswordModule { }
