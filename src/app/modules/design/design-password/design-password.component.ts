import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';
import {AppConst} from '../../../consts/app.const';

@Component({
    selector: 'app-design-password',
    templateUrl: './design-password.component.html',
    styleUrls: ['./design-password.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DesignPasswordComponent implements OnInit {

    @Input() label: string;
    @Input() formGroupInput: FormGroup;
    @Input() formControlNameInput: string;
    @Input() control: AbstractControl;
    @Input() submitted: boolean;
    @Input() tabIndex = 1;
    @Input() showStrength = true;
    @Input() autocomplete: 'new' | 'off' = 'new';

    @Output() changeStrengthPassword: EventEmitter<number> = new EventEmitter<number>();

    passwordType = 'password';
    strengthPassword: number;

    get VALID_STRENGTH_PASSWORD() {
        return AppConst.VALID_STRENGTH_PASSWORD;
    }

    get showErrors() {
        return this.control && (this.control.dirty || this.control.touched) || this.submitted;
    }

    constructor() {
    }

    ngOnInit() {
    }

    toggleViewPassword() {
        this.passwordType = this.passwordType === 'password' ? 'text' : 'password';
    }

    onStrengthChange(strength: number): void {
        this.strengthPassword = strength;
        this.changeStrengthPassword.emit(strength);
    }

}
