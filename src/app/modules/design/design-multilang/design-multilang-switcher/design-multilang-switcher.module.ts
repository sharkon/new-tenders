import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesignMultilangSwitcherComponent } from './design-multilang-switcher.component';

@NgModule({
    declarations: [DesignMultilangSwitcherComponent],
    imports: [
        CommonModule
    ],
    exports: [DesignMultilangSwitcherComponent]
})
export class DesignMultilangSwitcherModule { }
