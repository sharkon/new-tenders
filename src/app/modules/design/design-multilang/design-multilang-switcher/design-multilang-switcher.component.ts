import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ILang} from '../../../../interfaces/lang.interface';
import {environment} from '../../../../../environments/environment';
import {AppLangConst} from '../../../../consts/app-lang.const';

@Component({
    selector: 'app-design-multilang-switcher',
    templateUrl: './design-multilang-switcher.component.html',
    styleUrls: ['./design-multilang-switcher.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DesignMultilangSwitcherComponent implements OnInit {

    @Input() defaultError: boolean;
    @Input() currentLang: ILang.LangType;

    @Output() switchLang: EventEmitter<ILang.LangType> = new EventEmitter<ILang.LangType>();

    get LANGS() {
        return AppLangConst.LANGS;
    }

    get DEFAULT_LANG() {
        return AppLangConst.DEFAULT_LANG;
    }

    constructor() {
    }

    ngOnInit() {
    }

    select(lang: ILang.LangType): void {

        this.switchLang.emit(lang);
    }

}
