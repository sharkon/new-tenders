import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ILang} from '../../../../interfaces/lang.interface';
import {FormGroup} from '@angular/forms';
import {AppLangConst} from '../../../../consts/app-lang.const';

@Component({
    selector: 'app-design-multilang-input',
    templateUrl: './design-multilang-input.component.html',
    styleUrls: ['./design-multilang-input.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DesignMultilangInputComponent implements OnInit {

    @Input() type: 'textarea' | 'input' = 'input';
    @Input() formGroupInput: FormGroup;

    currentLang: ILang.LangType = AppLangConst.DEFAULT_LANG;

    get LANGS() {
        return AppLangConst.LANGS;
    }

    constructor() {
    }

    ngOnInit() {
    }

    onSwitchLang(lang: ILang.LangType): void {

        this.currentLang = lang;
    }

    defaultHasErrors() {
        const control = this.formGroupInput && this.formGroupInput.controls[AppLangConst.DEFAULT_LANG];
        return !!(control && control.errors);
    }

    defaultHasTouched() {
        const control = this.formGroupInput && this.formGroupInput.controls[AppLangConst.DEFAULT_LANG];
        return !!(control && control.touched);
    }

}
