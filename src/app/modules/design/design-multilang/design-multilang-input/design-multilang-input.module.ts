import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesignMultilangInputComponent } from './design-multilang-input.component';
import { DesignMultilangSwitcherModule } from '../design-multilang-switcher/design-multilang-switcher.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [DesignMultilangInputComponent],
    imports: [
        CommonModule,
        DesignMultilangSwitcherModule,
        ReactiveFormsModule,
    ],
    exports: [DesignMultilangInputComponent]
})
export class DesignMultilangInputModule { }
