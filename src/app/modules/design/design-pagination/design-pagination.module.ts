import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesignPaginationComponent } from './design-pagination.component';
import { RouterModule } from '@angular/router';
import {PipesModule} from '../../../pipes/pipes.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        PipesModule,
    ],
    declarations: [DesignPaginationComponent],
    exports: [DesignPaginationComponent],
})
export class DesignPaginationModule {
}
