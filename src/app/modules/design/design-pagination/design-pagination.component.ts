import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PaginationStore } from '../../../reducers/pagination/pagination.reducer';
import { Observable, Subscription } from 'rxjs';
import { Params } from '@angular/router/src/shared';
import { PaginationUtils } from '../../../utils/pagination-utils';
import { filter, withLatestFrom } from 'rxjs/operators';
import { IPagination } from '../../../interfaces/pagination.interface';

@Component({
    selector: 'app-design-pagination',
    templateUrl: './design-pagination.component.html',
    styleUrls: ['./design-pagination.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DesignPaginationComponent implements OnInit, OnDestroy {

    @Input() storeUid: string;
    @Input() selectFunction: PaginationStore.ISelects;

    request$: Observable<IPagination.IPaginationOnlyRequest>;
    response$: Observable<IPagination.IPaginationResponse>;
    config$: Observable<IPagination.IPaginationConfig>;

    isFirstPage: boolean;
    isLastPage: boolean;

    previousPageQueryParams: Params;
    nextPageQueryParams: Params;

    currentPage: number;
    pages: IPagination.IPaginationPages[];
    pageQueryParams: Params[];

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    constructor(private store$: Store<PaginationStore.IState>,
        private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
    }

    private initStore() {

        this.request$ = this.store$.select(this.selectFunction.request);
        this.response$ = this.store$.select(this.selectFunction.response);
        this.config$ = this.store$.select(this.selectFunction.config);

        this.subs = this.request$
            .pipe(filter(val => !!val))
            .subscribe(request => {

                try {
                    this.currentPage = Math.ceil(request.offset / request.limit) + 1;
                } catch (e) {
                    this.currentPage = null;
                }

                this.cdr.detectChanges();
            });

        this.subs = this.response$.pipe(
            filter(val => !!val)
            , withLatestFrom(this.config$)
        )
            .subscribe(([response, config]) => {

                this.isFirstPage = !response.hasPrevPage;
                this.isLastPage = !response.hasNextPage;

                if (response.hasPrevPage) {

                    this.previousPageQueryParams = {
                        offset: Math.max(0, response.offset - response.limit),
                        limit: response.limit,
                    };

                } else {

                    this.previousPageQueryParams = null;
                }

                if (response.hasNextPage) {

                    this.nextPageQueryParams = {
                        offset: response.offset + response.limit,
                        limit: response.limit,
                    };

                } else {

                    this.nextPageQueryParams = null;
                }

                try {
                    this.currentPage = Math.ceil(response.offset / response.limit) + 1;
                } catch (e) {
                    this.currentPage = null;
                }

                this.pages = PaginationUtils.PaginationCreatePageArray(response, this.currentPage, 7);

                this.pageQueryParams = this.pages
                    .map(page => {
                        return {
                            offset: response.limit * (page.value - 1),
                            limit: response.limit,
                        };
                    });

                this.cdr.detectChanges();
            });
    }

}
