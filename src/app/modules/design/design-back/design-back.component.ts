import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppStore} from '../../../store/app.store';
import {AppRouterActions} from '../../../store/router/app-router.actions';

@Component({
    selector: 'app-design-back',
    templateUrl: './design-back.component.html',
    styleUrls: ['./design-back.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class DesignBackComponent implements OnInit {

    constructor(private store$: Store<AppStore.IState>) {
    }

    ngOnInit() {

    }

    back(event: TouchEvent | MouseEvent) {
        event.preventDefault();
        this.store$.dispatch(new AppRouterActions.Back());
    }

}
