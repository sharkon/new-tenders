import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignBackComponent } from './design-back.component';

describe('DesignBackComponent', () => {
  let component: DesignBackComponent;
  let fixture: ComponentFixture<DesignBackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignBackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
