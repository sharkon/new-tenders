import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DesignBackComponent} from './design-back.component';
import {PipesModule} from '../../../pipes/pipes.module';

@NgModule({
    declarations: [DesignBackComponent],
    exports: [DesignBackComponent],
    imports: [
        CommonModule,
        PipesModule,
    ]
})
export class DesignBackModule {
}
