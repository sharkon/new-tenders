import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges
} from '@angular/core';
import {AuthService} from '../../../services/auth/auth.service';
import {environment} from '../../../../environments/environment';
import {IFile} from '../../../interfaces/file.interface';
import {UploaderOptions, UploadFile, UploadInput, UploadOutput, UploadStatus} from 'ngx-uploader';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-design-file-uploader',
    templateUrl: './design-file-uploader.component.html',
    styleUrls: ['./design-file-uploader.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DesignFileUploaderComponent implements OnInit, OnChanges, OnDestroy {

    @Input() loadedFiles: IFile.IFileResponse[];
    @Input() maxFilesLength = 10;
    @Input() maxFileSizeMB = 16;
    @Input() restUrl = 'cabinet/files/';

    @Output() updateFiles: EventEmitter<UploadFile[]> = new EventEmitter<UploadFile[]>();
    @Output() onAddFiles: EventEmitter<UploadFile[]> = new EventEmitter<UploadFile[]>();
    @Output() onRemoveFile: EventEmitter<string> = new EventEmitter<string>();
    @Output() updateLoadStatus: EventEmitter<boolean> = new EventEmitter<boolean>();

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    files: IFile.IUploadFile[];

    uploadInput: EventEmitter<UploadInput>;
    uploadOptions: UploaderOptions;

    get uploadUrl() {
        return `${environment.rest.url}/${environment.rest.lang}/${this.restUrl}`;
    }

    errors: IFile.IError[] = [];

    get ERROR_MESSAGE() {
        return IFile.ERROR_MESSAGE;
    }

    constructor(private authService: AuthService) {
    }

    ngOnInit() {

        this.initFileUploader();
    }

    ngOnChanges(simpleChanges: SimpleChanges) {

        if (simpleChanges['loadedFiles']) {

            if (this.loadedFiles) {

                this.files = this.loadedFiles.map((file: IFile.IFileResponse, idx) => {

                    return {
                        id: `${file.id}`,
                        fileIndex: idx,
                        lastModifiedDate: null,
                        name: file.name,
                        size: null,
                        type: null,
                        form: null,
                        progress: null,
                        url: file.url,
                    };
                });

            } else {

                this.files = [];
            }
        }
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
    }

    private initFileUploader(): void {

        this.uploadOptions = {
            concurrency: 1,
        };

        this.uploadInput = new EventEmitter<UploadInput>();
    }

    onUpload(output: UploadOutput): void {

        if (output.type === 'allAddedToQueue') {

            const authHeader = this.authService.getAuthorizationHeader();

            const event: UploadInput = {
                type: 'uploadAll',
                url: this.uploadUrl,
                headers: {},
            };

            if (authHeader) {

                event.headers['Authorization'] = authHeader;
            }

            this.uploadInput.emit(event);

        } else if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') {

            this.files.push(<IFile.IUploadFile>output.file);

            this.updateLoadStatus.emit(true);

        } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {

            const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
            this.files[index] = <IFile.IUploadFile>output.file;

            this.updateLoadStatus.emit(true);

        } else if (output.type === 'rejected' && typeof output.file !== 'undefined') {

            const error: IFile.IError = {
                id: output.file.id,
                name: output.file.name,
            };

            error.rejectReason = 'INVALID_FILE_SIZE';

            this.errors.push(error);
            this.updateLoadStatus.emit(false);

        } else if (output.type === 'cancelled' || output.type === 'removed') {

            this.files = this.files.filter((file: UploadFile) => file !== output.file);

            this.updateFiles.emit(this.files);
            this.updateLoadStatus.emit(false);

        } else if (output.type === 'done') {

            this.files
                .filter((file: UploadFile) => file.responseStatus === 400 || file.responseStatus === 403)
                .forEach((file: UploadFile) => {

                    this.errors.push({
                        id: file.id,
                        name: file.name,
                        rejectReason: file.response,
                    });
                });

            this.files = this.files
                .filter((file: UploadFile) => file.responseStatus !== 400 && file.responseStatus !== 403)
                .map(f => {
                    return {...f};
                });

            this.updateFiles.emit(this.files);
            this.onAddFiles.emit(this.files);
            this.updateLoadStatus.emit(false);
        }
    }

    cancelOrRemoveFile(id: string, status: UploadStatus): void {

        if (status !== UploadStatus.Done) {

            this.uploadInput.emit({type: 'cancel', id: id});
            this.updateFiles.emit(this.files);

        } else {

            this.removeFile(id);
        }
    }

    removeFile(id: string) {

        this.files = this.files.filter(file => '' + file.id !== '' + id);
        this.updateFiles.emit(this.files);
        this.onRemoveFile.emit(id);
    }

    onUpdateLink(file: IFile.IUploadFile) {

        const findIndex = this.files.findIndex(f => '' + f.id === '' + file.id);

        this.files = this.files.slice();
        this.files.splice(findIndex, 1, {...file});

        this.updateFiles.emit(this.files);
        this.onAddFiles.emit(this.files);
    }
}
