import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesignFileUploaderComponent } from './design-file-uploader.component';
import { PipesModule } from '../../../pipes/pipes.module';
import { NgxUploaderModule } from 'ngx-uploader';
import {DesignFileLinkModule} from '../design-file-link/design-file-link.module';

@NgModule({
    declarations: [DesignFileUploaderComponent],
    imports: [
        CommonModule,
        PipesModule,
        NgxUploaderModule,
        DesignFileLinkModule,
    ],
    exports: [DesignFileUploaderComponent]
})
export class DesignFileUploaderModule { }
