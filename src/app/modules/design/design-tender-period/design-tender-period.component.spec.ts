import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignTenderPeriodComponent } from './design-tender-period.component';

describe('DesignTenderPeriodComponent', () => {
  let component: DesignTenderPeriodComponent;
  let fixture: ComponentFixture<DesignTenderPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignTenderPeriodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignTenderPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
