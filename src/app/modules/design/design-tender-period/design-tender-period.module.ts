import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DesignTenderPeriodComponent} from './design-tender-period.component';
import {PipesModule} from '../../../pipes/pipes.module';

@NgModule({
    declarations: [DesignTenderPeriodComponent],
    exports: [DesignTenderPeriodComponent],
    imports: [
        CommonModule,
        PipesModule,
    ]
})
export class DesignTenderPeriodModule {
}
