import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ITender} from '../../../interfaces/tender.interface';
import {AppConst} from '../../../consts/app.const';

@Component({
    selector: 'app-design-tender-period',
    templateUrl: './design-tender-period.component.html',
    styleUrls: ['./design-tender-period.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DesignTenderPeriodComponent implements OnInit {

    @Input() tender: ITender.IPeriodOfModel;

    get TENDER_PERIOD() {
        return ITender.PERIOD;
    }

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutTime;
    }

    constructor() {
    }

    ngOnInit() {
    }

}
