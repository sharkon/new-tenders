import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
    ViewEncapsulation
} from '@angular/core';
import {IFile} from '../../../interfaces/file.interface';
import {Subscription} from 'rxjs';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../services/crud/store/crud-service.actions';
import {Store} from '@ngrx/store';
import {AppStore} from '../../../store/app.store';
import {Actions, ofType} from '@ngrx/effects';
import {filter, map} from 'rxjs/operators';

@Component({
    selector: 'app-design-file-link',
    templateUrl: './design-file-link.component.html',
    styleUrls: ['./design-file-link.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class DesignFileLinkComponent implements OnInit, OnChanges, OnDestroy {

    @Input() inFile: IFile.IUploadFile;
    @Output() updateFile: EventEmitter<IFile.IUploadFile> = new EventEmitter<IFile.IUploadFile>();

    file: IFile.IUploadFile;
    fileId: number;
    fileUrl: string;
    storeUid: string;

    lockedFile: boolean;

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    constructor(private store$: Store<AppStore.IState>,
                private actions$: Actions,
                private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnChanges(changes: SimpleChanges): void {

        this.file = this.inFile;

        this.fileId = this.file ? +(this.file.response || this.file).id : null;
        this.fileUrl = this.file ? (this.file.response || this.file).url : null;

        this.storeUid = `[DesignFileLinkComponent] UpdateFileUrl ${this.fileId}`;
    }

    ngOnDestroy(): void {

        this._subs.forEach(s => s.unsubscribe());
    }

    private initStore() {

        this.subs = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
                map((action: CrudServiceActions.GetItemSuccess) => action.payload),
                filter(payload => payload.uid === this.storeUid && !!payload.item),
            ).subscribe((payload: any) => {

                this.fileUrl = payload.item.url;
                this.lockedFile = false;

                const response = this.inFile.response
                    ? {
                        ...this.inFile.response,
                        url: this.fileUrl,
                    }
                    : this.inFile.response;

                const url = this.inFile.response
                    ? this.inFile.url
                    : this.fileUrl;

                this.updateFile.emit({
                    ...this.inFile,
                    response,
                    url,
                });

                this.cdr.detectChanges();
            });
    }

    updateFileUrl(): void {

        this.lockedFile = true;

        this.store$.dispatch(new CrudServiceActions.GetItem({
            url: `files/${this.fileId}`,
            uid: this.storeUid,
        }));

    }

}
