import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DesignFileLinkComponent} from './design-file-link.component';

@NgModule({
    declarations: [DesignFileLinkComponent],
    exports: [DesignFileLinkComponent],
    imports: [
        CommonModule
    ],
})
export class DesignFileLinkModule {
}
