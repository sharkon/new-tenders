import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignFileLinkComponent } from './design-file-link.component';

describe('DesignFileLinkComponent', () => {
  let component: DesignFileLinkComponent;
  let fixture: ComponentFixture<DesignFileLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignFileLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignFileLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
