import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {IAuth} from '../../../interfaces/auth.interface';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../../store/app.store';
import {map} from 'rxjs/operators';
import {AppConst} from '../../../consts/app.const';

@Component({
    selector: 'app-auth-root',
    templateUrl: './auth-root.component.html',
    styleUrls: ['./auth-root.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthRootComponent implements OnInit {

    activeView: 'login' | 'recovery' = 'login';
    recoveryData: IAuth.ISignInRequest;

    loading$: Observable<boolean>;

    constructor(private store$: Store<AppStore.IState>) {
    }

    ngOnInit() {

        this.loading$ = this.store$.pipe(
            select(AppStore.Selects.appBlock.locks),
            map(locks => {
                return locks[AppConst.AUTH_SERVICES_UID];
            }),
        );
    }

    showRecovery(): void {

        this.activeView = 'recovery';
    }

    showLogin(): void {

        this.activeView = 'login';
    }

    onLoginFormUpdate(data: IAuth.ISignInRequest): void {

        this.recoveryData = data;
    }

}
