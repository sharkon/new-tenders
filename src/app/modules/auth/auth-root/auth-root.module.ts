import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRootComponent } from './auth-root.component';
import { RouterModule } from '@angular/router';
import { PipesModule } from '../../../pipes/pipes.module';
import { AuthLoginModule } from '../auth-login/auth-login.module';
import { AuthRecoveryModule } from '../auth-recovery/auth-recovery.module';
import {DesignPreloaderModule} from '../../design/design-preloader/design-preloader.module';

@NgModule({
    declarations: [AuthRootComponent],
    imports: [
        CommonModule,
        PipesModule,
        AuthLoginModule,
        AuthRecoveryModule,
        DesignPreloaderModule,
        RouterModule.forChild([
            {
                path: '',
                component: AuthRootComponent,
            }
        ]),
    ]
})
export class AuthRootModule { }
