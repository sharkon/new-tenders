import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core';
import {IAuth} from '../../../interfaces/auth.interface';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../../store/app.store';
import {Subscription} from 'rxjs';
import {AppAuthActions, AppAuthActionTypes} from '../../../store/auth/app-auth.actions';
import {Actions, ofType} from '@ngrx/effects';
import {AppErrorsUtils} from '../../../utils/app-errors.utils';
import {TranslateService} from '../../../services/translate/translate.service';

@Component({
    selector: 'app-auth-recovery',
    templateUrl: './auth-recovery.component.html',
    styleUrls: ['./auth-recovery.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthRecoveryComponent implements OnInit, OnDestroy {

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get captchaImageString(): string {
        return this.captcha
            ? `data:${this.captcha.image_type};${this.captcha.image_decode},${this.captcha.captcha_image}`
            : null;
    }

    get emailControl(): AbstractControl {
        return this.getFormControl('email');
    }

    get captchaControl(): AbstractControl {
        return this.getFormControl('captcha_value');
    }

    captcha: IAuth.ICaptchaResponse;
    showSuccessMessage: boolean;
    errorMessage: string;

    @Input() recoveryData: IAuth.ISignInRequest;
    @Output() toggleView: EventEmitter<void> = new EventEmitter<void>();

    formGroup: FormGroup;
    submitted: boolean;

    constructor(private formBuilder: FormBuilder,
                private store$: Store<AppStore.IState>,
                private actions$: Actions,
                private cdr: ChangeDetectorRef,
                private ts: TranslateService) {
    }

    ngOnInit() {

        this.initForm();
        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
    }

    private initForm(): void {

        this.formGroup = this.formBuilder.group({
            email: [
                this.recoveryData ? this.recoveryData.email : '',
                [Validators.email, Validators.required]
            ],
            captcha_value: ['', [Validators.required]],
        });
    }

    private initStore(): void {

        this.store$.dispatch(new AppAuthActions.GetCaptcha());

        this.subs = this.store$
            .pipe(
                select(AppStore.Selects.appAuth.captcha),
            )
            .subscribe((captcha: IAuth.ICaptchaResponse) => {

                this.captcha = captcha;
                this.cdr.detectChanges();
            });

        this.subs = this.actions$
            .pipe(
                ofType(AppAuthActionTypes.RECOVERY_SUCCESS),
            )
            .subscribe((action: AppAuthActions.RecoverySuccess) => {

                this.showSuccessMessage = true;
                this.errorMessage = '';
                this.cdr.detectChanges();

                this.resetCaptcha();
            });

        this.subs = this.actions$
            .pipe(
                ofType(AppAuthActionTypes.RECOVERY_FAILED),
            )
            .subscribe((action: AppAuthActions.RecoveryFailed) => {

                this.showSuccessMessage = false;
                this.errorMessage = AppErrorsUtils.parseErrors(action.payload, this.ts);
                this.cdr.detectChanges();

                this.resetCaptcha();
            });
    }

    private getFormControl(key: string): AbstractControl {

        return this.formGroup && this.formGroup.controls[key]
            ? this.formGroup.controls[key]
            : null;
    }

    reloadCaptcha(): void {

        this.store$.dispatch(new AppAuthActions.GetCaptcha());
    }

    resetCaptcha(): void {

        if (this.getFormControl('captcha_value')) {

            this.getFormControl('captcha_value').setValue('');
            this.reloadCaptcha();
        }
    }

    onSubmit() {

        this.submitted = true;

        if (this.formGroup.valid) {

            const request: IAuth.IRecoveryRequest = {
                email: this.formGroup.controls['email'].value,
                captcha_value: this.formGroup.controls['captcha_value'].value,
                captcha_key: this.captcha.captcha_key
            };

            this.store$.dispatch(new AppAuthActions.Recovery(request));
        }
    }

}
