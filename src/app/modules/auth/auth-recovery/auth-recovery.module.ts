import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRecoveryComponent } from './auth-recovery.component';
import { PipesModule } from '../../../pipes/pipes.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [AuthRecoveryComponent],
    imports: [
        CommonModule,
        PipesModule,
        ReactiveFormsModule
    ],
    exports: [AuthRecoveryComponent]
})
export class AuthRecoveryModule { }
