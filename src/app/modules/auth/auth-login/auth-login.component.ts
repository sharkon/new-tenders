import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {IAuth} from '../../../interfaces/auth.interface';
import {AppStore} from '../../../store/app.store';
import {select, Store} from '@ngrx/store';
import {AppAuthActions, AppAuthActionTypes} from '../../../store/auth/app-auth.actions';
import {Observable, Subscription} from 'rxjs';
import {Actions, ofType} from '@ngrx/effects';
import {AppErrorsUtils} from '../../../utils/app-errors.utils';
import {AuthService} from '../../../services/auth/auth.service';
import {TranslateService} from '../../../services/translate/translate.service';

@Component({
    selector: 'app-auth-login',
    templateUrl: './auth-login.component.html',
    styleUrls: ['./auth-login.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthLoginComponent implements OnInit, OnDestroy {

    @Input() recoveryData: IAuth.ISignInRequest;
    @Output() formUpdated: EventEmitter<IAuth.ISignInRequest> = new EventEmitter<IAuth.ISignInRequest>();
    @Output() toggleView: EventEmitter<void> = new EventEmitter<void>();

    formGroup: FormGroup;
    submitted: boolean;

    passwordType = 'password';

    private _captcha: IAuth.ICaptchaResponse;
    captchaImageString: string;

    get captcha() {
        return this._captcha;
    }

    set captcha(captcha: IAuth.ICaptchaResponse) {

        this._captcha = captcha;

        this.captchaImageString = captcha
            ? `data:${captcha.image_type};${captcha.image_decode},${captcha.captcha_image}`
            : null;
    }

    authErrorMessage$: Observable<string>;
    captchaErrorMessage$: Observable<string>;
    userErrorMessage$: Observable<string>;

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get emailControl(): AbstractControl {
        return this.getFormControl('email');
    }

    get passwordControl(): AbstractControl {
        return this.getFormControl('password');
    }

    get captchaControl(): AbstractControl {
        return this.getFormControl('captcha_value');
    }

    constructor(private formBuilder: FormBuilder,
                private store$: Store<AppStore.IState>,
                private actions$: Actions,
                private cdr: ChangeDetectorRef,
                private ts: TranslateService) {
    }

    ngOnInit() {

        this.initForm();
        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
    }

    private initForm(): void {

        this.formGroup = this.formBuilder.group({
            email: [
                this.recoveryData ? this.recoveryData.email : '',
                [Validators.email, Validators.required]
            ],
            password: ['', [Validators.required]],
            captcha_value: ['', [Validators.required]]
        });

        this.subs = this.formGroup.valueChanges
            .pipe(
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe((values: IAuth.ISignInRequest) => {

                this.formUpdated.emit(values);
            });
    }

    private initStore(): void {

        this.store$.dispatch(new AppAuthActions.GetCaptcha());

        this.subs = this.store$
            .pipe(
                select(AppStore.Selects.appAuth.captcha),
            )
            .subscribe((captcha: IAuth.ICaptchaResponse) => {

                this.captcha = captcha;
                this.cdr.detectChanges();
            });

        this.authErrorMessage$ = this.store$
            .pipe(
                select(AppStore.Selects.appAuth.authErrorMessage),
                map(e => AppErrorsUtils.parseErrors(e, this.ts)),
            );

        this.captchaErrorMessage$ = this.store$
            .pipe(
                select(AppStore.Selects.appAuth.captchaErrorMessage),
                map(e => AppErrorsUtils.parseErrors(e, this.ts)),
            );

        this.userErrorMessage$ = this.store$
            .pipe(
                select(AppStore.Selects.appAuth.userErrorMessage),
                map(e => AppErrorsUtils.parseErrors(e, this.ts)),
            );

        this.subs = this.actions$
            .pipe(ofType(AppAuthActionTypes.GET_CAPTCHA))
            .subscribe(action => {
                if (this.getFormControl('captcha_value')) {
                    this.getFormControl('captcha_value').setValue('');
                }
            });
    }

    private getFormControl(key: string): AbstractControl {

        return this.formGroup && this.formGroup.controls[key]
            ? this.formGroup.controls[key]
            : null;
    }

    reloadCaptcha(): void {

        this.store$.dispatch(new AppAuthActions.GetCaptcha());
    }

    onSubmit() {

        this.submitted = true;

        if (this.formGroup.valid) {

            const loginData: IAuth.ISignInRequest = this.formGroup.value;
            const captcha_key: string = this.captcha ? this.captcha.captcha_key : null;

            this.store$.dispatch(new AppAuthActions.LogIn({
                ...loginData,
                captcha_key
            }));
        }
    }

    toggleViewPassword() {
        this.passwordType = this.passwordType === 'password' ? 'text' : 'password';
    }

}
