import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthLoginComponent } from './auth-login.component';
import { PipesModule } from '../../../pipes/pipes.module';
import { ReactiveFormsModule } from '@angular/forms';
import {FontAwesomeModule} from 'ngx-icons';

@NgModule({
    declarations: [AuthLoginComponent],
    imports: [
        CommonModule,
        PipesModule,
        ReactiveFormsModule,
        FontAwesomeModule,
    ],
    exports: [AuthLoginComponent]
})
export class AuthLoginModule { }
