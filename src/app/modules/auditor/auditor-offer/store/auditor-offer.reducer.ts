import { createSelector, MemoizedSelector } from '@ngrx/store';
import { AuditorOfferActions, AuditorOfferActionTypes } from './auditor-offer.actions';
import { IOffer } from '../../../../interfaces/offer.interface';

export namespace AuditorOfferReducer {

    export interface IState {
        offer: IOffer.IModelForAuditorDetails;
    }

    const initialState: IState = {
        offer: null,
    };

    export interface ISelects {
        offer: (state: IState) => IOffer.IModelForAuditorDetails;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            offer = createSelector(
                selector,
                ((state: IState) => state.offer),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = { ...initialState }, action: AuditorOfferActions.All): IState {

        switch (action.type) {

            case AuditorOfferActionTypes.SET_OFFER: {

                const offer: IOffer.IModelForAuditorDetails = action.payload ? { ...action.payload } : null;

                return {
                    ...state,
                    offer,
                };
            }

            case AuditorOfferActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
