import { AuditorOfferReducer } from './auditor-offer.reducer';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import { AppStore } from '../../../../store/app.store';

export namespace AuditorOfferStore {

    export const FEATURE_UID = '[AuditorOfferStore] FEATURE_UID';
    export const MODULE_UID = '[AuditorOfferStore] MODULE_UID';

    export const SERVICES_URL = {
        offer: (offerId: string | number): string => {
            const url = 'cabinet/auditor/tender/proposal/';
            return offerId ? `${url}${offerId}` : url;
        },
    };

    export const SERVICES_UID = {
        offer: '[AuditorOfferStore][SERVICES_UID] offer',
    };

    interface IStore {
        selfStore: AuditorOfferReducer.IState;
    }

    export interface IState extends AppStore.IState {
        auditorOfferStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: AuditorOfferReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = AuditorOfferReducer.createStoreSelector(selfSelector);

    }

}
