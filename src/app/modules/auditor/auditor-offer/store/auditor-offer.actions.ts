import { Action } from '@ngrx/store';
import { IOffer } from '../../../../interfaces/offer.interface';

export namespace AuditorOfferActionTypes {

    export const SET_OFFER = '[AuditorOffer] SET_OFFER';
    export const DESTROY_STATE = '[AuditorOffer] DESTROY_STATE';
}

export namespace AuditorOfferActions {

    export class SetOffer implements Action {

        readonly type = AuditorOfferActionTypes.SET_OFFER;

        constructor(public payload: IOffer.IModelForAuditorDetails) {
        }
    }

    export class DestroyState implements Action {

        readonly type = AuditorOfferActionTypes.DESTROY_STATE;
    }

    export type All =
        DestroyState
        | SetOffer;

}
