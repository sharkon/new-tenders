import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';
import { ROUTER_NAVIGATED, RouterNavigationAction } from '@ngrx/router-store';
import { AuditorOfferActions } from './auditor-offer.actions';
import { AuditorOfferActionTypes } from './auditor-offer.actions';
import { AuditorOfferStore } from './auditor-offer.store';
import { RouterStateUrl } from '../../../../providers/custom-router-state-serializer';
import { CrudServiceActions, CrudServiceActionTypes } from '../../../../services/crud/store/crud-service.actions';
import { IOffer } from '../../../../interfaces/offer.interface';

@Injectable()
export class AuditorOfferEffects {


    @Effect()
    onRoute$: Observable<Action> =
        this.actions$.pipe(
            ofType(ROUTER_NAVIGATED),
            map((action: RouterNavigationAction<RouterStateUrl>) => action.payload),
            filter(payload => {
                return payload.routerState.data.module === AuditorOfferStore.MODULE_UID;
            }),
            mergeMap((payload) => {

                const offerId: string = payload.routerState.params['offerId'];

                return [
                    new CrudServiceActions.GetItem({
                        url: `${AuditorOfferStore.SERVICES_URL.offer(offerId)}/`,
                        uid: AuditorOfferStore.SERVICES_UID.offer,
                    }),

                ];
            }),
        );

    @Effect()
    loadOfferSuccess$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
                map((action: CrudServiceActions.GetItemSuccess) => action.payload),
                filter(payload => payload.uid === AuditorOfferStore.SERVICES_UID.offer),
                map(payload => {

                    return new AuditorOfferActions.SetOffer(<IOffer.IModelForAuditorDetails>payload.item);
                }),
            );



    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(AuditorOfferActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        AuditorOfferStore.SERVICES_UID.offer,
                    ]);
                })
            );

    constructor(private actions$: Actions) {
    }
}
