import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditorOfferComponent } from './auditor-offer.component';

describe('AuditorOfferComponent', () => {
  let component: AuditorOfferComponent;
  let fixture: ComponentFixture<AuditorOfferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditorOfferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditorOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
