import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {filter, map} from 'rxjs/operators';
import {ITender} from '../../../../interfaces/tender.interface';
import {AuditorRootStore} from '../../auditor-root/store/auditor-root.store';
import {AppAuthActions} from '../../../../store/auth/app-auth.actions';

@Injectable()
export class AuditorOfferGuard implements CanActivate {
    constructor(private store$: Store<AuditorRootStore.IState>,
                private router: Router) {
    }

    canActivate(): Observable<boolean> | Promise<boolean> | boolean {

        return this.store$.pipe(
            select(AuditorRootStore.Selects.self.tender),
            filter(tender => !!tender),
            map((tender: ITender.IModelForAuditor) => {

                if (tender.state !== ITender.STATE_TYPES.UNLOCK) {

                    this.router.navigateByUrl('/auditor/decision', {
                        replaceUrl: true,
                    });
                }

                return tender.state === ITender.STATE_TYPES.UNLOCK;
            })
        );
    }
}
