import { TestBed, async, inject } from '@angular/core/testing';

import { AuditorOfferGuard } from './auditor-offer.guard';

describe('AuditorOfferGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [AuditorOfferGuard]
        });
    });

    it('should ...', inject([AuditorOfferGuard], (guard: AuditorOfferGuard) => {
        expect(guard).toBeTruthy();
    }));
});
