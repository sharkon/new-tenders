import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditorOfferComponent } from './auditor-offer.component';
import { RouterModule } from '@angular/router';
import { AuditorOfferGuard } from './guards/auditor-offer.guard';
import { PipesModule } from '../../../pipes/pipes.module';
import { StoreModule } from '@ngrx/store';
import { AuditorOfferStore } from './store/auditor-offer.store';
import { EffectsModule } from '@ngrx/effects';
import { AuditorOfferEffects } from './store/auditor-offer.effects';
import { DesignPreloaderModule } from '../../design/design-preloader/design-preloader.module';
import { DesignFileLinkModule } from '../../design/design-file-link/design-file-link.module';
import {DesignBackModule} from '../../design/design-back/design-back.module';

@NgModule({
    declarations: [AuditorOfferComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: AuditorOfferComponent,
                canActivate: [AuditorOfferGuard],
                data: {
                    module: AuditorOfferStore.MODULE_UID,
                },
            }
        ]),
        PipesModule,
        DesignPreloaderModule,
        DesignFileLinkModule,
        StoreModule.forFeature(AuditorOfferStore.FEATURE_UID, AuditorOfferStore.mapReducers),
        EffectsModule.forFeature([
            AuditorOfferEffects,
        ]),
        DesignBackModule,
    ],
    providers: [
        AuditorOfferGuard,
    ],
})
export class AuditorOfferModule {
}
