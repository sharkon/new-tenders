import { ChangeDetectionStrategy, Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { IOffer } from '../../../interfaces/offer.interface';
import { Store, select } from '@ngrx/store';
import { AuditorOfferStore } from './store/auditor-offer.store';
import { ITender } from '../../../interfaces/tender.interface';
import { AuditorRootStore } from '../auditor-root/store/auditor-root.store';
import { IMultilang } from '../../../interfaces/multilang.interface';
import { TranslateService } from '../../../services/translate/translate.service';
import { IUser } from '../../../interfaces/user.interface';
import { IFile } from '../../../interfaces/file.interface';
import {AppConst} from '../../../consts/app.const';
import {AuditorOfferActions} from './store/auditor-offer.actions';

@Component({
    selector: 'app-auditor-offer',
    templateUrl: './auditor-offer.component.html',
    styleUrls: ['./auditor-offer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuditorOfferComponent implements OnInit, OnDestroy {

    tender: ITender.IModelForAuditor;
    offer: IOffer.IModelForAuditorDetails;

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    get DATE_FORMAT_MESSAGE() {
        return AppConst.DATE_PIPE_FORMAT.withSeconds;
    }

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get USER_ROLES() {
        return IUser.USER_ROLES;
    }

    get curatorFiles() {

        let files: IFile.IChatFile[];

        if (this.offer && this.offer.chatFiles) {
            files = this.offer.chatFiles.filter(f => f.roleUpload === IUser.USER_ROLES.CURATOR);
        }

        return files;
    }

    get sellerFiles() {

        let files: IFile.IChatFile[];

        if (this.offer && this.offer.chatFiles) {
            files = this.offer.chatFiles.filter(f => f.roleUpload === IUser.USER_ROLES.SELLER);
        }

        return files;
    }

    constructor(private store$: Store<AuditorOfferStore.IState>,
                private translateService: TranslateService,
                private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new AuditorOfferActions.DestroyState());
    }

    private initStore() {

        this.subs = this.store$.pipe(select(AuditorRootStore.Selects.self.tender))
            .subscribe((tender: ITender.IModelForAuditor) => {

                this.tender = tender;
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(AuditorOfferStore.Selects.self.offer))
            .subscribe((offer: IOffer.IModelForAuditorDetails) => {

                this.offer = offer;
                this.cdr.detectChanges();
            });
    }

}
