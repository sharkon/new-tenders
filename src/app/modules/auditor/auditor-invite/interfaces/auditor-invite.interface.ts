import {ICrud} from '../../../../interfaces/crud.interface';
import {ITender} from '../../../../interfaces/tender.interface';
import {IAuth} from '../../../../interfaces/auth.interface';

export namespace IAuditorInvite {

    export interface InviteResponse extends ICrud.IModel, IAuth.ICaptchaResponse {
        tender_id: number;
        tender_state: ITender.StateType;
        passwordsCount: number; // кол-во высланных по данному токену паролей
    }
}
