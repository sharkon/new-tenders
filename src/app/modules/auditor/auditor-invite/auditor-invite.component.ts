import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IAuth} from '../../../interfaces/auth.interface';
import {Observable, of, Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../../store/app.store';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../services/crud/store/crud-service.actions';
import {catchError, filter, map} from 'rxjs/operators';
import {TranslateService} from '../../../services/translate/translate.service';
import {IAuditorInvite} from './interfaces/auditor-invite.interface';
import {AuditorInviteStore} from './store/auditor-invite.store';
import {IError} from '../../../interfaces/error.interface';
import {AppErrorsConst} from '../../../consts/app-errors.const';
import {Actions, ofType} from '@ngrx/effects';
import {AppErrorsUtils} from '../../../utils/app-errors.utils';
import {AuditorRootStore} from '../auditor-root/store/auditor-root.store';

@Component({
    selector: 'app-auditor-invite',
    templateUrl: './auditor-invite.component.html',
    styleUrls: ['./auditor-invite.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuditorInviteComponent implements OnInit, OnDestroy {

    passwordType = 'password';
    passwordTypes = [];

    inviteResponse: IAuditorInvite.InviteResponse;
    returnUrl: string;

    inviteFormGroup: FormGroup;
    inviteFormGroupSubmitted: boolean;

    private _captcha: IAuth.ICaptchaResponse;
    captchaImageString: string;

    get captcha() {
        return this._captcha;
    }

    set captcha(captcha: IAuth.ICaptchaResponse) {

        this._captcha = captcha;

        this.captchaImageString = captcha
            ? `data:${captcha.image_type};${captcha.image_decode},${captcha.captcha_image}`
            : null;
    }

    captchaErrorMessage: string;

    loading$: Observable<boolean>;

    private inviteToken: string;

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get keysControl(): FormArray {
        return <FormArray>this.inviteFormGroup.get('keys');
    }

    get captchaControl(): AbstractControl {
        return this.getFormControl('captcha_value');
    }

    constructor(private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private store$: Store<AuditorInviteStore.IState>,
                private actions$: Actions,
                private cdr: ChangeDetectorRef,
                private ts: TranslateService) {
    }

    ngOnInit() {

        this.initForm();
        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
    }

    private initForm(): void {

        this.inviteFormGroup = this.formBuilder.group({
            keys: this.formBuilder.array([]),
            captcha_value: ['', [Validators.required]],
        });
    }

    private addFormKey(keysControl: FormArray, index: number): void {

        const keyNameGroup = this.formBuilder.group({
            keyName: ['', [Validators.minLength(8), Validators.required]]
        });

        keysControl.push(keyNameGroup);
    }

    private initStore(): void {

        this.inviteToken = this.route.snapshot.params['inviteToken'] || null;

        this.loading$ = this.store$.pipe(
            select(AppStore.Selects.appBlock.locks),
            map(locks => {
                return locks[AuditorInviteStore.SERVICES_UID.invite]
                    || locks[AuditorRootStore.SERVICES_UID.tender];
            }),
        );

        this.subs = this.store$.pipe(select(AuditorInviteStore.Selects.self.inviteResponse))
            .subscribe(inviteResponse => {

                    this.inviteResponse = inviteResponse;

                    if (inviteResponse && inviteResponse.passwordsCount && this.inviteFormGroup) {

                        const keysControl = <FormArray>this.inviteFormGroup.controls['keys'];

                        if (!keysControl.length) {
                            for (let i = inviteResponse.passwordsCount - 1; i >= 0; i--) {
                                keysControl.removeAt(i);
                            }

                            for (let i = 0; i < inviteResponse.passwordsCount; i++) {
                                this.addFormKey(keysControl, i);
                                this.passwordTypes[i] = this.passwordType;
                            }
                        }
                    }

                    this.captcha = {
                        captcha_key: inviteResponse.captcha_key,
                        captcha_image: inviteResponse.captcha_image,
                        image_type: inviteResponse.image_type,
                        image_decode: inviteResponse.image_decode,
                    };

                    if (this.getFormControl('captcha_value')) {
                        this.getFormControl('captcha_value').setValue('');
                    }

                    this.cdr.detectChanges();
                }, catchError(e => {
                    console.error(e);
                    return of(e);
                })
            );

        this.subs = this.store$.pipe(select(AuditorInviteStore.Selects.self.returnUrl))
            .subscribe(returnUrl => {

                this.returnUrl = returnUrl;
                this.cdr.detectChanges();
            });

        this.subs = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.CREATE_FAILED),
                map((action: CrudServiceActions.CreateFailed) => action.payload),
                filter(payload => payload.uid === AuditorInviteStore.SERVICES_UID.invite),
            )
            .subscribe((payload: any) => {

                this.captchaErrorMessage = AppErrorsUtils.parseErrors(payload.error, this.ts);
                this.reloadCaptcha();

                this.cdr.detectChanges();
            });
    }

    private getFormControl(key: string): AbstractControl {
        return this.inviteFormGroup && this.inviteFormGroup.controls[key]
            ? this.inviteFormGroup.controls[key]
            : null;
    }

    onSubmit(event: Event): void {

        event.preventDefault();

        this.inviteFormGroupSubmitted = true;

        if (this.inviteFormGroup.valid) {

            const formValues = this.inviteFormGroup.value;
            const passwords: string[] = formValues.keys ? formValues.keys.map(k => k['keyName']) : null;

            const request: any = {
                ...this.inviteResponse,
                passwords,
                captcha_key: this.captcha ? this.captcha.captcha_key : null,
                captcha_value: formValues.captcha_value || null,
            };

            this.store$.dispatch(new CrudServiceActions.Create({
                url: `${AuditorInviteStore.SERVICES_URL.invite}/${this.inviteToken}`,
                uid: AuditorInviteStore.SERVICES_UID.invite,
                item: {
                    ...request,
                },
                mapMessage: this.getMapMessage(),
                deniedNotifySuccess: true,
            }));
        }
    }

    reloadCaptcha(): void {
        this.store$.dispatch(new CrudServiceActions.GetItem({
            url: `${AuditorInviteStore.SERVICES_URL.invite}/${this.inviteToken}`,
            uid: AuditorInviteStore.SERVICES_UID.invite,
        }));
    }

    toggleViewPassword(i: number) {
        this.passwordTypes[i] = this.passwordTypes[i] === 'password' ? 'text' : 'password';
    }

    private getMapMessage(): IError.IBackendTranslateError {

        const mapMessage: IError.IBackendTranslateError = {
            'captcha_value': {
                'captcha_value': this.ts.data.T.authErrors.error,
            },
            'non_field_errors': {
                'non_field_errors': this.ts.data.T.authErrors.error,
            },
            'passwords': {
                'passwords': this.ts.data.T.authErrors.error,
            },
        };

        mapMessage['captcha_value'][AppErrorsConst.BACKEND_ERROR_TYPES.MAX_LENGTH] = this.ts.data.T.authErrors.nonFieldErrors.captchaInvalid;
        mapMessage['non_field_errors']['CAPTCHA_INVALID'] = this.ts.data.T.authErrors.nonFieldErrors.captchaInvalid;
        mapMessage['non_field_errors']['CAPTCHA_INVALID_OR_EXPIRED'] = this.ts.data.T.authErrors.nonFieldErrors.captchaInvalid;
        mapMessage['passwords'][AppErrorsConst.BACKEND_ERROR_TYPES.INVALID_VALUE] = this.ts.data.T.authErrors.auth.passwordValue;

        return mapMessage;
    }

}
