import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditorInviteComponent } from './auditor-invite.component';
import { RouterModule } from '@angular/router';
import { PipesModule } from '../../../pipes/pipes.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from 'ngx-icons';
import { DesignPreloaderModule } from '../../design/design-preloader/design-preloader.module';
import { StoreModule } from '@ngrx/store';
import { AuditorInviteStore } from './store/auditor-invite.store';
import { EffectsModule } from '@ngrx/effects';
import { AuditorInviteEffects } from './store/auditor-invite.effects';

@NgModule({
    declarations: [AuditorInviteComponent],
    imports: [
        CommonModule,
        PipesModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: AuditorInviteComponent,
            }
        ]),
        FontAwesomeModule,
        DesignPreloaderModule,
        StoreModule.forFeature(AuditorInviteStore.FEATURE_UID, AuditorInviteStore.mapReducers),
        EffectsModule.forFeature([
            AuditorInviteEffects,
        ]),
    ]
})
export class AuditorInviteModule { }
