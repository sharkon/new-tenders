import { TestBed, async, inject } from '@angular/core/testing';

import { AuditorInviteDataGuard } from './auditor-invite-data.guard';

describe('AuditorInviteDataGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuditorInviteDataGuard]
    });
  });

  it('should ...', inject([AuditorInviteDataGuard], (guard: AuditorInviteDataGuard) => {
    expect(guard).toBeTruthy();
  }));
});
