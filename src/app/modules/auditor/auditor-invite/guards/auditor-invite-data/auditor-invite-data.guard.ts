import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AuditorInviteStore} from '../../store/auditor-invite.store';
import {Actions, ofType} from '@ngrx/effects';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../../services/crud/store/crud-service.actions';
import {catchError, filter, switchMap, withLatestFrom} from 'rxjs/operators';
import {AppStore} from '../../../../../store/app.store';
import {IAuditorInvite} from '../../interfaces/auditor-invite.interface';
import {ITender} from '../../../../../interfaces/tender.interface';
import {NotifyService} from '../../../../../services/notify/notify.service';
import {TranslateService} from '../../../../../services/translate/translate.service';
import {AuditorInviteActions} from '../../store/auditor-invite.actions';
import {AppAuthActions} from '../../../../../store/auth/app-auth.actions';

@Injectable({
    providedIn: 'root'
})
export class AuditorInviteDataGuard implements CanActivate {

    constructor(private store$: Store<AuditorInviteStore.IState>,
                private actions$: Actions,
                private router: Router,
                private notifyService: NotifyService,
                private ts: TranslateService) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        const inviteToken = next.params['inviteToken'] || null;

        if (!inviteToken) {
            return false;
        }

        this.store$.dispatch(new CrudServiceActions.GetItem({
            url: `${AuditorInviteStore.SERVICES_URL.invite}/${inviteToken}`,
            uid: AuditorInviteStore.SERVICES_UID.invite,
        }));

        const failed = (authenticated?: boolean) => {

            if (authenticated) {
                this.store$.dispatch(new AppAuthActions.LogOut(`${AuditorInviteStore.SERVICES_URL.invite}/${inviteToken}`));
            } else {
                this.router.navigate(['/']);
            }

            return of(false);
        };

        return this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS, CrudServiceActionTypes.GET_ITEM_FAILED),
                filter((action: CrudServiceActions.GetItemSuccess | CrudServiceActions.GetItemFailed) => {
                    return action.payload.uid === AuditorInviteStore.SERVICES_UID.invite;
                }),
                withLatestFrom(
                    this.store$.pipe(select(AppStore.Selects.appAuth.authenticated)),
                ),
                switchMap(([action, authenticated]) => {

                    if (action.type === CrudServiceActionTypes.GET_ITEM_FAILED) {

                        return failed(authenticated);
                    }

                    const inviteResponse = action.payload.item as IAuditorInvite.InviteResponse;

                    let returnUrl = '/';

                    if (inviteResponse.tender_state === ITender.STATE_TYPES.UNLOCK) {

                        if (authenticated) {
                            this.notifyService.warning({
                                dialogContent: this.ts.data.T.auditorPages.invite.opened,
                            });
                        }

                        returnUrl = `/auditor/tender`;

                    } else if (inviteResponse.tender_state === ITender.STATE_TYPES.CLOSE) {

                        returnUrl = `/auditor/decision`;

                    } else {

                        if (authenticated) {
                            this.notifyService.warning({
                                dialogContent: this.ts.data.T.auditorPages.invite.warning,
                            });
                        }

                        return failed();
                    }

                    if (authenticated) {

                        this.router.navigate([returnUrl]);
                        return of(false);
                    }

                    this.store$.dispatch(new AuditorInviteActions.SetInviteResponse(inviteResponse));
                    this.store$.dispatch(new AuditorInviteActions.SetReturnUrl(returnUrl));

                    return of(true);
                }),
                catchError((e) => failed())
            );
    }

}
