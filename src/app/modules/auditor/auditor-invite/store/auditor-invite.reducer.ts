import {createSelector, MemoizedSelector} from '@ngrx/store';
import {AuditorInviteActions, AuditorInviteActionTypes} from './auditor-invite.actions';
import {IAuditorInvite} from '../interfaces/auditor-invite.interface';

export namespace AuditorInviteReducer {

    export interface IState {
        inviteResponse: IAuditorInvite.InviteResponse;
        returnUrl: string;
    }

    const initialState: IState = {
        inviteResponse: null,
        returnUrl: null,
    };

    export interface ISelects {
        inviteResponse: (state: IState) => IAuditorInvite.InviteResponse;
        returnUrl: (state: IState) => string;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {
            inviteResponse = createSelector(
                selector,
                ((state: IState) => state.inviteResponse),
            );

            returnUrl = createSelector(
                selector,
                ((state: IState) => state.returnUrl),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = { ...initialState }, action: AuditorInviteActions.All): IState {

        switch (action.type) {

            case AuditorInviteActionTypes.SET_INVITE_RESPONSE: {

                return {
                    ...state,
                    inviteResponse: action.payload ? {...action.payload} : null,
                };
            }

            case AuditorInviteActionTypes.SET_RETURN_URL: {

                return {
                    ...state,
                    returnUrl: action.payload,
                };
            }

            case AuditorInviteActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
