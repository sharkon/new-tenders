import {AuditorInviteReducer} from './auditor-invite.reducer';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {AppStore} from '../../../../store/app.store';

export namespace AuditorInviteStore {

    export const FEATURE_UID = '[AuditorInviteStore] FEATURE_UID';
    export const MODULE_UID = '[AuditorInviteStore] MODULE_UID';

    export const SERVICES_URL = {
        invite: 'auditor/invite',
    };

    export const SERVICES_UID = {
        invite: '[AuditorInviteStore][SERVICES_UID] invite',
    };

    interface IStore {
        selfStore: AuditorInviteReducer.IState;
    }

    export interface IState extends AppStore.IState {
        auditorInviteStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: AuditorInviteReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = AuditorInviteReducer.createStoreSelector(selfSelector);

    }

}
