import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, withLatestFrom } from 'rxjs/operators';
import { AuditorInviteActions } from './auditor-invite.actions';
import { AuditorInviteActionTypes } from './auditor-invite.actions';
import { AuditorInviteStore } from './auditor-invite.store';
import { CrudServiceActions, CrudServiceActionTypes } from '../../../../services/crud/store/crud-service.actions';
import { IAuditorInvite } from '../interfaces/auditor-invite.interface';
import { AppAuthActions } from '../../../../store/auth/app-auth.actions';
import { AuthService } from '../../../../services/auth/auth.service';
import { IAuth } from '../../../../interfaces/auth.interface';

@Injectable()
export class AuditorInviteEffects {

    @Effect()
    getInviteSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
            map((action: CrudServiceActions.GetItemSuccess) => action.payload),
            filter(payload => payload.uid === AuditorInviteStore.SERVICES_UID.invite),
            map((payload: any) => {

                const inviteResponse = payload.item as IAuditorInvite.InviteResponse;
                return new AuditorInviteActions.SetInviteResponse(inviteResponse);
            })
        );

    @Effect()
    createInviteSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CREATE_SUCCESS),
            map((action: CrudServiceActions.CreateSuccess) => action.payload),
            filter(payload => payload.uid === AuditorInviteStore.SERVICES_UID.invite),
            withLatestFrom(
                this.store$.pipe(select(AuditorInviteStore.Selects.self.returnUrl))
            ),
            map(([payload, returnUrl]: [any, string]) => {

                const response: IAuth.IAuditorSignInResponse = payload.item;
                this.authService.setToken(response.token.key, response.liveTime || 30);

                return new AppAuthActions.LogInSuccess(returnUrl || '/');
            })
        );

    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(AuditorInviteActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        AuditorInviteStore.SERVICES_UID.invite,
                    ]);
                })
            );

    constructor(private actions$: Actions,
                private store$: Store<AuditorInviteStore.IState>,
                private authService: AuthService) {
    }
}
