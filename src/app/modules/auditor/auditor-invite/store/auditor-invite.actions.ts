import {Action} from '@ngrx/store';
import {IAuditorInvite} from '../interfaces/auditor-invite.interface';

export namespace AuditorInviteActionTypes {

    export const SET_INVITE_RESPONSE = '[AuditorInvite] SET_INVITE_RESPONSE';
    export const SET_RETURN_URL = '[AuditorInvite] SET_RETURN_URL';
    export const DESTROY_STATE = '[AuditorInvite] DESTROY_STATE';
}

export namespace AuditorInviteActions {

    export class SetInviteResponse implements Action {

        readonly type = AuditorInviteActionTypes.SET_INVITE_RESPONSE;

        constructor(public payload: IAuditorInvite.InviteResponse) {
        }
    }

    export class SetReturnUrl implements Action {

        readonly type = AuditorInviteActionTypes.SET_RETURN_URL;

        constructor(public payload: string) {
        }
    }

    export class DestroyState implements Action {

        readonly type = AuditorInviteActionTypes.DESTROY_STATE;
    }

    export type All =
        SetInviteResponse
        | SetReturnUrl
        | DestroyState;

}
