import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditorRootComponent } from './auditor-root.component';

describe('AuditorRootComponent', () => {
  let component: AuditorRootComponent;
  let fixture: ComponentFixture<AuditorRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditorRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditorRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
