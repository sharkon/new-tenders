import { AuditorRootReducer } from './auditor-root.reducer';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import { AppStore } from '../../../../store/app.store';

export namespace AuditorRootStore {

    export const FEATURE_UID = '[AuditorRootStore] FEATURE_UID';
    export const MODULE_UID = '[AuditorRootStore] MODULE_UID';

    export const SERVICES_URL = {
        tender: 'cabinet/auditor/tender/',
    };

    export const SERVICES_UID = {
        tender: '[AuditorRootStore][SERVICES_UID] tender',
    };

    interface IStore {
        selfStore: AuditorRootReducer.IState;
    }

    export interface IState extends AppStore.IState {
        auditorRootStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: AuditorRootReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = AuditorRootReducer.createStoreSelector(selfSelector);
    }
}
