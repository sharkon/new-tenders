import { Action } from '@ngrx/store';
import { ITender } from '../../../../interfaces/tender.interface';

export namespace AuditorRootActionTypes {

    export const SET_TENDER = '[AuditorRoot] SET_TENDER';
    export const DESTROY_STATE = '[AuditorRoot] DESTROY_STATE';
}

export namespace AuditorRootActions {

    export class SetTender implements Action {

        readonly type = AuditorRootActionTypes.SET_TENDER;

        constructor(public payload: ITender.IModelForAuditor) {
        }
    }

    export class DestroyState implements Action {

        readonly type = AuditorRootActionTypes.DESTROY_STATE;
    }

    export type All =
        DestroyState
        | SetTender;

}
