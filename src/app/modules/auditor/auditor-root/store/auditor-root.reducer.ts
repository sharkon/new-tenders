import { createSelector, MemoizedSelector } from '@ngrx/store';
import { AuditorRootActions, AuditorRootActionTypes } from './auditor-root.actions';
import { ICrud } from '../../../../interfaces/crud.interface';
import { ITender } from '../../../../interfaces/tender.interface';

export namespace AuditorRootReducer {

    export interface IState {
        tender: ITender.IModelForAuditor;
    }

    const initialState: IState = {
        tender: null,
    };

    export interface ISelects {
        tender: (state: IState) => ITender.IModelForAuditor;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            tender = createSelector(
                selector,
                ((state: IState) => state.tender),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = { ...initialState }, action: AuditorRootActions.All): IState {

        switch (action.type) {

            case AuditorRootActionTypes.SET_TENDER: {

                const tender: ITender.IModelForAuditor = action.payload ? { ...action.payload } : null;

                return {
                    ...state,
                    tender,
                };
            }

            case AuditorRootActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
