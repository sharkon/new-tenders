import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, withLatestFrom, mergeMap } from 'rxjs/operators';
import { RouterNavigationAction, ROUTER_NAVIGATION } from '@ngrx/router-store';
import { AuditorRootActions } from './auditor-root.actions';
import { AuditorRootActionTypes } from './auditor-root.actions';
import { AuditorRootStore } from './auditor-root.store';
import { RouterStateUrl } from '../../../../providers/custom-router-state-serializer';
import { CrudServiceActions, CrudServiceActionTypes } from '../../../../services/crud/store/crud-service.actions';
import { AuditorTenderStore } from '../../auditor-tender/store/auditor-tender.store';
import { AuditorDecisionStore } from '../../auditor-decision/store/auditor-decision.store';
import { ITender } from '../../../../interfaces/tender.interface';
import { AppStore } from '../../../../store/app.store';
import { Router } from '@angular/router';
import { IUser } from '../../../../interfaces/user.interface';
import {AuditorOfferStore} from '../../auditor-offer/store/auditor-offer.store';

@Injectable()
export class AuditorRootEffects {

    @Effect()
    onRoute$: Observable<Action> =
        this.actions$.pipe(
            ofType(ROUTER_NAVIGATION),
            map((action: RouterNavigationAction<RouterStateUrl>) => action.payload),
            filter(payload => {
                return payload.routerState.data.module === AuditorTenderStore.MODULE_UID
                    || payload.routerState.data.module === AuditorDecisionStore.MODULE_UID
                    || payload.routerState.data.module === AuditorOfferStore.MODULE_UID;
            }),
            withLatestFrom(
                this.store$.pipe(select(AppStore.Selects.appAuth.user)),
            ),
            mergeMap(([payload, user]) => {

                const actions: Action[] = [];

                if (user && user.role.indexOf(IUser.USER_ROLES.AUDITOR) > -1) {
                    actions.push(new CrudServiceActions.GetItem({
                        url: `${AuditorRootStore.SERVICES_URL.tender}`,
                        uid: AuditorRootStore.SERVICES_UID.tender,
                    }));
                }

                return actions;
            }),
        );

    @Effect()
    loadTenderSuccess$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
                map((action: CrudServiceActions.GetItemSuccess) => action.payload),
                filter(payload => payload.uid === AuditorRootStore.SERVICES_UID.tender),
                map(payload => {

                    const tender = <ITender.IModelForAuditor>payload.item;
                    return new AuditorRootActions.SetTender(tender);
                }),
            );



    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(AuditorRootActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        AuditorRootStore.SERVICES_UID.tender,

                    ]);
                })
            );

    constructor(private actions$: Actions,
                private store$: Store<AppStore.IState>,
                private router: Router) {
    }
}
