import { ChangeDetectionStrategy, Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AuditorRootStore } from './store/auditor-root.store';
import { AuditorRootActions } from './store/auditor-root.actions';

@Component({
    selector: 'app-auditor-root',
    templateUrl: './auditor-root.component.html',
    styleUrls: ['./auditor-root.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuditorRootComponent implements OnInit, OnDestroy {

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    constructor(private store$: Store<AuditorRootStore.IState>) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new AuditorRootActions.DestroyState());
    }

}
