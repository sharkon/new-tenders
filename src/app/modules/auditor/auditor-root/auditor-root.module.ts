import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuditorRootComponent} from './auditor-root.component';
import {RouterModule} from '@angular/router';
import {AuditorRootStore} from './store/auditor-root.store';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {AuditorRootEffects} from './store/auditor-root.effects';

@NgModule({
    declarations: [AuditorRootComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: AuditorRootComponent,
                data: {
                    module: AuditorRootStore.MODULE_UID,
                },
                children: [
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'decision',
                    },
                    {
                        path: 'decision',
                        loadChildren: '../auditor-decision/auditor-decision.module#AuditorDecisionModule',
                    },
                    {
                        path: 'tender',
                        loadChildren: '../auditor-tender/auditor-tender.module#AuditorTenderModule',
                    },
                    {
                        path: 'offer/:offerId',
                        loadChildren: '../auditor-offer/auditor-offer.module#AuditorOfferModule',
                    },
                ]
            },
        ]),
        StoreModule.forFeature(AuditorRootStore.FEATURE_UID, AuditorRootStore.mapReducers),
        EffectsModule.forFeature([
            AuditorRootEffects,
        ]),
    ]
})
export class AuditorRootModule {
}
