import { Action } from '@ngrx/store';
import {ITender} from '../../../../interfaces/tender.interface';

export namespace AuditorDecisionActionTypes {

    export const SET_STAT = '[AuditorDescision] SET_STAT';

    export const PATCH_PRINTED = '[AuditorDescision] PATCH_PRINTED';
    export const UPDATE_TENDER = '[AuditorDescision] UPDATE_TENDER';
    export const UNLOCK_TENDER = '[AuditorDescision] UNLOCK_TENDER';
    export const PRINT_TENDER = '[AuditorDescision] PRINT_TENDER';
    export const DESTROY_STATE = '[AuditorDescision] DESTROY_STATE';
}

export namespace AuditorDecisionActions {

    export class SetStat implements Action {

        readonly type = AuditorDecisionActionTypes.SET_STAT;

        constructor(public payload: ITender.ITenderStatistics) {
        }
    }

    export class PatchPrinted implements Action {

        readonly type = AuditorDecisionActionTypes.PATCH_PRINTED;

        constructor(public payload: boolean) {
        }
    }

    export class UpdateTender implements Action {

        readonly type = AuditorDecisionActionTypes.UPDATE_TENDER;

        constructor(public payload: string) {
        }
    }

    export class UnlockTender implements Action {

        readonly type = AuditorDecisionActionTypes.UNLOCK_TENDER;
    }

    export class PrintTender implements Action {

        readonly type = AuditorDecisionActionTypes.PRINT_TENDER;
    }

    export class DestroyState implements Action {

        readonly type = AuditorDecisionActionTypes.DESTROY_STATE;
    }

    export type All =
        DestroyState
        | SetStat
        | PatchPrinted
        | UpdateTender
        | UnlockTender
        | PrintTender;

}
