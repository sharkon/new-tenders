import { AuditorDecisionReducer } from './auditor-decision.reducer';
import { AppStore } from '../../../../store/app.store';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

export namespace AuditorDecisionStore {

    export const FEATURE_UID = '[AuditorDecisionStore] FEATURE_UID';
    export const MODULE_UID = '[AuditorDecisionStore] MODULE_UID';

    const crudUrl = 'cabinet/auditor/tender/';

    export const SERVICES_URL = {
        stat: `${crudUrl}stats/`,
        update: `${crudUrl}change-state/`,
        unlock: `${crudUrl}change-state/`,
        print: `${crudUrl}preopen/print/`,

    };

    export const SERVICES_UID = {
        stat: '[AuditorDecisionStore][SERVICES_UID] stat',
        update: '[AuditorDecisionStore][SERVICES_UID] update',
        unlock: '[AuditorDecisionStore][SERVICES_UID] unlock',
        print: '[AuditorDecisionStore][SERVICES_UID] print',
    };

    interface IStore {
        selfStore: AuditorDecisionReducer.IState;
    }

    export interface IState extends AppStore.IState {
        auditorDecisionStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: AuditorDecisionReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = AuditorDecisionReducer.createStoreSelector(selfSelector);
    }
}
