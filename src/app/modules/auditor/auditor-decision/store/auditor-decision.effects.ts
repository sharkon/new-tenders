import {Inject, Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {AuditorDecisionActions, AuditorDecisionActionTypes} from './auditor-decision.actions';
import {AuditorDecisionStore} from './auditor-decision.store';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../services/crud/store/crud-service.actions';
import {TranslateService} from '../../../../services/translate/translate.service';
import {IFile} from '../../../../interfaces/file.interface';
import {AppFileUtils} from '../../../../utils/app-file.utils';
import {DOCUMENT} from '@angular/common';
import {AuditorRootStore} from '../../auditor-root/store/auditor-root.store';
import {ROUTER_NAVIGATED, RouterNavigationAction} from '@ngrx/router-store';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';
import {ITender} from '../../../../interfaces/tender.interface';
import {ICrud} from '../../../../interfaces/crud.interface';
import {IError} from '../../../../interfaces/error.interface';
import {AppErrorsConst} from '../../../../consts/app-errors.const';
import {AppAuthActions} from '../../../../store/auth/app-auth.actions';
import {AppRouterActions} from '../../../../store/router/app-router.actions';

@Injectable()
export class AuditorDecisionEffects {

    get tenderMapMessage(): IError.IBackendTranslateError {

        const mapMessage: IError.IBackendTranslateError = {
            'endDate': {
                'endDate': this.ts.data.T.newTender.endDate,
            },
        };

        Object.keys(mapMessage).forEach(key => {
            mapMessage[key][AppErrorsConst.BACKEND_ERROR_TYPES.INVALID_VALUE] = this.ts.data.T.formErrors.invalidValue;
            mapMessage[key][AppErrorsConst.BACKEND_ERROR_TYPES.IS_NULL] = this.ts.data.T.formErrors.invalidValue;
        });

        return mapMessage;
    }

    @Effect()
    onRoute$: Observable<Action> =
        this.actions$.pipe(
            ofType(ROUTER_NAVIGATED),
            map((action: RouterNavigationAction<RouterStateUrl>) => action.payload),
            filter(payload => payload.routerState.data.module === AuditorDecisionStore.MODULE_UID),
            map(payload => {

                return new CrudServiceActions.GetItem({
                    url: `${AuditorDecisionStore.SERVICES_URL.stat}`,
                    uid: AuditorDecisionStore.SERVICES_UID.stat,
                });
            }),
        );

    @Effect()
    getStatSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
            map((action: CrudServiceActions.GetItemSuccess) => action.payload),
            filter(payload => payload.uid === AuditorDecisionStore.SERVICES_UID.stat),
            map(payload => {

                return new AuditorDecisionActions.SetStat(<ITender.ITenderStatistics>payload.item);
            })
        );

    @Effect()
    updateTender$: Observable<Action> =
        this.actions$.pipe(
            ofType(AuditorDecisionActionTypes.UPDATE_TENDER),
            map((action: AuditorDecisionActions.UpdateTender) => action.payload),
            map((payload) => {

                return new CrudServiceActions.Update({
                    url: `${AuditorDecisionStore.SERVICES_URL.update}`,
                    uid: AuditorDecisionStore.SERVICES_UID.update,
                    item: <ICrud.IModel>{
                        id: null,
                        endDate: payload,
                    },
                    mapMessage: this.tenderMapMessage,
                });
            })
        );

    @Effect()
    updateSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.UPDATE_SUCCESS),
            map((action: CrudServiceActions.UpdateSuccess) => action.payload),
            filter(payload => payload.uid === AuditorDecisionStore.SERVICES_UID.update),
            map(payload => {

                return new AppAuthActions.LogOut('/');
            })
        );

    @Effect()
    unlockTender$: Observable<Action> =
        this.actions$.pipe(
            ofType(AuditorDecisionActionTypes.UNLOCK_TENDER),
            map((action) => {

                return new CrudServiceActions.Create({
                    url: `${AuditorDecisionStore.SERVICES_URL.unlock}`,
                    uid: AuditorDecisionStore.SERVICES_UID.unlock,
                    item: null,
                    deniedNotifySuccess: true,
                });
            })
        );

    @Effect()
    unlockSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CREATE_SUCCESS),
            map((action: CrudServiceActions.UpdateSuccess) => action.payload),
            filter(payload => payload.uid === AuditorDecisionStore.SERVICES_UID.unlock),
            map(payload => {

                return new AppRouterActions.Go({
                    path: [`/auditor/tender`],
                    extras: {
                        replaceUrl: true,
                    }
                });
            })
        );

    @Effect()
    printTender$: Observable<Action> =
        this.actions$.pipe(
            ofType(AuditorDecisionActionTypes.PRINT_TENDER),
            map((action) => {

                const lang = this.ts.lang;

                return new CrudServiceActions.GetItem({
                    url: `${AuditorDecisionStore.SERVICES_URL.print}?lang=${lang}`,
                    uid: AuditorDecisionStore.SERVICES_UID.print
                });
            })
        );

    @Effect()
    printTenderSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
            map((action: CrudServiceActions.GetItemSuccess) => action.payload),
            filter(payload => payload.uid === AuditorDecisionStore.SERVICES_UID.print),
            map((payload: any) => {

                const file: IFile.IFileResponse = <IFile.IFileResponse>payload.item;
                AppFileUtils.downloadFile(file ? file.url : '', this.document);

                return new AuditorDecisionActions.PatchPrinted(true);
            })
        );

    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(AuditorDecisionActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        AuditorDecisionStore.SERVICES_UID.stat,
                        AuditorDecisionStore.SERVICES_UID.update,
                        AuditorDecisionStore.SERVICES_UID.unlock,
                        AuditorDecisionStore.SERVICES_UID.print,
                    ]);
                })
            );

    constructor(private actions$: Actions,
                private store$: Store<AuditorRootStore.IState>,
                private ts: TranslateService,
                @Inject(DOCUMENT) private document: Document) {
    }
}
