import {createSelector, MemoizedSelector} from '@ngrx/store';
import {AuditorDecisionActions, AuditorDecisionActionTypes} from './auditor-decision.actions';
import {ITender} from '../../../../interfaces/tender.interface';

export namespace AuditorDecisionReducer {

    export interface IState {
        printed: boolean;
        statistics: ITender.ITenderStatistics;
    }

    const initialState: IState = {
        printed: null,
        statistics: null,
    };

    export interface ISelects {
        printed: (state: IState) => boolean;
        statistics: (state: IState) => ITender.ITenderStatistics;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            printed = createSelector(
                selector,
                ((state: IState) => state.printed),
            );

            statistics = createSelector(
                selector,
                ((state: IState) => state.statistics),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = {...initialState}, action: AuditorDecisionActions.All): IState {

        switch (action.type) {

            case AuditorDecisionActionTypes.SET_STAT: {

                const statistics: ITender.ITenderStatistics = action ? {...action.payload} : null;

                return {
                    ...state,
                    statistics,
                };
            }

            case AuditorDecisionActionTypes.PATCH_PRINTED: {

                return {
                    ...state,
                    printed: action.payload,
                };
            }

            case AuditorDecisionActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
