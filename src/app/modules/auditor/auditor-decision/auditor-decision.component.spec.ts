import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditorDecisionComponent } from './auditor-decision.component';

describe('AuditorDecisionComponent', () => {
  let component: AuditorDecisionComponent;
  let fixture: ComponentFixture<AuditorDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditorDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditorDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
