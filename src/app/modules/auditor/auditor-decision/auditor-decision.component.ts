import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AuditorDecisionStore} from './store/auditor-decision.store';
import {AuditorDecisionActions} from './store/auditor-decision.actions';
import {ITender} from '../../../interfaces/tender.interface';
import {TranslateService} from '../../../services/translate/translate.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NotifyService} from '../../../services/notify/notify.service';
import {AppStore} from '../../../store/app.store';
import {AuditorRootStore} from '../auditor-root/store/auditor-root.store';
import {AppConst} from '../../../consts/app.const';
import * as moment from 'moment';

@Component({
    selector: 'app-auditor-decision',
    templateUrl: './auditor-decision.component.html',
    styleUrls: ['./auditor-decision.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuditorDecisionComponent implements OnInit, OnDestroy {

    tender: ITender.IModelForAuditor;
    statistics: ITender.ITenderStatistics;

    printed: boolean;
    lockButtons: boolean;

    pageOpenDate: moment.Moment;
    formGroup: FormGroup;

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get PERIOD_TYPE() {
        return ITender.PERIOD;
    }

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    constructor(private store$: Store<AuditorRootStore.IState>,
                private ts: TranslateService,
                private formBuilder: FormBuilder,
                private cdr: ChangeDetectorRef,
                private notifyService: NotifyService) {
    }

    ngOnInit() {

        this.pageOpenDate = moment();

        this.formGroup = this.formBuilder.group({
            endDate: [null],
        });

        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new AuditorDecisionActions.DestroyState());
    }

    private initStore() {

        this.subs = this.store$.pipe(select(AuditorDecisionStore.Selects.self.statistics))
            .subscribe((statistics: ITender.ITenderStatistics) => {

                this.statistics = statistics;
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(AuditorRootStore.Selects.self.tender))
            .subscribe((tender: ITender.IModelForAuditor) => {

                this.tender = <ITender.IModelForAuditor>tender;

                if (tender && tender.endDate) {
                    this.formGroup.controls.endDate.setValue(moment(), {emitEvent: false});
                }

                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(AuditorDecisionStore.Selects.self.printed))
            .subscribe((printed: boolean) => {

                this.printed = printed;
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(AppStore.Selects.appBlock.locks))
            .subscribe(locks => {

                this.lockButtons = locks[AuditorDecisionStore.SERVICES_UID.update]
                    || locks[AuditorDecisionStore.SERVICES_UID.unlock]
                    || locks[AuditorDecisionStore.SERVICES_UID.print];

                this.cdr.detectChanges();
            });
    }

    onUpdate() {

        const confirm = this.notifyService.confirm({
            dialogTitle: this.ts.data.T.dialogs.updateTenderQuestion,
        });

        if (confirm) {
            const endDate = this.formGroup.controls.endDate.value.format();
            this.store$.dispatch(new AuditorDecisionActions.UpdateTender(endDate));
        }
    }

    onUnlock() {

        const confirm = this.notifyService.confirm({
            dialogTitle: this.ts.data.T.dialogs.unlockTenderQuestion,
        });

        if (confirm) {
            this.store$.dispatch(new AuditorDecisionActions.UnlockTender());
        }
    }

    onPrint() {

        this.store$.dispatch(new AuditorDecisionActions.PrintTender());
    }
}
