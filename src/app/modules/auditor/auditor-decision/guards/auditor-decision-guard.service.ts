import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {ITender} from '../../../../interfaces/tender.interface';
import {AuditorRootStore} from '../../auditor-root/store/auditor-root.store';

@Injectable()
export class AuditorDecisionGuard implements CanActivate {

    constructor(private store$: Store<AuditorRootStore.IState>,
                private router: Router) {
    }

    canActivate(): Observable<boolean> | Promise<boolean> | boolean {

        return this.store$.pipe(
            select(AuditorRootStore.Selects.self.tender),
            filter(tender => !!tender),
            map((tender: ITender.IModelForAuditor) => {

                if (tender.state !== ITender.STATE_TYPES.CLOSE) {

                    this.router.navigateByUrl('/auditor/tender', {
                        replaceUrl: true,
                    });
                }

                return tender.state === ITender.STATE_TYPES.CLOSE;
            })
        );
    }
}
