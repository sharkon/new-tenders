import { TestBed, async, inject } from '@angular/core/testing';

import { AuditorDecisionGuard } from './auditor-decision-guard.service';

describe('AuditorDecisionGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [AuditorDecisionGuard]
        });
    });

    it('should ...', inject([AuditorDecisionGuard], (guard: AuditorDecisionGuard) => {
        expect(guard).toBeTruthy();
    }));
});
