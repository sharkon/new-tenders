import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditorDecisionComponent } from './auditor-decision.component';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { AuditorDecisionStore } from './store/auditor-decision.store';
import { EffectsModule } from '@ngrx/effects';
import { AuditorDecisionEffects } from './store/auditor-decision.effects';
import { PipesModule } from '../../../pipes/pipes.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DesignDateTimeModule } from '../../design/design-date-time/design-date-time.module';
import { AuditorDecisionGuard } from './guards/auditor-decision-guard.service';
import {DesignPreloaderModule} from '../../design/design-preloader/design-preloader.module';

@NgModule({
    declarations: [AuditorDecisionComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: AuditorDecisionComponent,
                canActivate: [AuditorDecisionGuard],
                data: {
                    module: AuditorDecisionStore.MODULE_UID,
                },
            }
        ]),
        StoreModule.forFeature(AuditorDecisionStore.FEATURE_UID, AuditorDecisionStore.mapReducers),
        EffectsModule.forFeature([
            AuditorDecisionEffects,
        ]),
        PipesModule,
        ReactiveFormsModule,
        DesignDateTimeModule,
        DesignPreloaderModule,
    ],
    providers: [
        AuditorDecisionGuard,
    ],
})
export class AuditorDecisionModule {
}
