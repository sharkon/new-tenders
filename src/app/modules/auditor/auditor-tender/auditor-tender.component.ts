import { ChangeDetectionStrategy, Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ITender } from '../../../interfaces/tender.interface';
import { Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AuditorTenderStore } from './store/auditor-tender.store';
import { AuditorTenderActions } from './store/auditor-tender.actions';
import { IMultilang } from '../../../interfaces/multilang.interface';
import { TranslateService } from '../../../services/translate/translate.service';
import { IOffer } from '../../../interfaces/offer.interface';
import { AppStore } from '../../../store/app.store';
import { AuditorRootStore } from '../auditor-root/store/auditor-root.store';
import {AppConst} from '../../../consts/app.const';

@Component({
    selector: 'app-auditor-tender',
    templateUrl: './auditor-tender.component.html',
    styleUrls: ['./auditor-tender.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuditorTenderComponent implements OnInit, OnDestroy {

    tender: ITender.IModelForAuditor;
    offers: IOffer.IListModelForAuditorTender[];
    proposals: IOffer.IListModelForAuditorTender[];
    questions: IOffer.IListModelForAuditorTender[];
    lockButtons: boolean;

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get PERIOD_TYPE() {
        return ITender.PERIOD;
    }

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    constructor(private store$: Store<AuditorTenderStore.IState>,
                private translateService: TranslateService,
                private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new AuditorTenderActions.DestroyState());
    }

    private initStore() {

        this.subs = this.store$.pipe(select(AuditorRootStore.Selects.self.tender))
            .subscribe((tender: ITender.IModelForAuditor) => {

                this.tender = tender;
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(AuditorTenderStore.Selects.self.offers))
            .subscribe((offers: IOffer.IListModelForAuditorTender[]) => {

                this.offers = offers;
                this.proposals = [];
                this.questions = [];

                if (offers) {

                    offers.forEach(offer => {

                        if (offer.state === IOffer.STATE_TYPES.PROPOSAL_PUBLISH) {
                            this.proposals.push(offer);
                        }

                        if (offer.state === IOffer.STATE_TYPES.CHAT_PUBLISH) {
                            this.questions.push(offer);
                        }
                    });
                }

                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(AppStore.Selects.appBlock.locks))
            .subscribe(locks => {

                this.lockButtons = locks[AuditorTenderStore.SERVICES_UID.print];
                this.cdr.detectChanges();
            });
    }

    onPrint(): void {

        this.store$.dispatch(new AuditorTenderActions.PrintTender());
    }

}
