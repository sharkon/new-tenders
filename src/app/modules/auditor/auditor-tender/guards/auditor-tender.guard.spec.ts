import { TestBed, async, inject } from '@angular/core/testing';
import { AuditorTenderGuard } from './auditor-tender.guard';

describe('AuditorTenderGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [AuditorTenderGuard]
        });
    });

    it('should ...', inject([AuditorTenderGuard], (guard: AuditorTenderGuard) => {
        expect(guard).toBeTruthy();
    }));
});
