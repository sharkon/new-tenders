import { Action } from '@ngrx/store';
import { IOffer } from '../../../../interfaces/offer.interface';

export namespace AuditorTenderActionTypes {

    export const SET_OFFERS = '[AuditorTender] SET_OFFERS';
    export const PRINT_TENDER = '[AuditorTender] PRINT_TENDER';
    export const DESTROY_STATE = '[AuditorTender] DESTROY_STATE';
}

export namespace AuditorTenderActions {

    export class SetOffers implements Action {

        readonly type = AuditorTenderActionTypes.SET_OFFERS;

        constructor(public payload: IOffer.IListModelForAuditorTender[]) {
        }
    }

    export class PrintTender implements Action {

        readonly type = AuditorTenderActionTypes.PRINT_TENDER;
    }

    export class DestroyState implements Action {

        readonly type = AuditorTenderActionTypes.DESTROY_STATE;
    }

    export type All =
        DestroyState
        | SetOffers
        | PrintTender;

}
