import { createSelector, MemoizedSelector } from '@ngrx/store';
import { AuditorTenderActions, AuditorTenderActionTypes } from './auditor-tender.actions';
import { IOffer } from '../../../../interfaces/offer.interface';

export namespace AuditorTenderReducer {

    export interface IState {
        offers: IOffer.IListModelForAuditorTender[];
    }

    const initialState: IState = {
        offers: null,
    };

    export interface ISelects {
        offers: (state: IState) => IOffer.IListModelForAuditorTender[];
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            offers = createSelector(
                selector,
                ((state: IState) => state.offers),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = { ...initialState }, action: AuditorTenderActions.All): IState {

        switch (action.type) {

            case AuditorTenderActionTypes.SET_OFFERS: {

                const offers: IOffer.IListModelForAuditorTender[] = action.payload ? action.payload.slice() : null;

                return {
                    ...state,
                    offers,
                };
            }

            case AuditorTenderActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
