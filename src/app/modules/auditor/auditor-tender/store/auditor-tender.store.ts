import { AuditorTenderReducer } from './auditor-tender.reducer';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import { AppStore } from '../../../../store/app.store';

export namespace AuditorTenderStore {

    export const FEATURE_UID = '[AuditorTenderStore] FEATURE_UID';
    export const MODULE_UID = '[AuditorTenderStore] MODULE_UID';

    export const CRUD_STORE_UID = '[AuditorTenderStore] CRUD_STORE_UID';

    const crudUrl = 'cabinet/auditor/tender/';

    export const SERVICES_URL = {
        offers: (offerId?: string | number): string => {
            return offerId ? `${crudUrl}proposal/${offerId}` : `${crudUrl}proposal`;
        },
        print: `${crudUrl}opened/print`,
    };

    export const SERVICES_UID = {
        offers: '[AuditorTenderStore][SERVICES_UID] offer',
        print: '[AuditorTenderStore][SERVICES_UID] print',
    };

    interface IStore {
        selfStore: AuditorTenderReducer.IState;

    }

    export interface IState extends AppStore.IState {
        auditorTenderStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: AuditorTenderReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = AuditorTenderReducer.createStoreSelector(selfSelector);

    }

}
