import { Injectable, Inject } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, mergeMap, withLatestFrom, tap } from 'rxjs/operators';
import { ROUTER_NAVIGATED, RouterNavigationAction } from '@ngrx/router-store';
import { AuditorTenderActions } from './auditor-tender.actions';
import { AuditorTenderActionTypes } from './auditor-tender.actions';
import { AuditorTenderStore } from './auditor-tender.store';
import { RouterStateUrl } from '../../../../providers/custom-router-state-serializer';
import { CrudServiceActions, CrudServiceActionTypes } from '../../../../services/crud/store/crud-service.actions';
import { IOffer } from '../../../../interfaces/offer.interface';
import { TranslateService } from '../../../../services/translate/translate.service';
import { IFile } from '../../../../interfaces/file.interface';
import { AppFileUtils } from '../../../../utils/app-file.utils';
import { DOCUMENT } from '@angular/common';
import { AuditorRootStore } from '../../auditor-root/store/auditor-root.store';

@Injectable()
export class AuditorTenderEffects {

    @Effect()
    onRoute$: Observable<Action> =
        this.actions$.pipe(
            ofType(ROUTER_NAVIGATED),
            map((action: RouterNavigationAction<RouterStateUrl>) => action.payload),
            filter(payload => {
                return payload.routerState.data.module === AuditorTenderStore.MODULE_UID;
            }),
            mergeMap((payload) => {

                return [
                    new CrudServiceActions.GetList({
                        url: `${AuditorTenderStore.SERVICES_URL.offers()}/`,
                        uid: AuditorTenderStore.SERVICES_UID.offers,
                    }),
                ];
            }),
        );


    @Effect()
    getOffersSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === AuditorTenderStore.SERVICES_UID.offers),
            map((payload: IOffer.IListForAuditorTender) => {

                return new AuditorTenderActions.SetOffers(<IOffer.IListModelForAuditorTender[]>payload.data);
            })
        );

    @Effect()
    printTender$: Observable<Action> =
        this.actions$.pipe(
            ofType(AuditorTenderActionTypes.PRINT_TENDER),
            map(() => {

                const lang = this.translateService.lang;

                return new CrudServiceActions.GetItem({
                    url: `${AuditorTenderStore.SERVICES_URL.print}?lang=${lang}`,
                    uid: AuditorTenderStore.SERVICES_UID.print
                });
            })
        );

    @Effect({dispatch: false})
    printTenderSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
            map((action: CrudServiceActions.GetItemSuccess) => action.payload),
            filter(payload => payload.uid === AuditorTenderStore.SERVICES_UID.print),
            tap((payload: any) => {

                const file: IFile.IFileResponse = <IFile.IFileResponse>payload.item;
                AppFileUtils.downloadFile(file ? file.url : '', this.document);
            })
        );

    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(AuditorTenderActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        AuditorTenderStore.SERVICES_UID.offers,
                        AuditorTenderStore.SERVICES_UID.print,
                    ]);
                })
            );

    constructor(private actions$: Actions,
        private store$: Store<AuditorTenderStore.IState>,
        private translateService: TranslateService,
        @Inject(DOCUMENT) private document: Document) {
    }
}
