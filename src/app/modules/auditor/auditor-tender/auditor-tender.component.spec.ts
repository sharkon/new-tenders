import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditorTenderComponent } from './auditor-tender.component';

describe('AuditorTenderComponent', () => {
  let component: AuditorTenderComponent;
  let fixture: ComponentFixture<AuditorTenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditorTenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditorTenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
