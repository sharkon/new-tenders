import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditorTenderComponent } from './auditor-tender.component';
import { RouterModule } from '@angular/router';
import { PipesModule } from '../../../pipes/pipes.module';
import { StoreModule } from '@ngrx/store';
import { AuditorTenderStore } from './store/auditor-tender.store';
import { EffectsModule } from '@ngrx/effects';
import { AuditorTenderEffects } from './store/auditor-tender.effects';
import { AuditorTenderGuard } from './guards/auditor-tender.guard';
import { DesignPreloaderModule } from '../../design/design-preloader/design-preloader.module';

@NgModule({
    declarations: [AuditorTenderComponent],
    imports: [
        CommonModule,
        PipesModule,
        RouterModule.forChild([
            {
                path: '',
                component: AuditorTenderComponent,
                canActivate: [AuditorTenderGuard],
                data: {
                    module: AuditorTenderStore.MODULE_UID,
                },
            }
        ]),
        StoreModule.forFeature(AuditorTenderStore.FEATURE_UID, AuditorTenderStore.mapReducers),
        EffectsModule.forFeature([
            AuditorTenderEffects,
        ]),
        PipesModule,
        DesignPreloaderModule
    ],
    providers: [
        AuditorTenderGuard,
    ],
})
export class AuditorTenderModule {
}
