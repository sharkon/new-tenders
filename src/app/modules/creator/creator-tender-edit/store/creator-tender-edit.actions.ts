import {Action} from '@ngrx/store';
import {ITender} from '../../../../interfaces/tender.interface';

export namespace CreatorTenderEditActionTypes {

    export const SET_CAN_DEACTIVATE = '[CreatorTenderEdit] SET_CAN_DEACTIVATE';

    export const IMPORT_TENDER = '[CreatorTenderEdit] IMPORT_TENDER';

    export const SAVE_TENDER = '[CreatorTenderEdit] SAVE_TENDER';
    export const PUBLISH_TENDER = '[CreatorTenderEdit] PUBLISH_TENDER';
    export const PRINT_TENDER = '[CreatorTenderEdit] PRINT_TENDER';

    export const DESTROY_STATE = '[CreatorTenderEdit] DESTROY_STATE';
}

export namespace CreatorTenderEditActions {

    export class SetCanDeactivate implements Action {

        readonly type = CreatorTenderEditActionTypes.SET_CAN_DEACTIVATE;

        constructor(public payload: boolean) {
        }
    }

    export class ImportTender implements Action {

        readonly type = CreatorTenderEditActionTypes.IMPORT_TENDER;

        constructor(public payload: ITender.IModel) {
        }
    }

    export class SaveTender implements Action {

        readonly type = CreatorTenderEditActionTypes.SAVE_TENDER;

        constructor(public payload: ITender.IModel) {
        }
    }

    export class PublishTender implements Action {

        readonly type = CreatorTenderEditActionTypes.PUBLISH_TENDER;

        constructor(public payload: ITender.IModel) {
        }
    }

    export class PrintTender implements Action {

        readonly type = CreatorTenderEditActionTypes.PRINT_TENDER;

        constructor(public payload: ITender.IModel) {
        }
    }

    export class DestroyState implements Action {

        readonly type = CreatorTenderEditActionTypes.DESTROY_STATE;
    }

    export type All =
        SetCanDeactivate
        | ImportTender
        | SaveTender
        | PublishTender
        | PrintTender
        | DestroyState;

}
