import {CreatorTenderEditReducer} from './creator-tender-edit.reducer';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {AppStore} from '../../../../store/app.store';

export namespace CreatorTenderEditStore {

    export const FEATURE_UID = '[CreatorTenderEditStore] FEATURE_UID';
    export const MODULE_UID = '[CreatorTenderEditStore] MODULE_UID';

    export const SERVICES_UID = {
        crud: '[CreatorTenderEditStore][SERVICES_UID] crud',
        publish: '[CreatorTenderEditStore][SERVICES_UID] publish',
        saveBeforePrint: '[CreatorTenderEditStore][SERVICES_UID] saveBeforePrint',
        getPrintPage: '[CreatorTenderEditStore][SERVICES_UID] getPrintPage',
    };

    export const SERVICES_URL = {

        crud: (tenderId: string | number): string => {
            const url = 'cabinet/creator/tender/';
            return tenderId ? `${url}${tenderId}` : url;
        },
    };

    interface IStore {
        selfStore: CreatorTenderEditReducer.IState;
    }

    export interface IState extends AppStore.IState {
        creatorTenderEditStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: CreatorTenderEditReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = CreatorTenderEditReducer.createStoreSelector(selfSelector);

    }

}

