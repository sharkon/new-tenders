import {Inject, Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {filter, map, mergeMap, tap, withLatestFrom} from 'rxjs/operators';
import {ROUTER_NAVIGATED, RouterNavigationAction} from '@ngrx/router-store';
import {CreatorTenderEditActions, CreatorTenderEditActionTypes} from './creator-tender-edit.actions';
import {CreatorTenderEditStore} from './creator-tender-edit.store';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../services/crud/store/crud-service.actions';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';
import {ITender} from '../../../../interfaces/tender.interface';
import {WINDOW} from '../../../../providers/window.providers';
import {ICrud} from '../../../../interfaces/crud.interface';
import {AppRouterActions} from '../../../../store/router/app-router.actions';
import {TranslateService} from '../../../../services/translate/translate.service';
import {IFile} from '../../../../interfaces/file.interface';
import {AppConst} from '../../../../consts/app.const';
import {NotifyService} from '../../../../services/notify/notify.service';
import {IError} from '../../../../interfaces/error.interface';
import {AppErrorsConst} from '../../../../consts/app-errors.const';
import {AppFileUtils} from '../../../../utils/app-file.utils';
import {DOCUMENT} from '@angular/common';
import {CreatorStore} from '../../store/creator.store';

@Injectable()
export class CreatorTenderEditEffects {

    get tenderMapMessage(): IError.IBackendTranslateError {

        const mapMessage: IError.IBackendTranslateError = {
            'startDate': {
                'startDate': this.ts.data.T.newTender.startDate,
            },
            'endDate': {
                'endDate': this.ts.data.T.newTender.endDate,
            },
            'period': {
                'period': this.ts.data.T.newTender.period,
            },
            'sellerGroupId': {
                'sellerGroupId': this.ts.data.T.tenders.groupSellers,
            },
            'currency': {
                'currency': this.ts.data.T.tenders.currency,
            },
            'curatorId': {
                'curatorId': this.ts.data.T.newTender.curator,
            },
        };

        Object.keys(mapMessage).forEach(key => {
            mapMessage[key][AppErrorsConst.BACKEND_ERROR_TYPES.INVALID_VALUE] = this.ts.data.T.formErrors.invalidValue;
            mapMessage[key][AppErrorsConst.BACKEND_ERROR_TYPES.IS_NULL] = this.ts.data.T.formErrors.invalidValue;
        });

        return mapMessage;
    }

    @Effect()
    onRoute$: Observable<Action> =
        this.actions$.pipe(
            ofType(ROUTER_NAVIGATED),
            map((action: RouterNavigationAction<RouterStateUrl>) => action.payload),
            filter(payload => {
                return payload.routerState.data.module === CreatorTenderEditStore.MODULE_UID;
            }),
            map((payload) => {

                const tenderId: string = payload.routerState.params['tenderId'];

                if (tenderId === AppConst.CRUD_NEW_ID) {

                    return new CreatorTenderEditActions.ImportTender(null);

                } else {

                    return new CrudServiceActions.GetItem({
                        uid: CreatorTenderEditStore.SERVICES_UID.crud,
                        url: CreatorTenderEditStore.SERVICES_URL.crud(tenderId),
                    });
                }
            }),
        );

    @Effect()
    importTender$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
            map((action: CrudServiceActions.GetItemSuccess) => action.payload),
            filter(payload => {
                return payload.uid === CreatorTenderEditStore.SERVICES_UID.crud;
            }),
            filter(payload => {
                const tender = <ITender.IModel>payload.item;
                return tender.state === ITender.STATE_TYPES.DRAFT;
            }),
            map(payload => {

                return new CreatorTenderEditActions.ImportTender(<ITender.IModel>payload.item);
            }),
        );

    @Effect()
    importTenderError$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM_FAILED),
            map((action: CrudServiceActions.GetItemFailed) => action.payload),
            filter(payload => {
                return payload.uid === CreatorTenderEditStore.SERVICES_UID.crud;
            }),
            map(payload => {

                return new CreatorTenderEditActions.SetCanDeactivate(true);
            }),
        );

    @Effect()
    importTenderLocked$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
            map((action: CrudServiceActions.GetItemSuccess) => action.payload),
            filter(payload => {
                return payload.uid === CreatorTenderEditStore.SERVICES_UID.crud;
            }),
            filter(payload => {
                const tender = <ITender.IModel>payload.item;
                return tender.state !== ITender.STATE_TYPES.DRAFT;
            }),
            mergeMap(payload => {

                this.notifyService.warning({
                    dialogContent: this.ts.data.T.newTenderDialogs.noEditPublish,
                });

                return [
                    new CreatorTenderEditActions.SetCanDeactivate(true),
                    new AppRouterActions.Go({
                        path: [`${CreatorStore.ROOT_URL}`],
                        extras: {
                            replaceUrl: true,
                        }
                    }),
                ];
            }),
        );

    @Effect()
    saveTender$: Observable<Action> =
        this.actions$.pipe(
            ofType(CreatorTenderEditActionTypes.SAVE_TENDER),
            map((action: CreatorTenderEditActions.SaveTender) => action.payload),
            map(tender => {

                const request: ICrud.ICRUDCreateRequest = {
                    url: CreatorTenderEditStore.SERVICES_URL.crud(tender.id),
                    uid: CreatorTenderEditStore.SERVICES_UID.crud,
                    item: tender,
                    mapMessage: this.tenderMapMessage,
                    deniedNotifySuccess: true,
                };

                if (tender.id) {
                    return new CrudServiceActions.Update(request);
                } else {
                    return new CrudServiceActions.Create(request);
                }
            }),
        );

    @Effect()
    saveTenderSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CREATE_SUCCESS, CrudServiceActionTypes.UPDATE_SUCCESS),
            map((action: CrudServiceActions.CreateSuccess | CrudServiceActions.UpdateSuccess) => action.payload),
            filter(payload => payload.uid === CreatorTenderEditStore.SERVICES_UID.crud),
            mergeMap(payload => {

                this.notifyService.success({
                    dialogTitle: this.ts.data.T.newTenderDialogs.noForgetPublish,
                    dialogContent: this.ts.data.T.newTenderDialogs.successSaved,
                });

                return [
                    new CreatorTenderEditActions.SetCanDeactivate(true),
                    new AppRouterActions.Go({
                        path: [`/creator/tender-edit/${payload.item.id}`],
                        extras: {
                            replaceUrl: true,
                        }
                    })
                ];
            })
        );

    @Effect({dispatch: false})
    saveTenderFailed$ =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CREATE_FAILED, CrudServiceActionTypes.UPDATE_FAILED),
            map((action: CrudServiceActions.CreateFailed | CrudServiceActions.UpdateFailed) => action.payload),
            filter(payload => {
                    return payload.uid === CreatorTenderEditStore.SERVICES_UID.crud
                        || payload.uid === CreatorTenderEditStore.SERVICES_UID.saveBeforePrint;
                }
            ),
            tap(payload => {

                this.notifyService.error({
                    dialogTitle: this.ts.data.T.dialogs.repeatAfterError,
                    dialogContent: this.ts.data.T.newTenderDialogs.notSaved,
                });
            })
        );

    @Effect()
    publishTender$: Observable<Action> =
        this.actions$.pipe(
            ofType(CreatorTenderEditActionTypes.PUBLISH_TENDER),
            map((action: CreatorTenderEditActions.PublishTender) => action.payload),
            map(tender => {

                const publishTender: ITender.IModel = {
                    ...tender,
                    state: ITender.STATE_TYPES.PUBLISH,
                };

                const request: ICrud.ICRUDCreateRequest = {
                    url: `${CreatorTenderEditStore.SERVICES_URL.crud(tender.id)}/publish`,
                    uid: CreatorTenderEditStore.SERVICES_UID.publish,
                    item: publishTender,
                    mapMessage: this.tenderMapMessage,
                    deniedNotifySuccess: true,
                };

                return new CrudServiceActions.Update(request);
            })
        );

    @Effect()
    publishTenderSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.UPDATE_SUCCESS),
            map((action: CrudServiceActions.UpdateSuccess) => action.payload),
            filter(payload => payload.uid === CreatorTenderEditStore.SERVICES_UID.publish),
            mergeMap(payload => {

                this.notifyService.success({
                    dialogTitle: this.ts.data.T.newTenderDialogs.successPublished,
                });

                return [
                    new CreatorTenderEditActions.SetCanDeactivate(true),
                    new AppRouterActions.Go({
                        path: [`${CreatorStore.ROOT_URL}`],
                        extras: {
                            replaceUrl: true,
                        }
                    }),
                ];
            })
        );

    @Effect({dispatch: false})
    publishTenderFailed$ =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.UPDATE_FAILED),
            map((action: CrudServiceActions.UpdateFailed) => action.payload),
            filter(payload => payload.uid === CreatorTenderEditStore.SERVICES_UID.publish),
            tap(payload => {

                this.notifyService.error({
                    dialogTitle: this.ts.data.T.dialogs.repeatAfterError,
                    dialogContent: this.ts.data.T.newTenderDialogs.notPublished,
                });
            }),
        );

    @Effect()
    printTender$: Observable<Action> =
        this.actions$.pipe(
            ofType(CreatorTenderEditActionTypes.PRINT_TENDER),
            map((action: CreatorTenderEditActions.PrintTender) => action.payload),
            map(tender => {

                return new CrudServiceActions.Update({
                    url: CreatorTenderEditStore.SERVICES_URL.crud(tender.id),
                    uid: CreatorTenderEditStore.SERVICES_UID.saveBeforePrint,
                    item: tender,
                    mapMessage: this.tenderMapMessage,
                    deniedNotifySuccess: true,
                });
            })
        );

    @Effect()
    saveBeforePrintTenderSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.UPDATE_SUCCESS),
            map((action: CrudServiceActions.UpdateSuccess) => action.payload),
            filter(payload => payload.uid === CreatorTenderEditStore.SERVICES_UID.saveBeforePrint),
            withLatestFrom(this.store$.pipe(select(CreatorTenderEditStore.Selects.self.tenderId))),
            mergeMap(([payload, tenderId]) => {

                return [
                    new CreatorTenderEditActions.SetCanDeactivate(true),
                    new CrudServiceActions.GetItem({
                        url: `${CreatorTenderEditStore.SERVICES_URL.crud(tenderId)}/print?lang=${this.ts.lang}`,
                        uid: CreatorTenderEditStore.SERVICES_UID.getPrintPage,
                    }),
                ];
            })
        );

    @Effect({dispatch: false})
    printTenderSuccess$ =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
            map((action: CrudServiceActions.GetItemSuccess) => action.payload),
            filter(payload => payload.uid === CreatorTenderEditStore.SERVICES_UID.getPrintPage),
            tap(payload => {

                const file: IFile.IFileResponse = <IFile.IFileResponse>payload.item;
                AppFileUtils.downloadFile(file ? file.url : '', this.document);
            }),
        );

    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(CreatorTenderEditActionTypes.DESTROY_STATE),
                map(action => {

                    return new CrudServiceActions.CancelRequests([
                        CreatorTenderEditStore.SERVICES_UID.crud,
                        CreatorTenderEditStore.SERVICES_UID.publish,
                        CreatorTenderEditStore.SERVICES_UID.saveBeforePrint,
                        CreatorTenderEditStore.SERVICES_UID.getPrintPage,
                    ]);
                })
            );

    constructor(private actions$: Actions,
                private store$: Store<CreatorTenderEditStore.IState>,
                private ts: TranslateService,
                private notifyService: NotifyService,
                @Inject(WINDOW) private window: Window,
                @Inject(DOCUMENT) private document: Document) {
    }
}
