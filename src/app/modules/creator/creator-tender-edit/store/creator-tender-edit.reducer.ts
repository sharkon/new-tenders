import {createSelector, MemoizedSelector} from '@ngrx/store';
import {CreatorTenderEditActions, CreatorTenderEditActionTypes} from './creator-tender-edit.actions';

export namespace CreatorTenderEditReducer {

    export interface IState {
        tenderId: number;
        canDeactivate: boolean;
    }

    const initialState: IState = {
        tenderId: null,
        canDeactivate: true,
    };

    export interface ISelects {
        tenderId: (state: IState) => number;
        canDeactivate: (state: IState) => boolean;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            tenderId = createSelector(
                selector,
                ((state: IState) => state.tenderId),
            );

            canDeactivate = createSelector(
                selector,
                ((state: IState) => state.canDeactivate),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = {...initialState}, action: CreatorTenderEditActions.All): IState {

        switch (action.type) {

            case CreatorTenderEditActionTypes.IMPORT_TENDER: {

                return {
                    ...state,
                    tenderId: action.payload ? action.payload.id : null,
                };
            }

            case CreatorTenderEditActionTypes.SET_CAN_DEACTIVATE: {

                return {
                    ...state,
                    canDeactivate: action.payload,
                };
            }

            case CreatorTenderEditActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
