import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreatorTenderEditComponent} from './creator-tender-edit.component';
import {PipesModule} from '../../../pipes/pipes.module';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {CreatorTenderEditStore} from './store/creator-tender-edit.store';
import {CreatorTenderEditEffects} from './store/creator-tender-edit.effects';
import {RouterModule} from '@angular/router';
import {ComponentDeactivateGuard} from '../../../guards/component-deactivate/component-deactivate.guard';
import {DesignPreloaderModule} from '../../design/design-preloader/design-preloader.module';
import {DesignBackModule} from '../../design/design-back/design-back.module';
import {DesignMultilangInputModule} from '../../design/design-multilang/design-multilang-input/design-multilang-input.module';
import {DesignFileUploaderModule} from '../../design/design-file-uploader/design-file-uploader.module';
import {Select2Module} from 'ng2-select2';
import {ReactiveFormsModule} from '@angular/forms';
import {DesignDateTimeModule} from '../../design/design-date-time/design-date-time.module';

@NgModule({
    declarations: [CreatorTenderEditComponent],
    imports: [
        CommonModule,
        PipesModule,
        DesignBackModule,
        DesignPreloaderModule,
        RouterModule.forChild([
            {
                path: '',
                component: CreatorTenderEditComponent,
                canDeactivate: [ComponentDeactivateGuard],
                data: {
                    module: CreatorTenderEditStore.MODULE_UID,
                }
            }
        ]),
        StoreModule.forFeature(CreatorTenderEditStore.FEATURE_UID, CreatorTenderEditStore.mapReducers),
        EffectsModule.forFeature([
            CreatorTenderEditEffects,
        ]),
        DesignMultilangInputModule,
        DesignFileUploaderModule,
        Select2Module,
        ReactiveFormsModule,
        DesignDateTimeModule,
    ],
})
export class CreatorTenderEditModule {
}
