import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {CreatorTenderEditStore} from './store/creator-tender-edit.store';
import {ActivatedRoute} from '@angular/router';
import {CreatorTenderEditActions, CreatorTenderEditActionTypes} from './store/creator-tender-edit.actions';
import {AppStore} from '../../../store/app.store';
import {FormArray, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {filter, map} from 'rxjs/operators';
import {Actions, ofType} from '@ngrx/effects';
import {ITender} from '../../../interfaces/tender.interface';
import {Select2OptionData} from 'ng2-select2';
import {CreatorStore} from '../store/creator.store';
import {UploadFile} from 'ngx-uploader';
import {IFile} from '../../../interfaces/file.interface';
import {AppFormUtils} from '../../../utils/app-form.utils';
import {AppConst} from '../../../consts/app.const';
import * as moment from 'moment';
import {NotifyService} from '../../../services/notify/notify.service';
import {TranslateService} from '../../../services/translate/translate.service';

@Component({
    selector: 'app-creator-tender-edit',
    templateUrl: './creator-tender-edit.component.html',
    styleUrls: ['./creator-tender-edit.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreatorTenderEditComponent implements OnInit, OnDestroy {

    tenderId: number;
    formGroup: FormGroup;

    curatorSelect2Options;

    curatorSelect2Value: string;
    curatorsSelect2Data: Select2OptionData[];

    sellerGroups: ITender.ISellerGroup[];

    canDeactivateComponent = true;
    serviceLoading: boolean;

    initFiles: IFile.IFileResponse[] = [];
    filesLoading: boolean;

    pageOpenDate: moment.Moment;

    maxFilesLength = 10;
    maxFileSizeMB = 16;

    get minDateForEndDate(): moment.Moment {

        return this.formGroup && this.formGroup.controls['startDate']
            ? this.formGroup.controls['startDate'].value
            : this.pageOpenDate;
    }

    get TENDER_PERIOD() {
        return ITender.PERIOD;
    }

    get OPERATING_COMPANIES() {
        return AppConst.OPERATING_COMPANIES;
    }

    get CURRENCIES() {
        return AppConst.CURRENCIES;
    }

    get filesFormArray(): FormArray {
        return this.formGroup ? this.formGroup.controls['files'] as FormArray : null;
    }

    set filesFormArray(formArray: FormArray) {
        this.formGroup.controls['files'] = formArray;
    }

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    constructor(private store$: Store<CreatorTenderEditStore.IState>,
                private actions$: Actions,
                private route: ActivatedRoute,
                private cdr: ChangeDetectorRef,
                private fb: FormBuilder,
                private notifyService: NotifyService,
                private ts: TranslateService) {
    }

    ngOnInit() {
        this.pageOpenDate = moment();

        this.initForm();
        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new CreatorTenderEditActions.DestroyState());
    }

    canDeactivate() {

        if (!this.canDeactivateComponent) {

            return this.notifyService.confirm({
                dialogTitle: this.ts.data.T.dialogs.outPageQuestion,
                dialogContent: this.ts.data.T.dialogs.haveUnSaveChanges,
            });
        }

        return true;
    }

    private initForm() {

        this.formGroup = this.fb.group({
            id: [null],
            name: this.fb.group({
                az: [null],
                en: [null],
                ru: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(1024)]],
            }),
            operatingCompany: [null, [Validators.required]],
            startDate: [null, [Validators.required]],
            endDate: [null, [Validators.required]],
            period: [null, [Validators.required]],
            urgentNumber: [null, Validators.maxLength(256)],
            urgentData: [null],
            quarter: [null],
            curatorId: [null, [Validators.required]],
            sellerGroupId: [null, [Validators.required]],
            currency: [null, [Validators.required]],
            description: this.fb.group({
                az: [null],
                en: [null],
                ru: [null],
            }),
            files: this.fb.array([]),
        });

        this.subs = this.formGroup.controls['period'].valueChanges
            .subscribe(period => {

                let data: Object;

                if (period === ITender.PERIOD.QUARTER) {

                    data = {
                        urgentNumber: null,
                        urgentData: null,
                    };

                    this.setValidator('urgentNumber', []);
                    this.setValidator('urgentData', []);
                    this.setValidator('quarter', [Validators.required]);

                } else if (period === ITender.PERIOD.URGENT) {

                    data = {
                        quarter: null,
                    };

                    this.setValidator('urgentNumber', [Validators.required]);
                    this.setValidator('urgentData', [Validators.required]);
                    this.setValidator('quarter', []);

                } else {

                    data = {
                        urgentNumber: null,
                        urgentData: null,
                        quarter: null,
                    };

                    this.setValidator('urgentNumber', []);
                    this.setValidator('urgentData', []);
                    this.setValidator('quarter', []);
                }

                this.formGroup.patchValue(data);
            });

        this.subs = this.formGroup.controls['startDate'].valueChanges
            .pipe(filter(val => !!val))
            .subscribe(startDate => {

                const endDate = this.formGroup.controls['endDate'].value;

                if (!endDate || endDate < startDate) {

                    this.formGroup.patchValue({
                        endDate: startDate,
                    });
                }
            });

        this.subs = this.formGroup.valueChanges
            .pipe(
                filter(() => this.canDeactivateComponent),
            )
            .subscribe(value => {

                this.store$.dispatch(new CreatorTenderEditActions.SetCanDeactivate(false));
            });
    }

    private initStore() {

        this.subs = this.store$.pipe(select(AppStore.Selects.appTranslate.data))
            .subscribe(data => {

                this.curatorSelect2Options = {
                    allowClear: true,
                    placeholder: data.T.newTender.fastSearch,
                };

                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(CreatorTenderEditStore.Selects.self.tenderId))
            .subscribe(tenderId => {

                this.tenderId = tenderId;
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(CreatorTenderEditStore.Selects.self.canDeactivate))
            .subscribe(canDeactivate => {

                this.canDeactivateComponent = canDeactivate;
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(AppStore.Selects.appBlock.locks))
            .subscribe(locks => {

                this.serviceLoading =
                    locks[CreatorStore.SERVICES_UID.curators]
                    || locks[CreatorStore.SERVICES_UID.sellerGroups]
                    || locks[CreatorTenderEditStore.SERVICES_UID.crud]
                    || locks[CreatorTenderEditStore.SERVICES_UID.publish]
                    || locks[CreatorTenderEditStore.SERVICES_UID.saveBeforePrint]
                    || locks[CreatorTenderEditStore.SERVICES_UID.getPrintPage];

                this.cdr.detectChanges();
            });

        this.subs = this.actions$
            .pipe(
                ofType(CreatorTenderEditActionTypes.IMPORT_TENDER),
                map((action: CreatorTenderEditActions.ImportTender) => action.payload),
            )
            .subscribe(tender => {

                this.formGroup.reset({
                    emitEvent: false,
                });

                this.saveFilesToForm(tender ? tender.files : null);

                if (tender) {

                    this.formGroup.patchValue(tender, {
                        emitEvent: false,
                    });
                }

                const urgentData: moment.Moment =
                    !tender || !tender.urgentData
                        ? null
                        : moment(tender.urgentData);

                const startDate: moment.Moment =
                    !tender || !tender.startDate || moment(tender.startDate, AppConst.MY_MOMENT_FORMATS.parseInput) < this.pageOpenDate
                        ? this.pageOpenDate
                        : moment(tender.startDate);

                const endDate: moment.Moment =
                    !tender || !tender.endDate || moment(tender.endDate, AppConst.MY_MOMENT_FORMATS.parseInput) < startDate
                        ? null
                        : moment(tender.endDate);

                this.formGroup.patchValue({
                    urgentData,
                    startDate,
                    endDate,
                }, {
                    emitEvent: false,
                });

                this.curatorSelect2Value = tender && tender.curatorId ? '' + tender.curatorId : '';

                this.initFiles = this.filesFormArray.getRawValue();
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(
            select(CreatorStore.Selects.self.curators),
            filter(data => !!data),
        )
            .subscribe(curators => {

                this.curatorsSelect2Data = curators
                    .filter((curator: ITender.ICurator) => !curator.closed)
                    .map((curator: ITender.ICurator) => {

                        return <Select2OptionData>{
                            id: `${curator.id}`,
                            text: `${curator.name} (${curator.email})`,
                        };
                    });

                this.curatorsSelect2Data.unshift({
                    id: '',
                    text: '',
                });

                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(
            select(CreatorStore.Selects.self.sellerGroups),
            filter(data => !!data),
        )
            .subscribe(sellerGroups => {

                this.sellerGroups = sellerGroups
                    ? sellerGroups.filter(group => !group.closed)
                    : null;

                this.cdr.detectChanges();
            });

    }

    selectCurator(data: { value: string, data: Select2OptionData }): void {

        const curatorId: number = data.value ? parseInt(data.value, 10) : null;
        this.formGroup.controls['curatorId'].setValue(curatorId);
    }

    onFilesUpdate(files: UploadFile[]): void {

        const formFiles = files ? files.map(f => <IFile.IFileResponse>(f.response || f)) : [];
        this.saveFilesToForm(formFiles);
    }

    onUpdateLoadStatus(loading: boolean): void {

        this.filesLoading = loading;
    }

    private getTenderValue() {

        const form = this.formGroup.value;

        const tender: ITender.IModel = {
            ...this.formGroup.value,
            startDate: !form.startDate ? form.startDate : form.startDate.format(),
            endDate: !form.endDate ? form.endDate : form.endDate.format(),
            urgentData: !form.urgentData ? form.urgentData : form.urgentData.format(AppConst.MY_MOMENT_FORMATS.datePickerInput),
        };

        return tender;
    }

    save(): void {
        this.store$.dispatch(new CreatorTenderEditActions.SaveTender(this.getTenderValue()));
    }

    print(): void {
        this.store$.dispatch(new CreatorTenderEditActions.PrintTender(this.getTenderValue()));
    }

    publish(): void {

        const confirm = this.notifyService.confirm({
            dialogTitle: this.ts.data.T.newTenderDialogs.isPublishTender,
            dialogContent: this.ts.data.T.newTenderDialogs.afterPublishNoChanges,
        });

        if (confirm) {
            this.store$.dispatch(new CreatorTenderEditActions.PublishTender(this.getTenderValue()));
        }
    }

    private saveFilesToForm(files: IFile.IFileResponse[]) {

        AppFormUtils.clearFormArray(this.filesFormArray);

        if (files) {

            files.forEach((file) => {
                this.filesFormArray.push(this.fb.control(file));
            });
        }
    }

    private setValidator(controlName: string, validators: ValidatorFn | ValidatorFn[]): void {

        this.formGroup.controls[controlName].setValidators(validators);
        this.formGroup.controls[controlName].updateValueAndValidity();
    }

}
