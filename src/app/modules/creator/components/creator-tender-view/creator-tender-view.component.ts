import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Inject,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    SimpleChanges
} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../../../store/app.store';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../services/crud/store/crud-service.actions';
import {Subscription} from 'rxjs';
import {Actions, ofType} from '@ngrx/effects';
import {filter, map} from 'rxjs/operators';
import {IMultilang} from '../../../../interfaces/multilang.interface';
import {TranslateService} from '../../../../services/translate/translate.service';
import {IFile} from '../../../../interfaces/file.interface';
import {WINDOW} from '../../../../providers/window.providers';
import {ITender} from '../../../../interfaces/tender.interface';
import {AppConst} from '../../../../consts/app.const';
import {AppFileUtils} from '../../../../utils/app-file.utils';
import {DOCUMENT} from '@angular/common';

@Component({
    selector: 'app-creator-tender-view',
    templateUrl: './creator-tender-view.component.html',
    styleUrls: ['./creator-tender-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreatorTenderViewComponent implements OnInit, OnDestroy {

    @Input() tender: ITender.IModel;
    @Input() sellerGroupName: string;
    @Input() crudUrl: string;

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    lockPrintButton: boolean;

    private printUid = '[CreatorTenderViewComponent] printUid';

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    constructor(private store$: Store<AppStore.IState>,
                private actions$: Actions,
                private cdr: ChangeDetectorRef,
                private translateService: TranslateService,
                @Inject(WINDOW) private window: Window,
                @Inject(DOCUMENT) private document: Document) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());

        this.store$.dispatch(new CrudServiceActions.CancelRequests([
            this.printUid,
        ]));
    }

    private initStore() {

        this.onPrintSuccess();

        this.subs = this.store$.pipe(select(AppStore.Selects.appBlock.locks))
            .subscribe(locks => {

                this.lockPrintButton = locks[this.printUid];

                this.cdr.detectChanges();
            });
    }

    onPrint(): void {

        if (!this.crudUrl) {
            return;
        }

        const lang = this.translateService.lang;

        this.store$.dispatch(new CrudServiceActions.GetItem({
            url: `${this.crudUrl}/print?lang=${lang}`,
            uid: this.printUid,
        }));
    }

    private onPrintSuccess(): void {

        this.subs = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
                map((action: CrudServiceActions.GetItemSuccess) => action.payload),
                filter(payload => payload.uid === this.printUid),
            ).subscribe((payload: any) => {

                const file: IFile.IFileResponse = <IFile.IFileResponse>payload.item;
                AppFileUtils.downloadFile(file ? file.url : '', this.document);
            });
    }

}
