import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreatorTenderViewComponent} from './creator-tender-view.component';
import {PipesModule} from '../../../../pipes/pipes.module';
import {DesignPreloaderModule} from '../../../design/design-preloader/design-preloader.module';
import {DesignFileLinkModule} from '../../../design/design-file-link/design-file-link.module';

@NgModule({
    declarations: [CreatorTenderViewComponent],
    imports: [
        CommonModule,
        PipesModule,
        DesignPreloaderModule,
        DesignFileLinkModule,
    ],
    exports: [CreatorTenderViewComponent]
})
export class CreatorTenderViewModule {
}
