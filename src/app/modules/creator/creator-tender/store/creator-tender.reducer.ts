import {createSelector, MemoizedSelector} from '@ngrx/store';
import {CreatorTenderActions, CreatorTenderActionTypes} from './creator-tender.actions';
import {ITender} from '../../../../interfaces/tender.interface';

export namespace CreatorTenderReducer {

    export interface IState {
        tender: ITender.IModel;
    }

    const initialState: IState = {
        tender: null,
    };

    export interface ISelects {
        tender: (state: IState) => ITender.IModel;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            tender = createSelector(
                selector,
                ((state: IState) => state.tender),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = {...initialState}, action: CreatorTenderActions.All): IState {

        switch (action.type) {

            case CreatorTenderActionTypes.SET_TENDER: {

                const tender = action.payload ? {...action.payload} : null;

                return {
                    ...state,
                    tender,
                };
            }

            case CreatorTenderActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
