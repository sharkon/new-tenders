import {CreatorTenderReducer} from './creator-tender.reducer';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {AppStore} from '../../../../store/app.store';

export namespace CreatorTenderStore {

    export const FEATURE_UID = '[CreatorTenderStore] FEATURE_UID';
    export const MODULE_UID = '[CreatorTenderStore] MODULE_UID';

    export const SERVICES_UID = {
        tender: '[CreatorTenderStore][SERVICES_UID] tender',
    };

    export const SERVICES_URL = {
        tender: (tenderId: string | number): string => {
            const url = 'cabinet/creator/tender/';
            return tenderId ? `${url}${tenderId}` : url;
        },
    };

    interface IStore {
        selfStore: CreatorTenderReducer.IState;
    }

    export interface IState extends AppStore.IState {
        creatorTenderStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: CreatorTenderReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = CreatorTenderReducer.createStoreSelector(selfSelector);

    }

}
