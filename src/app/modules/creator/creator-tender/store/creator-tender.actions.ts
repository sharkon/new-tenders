import {Action} from '@ngrx/store';
import {ITender} from '../../../../interfaces/tender.interface';

export namespace CreatorTenderActionTypes {

    export const SET_TENDER = '[CreatorTender] SET_TENDER';
    export const DESTROY_STATE = '[CreatorTender] DESTROY_STATE';
}

export namespace CreatorTenderActions {

    export class SetTender implements Action {

        readonly type = CreatorTenderActionTypes.SET_TENDER;

        constructor(public payload: ITender.IModel) {
        }

    }

    export class DestroyState implements Action {

        readonly type = CreatorTenderActionTypes.DESTROY_STATE;
    }

    export type All =
        SetTender
        | DestroyState;

}
