import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {ROUTER_NAVIGATED, RouterNavigationAction} from '@ngrx/router-store';
import {CreatorTenderActions, CreatorTenderActionTypes} from './creator-tender.actions';
import {CreatorTenderStore} from './creator-tender.store';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../services/crud/store/crud-service.actions';
import {ITender} from '../../../../interfaces/tender.interface';

@Injectable()
export class CreatorTenderEffects {

    @Effect()
    onRoute$: Observable<Action> =
        this.actions$.pipe(
            ofType(ROUTER_NAVIGATED),
            map((action: RouterNavigationAction<RouterStateUrl>) => action.payload),
            filter(payload => {
                return payload.routerState.data.module === CreatorTenderStore.MODULE_UID;
            }),
            map((payload) => {

                const tenderId: string = payload.routerState.params['tenderId'];

                return new CrudServiceActions.GetItem({
                    url: `${CreatorTenderStore.SERVICES_URL.tender(tenderId)}/`,
                    uid: CreatorTenderStore.SERVICES_UID.tender,
                });
            }),
        );

    @Effect()
    loadTenderSuccess$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
                map((action: CrudServiceActions.GetItemSuccess) => action.payload),
                filter(payload => payload.uid === CreatorTenderStore.SERVICES_UID.tender),
                map(payload => {

                    return new CreatorTenderActions.SetTender(<ITender.IModel>payload.item);
                }),
            );

    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(CreatorTenderActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        CreatorTenderStore.SERVICES_UID.tender,
                    ]);
                })
            );

    constructor(private actions$: Actions) {
    }
}
