import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreatorTenderComponent} from './creator-tender.component';
import {RouterModule} from '@angular/router';
import {CreatorTenderViewModule} from '../components/creator-tender-view/creator-tender-view.module';
import {PipesModule} from '../../../pipes/pipes.module';
import {DesignBackModule} from '../../design/design-back/design-back.module';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {CreatorTenderStore} from './store/creator-tender.store';
import {CreatorTenderEffects} from './store/creator-tender.effects';

@NgModule({
    declarations: [CreatorTenderComponent],
    imports: [
        CommonModule,
        CreatorTenderViewModule,
        PipesModule,
        DesignBackModule,
        RouterModule.forChild([
            {
                path: '',
                component: CreatorTenderComponent,
                data: {
                    module: CreatorTenderStore.MODULE_UID,
                },
            }
        ]),
        StoreModule.forFeature(CreatorTenderStore.FEATURE_UID, CreatorTenderStore.mapReducers),
        EffectsModule.forFeature([
            CreatorTenderEffects,
        ]),
    ]
})
export class CreatorTenderModule {
}
