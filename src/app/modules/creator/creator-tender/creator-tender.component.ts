import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {combineLatest, Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../../store/app.store';
import {ITender} from '../../../interfaces/tender.interface';
import {CreatorTenderStore} from './store/creator-tender.store';
import {CreatorStore} from '../store/creator.store';
import {filter} from 'rxjs/operators';
import {CreatorTenderActions} from './store/creator-tender.actions';

@Component({
    selector: 'app-creator-tender',
    templateUrl: './creator-tender.component.html',
    styleUrls: ['./creator-tender.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreatorTenderComponent implements OnInit, OnDestroy {

    tender: ITender.IModel;
    sellerGroupName: string;
    crudUrl = CreatorTenderStore.SERVICES_URL.tender;

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    constructor(private store$: Store<AppStore.IState>,
                private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new CreatorTenderActions.DestroyState());
    }

    private initStore() {

        const data$ = combineLatest(
            this.store$.pipe(select(CreatorTenderStore.Selects.self.tender)),
            this.store$.pipe(select(CreatorStore.Selects.self.sellerGroups)),
        );

        this.subs = data$.pipe(
            filter((data) => data.every(d => !!d)),
        ).subscribe(([tender, sellerGroups]) => {

            this.tender = <ITender.IModel>tender;

            if (this.tender && this.tender.sellerGroupId && sellerGroups) {

                const sellerGroup = sellerGroups.find(g => g.id === this.tender.sellerGroupId);
                this.sellerGroupName = sellerGroup ? sellerGroup.name : null;
            }

            this.cdr.detectChanges();
        });
    }

}
