import {Inject, Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable} from 'rxjs';
import {Action, select, Store} from '@ngrx/store';
import {filter, map, mergeMap, tap, withLatestFrom} from 'rxjs/operators';
import {PaginationActions, PaginationActionTypes} from '../../../../reducers/pagination/pagination.actions';
import {CreatorTendersStore} from './creator-tenders.store';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../services/crud/store/crud-service.actions';
import {CrudActions} from '../../../../reducers/crud/crud.actions';
import {CreatorTendersActionTypes} from './creator-tenders.actions';
import {AppStore} from '../../../../store/app.store';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';
import {WINDOW} from '../../../../providers/window.providers';
import {IFile} from '../../../../interfaces/file.interface';
import {NotifyService} from '../../../../services/notify/notify.service';
import {TranslateService} from '../../../../services/translate/translate.service';
import {AppFileUtils} from '../../../../utils/app-file.utils';
import {DOCUMENT} from '@angular/common';

@Injectable()
export class CreatorTendersEffects {

    @Effect()
    getList$: Observable<Action> =
        this.actions$.pipe(
            ofType(PaginationActionTypes.SET_REQUEST),
            withLatestFrom(
                this.store$.pipe(select(AppStore.Selects.AppRouter.getState)),
                this.store$.pipe(select(CreatorTendersStore.Selects.pagination.request)),
            ),
            filter(([payload, routerState]) => {
                return (<RouterStateUrl>routerState).data.module === CreatorTendersStore.MODULE_UID;
            }),
            mergeMap(([payload, routerState, paginationRequest]) => {

                return [
                    new CrudActions.DestroyState(CreatorTendersStore.CRUD_STORE_UID),
                    new CrudServiceActions.GetList({
                        url: CreatorTendersStore.getCrudUrl(<RouterStateUrl>routerState, paginationRequest),
                        uid: CreatorTendersStore.SERVICES_UID.list,
                    }),
                ];
            })
        );

    @Effect()
    getListSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === CreatorTendersStore.SERVICES_UID.list),
            mergeMap(payload => {

                return [
                    new CrudActions.ImportModels(CreatorTendersStore.CRUD_STORE_UID, payload.data),
                    new PaginationActions.SetResponse(CreatorTendersStore.PAGINATION_STORE_UID, payload.pageInfo),
                ];
            })
        );

    @Effect({dispatch: false})
    printTenderSuccess$ =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CREATE_SUCCESS),
            map((action: CrudServiceActions.CreateSuccess) => action.payload),
            filter(payload => payload.uid === CreatorTendersStore.SERVICES_UID.print),
            tap(payload => {

                const file: IFile.IFileResponse = <IFile.IFileResponse>payload.item;
                AppFileUtils.downloadFile(file ? file.url : '', this.document);
            })
        );

    @Effect()
    deleteTenderSuccess$ =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CREATE_SUCCESS),
            map((action: CrudServiceActions.CreateSuccess) => action.payload),
            filter(payload => payload.uid === CreatorTendersStore.SERVICES_UID.delete),
            withLatestFrom(
                this.store$.pipe(select(AppStore.Selects.AppRouter.getState)),
            ),
            map(([payload, routerState]) => {

                this.notifyService.success({
                    dialogTitle: this.ts.data.T.newTenderDialogs.successDeleted,
                });

                return new CrudServiceActions.GetList({
                    url: CreatorTendersStore.getCrudUrl(<RouterStateUrl>routerState),
                    uid: CreatorTendersStore.SERVICES_UID.list,
                });
            })
        );

    @Effect()
    cancelRequests$: Observable<Action> =
        this.actions$.pipe(
            ofType(CreatorTendersActionTypes.DESTROY_STATE),
            mergeMap(action => {

                return [
                    new CrudServiceActions.CancelRequests([
                        CreatorTendersStore.SERVICES_UID.list,
                        CreatorTendersStore.SERVICES_UID.print,
                        CreatorTendersStore.SERVICES_UID.delete,
                    ]),
                ];
            })
        );

    constructor(private actions$: Actions,
                private store$: Store<CreatorTendersStore.IState>,
                private notifyService: NotifyService,
                private ts: TranslateService,
                @Inject(WINDOW) private window: Window,
                @Inject(DOCUMENT) private document: Document) {
    }
}
