import { Action } from '@ngrx/store';

export namespace CreatorTendersActionTypes {

    export const DESTROY_STATE = '[CreatorTenders] DESTROY_STATE';
}

export namespace CreatorTendersActions {

    export class DestroyState implements Action {

        readonly type = CreatorTendersActionTypes.DESTROY_STATE;
    }

    export type All =
        DestroyState;

}
