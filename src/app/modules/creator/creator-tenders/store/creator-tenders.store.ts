import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {CrudStore} from '../../../../reducers/crud/crud.reducer';
import {PaginationStore} from '../../../../reducers/pagination/pagination.reducer';
import {AppStore} from '../../../../store/app.store';
import {Params} from '@angular/router';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';
import {ITender} from '../../../../interfaces/tender.interface';
import {PaginationUtils} from '../../../../utils/pagination-utils';
import {IPagination} from '../../../../interfaces/pagination.interface';

export namespace CreatorTendersStore {

    export const MODULE_UID = '[CreatorTendersStore] MODULE_UID';
    export const FEATURE_UID = '[CreatorTendersStore] FEATURE_UID';

    export const CRUD_STORE_UID = '[CreatorTendersStore] CRUD_STORE_UID';

    export const PAGINATION_STORE_UID = '[CreatorTendersStore] PAGINATION_STORE_UID';

    export const PAGE_LIMIT = 20;

    export const SERVICES_URL = {
        list: 'cabinet/creator/tender',
        print: 'cabinet/creator/tender/print',
        delete: 'cabinet/creator/tender/del',
    };

    export const SERVICES_UID = {
        list: '[CreatorTendersStore][SERVICES_UID] list',
        print: '[CreatorTendersStore][SERVICES_UID] print',
        delete: '[CreatorTendersStore][SERVICES_UID] delete',
    };

    export interface ITendersListState {
        crud: CrudStore.IState;
        pagination: PaginationStore.IState;
    }

    export interface IState extends AppStore.IState {
        creatorTendersStore: ITendersListState;
    }

    export const mapReducers: ActionReducerMap<ITendersListState> = {
        crud: CrudStore.createReducer(CRUD_STORE_UID),
        pagination: PaginationStore.createReducer(PAGINATION_STORE_UID),
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<ITendersListState>(FEATURE_UID);

        // crud

        const crudSelector = createSelector(
            featureSelector,
            (state: ITendersListState) => state.crud,
        );

        export const crud = CrudStore.createStoreSelector(crudSelector);

        // pagination

        const paginationSelector = createSelector(
            featureSelector,
            (state: ITendersListState) => state.pagination,
        );

        export const pagination = PaginationStore.createStoreSelector(paginationSelector);
    }

    export function getCrudUrl(routerState: RouterStateUrl, paginationRequest: IPagination.IPaginationOnlyRequest = {offset: 0, limit: PAGE_LIMIT}): string {

        const queryParams: Params = routerState
            ? routerState.queryParams : null;

        const status: ITender.QueryStatusType = queryParams
            ? queryParams[ITender.QUERY_STATUS_NAME] || ITender.QUERY_STATUS.ACTIVE
            : ITender.QUERY_STATUS.ACTIVE;

        const url = `${SERVICES_URL.list}?state=${status}`
            + `${PaginationUtils.GetPaginationUrlQueryString(paginationRequest)}`;

        return url;
    }
}
