import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreatorTendersComponent} from './creator-tenders.component';
import {RouterModule} from '@angular/router';
import {PipesModule} from '../../../pipes/pipes.module';
import {StoreModule} from '@ngrx/store';
import {CreatorTendersStore} from './store/creator-tenders.store';
import {EffectsModule} from '@ngrx/effects';
import {PaginationEffects} from '../../../reducers/pagination/pagination.effects';
import {CurdEffects} from '../../../reducers/crud/crud.effects';
import {CreatorTendersEffects} from './store/creator-tenders.effects';
import {PaginationStoreConfig} from '../../../reducers/pagination/pagination.store.config';
import {CrudStoreConfig} from '../../../reducers/crud/crud.store.config';
import {DesignPaginationModule} from '../../design/design-pagination/design-pagination.module';
import {CreatorTendersTableModule} from './components/creator-tenders-table/creator-tenders-table.module';
import {CreatorTendersStatusGuard} from './guards/creator-tenders-status/creator-tenders-status.guard';

@NgModule({
    declarations: [CreatorTendersComponent],
    imports: [
        CommonModule,
        PipesModule,
        CreatorTendersTableModule,
        DesignPaginationModule,
        RouterModule.forChild([
            {
                path: '',
                component: CreatorTendersComponent,
                data: {
                    module: CreatorTendersStore.MODULE_UID,
                },
                canActivate: [CreatorTendersStatusGuard],
            }
        ]),
        StoreModule.forFeature(CreatorTendersStore.FEATURE_UID, CreatorTendersStore.mapReducers),
        EffectsModule.forFeature([
            PaginationEffects,
            CurdEffects,
            CreatorTendersEffects,
        ]),
    ],
    providers: [
        CreatorTendersStatusGuard,
        {
            provide: PaginationStoreConfig,
            useValue: <PaginationStoreConfig>{
                storeUid: CreatorTendersStore.PAGINATION_STORE_UID,
                moduleUid: CreatorTendersStore.MODULE_UID,
                config: {
                    defaultRequest: {
                        offset: 0,
                        limit: CreatorTendersStore.PAGE_LIMIT,
                    },
                    limitValues: [CreatorTendersStore.PAGE_LIMIT],
                    seeMoreOption: false,
                    perPageOption: false,
                },
            }
        },
        {
            provide: CrudStoreConfig,
            useValue: <CrudStoreConfig>{
                storeUid: CreatorTendersStore.CRUD_STORE_UID,
                config: {
                    crudUrl: CreatorTendersStore.SERVICES_URL.list,
                    serviceUid: CreatorTendersStore.SERVICES_UID.list,
                }
            }
        },
    ]
})
export class CreatorTendersModule {
}
