import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Input,
    OnChanges,
    OnDestroy,
    OnInit
} from '@angular/core';
import {ITender} from '../../../../../interfaces/tender.interface';
import {TranslateService} from '../../../../../services/translate/translate.service';
import {FormGroup} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {CreatorTendersStore} from '../../store/creator-tenders.store';
import {CrudServiceActions} from '../../../../../services/crud/store/crud-service.actions';
import {Subscription} from 'rxjs';
import {AppStore} from '../../../../../store/app.store';
import {AppConst} from '../../../../../consts/app.const';
import {NotifyService} from '../../../../../services/notify/notify.service';
import {IError} from '../../../../../interfaces/error.interface';
import {AppErrorsConst} from '../../../../../consts/app-errors.const';

@Component({
    selector: 'app-creator-tenders-table',
    templateUrl: './creator-tenders-table.component.html',
    styleUrls: ['./creator-tenders-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreatorTendersTableComponent implements OnInit, OnChanges, OnDestroy {

    @Input() models: ITender.IListModel[];
    @Input() queryStatus: ITender.QueryStatusType;

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get QUERY_STATUS() {
        return ITender.QUERY_STATUS;
    }

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    formGroup: FormGroup;
    lockButtons: boolean;

    selectedItems: number[];
    selectedAll: boolean;

    constructor(private ts: TranslateService,
                private store$: Store<CreatorTendersStore.IState>,
                private cdr: ChangeDetectorRef,
                private notifyService: NotifyService) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnChanges() {

        this.selectedItems = [];
        this.selectedAll = false;
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
    }

    private initStore(): void {

        this.subs = this.store$.pipe(select(AppStore.Selects.appBlock.locks))
            .subscribe(locks => {

                this.lockButtons = locks[CreatorTendersStore.SERVICES_UID.print]
                    || locks[CreatorTendersStore.SERVICES_UID.delete];
                this.cdr.detectChanges();
            });
    }

    toggleAllCheckboxes(): void {

        this.selectedItems = (!this.selectedAll) ? this.models.map(item => item.id) : [];
        this.selectedAll = !this.selectedAll;
    }

    toggleCheckbox(id: number): void {

        if (this.selectedItems.indexOf(id) > -1) {
            this.selectedItems = this.selectedItems.filter(selectedId => selectedId !== id);
        } else {
            this.selectedItems.push(id);
        }

        this.selectedAll = (this.selectedItems.length === this.models.length) ? true : false;
    }

    onPrint(): void {

        const request = {
            ids: this.selectedItems.slice(),
            lang: this.ts.lang,
        };

        const mapMessage: IError.IBackendTranslateError = {
            'ids': {
                'ids': this.ts.data.T.dialogs.error,
            },
        };

        mapMessage['ids'][AppErrorsConst.BACKEND_ERROR_TYPES.REQUIRED] = this.ts.data.T.tenders.printError;

        this.store$.dispatch(new CrudServiceActions.Create({
            url: CreatorTendersStore.SERVICES_URL.print,
            uid: CreatorTendersStore.SERVICES_UID.print,
            item: <any>request,
            deniedNotifySuccess: true,
            mapMessage,
        }));
    }

    onDelete(): void {

        const confirm = this.notifyService.confirm({
            dialogTitle: this.ts.data.T.newTenderDialogs.deleteSelected,
        });

        if (confirm) {

            const request = {
                ids: this.selectedItems,
            };

            this.store$.dispatch(new CrudServiceActions.Create({
                url: CreatorTendersStore.SERVICES_URL.delete,
                uid: CreatorTendersStore.SERVICES_UID.delete,
                item: <any>request,
                deniedNotifySuccess: true,
            }));
        }
    }

}
