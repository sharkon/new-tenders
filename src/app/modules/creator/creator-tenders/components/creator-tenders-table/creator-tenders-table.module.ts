import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreatorTendersTableComponent} from './creator-tenders-table.component';
import {PipesModule} from '../../../../../pipes/pipes.module';
import {RouterModule} from '@angular/router';
import {DesignPreloaderModule} from '../../../../design/design-preloader/design-preloader.module';

@NgModule({
    declarations: [CreatorTendersTableComponent],
    imports: [
        CommonModule,
        PipesModule,
        RouterModule,
        DesignPreloaderModule,
    ],
    exports: [CreatorTendersTableComponent],
})
export class CreatorTendersTableModule {
}
