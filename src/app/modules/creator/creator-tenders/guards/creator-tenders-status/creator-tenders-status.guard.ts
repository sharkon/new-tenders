import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ITender} from '../../../../../interfaces/tender.interface';

@Injectable()
export class CreatorTendersStatusGuard implements CanActivate {

    constructor(private router: Router) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        const status: ITender.QueryStatusType = state.root.queryParams && state.root.queryParams[ITender.QUERY_STATUS_NAME];

        if (status && (Object.keys(ITender.QUERY_STATUS).map(key => ITender.QUERY_STATUS[key])).indexOf(status) < 0) {

            this.router.navigate(['/creator/tenders'], {replaceUrl: true});
            return false;
        }

        return true;
    }
}
