import { TestBed, async, inject } from '@angular/core/testing';

import { CreatorTendersStatusGuard } from './creator-tenders-status.guard';

describe('CreatorTendersStatusGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreatorTendersStatusGuard]
    });
  });

  it('should ...', inject([CreatorTendersStatusGuard], (guard: CreatorTendersStatusGuard) => {
    expect(guard).toBeTruthy();
  }));
});
