import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {CreatorTendersStore} from './store/creator-tenders.store';
import {PaginationStore} from '../../../reducers/pagination/pagination.reducer';
import {ITender} from '../../../interfaces/tender.interface';
import {select, Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import {CreatorTendersActions} from './store/creator-tenders.actions';
import {AppStore} from '../../../store/app.store';
import {RouterStateUrl} from '../../../providers/custom-router-state-serializer';
import {Actions} from '@ngrx/effects';
import {Params} from '@angular/router';

@Component({
    selector: 'app-creator-tenders',
    templateUrl: './creator-tenders.component.html',
    styleUrls: ['./creator-tenders.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreatorTendersComponent implements OnInit, OnDestroy {

    models: ITender.IListModel[];
    queryStatus: ITender.QueryStatusType;

    queryParamsPublish: Params = {
        [ITender.QUERY_STATUS_NAME]: ITender.QUERY_STATUS.PUBLISH,
    };

    queryParamsFinish: Params = {
        [ITender.QUERY_STATUS_NAME]: ITender.QUERY_STATUS.FINISH,
    };

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get QUERY_STATUS() {
        return ITender.QUERY_STATUS;
    }

    get PAGINATION_STORE_UID(): string {
        return CreatorTendersStore.PAGINATION_STORE_UID;
    }

    get PAGINATION_SELECT_FUNCTION(): PaginationStore.ISelects {
        return CreatorTendersStore.Selects.pagination;
    }

    constructor(private store$: Store<CreatorTendersStore.IState>,
                private actions$: Actions,
                private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new CreatorTendersActions.DestroyState());
    }

    private initStore() {

        this.subs = this.store$.pipe(select(CreatorTendersStore.Selects.crud.models))
            .subscribe((models: ITender.IListModel[]) => {

                this.models = models;
                this.cdr.detectChanges();
            });

        this.subs = this.store$.pipe(select(AppStore.Selects.AppRouter.getState))
            .subscribe((routerState: RouterStateUrl) => {

                this.queryStatus = (routerState && routerState.queryParams) ? routerState.queryParams[ITender.QUERY_STATUS_NAME] : null;
                this.cdr.detectChanges();
            });
    }

}
