import {CreatorReducer} from './creator.reducer';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {AppStore} from '../../../store/app.store';

export namespace CreatorStore {

    export const ROOT_URL = '/creator';

    export const FEATURE_UID = '[CreatorStore] FEATURE_UID';
    export const MODULE_UID = '[CreatorStore] MODULE_UID';

    export const SERVICES_UID = {
        curators: '[CreatorStore][SERVICES_UID] curators',
        sellerGroups: '[CreatorStore][SERVICES_UID] sellerGroups',
    };

    export const SERVICES_URL = {
        curators: 'cabinet/creator/tender/potential-curator',
        sellerGroups: 'cabinet/creator/tender/potential-seller-group',
    };

    interface IStore {
        selfStore: CreatorReducer.IState;
    }

    export interface IState extends AppStore.IState {
        creatorStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: CreatorReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = CreatorReducer.createStoreSelector(selfSelector);

    }

}
