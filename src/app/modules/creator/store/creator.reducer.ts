import {createSelector, MemoizedSelector} from '@ngrx/store';
import {CreatorActions, CreatorActionTypes} from './creator.actions';
import {ITender} from '../../../interfaces/tender.interface';

export namespace CreatorReducer {

    export interface IState {
        curators: ITender.ICurator[];
        sellerGroups: ITender.ISellerGroup[];
    }

    const initialState: IState = {
        curators: null,
        sellerGroups: null,
    };

    export interface ISelects {
        curators: (state: IState) => any[];
        sellerGroups: (state: IState) => any[];
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            curators = createSelector(
                selector,
                ((state: IState) => state.curators),
            );

            sellerGroups = createSelector(
                selector,
                ((state: IState) => state.sellerGroups),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = {...initialState}, action: CreatorActions.All): IState {

        switch (action.type) {

            case CreatorActionTypes.SET_CURATORS: {

                const curators = action.payload ? action.payload.slice() : null;

                return {
                    ...state,
                    curators,
                };
            }

            case CreatorActionTypes.SET_SELLER_GROUPS: {

                const sellerGroups = action.payload ? action.payload.slice() : null;

                return {
                    ...state,
                    sellerGroups,
                };
            }

            case CreatorActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
