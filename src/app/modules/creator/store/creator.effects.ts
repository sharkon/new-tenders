import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {CreatorActions, CreatorActionTypes} from './creator.actions';
import {CreatorStore} from './creator.store';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../services/crud/store/crud-service.actions';
import {ITender} from '../../../interfaces/tender.interface';
import {ICrud} from '../../../interfaces/crud.interface';

@Injectable()
export class CreatorEffects {

    @Effect()
    getCurators$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === CreatorStore.SERVICES_UID.curators),
            map((payload: ICrud.ICRUDGetListResponse) => {

                const curators = <ITender.ICurator[]>payload.data;
                return new CreatorActions.SetCurators(curators);
            })
        );

    @Effect()
    getSellerGroups$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === CreatorStore.SERVICES_UID.sellerGroups),
            map((payload: ICrud.ICRUDGetListResponse) => {

                const sellerGroups = <ITender.ISellerGroup[]>payload.data;
                return new CreatorActions.SetSellerGroups(sellerGroups);
            })
        );

    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(CreatorActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        CreatorStore.SERVICES_UID.curators,
                        CreatorStore.SERVICES_UID.sellerGroups,
                    ]);
                })
            );

    constructor(private actions$: Actions) {
    }
}
