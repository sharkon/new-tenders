import {Action} from '@ngrx/store';
import {ITender} from '../../../interfaces/tender.interface';

export namespace CreatorActionTypes {

    export const SET_CURATORS = '[Creator] SET_CURATORS';
    export const SET_SELLER_GROUPS = '[Creator] SET_SELLER_GROUPS';

    export const DESTROY_STATE = '[Creator] DESTROY_STATE';
}

export namespace CreatorActions {

    export class SetCurators implements Action {

        readonly type = CreatorActionTypes.SET_CURATORS;

        constructor(public payload: ITender.ICurator[]) {
        }
    }

    export class SetSellerGroups implements Action {

        readonly type = CreatorActionTypes.SET_SELLER_GROUPS;

        constructor(public payload: ITender.ISellerGroup[]) {
        }
    }

    export class DestroyState implements Action {

        readonly type = CreatorActionTypes.DESTROY_STATE;
    }

    export type All =
        SetCurators
        | SetSellerGroups
        | DestroyState;

}
