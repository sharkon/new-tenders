import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../../store/app.store';
import {RouterStateUrl} from '../../../providers/custom-router-state-serializer';
import {filter, map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {ITender} from '../../../interfaces/tender.interface';
import {CreatorTendersStore} from '../creator-tenders/store/creator-tenders.store';
import {CrudServiceActions} from '../../../services/crud/store/crud-service.actions';
import {CreatorStore} from '../store/creator.store';
import {Params} from '@angular/router';

@Component({
    selector: 'app-creator-root',
    templateUrl: './creator-root.component.html',
    styleUrls: ['./creator-root.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreatorRootComponent implements OnInit {

    statusIsPublish$: Observable<boolean>;

    queryParams: Params = {
        [ITender.QUERY_STATUS_NAME]: ITender.QUERY_STATUS.DRAFT
    };

    constructor(private store$: Store<AppStore.IState>) {
    }

    ngOnInit() {

        this.store$.dispatch(
            new CrudServiceActions.GetList({
                url: CreatorStore.SERVICES_URL.curators,
                uid: CreatorStore.SERVICES_UID.curators,
            })
        );

        this.store$.dispatch(
            new CrudServiceActions.GetList({
                url: CreatorStore.SERVICES_URL.sellerGroups,
                uid: CreatorStore.SERVICES_UID.sellerGroups,
            })
        );

        this.statusIsPublish$ = this.store$.pipe(
            select(AppStore.Selects.AppRouter.getState),
            filter((routerState: RouterStateUrl) => !!(routerState && routerState.queryParams)),
            map((routerState: RouterStateUrl) => routerState.data.module === CreatorTendersStore.MODULE_UID ? routerState.queryParams[ITender.QUERY_STATUS_NAME] || null : undefined),
            map((status: ITender.QueryStatusType) => {

                return [ITender.QUERY_STATUS.ACTIVE, ITender.QUERY_STATUS.FINISH, ITender.QUERY_STATUS.PUBLISH, null].indexOf(status) > -1;
            }),
        );
    }

}
