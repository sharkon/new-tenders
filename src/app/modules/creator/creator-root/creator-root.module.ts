import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreatorRootComponent} from './creator-root.component';
import {RouterModule} from '@angular/router';
import {PipesModule} from '../../../pipes/pipes.module';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {CreatorStore} from '../store/creator.store';
import {CreatorEffects} from '../store/creator.effects';

@NgModule({
    declarations: [CreatorRootComponent],
    imports: [
        CommonModule,
        PipesModule,
        RouterModule.forChild([
            {
                path: '',
                component: CreatorRootComponent,
                children: [
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'tenders',
                    },
                    {
                        path: 'tenders',
                        loadChildren: '../creator-tenders/creator-tenders.module#CreatorTendersModule',
                    },
                    {
                        path: 'tender/:tenderId',
                        loadChildren: '../creator-tender/creator-tender.module#CreatorTenderModule',
                    },
                    {
                        path: 'tender-edit/:tenderId',
                        loadChildren: '../creator-tender-edit/creator-tender-edit.module#CreatorTenderEditModule',
                    },
                ]
            }
        ]),
        StoreModule.forFeature(CreatorStore.FEATURE_UID, CreatorStore.mapReducers),
        EffectsModule.forFeature([
            CreatorEffects,
        ]),
    ]
})
export class CreatorRootModule {
}
