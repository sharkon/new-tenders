import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppStore} from '../../store/app.store';
import {select, Store} from '@ngrx/store';
import {CrudServiceActions, CrudServiceActionTypes} from '../../services/crud/store/crud-service.actions';
import {Observable, Subscription} from 'rxjs';
import {Actions, ofType} from '@ngrx/effects';
import {filter, map} from 'rxjs/operators';
import {AuthService} from '../../services/auth/auth.service';
import {AppAuthActions} from '../../store/auth/app-auth.actions';
import {NotifyService} from '../../services/notify/notify.service';
import {TranslateService} from '../../services/translate/translate.service';
import {AppConst} from '../../consts/app.const';

const SERVICE_URL = 'auth/change-password';
const SERVICE_UID = '[SettingsComponent] change-password';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsComponent implements OnInit, OnDestroy {

    formGroup: FormGroup;
    submitted: boolean;

    strengthPassword: number;

    loading$: Observable<boolean>;

    get VALID_STRENGTH_PASSWORD() {
        return AppConst.VALID_STRENGTH_PASSWORD;
    }

    get passwordControl(): AbstractControl {
        return this.getFormControl('password');
    }

    get newPasswordControl(): AbstractControl {
        return this.getFormControl('newPassword');
    }

    get repeatNewPasswordControl(): AbstractControl {
        return this.getFormControl('repeatNewPassword');
    }

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    constructor(private formBuilder: FormBuilder,
                private store$: Store<AppStore.IState>,
                private actions$: Actions,
                private authService: AuthService,
                private notifyService: NotifyService,
                private ts: TranslateService) {
    }

    ngOnInit() {

        this.initForm();
        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
    }

    private initForm(): void {

        this.formGroup = this.formBuilder.group({
            password: ['', [Validators.minLength(8), Validators.required]],
            newPassword: ['', [Validators.minLength(8), Validators.required]],
            repeatNewPassword: ['', [Validators.minLength(8), Validators.required]],
        });
    }

    private getFormControl(key: string): AbstractControl {
        return this.formGroup && this.formGroup.controls[key]
            ? this.formGroup.controls[key]
            : null;
    }

    onSubmit() {
        this.submitted = true;

        const data = this.formGroup.value;

        if (data.newPassword !== data.repeatNewPassword) {

            this.notifyService.warning({
                dialogContent: this.ts.data.T.settingsPage.repeatError,
            });

        } else if (this.strengthPassword < this.VALID_STRENGTH_PASSWORD) {

            this.notifyService.warning({
                dialogContent: this.ts.data.T.form.help.lessPassword,
            });

        } else if (this.formGroup.valid) {

            const request: any = {
                oldPassword: data.password,
                newPassword: data.newPassword,
            };

            this.store$.dispatch(new CrudServiceActions.Create({
                url: SERVICE_URL,
                uid: SERVICE_UID,
                item: {
                    ...request,
                }
            }));
        }
    }

    private initStore() {

        this.loading$ = this.store$.pipe(
            select(AppStore.Selects.appBlock.locks),
            map(locks => {
                return locks[SERVICE_UID];
            }),
        );

        this.subs = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.CREATE_SUCCESS),
                map((action: CrudServiceActions.CreateSuccess) => action.payload),
                filter(payload => payload.uid === SERVICE_UID),
            )
            .subscribe((response: any) => {

                // current response
                // { "status": "ok" }
                this.store$.dispatch(new AppAuthActions.LogOut('/login'));
                // TODO: new token in response
                // this.authService.setToken(response.token.key);
                // this.store$.dispatch(new AppAuthActions.LogInSuccess());
            });

        this.subs = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.CREATE_FAILED),
                map((action: CrudServiceActions.CreateFailed) => action.payload),
                filter(payload => payload.uid === SERVICE_UID),
            )
            .subscribe((response: any) => {

                this.notifyService.warning({
                    dialogContent: this.ts.data.T.settingsPage.saveError,
                });
            });
    }

    onStrengthChange(strength: number): void {
        this.strengthPassword = strength;
    }

}
