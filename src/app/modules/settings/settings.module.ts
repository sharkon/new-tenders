import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { RouterModule } from '@angular/router';
import { PipesModule } from '../../pipes/pipes.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DesignPasswordModule } from '../design/design-password/design-password.module';
import {DesignPreloaderModule} from '../design/design-preloader/design-preloader.module';

@NgModule({
    declarations: [SettingsComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: SettingsComponent,
            }
        ]),
        PipesModule,
        ReactiveFormsModule,
        DesignPasswordModule,
        DesignPreloaderModule,
    ]
})
export class SettingsModule { }
