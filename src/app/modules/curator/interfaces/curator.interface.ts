import {ITender} from '../../../interfaces/tender.interface';
import {IChat} from '../../../interfaces/chat.interface';
import {ICrud} from '../../../interfaces/crud.interface';

export namespace ICurator {

    export interface IList {
        data: IListModel[];
    }

    export interface IListModel extends ICrud.IModel {
        chat: IChat.IListModelForCurator;
        tender: ITender.IListModelForChatCurator;
    }
}
