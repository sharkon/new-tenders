import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuratorRootComponent } from './curator-root.component';

describe('CuratorRootComponent', () => {
  let component: CuratorRootComponent;
  let fixture: ComponentFixture<CuratorRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuratorRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuratorRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
