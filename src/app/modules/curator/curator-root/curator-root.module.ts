import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuratorRootComponent} from './curator-root.component';
import {RouterModule} from '@angular/router';
import {StoreModule} from '@ngrx/store';
import {CuratorStore} from '../store/curator.store';
import {EffectsModule} from '@ngrx/effects';
import {CuratorEffects} from '../store/curator.effects';

@NgModule({
    declarations: [CuratorRootComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: CuratorRootComponent,
                data: {
                    module: CuratorStore.MODULE_UID,
                },
                children: [
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'chats',
                    },
                    {
                        path: 'chats',
                        loadChildren: '../curator-chats/curator-chats.module#CuratorChatsModule',
                    },
                    {
                        path: 'chat/:tenderId',
                        loadChildren: '../../chat/chat.module#ChatModule',
                    },
                    {
                        path: 'tender/:tenderId',
                        loadChildren: '../curator-tender/curator-tender.module#CuratorTenderModule',
                    },
                ],
            }
        ]),
        StoreModule.forFeature(CuratorStore.FEATURE_UID, CuratorStore.mapReducers),
        EffectsModule.forFeature([
            CuratorEffects,
        ]),
    ]
})
export class CuratorRootModule {
}
