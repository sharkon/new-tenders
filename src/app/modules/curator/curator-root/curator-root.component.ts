import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {CuratorStore} from '../store/curator.store';
import {CuratorActions} from '../store/curator.actions';

@Component({
    selector: 'app-curator-root',
    templateUrl: './curator-root.component.html',
    styleUrls: ['./curator-root.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CuratorRootComponent implements OnInit, OnDestroy {

    constructor(private store$: Store<CuratorStore.IState>) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {

        this.store$.dispatch(new CuratorActions.DestroyState());
    }
}
