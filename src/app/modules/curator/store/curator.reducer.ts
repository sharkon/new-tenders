import {createSelector, MemoizedSelector} from '@ngrx/store';
import {CuratorActions, CuratorActionTypes} from './curator.actions';
import {ICurator} from '../interfaces/curator.interface';

export namespace CuratorReducer {

    export interface IState {
        models: ICurator.IListModel[];
    }

    const initialState: IState = {
        models: null,
    };

    export interface ISelects {
        models: (state: IState) => ICurator.IListModel[];
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            models = createSelector(
                selector,
                ((state: IState) => state.models),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = {...initialState}, action: CuratorActions.All): IState {

        switch (action.type) {

            case CuratorActionTypes.IMPORT_MODELS: {

                const models = action.payload ? action.payload.slice() : null;

                return {
                    ...state,
                    models,
                };
            }

            case CuratorActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
