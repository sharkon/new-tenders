import {Action} from '@ngrx/store';
import {ICurator} from '../interfaces/curator.interface';

export namespace CuratorActionTypes {

    export const IMPORT_MODELS = '[Curator] IMPORT_MODELS';
    export const DESTROY_STATE = '[Curator] DESTROY_STATE';
}

export namespace CuratorActions {

    export class ImportModels implements Action {

        readonly type = CuratorActionTypes.IMPORT_MODELS;

        constructor(public payload: ICurator.IListModel[]) {
        }
    }

    export class DestroyState implements Action {

        readonly type = CuratorActionTypes.DESTROY_STATE;
    }

    export type All =
        DestroyState
        | ImportModels;

}
