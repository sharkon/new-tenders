import {CuratorReducer} from './curator.reducer';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {AppStore} from '../../../store/app.store';

export namespace CuratorStore {

    export const FEATURE_UID = '[CuratorStore] FEATURE_UID';
    export const MODULE_UID = '[CuratorStore] MODULE_UID';

    export const SERVICES_URL = {
        chats: 'cabinet/curator/chat-tender',
    };

    export const SERVICES_UID = {
        chats: '[CuratorStore][SERVICES_UID] chats',
    };

    interface IStore {
        selfStore: CuratorReducer.IState;
    }

    export interface IState extends AppStore.IState {
        curatorStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: CuratorReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = CuratorReducer.createStoreSelector(selfSelector);

    }

}
