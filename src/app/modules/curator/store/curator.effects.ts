import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {ROUTER_NAVIGATED, RouterNavigationAction} from '@ngrx/router-store';
import {CuratorActions, CuratorActionTypes} from './curator.actions';
import {CuratorStore} from './curator.store';
import {RouterStateUrl} from '../../../providers/custom-router-state-serializer';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../services/crud/store/crud-service.actions';
import {ICurator} from '../interfaces/curator.interface';

@Injectable()
export class CuratorEffects {

    @Effect()
    getModelsSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === CuratorStore.SERVICES_UID.chats),
            map(payload => {

                return new CuratorActions.ImportModels(<ICurator.IListModel[]>payload.data);
            }),
        );

    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(CuratorActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        CuratorStore.SERVICES_UID.chats,
                    ]);
                })
            );

    constructor(private actions$: Actions) {
    }
}
