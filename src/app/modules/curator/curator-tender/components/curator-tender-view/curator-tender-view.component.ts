import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ITender} from '../../../../../interfaces/tender.interface';
import {AppConst} from '../../../../../consts/app.const';

@Component({
    selector: 'app-curator-tender-view',
    templateUrl: './curator-tender-view.component.html',
    styleUrls: ['./curator-tender-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CuratorTenderViewComponent implements OnInit {

    @Input() tender: ITender.IModelForCurator;
    @Input() userIsSeller: boolean;

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    constructor() {
    }

    ngOnInit() {
    }

}
