import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuratorTenderViewComponent } from './curator-tender-view.component';

describe('CuratorTenderViewComponent', () => {
  let component: CuratorTenderViewComponent;
  let fixture: ComponentFixture<CuratorTenderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuratorTenderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuratorTenderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
