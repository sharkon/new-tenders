import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuratorTenderViewComponent} from './curator-tender-view.component';
import {RouterModule} from '@angular/router';
import {PipesModule} from '../../../../../pipes/pipes.module';
import {DesignPreloaderModule} from '../../../../design/design-preloader/design-preloader.module';
import {DesignTenderPeriodModule} from '../../../../design/design-tender-period/design-tender-period.module';
import {DesignFileLinkModule} from '../../../../design/design-file-link/design-file-link.module';

@NgModule({
    declarations: [CuratorTenderViewComponent],
    exports: [CuratorTenderViewComponent],
    imports: [
        CommonModule,
        PipesModule,
        RouterModule,
        DesignPreloaderModule,
        DesignTenderPeriodModule,
        DesignFileLinkModule,
    ],
})
export class CuratorTenderViewModule {
}
