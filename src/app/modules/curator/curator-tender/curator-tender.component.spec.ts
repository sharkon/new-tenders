import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuratorTenderComponent } from './curator-tender.component';

describe('CuratorTenderComponent', () => {
  let component: CuratorTenderComponent;
  let fixture: ComponentFixture<CuratorTenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuratorTenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuratorTenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
