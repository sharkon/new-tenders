import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action} from '@ngrx/store';
import {Observable} from 'rxjs';
import {filter, map, mergeMap} from 'rxjs/operators';
import {ROUTER_NAVIGATED, RouterNavigationAction} from '@ngrx/router-store';
import {CuratorTenderActions, CuratorTenderActionTypes} from './curator-tender.actions';
import {CuratorTenderStore} from './curator-tender.store';
import {RouterStateUrl} from '../../../../providers/custom-router-state-serializer';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../../services/crud/store/crud-service.actions';
import {ITender} from '../../../../interfaces/tender.interface';

@Injectable()
export class CuratorTenderEffects {


    @Effect()
    onRoute$: Observable<Action> =
        this.actions$.pipe(
            ofType(ROUTER_NAVIGATED),
            map((action: RouterNavigationAction<RouterStateUrl>) => action.payload),
            filter(payload => {
                return payload.routerState.data.module === CuratorTenderStore.MODULE_UID;
            }),
            mergeMap((payload) => {

                const tenderId: string = payload.routerState.params['tenderId'];

                return [
                    new CrudServiceActions.GetItem({
                        url: `${CuratorTenderStore.SERVICES_URL.tender(tenderId)}/`,
                        uid: CuratorTenderStore.SERVICES_UID.tender,
                    }),

                ];
            }),
        );

    @Effect()
    loadTenderSuccess$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
                map((action: CrudServiceActions.GetItemSuccess) => action.payload),
                filter(payload => payload.uid === CuratorTenderStore.SERVICES_UID.tender),
                map(payload => {

                    return new CuratorTenderActions.SetTender(<ITender.IModelForCurator>payload.item);
                }),
            );


    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(CuratorTenderActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        CuratorTenderStore.SERVICES_UID.tender,

                    ]);
                })
            );

    constructor(private actions$: Actions) {
    }
}
