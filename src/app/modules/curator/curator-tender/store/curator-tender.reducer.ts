import {createSelector, MemoizedSelector} from '@ngrx/store';
import {CuratorTenderActions, CuratorTenderActionTypes} from './curator-tender.actions';
import {ITender} from '../../../../interfaces/tender.interface';

export namespace CuratorTenderReducer {

    export interface IState {
        tender: ITender.IModelForCurator;
    }

    const initialState: IState = {
        tender: null,
    };

    export interface ISelects {
        tender: (state: IState) => ITender.IModelForCurator;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            tender = createSelector(
                selector,
                ((state: IState) => state.tender),
            );
        }

        return new Selects();
    }

    export function reducer(state: IState = {...initialState}, action: CuratorTenderActions.All): IState {

        switch (action.type) {

            case CuratorTenderActionTypes.SET_TENDER: {

                const tender: ITender.IModelForCurator = action.payload ? {...action.payload} : null;

                return {
                    ...state,
                    tender,
                };
            }

            case CuratorTenderActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
