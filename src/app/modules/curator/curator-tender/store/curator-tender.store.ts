import {CuratorTenderReducer} from './curator-tender.reducer';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {AppStore} from '../../../../store/app.store';

export namespace CuratorTenderStore {

    export const FEATURE_UID = '[CuratorTenderStore] FEATURE_UID';
    export const MODULE_UID = '[CuratorTenderStore] MODULE_UID';


    export const SERVICES_URL = {
        tender: (id: string | number): string => {
            const url = 'cabinet/curator/tender/';
            return id ? `${url}${id}` : url;
        },

    };

    export const SERVICES_UID = {
        tender: '[CuratorTenderStore][SERVICES_UID] tender',

    };

    interface IStore {
        selfStore: CuratorTenderReducer.IState;


    }

    export interface IState extends AppStore.IState {
        curatorTenderStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: CuratorTenderReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = CuratorTenderReducer.createStoreSelector(selfSelector);


    }


}
