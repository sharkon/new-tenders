import {Action} from '@ngrx/store';
import {ITender} from '../../../../interfaces/tender.interface';

export namespace CuratorTenderActionTypes {

    export const SET_TENDER = '[CuratorTender] SET_TENDER';
    export const DESTROY_STATE = '[CuratorTender] DESTROY_STATE';
}

export namespace CuratorTenderActions {

    export class SetTender implements Action {

        readonly type = CuratorTenderActionTypes.SET_TENDER;

        constructor(public payload: ITender.IModelForCurator) {
        }
    }

    export class DestroyState implements Action {

        readonly type = CuratorTenderActionTypes.DESTROY_STATE;
    }

    export type All =
        DestroyState
        | SetTender;

}
