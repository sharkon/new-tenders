import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ITender} from '../../../interfaces/tender.interface';
import {select, Store} from '@ngrx/store';
import {CuratorTenderStore} from './store/curator-tender.store';
import {CuratorTenderActions} from './store/curator-tender.actions';

@Component({
    selector: 'app-curator-tender',
    templateUrl: './curator-tender.component.html',
    styleUrls: ['./curator-tender.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CuratorTenderComponent implements OnInit, OnDestroy {

    tender$: Observable<ITender.IModelForCurator>;

    constructor(private store$: Store<CuratorTenderStore.IState>,
                private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy() {

        this.store$.dispatch(new CuratorTenderActions.DestroyState());
    }

    private initStore() {

        this.tender$ = this.store$.pipe(select(CuratorTenderStore.Selects.self.tender));
    }

}
