import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuratorTenderComponent} from './curator-tender.component';
import {RouterModule} from '@angular/router';
import {DesignPreloaderModule} from '../../design/design-preloader/design-preloader.module';
import {PipesModule} from '../../../pipes/pipes.module';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {CuratorTenderStore} from './store/curator-tender.store';
import {CuratorTenderEffects} from './store/curator-tender.effects';
import {CuratorTenderViewModule} from './components/curator-tender-view/curator-tender-view.module';
import {DesignBackModule} from '../../design/design-back/design-back.module';

@NgModule({
    declarations: [CuratorTenderComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: CuratorTenderComponent,
                data: {
                    module: CuratorTenderStore.MODULE_UID,
                }
            }
        ]),
        DesignPreloaderModule,
        PipesModule,
        StoreModule.forFeature(CuratorTenderStore.FEATURE_UID, CuratorTenderStore.mapReducers),
        EffectsModule.forFeature([
            CuratorTenderEffects,
        ]),
        CuratorTenderViewModule,
        DesignBackModule,
    ]
})
export class CuratorTenderModule {
}
