import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuratorChatsComponent} from './curator-chats.component';
import {RouterModule} from '@angular/router';
import {DesignPreloaderModule} from '../../design/design-preloader/design-preloader.module';
import {PipesModule} from '../../../pipes/pipes.module';
import {CuratorChatsTableModule} from './components/curator-chats-table/curator-chats-table.module';

@NgModule({
    declarations: [CuratorChatsComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: CuratorChatsComponent,
            }
        ]),
        DesignPreloaderModule,
        PipesModule,
        CuratorChatsTableModule,
    ]
})
export class CuratorChatsModule {
}
