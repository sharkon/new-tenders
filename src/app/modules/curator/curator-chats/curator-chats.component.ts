import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ICurator} from '../interfaces/curator.interface';
import {select, Store} from '@ngrx/store';
import {CuratorStore} from '../store/curator.store';
import {AppStore} from '../../../store/app.store';
import {map} from 'rxjs/operators';
import {CrudServiceActions} from '../../../services/crud/store/crud-service.actions';
import {IUser} from '../../../interfaces/user.interface';

@Component({
    selector: 'app-curator-chats',
    templateUrl: './curator-chats.component.html',
    styleUrls: ['./curator-chats.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CuratorChatsComponent implements OnInit {

    models$: Observable<ICurator.IListModel[]>;
    user$: Observable<IUser.IUserResponse>;
    serviceLoading$: Observable<boolean>;

    constructor(private store$: Store<CuratorStore.IState>) {
    }

    ngOnInit() {

        this.initStore();
    }

    private initStore() {

        this.models$ = this.store$.pipe(select(CuratorStore.Selects.self.models));
        this.user$ = this.store$.pipe(select(AppStore.Selects.appAuth.user));

        this.serviceLoading$ = this.store$.pipe(
            select(AppStore.Selects.appBlock.locks),
            map(locks => locks[CuratorStore.SERVICES_UID.chats]),
        );

        this.store$.dispatch(
            new CrudServiceActions.GetList({
                uid: CuratorStore.SERVICES_UID.chats,
                url: CuratorStore.SERVICES_URL.chats,
            })
        );
    }

}
