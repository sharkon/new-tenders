import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuratorChatsComponent } from './curator-chats.component';

describe('CuratorChatsComponent', () => {
  let component: CuratorChatsComponent;
  let fixture: ComponentFixture<CuratorChatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuratorChatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuratorChatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
