import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ICurator} from '../../../interfaces/curator.interface';
import {IUser} from '../../../../../interfaces/user.interface';
import {AppConst} from '../../../../../consts/app.const';

@Component({
    selector: 'app-curator-chats-table',
    templateUrl: './curator-chats-table.component.html',
    styleUrls: ['./curator-chats-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CuratorChatsTableComponent implements OnInit {

    @Input() models: ICurator.IListModel[];
    @Input() userRoles: IUser.UserRoleType[];

    get USER_IS_ADMIN() {
        return this.userRoles && this.userRoles.indexOf(IUser.USER_ROLES.SITE_ADMIN) > -1;
    }

    get USER_IS_CURATOR() {
        return this.userRoles && this.userRoles.indexOf(IUser.USER_ROLES.CURATOR) > -1;
    }

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    get DATE_FORMAT_SECONDS() {
        return AppConst.DATE_PIPE_FORMAT.withSeconds;
    }

    constructor() {
    }

    ngOnInit() {
    }

}
