import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuratorChatsTableComponent } from './curator-chats-table.component';

describe('CuratorChatsTableComponent', () => {
  let component: CuratorChatsTableComponent;
  let fixture: ComponentFixture<CuratorChatsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuratorChatsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuratorChatsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
