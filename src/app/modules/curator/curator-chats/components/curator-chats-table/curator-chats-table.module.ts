import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CuratorChatsTableComponent} from './curator-chats-table.component';
import {RouterModule} from '@angular/router';
import {PipesModule} from '../../../../../pipes/pipes.module';
import {DesignTenderPeriodModule} from '../../../../design/design-tender-period/design-tender-period.module';

@NgModule({
    declarations: [CuratorChatsTableComponent],
    exports: [CuratorChatsTableComponent],
    imports: [
        CommonModule,
        RouterModule,
        PipesModule,
        DesignTenderPeriodModule,
    ]
})
export class CuratorChatsTableModule {
}
