import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { PipesModule } from '../../pipes/pipes.module';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from 'ngx-icons';

@NgModule({
    imports: [
        CommonModule,
        PipesModule,
        RouterModule,
        FontAwesomeModule
    ],
    declarations: [HeaderComponent],
    exports: [HeaderComponent]
})
export class HeaderModule { }
