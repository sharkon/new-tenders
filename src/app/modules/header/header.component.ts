import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {TranslateService} from '../../services/translate/translate.service';
import {ILang} from '../../interfaces/lang.interface';
import {select, Store} from '@ngrx/store';
import {AppStore} from '../../store/app.store';
import {Subscription} from 'rxjs';
import {WINDOW} from '../../providers/window.providers';
import {IUser} from '../../interfaces/user.interface';
import {AppAuthActions} from '../../store/auth/app-auth.actions';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit, OnDestroy {

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    get USER_ROLES() {
        return IUser.USER_ROLES;
    }

    user: IUser.IUserResponse;
    isAuthenticated: boolean;

    constructor(private translateService: TranslateService,
                private store$: Store<AppStore.IState>,
                private cdr: ChangeDetectorRef,
                @Inject(WINDOW) private window: Window) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
    }

    private initStore(): void {

        this.subs = this.store$
            .pipe(
                select(AppStore.Selects.appAuth.authenticated)
            )
            .subscribe((authenticated: boolean) => {

                this.isAuthenticated = authenticated;
                this.cdr.detectChanges();
            });

        this.subs = this.store$
            .pipe(
                select(AppStore.Selects.appAuth.user)
            )
            .subscribe((user: IUser.IUserResponse) => {

                this.user = user;
                this.cdr.detectChanges();
            });
    }

    changeLanguage(lang: ILang.LangType): void {

        this.translateService.use(lang);
    }

    logout() {
        this.store$.dispatch(new AppAuthActions.LogOut());
    }

}
