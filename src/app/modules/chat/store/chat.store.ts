import {ChatReducer} from './chat.reducer';
import {ActionReducerMap, createFeatureSelector, createSelector} from '@ngrx/store';
import {AppStore} from '../../../store/app.store';
import {IUser} from '../../../interfaces/user.interface';

export namespace ChatStore {

    export const FEATURE_UID = '[ChatStore] FEATURE_UID';
    export const MODULE_UID = '[ChatStore] MODULE_UID';

    export const SERVICES_URL = {
        tender: (user: IUser.IUserResponse, id: string | number): string => {
            const rolePath = user.role.indexOf(IUser.USER_ROLES.SELLER) > -1 ? 'seller' : 'curator';
            return `cabinet/${rolePath}/tender/${id}`;
        },
        messages: (id: string | number): string => {
            return `chat/${id}/message`;
        },
        newMessages: (id: string | number, lastId?: number): string => {
            const url = `chat/${id}/message`;
            return lastId ? `${url}?last=${lastId}` : url;
        },
        myFiles: (id: string | number): string => {
            return `chat/${id}/message/files/owned/`;
        },
        otherFiles: (id: string | number): string => {
            return `chat/${id}/message/files/another/`;
        },
        file: (id: string | number): string => {
            return `chat/${id}/message/files/`;
        },
    };

    export const SERVICES_UID = {
        tender: '[ChatStore][SERVICES_UID] tender',
        messages: '[ChatStore][SERVICES_UID] messages',
        newMessages: '[ChatStore][SERVICES_UID] newMessages',
        myFiles: '[ChatStore][SERVICES_UID] myFiles',
        otherFiles: '[ChatStore][SERVICES_UID] otherFiles',
        file: '[ChatStore][SERVICES_UID] file',
        message: '[ChatStore][SERVICES_UID] message',
    };

    interface IStore {
        selfStore: ChatReducer.IState;
    }

    export interface IState extends AppStore.IState {
        chatStore: IStore;
    }

    export const mapReducers: ActionReducerMap<IStore> = {
        selfStore: ChatReducer.reducer,
    };

    export namespace Selects {

        const featureSelector = createFeatureSelector<IStore>(FEATURE_UID);

        // self

        const selfSelector = createSelector(
            featureSelector,
            (state: IStore) => state.selfStore,
        );

        export const self = ChatReducer.createStoreSelector(selfSelector);

    }

}
