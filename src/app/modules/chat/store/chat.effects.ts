import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {filter, map, mergeMap, withLatestFrom} from 'rxjs/operators';
import {ROUTER_NAVIGATED, RouterNavigationAction} from '@ngrx/router-store';
import {ChatActions, ChatActionTypes} from './chat.actions';
import {ChatStore} from './chat.store';
import {RouterStateUrl} from '../../../providers/custom-router-state-serializer';
import {CrudServiceActions, CrudServiceActionTypes} from '../../../services/crud/store/crud-service.actions';
import {AppStore} from '../../../store/app.store';
import {ITender} from '../../../interfaces/tender.interface';
import {IChat} from '../../../interfaces/chat.interface';
import {IFile} from '../../../interfaces/file.interface';
import {ICrud} from '../../../interfaces/crud.interface';
import {AppErrorActions} from '../../../store/error/app-error.actions';
import {IError} from '../../../interfaces/error.interface';
import {TranslateService} from '../../../services/translate/translate.service';

@Injectable()
export class ChatEffects {

    get chatMapMessage(): IError.IBackendTranslateError {

        const mapMessage: IError.IBackendTranslateError = {
            '403': {
                'dialogTitle': this.ts.data.T.tenders.closed,
            },
        };

        return mapMessage;
    }

    @Effect()
    onRoute$: Observable<Action> =
        this.actions$.pipe(
            ofType(ROUTER_NAVIGATED),
            map((action: RouterNavigationAction<RouterStateUrl>) => action.payload),
            filter(payload => {
                return payload.routerState.data.module === ChatStore.MODULE_UID;
            }),
            withLatestFrom(
                this.store$.pipe(select(AppStore.Selects.appAuth.user)),
            ),
            mergeMap(([payload, user]) => {

                const tenderId: string = payload.routerState.params['tenderId'];

                return [
                    new CrudServiceActions.GetItem({
                        url: `${ChatStore.SERVICES_URL.tender(user, tenderId)}/`,
                        uid: ChatStore.SERVICES_UID.tender,
                    }),
                    new CrudServiceActions.GetList({
                        url: ChatStore.SERVICES_URL.messages(tenderId),
                        uid: ChatStore.SERVICES_UID.messages,
                        mapMessage: this.chatMapMessage,
                    }),
                ];
            }),
        );

    @Effect()
    loadTenderSuccess$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.GET_ITEM_SUCCESS),
                map((action: CrudServiceActions.GetItemSuccess) => action.payload),
                filter(payload => payload.uid === ChatStore.SERVICES_UID.tender),
                map(payload => {

                    return new ChatActions.SetTender(<ITender.IModelForChat>payload.item);
                }),
            );

    @Effect()
    getMessagesSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === ChatStore.SERVICES_UID.messages
                || payload.uid === ChatStore.SERVICES_UID.newMessages),
            withLatestFrom(
                this.store$.pipe(select(AppStore.Selects.AppRouter.getState)),
            ),
            mergeMap(([payload, routerState]) => {

                const tenderId: string = (<RouterStateUrl>routerState).params['tenderId'];

                return [
                    new ChatActions.ImportMessages(<IChat.IMessage[]>payload.data),
                    new CrudServiceActions.GetList({
                        url: ChatStore.SERVICES_URL.myFiles(tenderId),
                        uid: ChatStore.SERVICES_UID.myFiles,
                        mapMessage: this.chatMapMessage,
                    }),
                    new CrudServiceActions.GetList({
                        url: ChatStore.SERVICES_URL.otherFiles(tenderId),
                        uid: ChatStore.SERVICES_UID.otherFiles,
                        mapMessage: this.chatMapMessage,
                    }),
                ];
            }),
        );

    @Effect()
    getNewMessagesSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === ChatStore.SERVICES_UID.newMessages),
            map(payload => {

                return new ChatActions.AddNewMessages(<IChat.IMessage[]>payload.data);
            }),
        );

    @Effect()
    getMyFilesSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === ChatStore.SERVICES_UID.myFiles),
            map(payload => {

                return new ChatActions.ImportMyFiles(<IFile.IFileResponse[]>payload.data);
            }),
        );

    @Effect()
    getOtherFilesSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === ChatStore.SERVICES_UID.otherFiles),
            map(payload => {

                return new ChatActions.ImportOtherFiles(<IFile.IFileResponse[]>payload.data);
            }),
        );

    @Effect()
    deleteMyFile$: Observable<Action> =
        this.actions$.pipe(
            ofType(ChatActionTypes.DELETE_MY_FILE)
            , map((action: ChatActions.DeleteMyFile) => action.payload)
            , withLatestFrom(
                this.store$.pipe(select(ChatStore.Selects.self.tender), map(tender => tender ? tender.id : null)),
            )
            , map(([payload, tenderId]) => {

                return new CrudServiceActions.Delete({
                    url: `${ChatStore.SERVICES_URL.file(tenderId)}${payload}/`,
                    uid: ChatStore.SERVICES_UID.file,
                });
            })
        );

    @Effect()
    deleteMyFileSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.DELETE_SUCCESS)
            , map((action: CrudServiceActions.DeleteSuccess) => action.payload)
            , filter(payload => payload.uid === ChatStore.SERVICES_UID.file)
            , map((payload: ICrud.ICRUDDeleteResponse) => {

                return new ChatActions.DeleteMyFileSuccess(payload.id);
            })
        );

    @Effect()
    deleteMyFileFailed$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.DELETE_FAILED)
            , map((action: CrudServiceActions.DeleteFailed) => action.payload)
            , filter(payload => payload.uid === ChatStore.SERVICES_UID.file)
            , mergeMap(payload => {

                return [
                    new AppErrorActions.AddError(payload),
                    new ChatActions.DeleteMyFileFailed(),
                ];
            })
        );

    @Effect()
    sendMessage$: Observable<Action> =
        this.actions$.pipe(
            ofType(ChatActionTypes.SEND_MESSAGE)
            , map((action: ChatActions.SendMessage) => action.payload)
            , withLatestFrom(
                this.store$.pipe(select(ChatStore.Selects.self.tender), map(tender => tender ? tender.id : null)),
            )
            , map(([payload, tenderId]) => {

                return new CrudServiceActions.Create({
                    deniedNotifySuccess: true,
                    url: ChatStore.SERVICES_URL.messages(tenderId),
                    uid: ChatStore.SERVICES_UID.message,
                    item: {
                        id: null,
                        ...payload,
                    },
                });
            })
        );

    @Effect()
    sendMessageSuccess$: Observable<Action> =
        this.actions$.pipe(
            ofType(CrudServiceActionTypes.CREATE_SUCCESS)
            , map((action: CrudServiceActions.CreateSuccess) => action.payload)
            , filter(payload => payload.uid === ChatStore.SERVICES_UID.message)
            , map((payload: ICrud.ICRUDCreateResponse) => {

                return new ChatActions.AddUserMessage(<IChat.IMessage>payload.item);
            })
        );

    @Effect()
    destroyState$: Observable<Action> =
        this.actions$
            .pipe(
                ofType(ChatActionTypes.DESTROY_STATE),
                map(action => {
                    return new CrudServiceActions.CancelRequests([
                        ChatStore.SERVICES_UID.tender,
                        ChatStore.SERVICES_UID.messages,
                        ChatStore.SERVICES_UID.newMessages,
                        ChatStore.SERVICES_UID.myFiles,
                        ChatStore.SERVICES_UID.otherFiles,
                        ChatStore.SERVICES_UID.file,
                        ChatStore.SERVICES_UID.message,
                    ]);
                })
            );

    constructor(private actions$: Actions,
                private store$: Store<ChatStore.IState>,
                private ts: TranslateService) {
    }
}
