import {Action} from '@ngrx/store';
import {ITender} from '../../../interfaces/tender.interface';
import {IChat} from '../../../interfaces/chat.interface';
import {IFile} from '../../../interfaces/file.interface';

export namespace ChatActionTypes {

    export const SET_TENDER = '[Chat] SET_TENDER';

    export const IMPORT_MESSAGES = '[Chat] IMPORT_MESSAGES';
    export const ADD_NEW_MESSAGES = '[Chat] ADD_NEW_MESSAGES';
    export const SEND_MESSAGE = '[Chat] SEND_MESSAGE';
    export const ADD_USER_MESSAGE = '[Chat] ADD_USER_MESSAGE';

    export const IMPORT_MY_FILES = '[Chat] IMPORT_MY_FILES';
    export const DELETE_MY_FILE = '[Chat] DELETE_MY_FILE';
    export const DELETE_MY_FILE_SUCCESS = '[Chat] DELETE_MY_FILE_SUCCESS';
    export const DELETE_MY_FILE_FAILED = '[Chat] DELETE_MY_FILE_FAILED';

    export const IMPORT_OTHER_FILES = '[Chat] IMPORT_OTHER_FILES';

    export const DESTROY_STATE = '[Chat] DESTROY_STATE';
}

export namespace ChatActions {

    export class SetTender implements Action {

        readonly type = ChatActionTypes.SET_TENDER;

        constructor(public payload: ITender.IModelForChat) {
        }
    }

    export class ImportMessages implements Action {

        readonly type = ChatActionTypes.IMPORT_MESSAGES;

        constructor(public payload: IChat.IMessage[]) {
        }
    }

    export class AddNewMessages implements Action {

        readonly type = ChatActionTypes.ADD_NEW_MESSAGES;

        constructor(public payload: IChat.IMessage[]) {
        }
    }

    export class SendMessage implements Action {

        readonly type = ChatActionTypes.SEND_MESSAGE;

        constructor(public payload: {message: string}) {
        }
    }

    export class AddUserMessage implements Action {

        readonly type = ChatActionTypes.ADD_USER_MESSAGE;

        constructor(public payload: IChat.IMessage) {
        }
    }

    export class ImportMyFiles implements Action {

        readonly type = ChatActionTypes.IMPORT_MY_FILES;

        constructor(public payload: IFile.IFileResponse[]) {
        }
    }

    export class DeleteMyFile implements Action {

        readonly type = ChatActionTypes.DELETE_MY_FILE;

        constructor(public payload: number) {
        }
    }

    export class DeleteMyFileSuccess implements Action {

        readonly type = ChatActionTypes.DELETE_MY_FILE_SUCCESS;

        constructor(public payload: number) {
        }
    }

    export class DeleteMyFileFailed implements Action {

        readonly type = ChatActionTypes.DELETE_MY_FILE_FAILED;
    }

    export class ImportOtherFiles implements Action {

        readonly type = ChatActionTypes.IMPORT_OTHER_FILES;

        constructor(public payload: IFile.IFileResponse[]) {
        }
    }

    export class DestroyState implements Action {

        readonly type = ChatActionTypes.DESTROY_STATE;
    }

    export type All =
        DestroyState
        | SetTender
        | ImportMessages
        | AddNewMessages
        | SendMessage
        | AddUserMessage
        | ImportMyFiles
        | DeleteMyFile
        | DeleteMyFileSuccess
        | DeleteMyFileFailed
        | ImportOtherFiles;

}
