import {createSelector, MemoizedSelector} from '@ngrx/store';
import {ChatActions, ChatActionTypes} from './chat.actions';
import {ITender} from '../../../interfaces/tender.interface';
import {IChat} from '../../../interfaces/chat.interface';
import {IFile} from '../../../interfaces/file.interface';

export namespace ChatReducer {

    export interface IState {
        tender: ITender.IModelForChat;
        messages: IChat.IMessage[];
        myFiles: IFile.IFileResponse[];
        otherFiles: IFile.IFileResponse[];
        backupMyFile: IFile.IFileResponse;
        backupIndex: number;
        userMessages: IChat.IMessage[];
    }

    const initialState: IState = {
        tender: null,
        messages: null,
        myFiles: null,
        otherFiles: null,
        backupMyFile: null,
        backupIndex: null,
        userMessages: null,
    };

    export interface ISelects {
        tender: (state: IState) => ITender.IModelForChat;
        messages: (state: IState) => IChat.IMessage[];
        myFiles: (state: IState) => IFile.IFileResponse[];
        otherFiles: (state: IState) => IFile.IFileResponse[];
        backupMyFile: (state: IState) => IFile.IFileResponse;
        backupIndex: (state: IState) => number;
        userMessages: (state: IState) => IChat.IMessage[];
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Selects implements ISelects {

            tender = createSelector(selector, ((state: IState) => state.tender));
            messages = createSelector(selector, ((state: IState) => state.messages));
            myFiles = createSelector(selector, ((state: IState) => state.myFiles));
            otherFiles = createSelector(selector, ((state: IState) => state.otherFiles));
            backupMyFile = createSelector(selector, (state: IState) => state.backupMyFile);
            backupIndex = createSelector(selector, (state: IState) => state.backupIndex);
            userMessages = createSelector(selector, (state: IState) => state.userMessages);
        }

        return new Selects();
    }

    export function reducer(state: IState = {...initialState}, action: ChatActions.All): IState {

        switch (action.type) {

            case ChatActionTypes.SET_TENDER: {

                const tender: ITender.IModelForChat = action.payload ? {...action.payload} : null;

                return {
                    ...state,
                    tender,
                };
            }

            case ChatActionTypes.IMPORT_MESSAGES: {

                const messages = action.payload ? action.payload.slice() : [];

                return {
                    ...state,
                    messages,
                    userMessages: null,
                };
            }

            case ChatActionTypes.ADD_NEW_MESSAGES: {

                const newMessages = action.payload ? action.payload.slice() : [];

                const messages = [
                    ...(state.messages || []),
                    ...newMessages,
                ];

                return {
                    ...state,
                    messages,
                    userMessages: null,
                };
            }

            case ChatActionTypes.ADD_USER_MESSAGE: {

                const userMessages = (state.userMessages || []).slice();
                userMessages.push(action.payload);

                return {
                    ...state,
                    userMessages,
                };
            }

            case ChatActionTypes.IMPORT_MY_FILES: {

                const myFiles = action.payload ? action.payload.slice() : null;

                return {
                    ...state,
                    myFiles,
                };
            }

            case ChatActionTypes.DELETE_MY_FILE: {

                const myFiles = state.myFiles.slice();

                const backupIndex = myFiles.findIndex(r => r.id === action.payload);
                const backupMyFile = {...myFiles[backupIndex]};

                myFiles.splice(backupIndex, 1);

                return {
                    ...state,
                    myFiles,
                    backupMyFile,
                    backupIndex,
                };
            }

            case ChatActionTypes.DELETE_MY_FILE_SUCCESS: {

                if (state.backupIndex !== null) {

                    return {
                        ...state,
                        backupMyFile: null,
                        backupIndex: null,
                    };

                } else {

                    // EXTERNAL DELETE

                    const myFiles = state.myFiles.slice();

                    const backupIndex = myFiles.findIndex(r => r.id === action.payload);

                    if (backupIndex > -1) {

                        myFiles.splice(backupIndex, 1);
                    }

                    return {
                        ...state,
                        myFiles,
                    };
                }
            }

            case ChatActionTypes.DELETE_MY_FILE_FAILED: {

                if (state.backupIndex !== null) {

                    const myFiles = state.myFiles.slice();
                    const deleteCount = 0;

                    const myFile: IFile.IFileResponse = {...state.backupMyFile};

                    myFiles.splice(state.backupIndex, deleteCount, myFile);

                    return {
                        ...state,
                        myFiles,
                        backupMyFile: null,
                        backupIndex: null,
                    };

                } else {

                    return state;
                }
            }

            case ChatActionTypes.IMPORT_OTHER_FILES: {

                const otherFiles = action.payload ? action.payload.slice() : null;

                return {
                    ...state,
                    otherFiles,
                };
            }

            case ChatActionTypes.DESTROY_STATE: {

                return {
                    ...initialState,
                };
            }

            default:
                return state;
        }
    }
}
