import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChatComponent} from './chat.component';
import {RouterModule} from '@angular/router';
import {PipesModule} from '../../pipes/pipes.module';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {ChatStore} from './store/chat.store';
import {ChatEffects} from './store/chat.effects';
import {ChatTenderViewModule} from './components/chat-tender-view/chat-tender-view.module';
import {DesignBackModule} from '../design/design-back/design-back.module';
import {DesignPreloaderModule} from '../design/design-preloader/design-preloader.module';
import {ChatMessagesModule} from './components/chat-messages/chat-messages.module';
import {ChatReplayModule} from './components/chat-replay/chat-replay.module';
import {DesignFileLinkModule} from '../design/design-file-link/design-file-link.module';
import {ChatUploadFilesModule} from './components/chat-upload-files/chat-upload-files.module';

@NgModule({
    declarations: [ChatComponent],
    imports: [
        CommonModule,
        PipesModule,
        RouterModule.forChild([
            {
                path: '',
                component: ChatComponent,
                data: {
                    module: ChatStore.MODULE_UID,
                }
            }
        ]),
        StoreModule.forFeature(ChatStore.FEATURE_UID, ChatStore.mapReducers),
        EffectsModule.forFeature([
            ChatEffects,
        ]),
        ChatTenderViewModule,
        DesignBackModule,
        DesignPreloaderModule,
        ChatMessagesModule,
        ChatReplayModule,
        DesignFileLinkModule,
        ChatUploadFilesModule,
    ]
})
export class ChatModule {
}
