import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {ChatStore} from '../../store/chat.store';
import {AppStore} from '../../../../store/app.store';
import {Observable, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {ChatActions, ChatActionTypes} from '../../store/chat.actions';
import {Actions, ofType} from '@ngrx/effects';

@Component({
    selector: 'app-chat-replay',
    templateUrl: './chat-replay.component.html',
    styleUrls: ['./chat-replay.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatReplayComponent implements OnInit, OnDestroy {

    formGroup: FormGroup;
    serviceLoading$: Observable<boolean>;

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    constructor(private store$: Store<ChatStore.IState>,
                private actions$: Actions,
                private fb: FormBuilder) {
    }

    ngOnInit() {

        this.initForm();
        this.initStore();
    }

    ngOnDestroy() {

        this._subs.forEach(s => s.unsubscribe());
    }

    private initForm() {

        this.formGroup = this.fb.group({
            message: [null, [Validators.required, Validators.maxLength(2048)]],
        });
    }

    private initStore() {

        this.serviceLoading$ = this.store$.pipe(
            select(AppStore.Selects.appBlock.locks),
            map(locks => !!locks[ChatStore.SERVICES_UID.message])
        );

        this.subs =
            this.actions$.pipe(ofType(ChatActionTypes.ADD_USER_MESSAGE))
                .subscribe(() => {

                    this.formGroup.reset();
                });
    }

    send() {

        this.store$.dispatch(new ChatActions.SendMessage(this.formGroup.value));
    }

}
