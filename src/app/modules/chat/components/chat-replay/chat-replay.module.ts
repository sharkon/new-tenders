import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChatReplayComponent} from './chat-replay.component';
import {ReactiveFormsModule} from '@angular/forms';
import {PipesModule} from '../../../../pipes/pipes.module';
import {DesignPreloaderModule} from '../../../design/design-preloader/design-preloader.module';

@NgModule({
    declarations: [ChatReplayComponent],
    exports: [ChatReplayComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        PipesModule,
        DesignPreloaderModule,
    ]
})
export class ChatReplayModule {
}
