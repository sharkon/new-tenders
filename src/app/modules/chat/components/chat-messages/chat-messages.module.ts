import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChatMessagesComponent} from './chat-messages.component';
import {PipesModule} from '../../../../pipes/pipes.module';

@NgModule({
    declarations: [ChatMessagesComponent],
    exports: [ChatMessagesComponent],
    imports: [
        CommonModule,
        PipesModule,
    ]
})
export class ChatMessagesModule {
}
