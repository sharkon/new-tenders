import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AppConst} from '../../../../consts/app.const';
import {IChat} from '../../../../interfaces/chat.interface';
import {IUser} from '../../../../interfaces/user.interface';

@Component({
    selector: 'app-chat-messages',
    templateUrl: './chat-messages.component.html',
    styleUrls: ['./chat-messages.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatMessagesComponent implements OnInit {

    @Input() messages: IChat.IMessage[];

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withSeconds;
    }

    get USER_ROLES() {
        return IUser.USER_ROLES;
    }

    constructor() {
    }

    ngOnInit() {
    }

}
