import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ITender} from '../../../../interfaces/tender.interface';
import {AppConst} from '../../../../consts/app.const';

@Component({
    selector: 'app-chat-tender-view',
    templateUrl: './chat-tender-view.component.html',
    styleUrls: ['./chat-tender-view.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatTenderViewComponent implements OnInit {

    @Input() tender: ITender.IModelForChat;
    @Input() userIsSeller: boolean;

    get DATE_FORMAT() {
        return AppConst.DATE_PIPE_FORMAT.withoutSeconds;
    }

    constructor() {
    }

    ngOnInit() {
    }

}
