import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatTenderViewComponent } from './chat-tender-view.component';

describe('CuratorTenderViewComponent', () => {
  let component: ChatTenderViewComponent;
  let fixture: ComponentFixture<ChatTenderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatTenderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatTenderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
