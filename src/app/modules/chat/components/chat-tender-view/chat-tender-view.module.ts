import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChatTenderViewComponent} from './chat-tender-view.component';
import {PipesModule} from '../../../../pipes/pipes.module';
import {RouterModule} from '@angular/router';
import {DesignPreloaderModule} from '../../../design/design-preloader/design-preloader.module';
import {DesignTenderPeriodModule} from '../../../design/design-tender-period/design-tender-period.module';

@NgModule({
    declarations: [ChatTenderViewComponent],
    exports: [ChatTenderViewComponent],
    imports: [
        CommonModule,
        PipesModule,
        RouterModule,
        DesignPreloaderModule,
        DesignTenderPeriodModule,
    ],
})
export class ChatTenderViewModule {
}
