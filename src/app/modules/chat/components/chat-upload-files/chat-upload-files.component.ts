import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {IFile} from '../../../../interfaces/file.interface';
import {Store} from '@ngrx/store';
import {ChatStore} from '../../store/chat.store';
import {ChatActions} from '../../store/chat.actions';
import {UploadFile} from 'ngx-uploader';

@Component({
    selector: 'app-chat-upload-files',
    templateUrl: './chat-upload-files.component.html',
    styleUrls: ['./chat-upload-files.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatUploadFilesComponent implements OnInit {

    @Input() files: Observable<IFile.IFileResponse[]>;
    @Input() chatId: number;

    maxFilesLength = 10;
    maxFileSizeMB = 16;

    get restUrl() {
        return ChatStore.SERVICES_URL.file(this.chatId);
    }

    constructor(private store$: Store<ChatStore.IState>) {
    }

    ngOnInit() {
    }

    onAddFiles(uploadFiles: UploadFile[]) {

        const files: IFile.IFileResponse[] = uploadFiles
            ? uploadFiles
                .map(f => <IFile.IFileResponse>(f.response || f))
                .map(f => {
                    return {...f, id: +f.id};
                })
            : [];

        this.store$.dispatch(new ChatActions.ImportMyFiles(files));
    }

    onRemoveFile(fileId: string) {
        this.store$.dispatch(new ChatActions.DeleteMyFile(+fileId));
    }
}
