import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChatUploadFilesComponent} from './chat-upload-files.component';
import {DesignFileUploaderModule} from '../../../design/design-file-uploader/design-file-uploader.module';
import {DesignFileLinkModule} from '../../../design/design-file-link/design-file-link.module';
import {PipesModule} from '../../../../pipes/pipes.module';

@NgModule({
    declarations: [ChatUploadFilesComponent],
    exports: [ChatUploadFilesComponent],
    imports: [
        CommonModule,
        DesignFileUploaderModule,
        DesignFileLinkModule,
        PipesModule,
    ]
})
export class ChatUploadFilesModule {
}
