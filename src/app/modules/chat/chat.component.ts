import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {ChatStore} from './store/chat.store';
import {ChatActions} from './store/chat.actions';
import {CrudServiceActions, CrudServiceActionTypes} from '../../services/crud/store/crud-service.actions';
import {Actions, ofType} from '@ngrx/effects';
import {combineLatest, interval, Observable, Subscription} from 'rxjs';
import {exhaustMap, filter, map, withLatestFrom} from 'rxjs/operators';
import {AppConst} from '../../consts/app.const';
import {IUser} from '../../interfaces/user.interface';
import {AppStore} from '../../store/app.store';
import {ITender} from '../../interfaces/tender.interface';
import {IChat} from '../../interfaces/chat.interface';
import {IFile} from '../../interfaces/file.interface';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatComponent implements OnInit, OnDestroy {

    serviceLoading$: Observable<boolean>;
    tender$: Observable<ITender.IModelForChat>;
    messages$: Observable<IChat.IMessage[]>;
    myFiles$: Observable<IFile.IFileResponse[]>;
    otherFiles$: Observable<IFile.IFileResponse[]>;

    userIsSeller: boolean;

    private _subs: Subscription[] = [];
    set subs(sub) {
        this._subs.push(sub);
    }

    constructor(private actions$: Actions,
                private store$: Store<ChatStore.IState>) {
    }

    ngOnInit() {

        this.initStore();
    }

    ngOnDestroy(): void {

        this._subs.forEach(s => s.unsubscribe());
        this.store$.dispatch(new ChatActions.DestroyState());
    }

    private initStore() {

        this.tender$ = this.store$.pipe(select(ChatStore.Selects.self.tender));

        this.messages$ = combineLatest(
            this.store$.pipe(select(ChatStore.Selects.self.messages)),
            this.store$.pipe(select(ChatStore.Selects.self.userMessages)),
        ).pipe(
            map(([messages, userMessages]) => {

                return [
                    ...(messages || []),
                    ...(userMessages || []),
                ];
            })
        );

        this.myFiles$ = this.store$.pipe(select(ChatStore.Selects.self.myFiles));
        this.otherFiles$ = this.store$.pipe(select(ChatStore.Selects.self.otherFiles));

        this.subs = this.store$.pipe(
            select(AppStore.Selects.appAuth.user),
            filter(user => !!user),
            map(user => (user.role || []).indexOf(IUser.USER_ROLES.SELLER) > -1),
        ).subscribe(userIsSeller => {
            this.userIsSeller = userIsSeller;
        });

        this.serviceLoading$ = this.store$.pipe(
            select(AppStore.Selects.appBlock.locks),
            map(locks => {
                return locks[ChatStore.SERVICES_UID.tender]
                    || locks[ChatStore.SERVICES_UID.messages]
                    || locks[ChatStore.SERVICES_UID.myFiles]
                    || locks[ChatStore.SERVICES_UID.otherFiles];
            }),
        );

        this.subs = this.actions$.pipe(
            ofType(CrudServiceActionTypes.GET_LIST_SUCCESS, CrudServiceActionTypes.GET_LIST_FAILED),
            map((action: CrudServiceActions.GetListSuccess) => action.payload),
            filter(payload => payload.uid === ChatStore.SERVICES_UID.messages || payload.uid === ChatStore.SERVICES_UID.newMessages),
            exhaustMap(() => interval(AppConst.RELOAD_LIST_INTERVAL)),
            withLatestFrom(
                this.store$.pipe(select(ChatStore.Selects.self.tender)),
                this.store$.pipe(select(ChatStore.Selects.self.messages)),
            ),
            filter(([payload, tender]) => !!tender),
        ).subscribe(([payload, tender, messages]) => {

            let lastId = null;

            if (messages && messages.length > 0) {
                lastId = messages[messages.length - 1].id;
            }

            this.store$.dispatch(
                new CrudServiceActions.GetList({
                    url: ChatStore.SERVICES_URL.newMessages(tender.id, lastId),
                    uid: ChatStore.SERVICES_UID.newMessages,
                })
            );
        });
    }

}
