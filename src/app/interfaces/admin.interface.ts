import {ICrud} from './crud.interface';

export namespace IAdmin {

    // GET LIST
    export interface IEmails extends ICrud.IList {
        data: IEmail[];
    }

    // CREATE AND UPDATE
    export interface IEmail extends ICrud.IModel {
        email: string; // max length = 256
    }

    export interface ISellerGroups extends ICrud.IList {
        data: ISellerGroup[];
    }

    export interface ISellerGroup extends ICrud.IModel {
        name: string; // max length = 256
    }

    export interface ISellerGroupEmails extends ICrud.IList {
        data: ISellerGroupEmail[];
    }

    export interface ISellerGroupEmail extends ICrud.IModel {
        name: string; // max length = 256
        email: string; // max length = 256
    }

    export interface ICurators extends ICrud.IList {
        data: ICurator[];
    }

    export interface ICurator extends ICrud.IModel {
        name: string; // max length = 256
        email: string; // max length = 256
        headEmail: string; // max length = 256
    }

}
