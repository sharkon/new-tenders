import {ICrud} from './crud.interface';
import {IUser} from './user.interface';

export namespace IChat {

    export interface IMessage extends ICrud.IModel {
        message: string;
        created: string;
        user_role: IUser.UserRoleType;
    }

    export interface IListModelForCurator {
        id: number;
        curatorAnswer: string; // date of boom
        lastMessage: string;
        lastMessageTime: string;
    }
}
