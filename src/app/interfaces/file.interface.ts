import {UploadFile} from 'ngx-uploader';
import {IUser} from './user.interface';

export namespace IFile {

    export interface IFileResponse {
        id: number;
        name: string;
        url: string;
        source?: string;
    }

    export interface IChatFile extends IFileResponse {
        createTime: string;
        deleteTime?: string;
        roleUpload: IUser.UserRoleType; // кто загрузил
        rolesDownload: IUser.UserRoleType[]; // кто скачивал
    }

    export interface IUploadFile extends UploadFile {
        url: string;
    }

    export interface IError {
        id: string;
        name: string;
        rejectReason?: string;
    }

    export const ERROR_MESSAGE = {
        LIMIT_EXCEEDED: <ErrorMessageType>'LIMIT_EXCEEDED',
        INVALID_VALUE: <ErrorMessageType>'INVALID_VALUE',
    };

    export type ErrorMessageType =
        'LIMIT_EXCEEDED'
        | 'INVALID_VALUE';
}
