export namespace IMap {

    export interface IMapOfString {
        [name: string]: string;
    }

    export interface IMapOfStringRecursive {
        [name: string]: IMapOfStringRecursive;
    }

    export interface IMapOfBoolean {
        [name: string]: boolean;
    }

    export interface IMapNumberOfBoolean {
        [name: number]: boolean;
    }

    export interface IMapOfNumber {
        [name: string]: number;
    }

    export interface IMapNumberOfNumber {
        [name: number]: number;
    }

    export interface IMapOfStringArray {
        [name: string]: string[];
    }
}
