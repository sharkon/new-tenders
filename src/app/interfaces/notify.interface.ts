export namespace INotify {

    export interface ISuccess {
        dialogTitle?: string;
        dialogContent?: string;
    }

    export interface IConfirm extends ISuccess {
    }

    export interface IError extends ISuccess {
    }

    export interface IWarning extends ISuccess {
    }
}
