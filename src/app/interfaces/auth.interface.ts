export namespace IAuth {

    export interface ICaptchaResponse {
        captcha_key: string;
        captcha_image: string;
        image_type: string;
        image_decode: string;
    }

    export interface IRecoveryRequest {
        email: string;
        captcha_value?: string;
        captcha_key?: string;
    }

    export interface ISignInRequest extends IRecoveryRequest {
        password: string;
    }

    export interface IInviteRequest {
        invite: string;
        fullName: string;
        password: string;
        extra: {
            phone: string;
            companyName: string;
            position: string;
        };
        captcha_key: string;
        captcha_value: string;
    }

    export interface IInviteGetItemResponse {
        email: string;
        tender_id: number;
        seller_exists: boolean;
      }

    export interface IAuditorInviteRequest {
        invite: string;
        passwords: string[];
        captcha_key: string;
        captcha_value: string;
    }

    export interface ISignInResponse {
        token: {
            key: string;
        };
    }

    export interface IAuditorSignInResponse extends ISignInResponse {
        liveTime: number; // время жизни токена в минутах
    }

    export interface ISellerSignUpRequest extends ISignInRequest {
        fullName: string;
        phone: string;
        companyName: string;
        inviteToken: string;
    }
}
