export namespace IMultilang {

    export interface IMultilangString {
        az?: string;
        en?: string;
        ru?: string;
    }
}
