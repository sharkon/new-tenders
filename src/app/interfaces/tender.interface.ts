import {ICrud} from './crud.interface';
import {IMultilang} from './multilang.interface';
import {IFile} from './file.interface';

export namespace ITender {

    // GET LIST
    export interface IList extends ICrud.IList {
        data: IListModel[];
    }

    export interface IListModel extends ICrud.IModel {
        name: IMultilang.IMultilangString;
        operatingCompany: OperatingCompanyType;
        curator?: string;
        startDate: string;
        endDate: string;
        sellerGroupName?: string;
    }

    export interface IPeriodOfModel {
        period: PeriodType;
        urgentNumber?: string;
        urgentData?: string;
        quarter?: string;
    }

    // CREATE AND UPDATE
    export interface IModel extends IListModel, IPeriodOfModel {
        description: IMultilang.IMultilangString;
        sellerGroupId: number;
        currency: CurrencyType;
        curatorId: number;
        files?: IFile.IFileResponse[];
        state: StateType;
    }

    export interface IListModelForSeller extends ICrud.IModel {
        name: IMultilang.IMultilangString;
        proposalState: boolean;
        startDate: string;
        endDate: string;
        sellerGroupName: string;
        description: IMultilang.IMultilangString;
        canOpenChat?: boolean;
    }

    export interface IModelForSeller extends IListModelForSeller {
        state: StateType;
        files?: IFile.IFileResponse[];
        operatingCompany?: OperatingCompanyType;
    }

    export interface IModelForChat extends IModelForSeller, IPeriodOfModel {
    }

    export interface IListModelForChatCurator extends ICrud.IModel, IPeriodOfModel {
        sellerGroupName: string;
        operatingCompany: OperatingCompanyType;
        endDate: string;
        emailcur: string;
        namecur: string;
    }

    export interface IModelForCurator extends IListModelForChatCurator {
        name: string;
        startDate: string;
        description: IMultilang.IMultilangString;
        files?: IFile.IFileResponse[];
    }

    export interface IModelForAuditor extends ICrud.IModel {
        name: IMultilang.IMultilangString;
        description: IMultilang.IMultilangString;
        creator: string;
        operatingCompany: OperatingCompanyType;
        startDate: string;
        endDate: string;
        period: PeriodType;
        urgentNumber?: string;
        urgentData?: string;
        quarter?: string;
        state: StateType;
        sellerGroupName?: string;
        curator?: string;
    }

    export interface ITenderStatistics extends ICrud.IModel {
        invites: number;
        proposals: number;
        questions: number;
    }

    // PRINT LIST & DETAIL
    export interface IListModelPrintRequest {
        startDate: string;
        endDate: string;
    }

    export interface IModelPrintRequest {
        tenderId: number;
    }

    export interface IModelPrintResponse {
        file: IFile.IFileResponse;
    }

    export const PERIOD = {
        URGENT: <PeriodType>'URGENT',
        QUARTER: <PeriodType>'QUARTERLY',
    };

    export type PeriodType =
        'URGENT'
        | 'QUARTER';

    export type CurrencyType =
        'USD'
        | 'EUR'
        | 'AZN'
        | 'RUR';

    export const STATE_TYPES = {
        DRAFT: <StateType>'DRAFT',
        PUBLISH: <StateType>'PUBLISH',
        ACTIVE: <StateType>'ACTIVE',
        CLOSE: <StateType>'CLOSE',
        CONTINUE: <StateType>'CONTINUE',
        UNLOCK: <StateType>'UNLOCK',
    };

    export type StateType =
        'DRAFT'
        | 'PUBLISH'
        | 'ACTIVE'
        | 'CLOSE'
        | 'CONTINUE'
        | 'UNLOCK';

    export const QUERY_STATUS_NAME = 'status';

    export const QUERY_STATUS = {
        PUBLISH: <QueryStatusType>'publish',
        ACTIVE: <QueryStatusType>'active',
        DRAFT: <QueryStatusType>'draft',
        FINISH: <QueryStatusType>'finish',
    };

    export type QueryStatusType =
        'publish'
        | 'active'
        | 'draft'
        | 'finish';

    export type OperatingCompanyType =
        'SOC'
        | 'KAOC'
        | 'NOC'
        | 'BOC'
        | 'AOC';

    export interface ICurator extends ICrud.IModel {
        email: string;
        name: string;
        headEmail: string;
        closed: boolean;
    }

    export interface ISellerGroup extends ICrud.IModel {
        name: string;
        closed: boolean;
    }
}
