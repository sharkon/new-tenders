export namespace IUser {

    export interface IUserResponse {
        id: number;
        data: IUserData;
        role: UserRoleType[];
        enabled: boolean;
        extraData: IExtraData;
    }

    export interface IUserData {
        email: string;
        fullName?: string;
    }

    export interface IExtraData {
        phone: string;
        companyName: string;
        position: string;
    }

    export const USER_ROLES = {
        CREATOR: <UserRoleType>'CREATOR',
        SELLER: <UserRoleType>'SELLER',
        SYSTEM_ADMIN: <UserRoleType>'SYSTEM_ADMIN',
        AUDITOR: <UserRoleType>'AUDITOR_FAKE',
        CURATOR: <UserRoleType>'CURATOR',
        CURATOR_BOSS: <UserRoleType>'CURATOR_BOSS',
        BOSS: <UserRoleType>'BOSS',
        SITE_ADMIN: <UserRoleType>'SITE_ADMIN',
    };

    export type UserRoleType =
        'CREATOR'
        | 'SELLER'
        | 'SYSTEM_ADMIN'
        | 'SITE_ADMIN'
        | 'AUDITOR_FAKE'
        | 'CURATOR'
        | 'CURATOR_BOSS'
        | 'BOSS';
}
