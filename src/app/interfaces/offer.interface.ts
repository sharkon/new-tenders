import {ICrud} from './crud.interface';
import {IMultilang} from './multilang.interface';
import {IFile} from './file.interface';
import {ITender} from './tender.interface';
import {IChat} from './chat.interface';

export namespace IOffer {

    // GET LIST
    export interface IList extends ICrud.IList {
        data: IListModel[];
    }

    export interface ITimeOfModel {
        createTime: string;
        publishedTime: string;
    }

    export interface IListModel extends ICrud.IModel, ITimeOfModel {
        tenderId: number;
        tenderName: IMultilang.IMultilangString;
        tenderStartDate: string;
        tenderEndDate: string;
        tenderState: ITender.StateType;
        contactName: string;
        contactPhone: string;
        contactPosition: string;
        state: StateType;
    }

    export interface IListModelForTender extends ICrud.IModel, ITimeOfModel {
        state: StateType;
    }

    export interface IListForAuditorTender extends ICrud.ICRUDGetListResponse {
        data: IListModelForAuditorTender[];
    }

    export interface IListModelForAuditorTender extends ICrud.IModel, ITimeOfModel {
        companyName: string;
        state: StateType;
    }

    export interface IModelForAuditorDetails extends IModel, ITimeOfModel {
        chatFiles?: IFile.IChatFile[]; // файлы только данного продавца по всем заявкам данного закупа + все файлы куратора
        messages?: IChat.IMessage[]; // сообщения только данного продавца по всем заявкам данного закупа + все сообщения куратора
    }

    // CREATE AND UPDATE FORM
    export interface IModel extends ICrud.IModel {
        tenderId: number;
        contactCompanyName: string;
        question?: string;
        contactName: string;
        contactPhone: string;
        contactPosition: string;
        description?: string;
        files?: IFile.IFileResponse[];
        state: StateType;
    }

    export const STATE_TYPES = {
        CHAT_PUBLISH: <StateType>'CHAT_PUBLISH',
        DRAFT: <StateType>'DRAFT',
        PROPOSAL_PUBLISH: <StateType>'PROPOSAL_PUBLISH',
    };

    export type StateType =
        'CHAT_PUBLISH'
        | 'DRAFT'
        | 'PROPOSAL_PUBLISH';

    export const QUERY_STATUS_NAME = 'status';

    export const QUERY_STATUS = {
        CHAT_PUBLISH: <QueryStatusType>'chat_publish',
        DRAFT: <QueryStatusType>'draft',
        PROPOSAL_PUBLISH: <QueryStatusType>'proposal_publish',
    };

    export type QueryStatusType =
        'chat_publish'
        | 'draft'
        | 'proposal_publish';
}
