export namespace ILang {

    export type LangType =
        'az' |
        'en' |
        'ru';
}
