
export interface DataTranslateInterface {
    T: {
        LANG: string,
        roles: {
            ADMINISTRATOR: string,
            CREATOR: string,
            SELLER: string,
            CURATOR: string,
            AUDITOR: string,
        },
        footer: {
            copyright: string,
            copyrightBy: string,
            address: {
                london: {
                    street: string,
                    country: string,
                },
                baku: {
                    street: string,
                    country: string,
                }
            }
        },
        pages: {
            COMPANY: string,
            SERVICES: string,
            PARTNERS: string,
            NEWS: string,
            USEFUL_INFORMATION: string,
            CONTACTS: string,
            PURCHASES: string
        },
        buttons: {
            save: string,
            saveAndPrint: string,
            saveAndPublish: string,
            publish: string,
            send: string,
            cancel: string,
            sendAll: string,
            add: string,
            print: string,
            delete: string,
            signin: string,
            file: string,
            seeMore: string,
        },
        actions: {
            edit: string,
            back: string,
            open: string,
            reply: string,
            instruction: string,
        },
        files: {
            documents: string,
            limit: string,
            prepare: string,
            name: string,
            size: string,
            status: string,
            actions: string,
            upload: string,
            cancel: string,
            remove: string,
            uploadAll: string,
            cancelAll: string,
            removeAll: string,
            queueProgress: string,
            loaded: string,
            error: string,
            sizeError: string,
            maxFiles: string,
            extentionError: string
        },
        tenders: {
            id: string,
            forAdd: string,
            forNumber: string,
            name: string,
            period: string,
            sphere: string,
            viewdocs: string,
            currency: string,
            additional: string,
            documents: string,
            groupSellers: string,
            emptyList: string,
            creator: string,
            number: string,
            closed: string,
            chatFiles: string,
            sellerFiles: string,
            operatingCompany: string,
            curator: string,
            status: string,
            startDate: string,
            endDate: string,
            printError: string,
            periodType: string,
        },
        goffers: {
            id: string,
            date: string,
            companyName: string,
            question: string,
            contactName: string,
            contactPhone: string,
            contactPosition: string,
            additional: string,
            documents: string,
            viewdocs: string,
            proposalStateFalse: string,
            proposalStateFalseText: string,
            proposalStateTrue: string,
            proposalStateTrueText: string,
        },
        header: {
            signin: string,
            signout: string,
            changePassword: string,
            rootPage: string,
        },
        openTenders: {
            pageHeader: string,
            createTender: string,
            changeTenders: string,
            changeTender: string,
            publishedTenders: string,
            activeTenders: string,
            finishedTenders: string,
            adminPage: string,
            closedTenders: string,
            myOffers: string
        },
        closedTenders: {
            pageHeader: string,
            emptyList: string,
        },
        sellerGroup: {
            pageHeader: string,
        },
        groupEmails: {
            pageHeader: string,
        },
        curators: {
            pageHeader: string,
            editBoss: string,
            addCurator: string
        },
        newTender: {
            pageHeader: string,
            nameOfTender: string,
            startDate: string,
            endDate: string,
            time: string,
            curator: string,
            emailcur: string,
            namecur: string,
            example: string,
            fileLock: string,
            period: string,
            urgentNumber: string,
            urgentDate: string,
            quarter: string,
            fastSearch: string,
        },
        newTenderDialogs: {
            noEditPublish: string,
            noForgetPublish: string,
            successSaved: string,
            notSaved: string,
            successPublished: string,
            notPublished: string,
            deleteSelected: string,
            successDeleted: string,
            isPublishTender: string,
            afterPublishNoChanges: string,
        },
        tendersEdit: {
            pageHeader: string,
        },
        checkSeller: {
            pageHeader: string,
            phankyou: string,
            askform: string,
            fio: string,
            your: string,
            yourPassword: string,
            phone: string,
            company: string,
            position: string
        },
        login: {
            pageHeader: string,
            login: string,
            password: string,
            forgot: string,
        },
        recovery: {
            successMessageStart: string,
            successMessageEnd: string,
        },
        changepwd: {
            pageHeader: string,
            password2: string,
        },
        admin: {
            pageHeader: string,
            save: string,
        },
        tender: {
            pageHeader: string,
            toOffer: string,
            toChat: string,
        },
        newOffer: {
            pageHeader: string,
            fileLock: string,
            publish: string,
            sendAsk: string,
            saveAndPublish: string,
            saveAndSendAsk: string,
        },
        chat: {
            pageHeader: string,
            seller: string,
            curator: string,
            header: string,
            boss: string,
            myFiles: string,
        },
        tenderGroupUnlock: {
            pageHeader: string,
            inputKeys: string,
            key: string,
            updateTender: string,
            unlock: string,
        },
        offers: {
            pageHeader: string,
            list: string,
            chat: string,
            dirty: string,
            files: string,
            chatFiles: string
        },
        offer: {
            pageHeader: string
        },
        offerDialogs: {
            successSaveTitle: string,
            successSaveContent: string,
            successPublished: string,
            notPublished: string,
            successPublishedToChat: string,
            notPublishedToChat: string,
            noEditPublish: string,
            notSaved: string,
            isPublishOffer: string,
            isPublishOfferToChat: string,
            afterPublishNoChanges: string,
        },
        sellerPage: {
            complete: string,
            chat: string,
            dirtyComplete: string,
            dirtyChat: string,
        },
        curatorPage: {
            pageHeader: string,
            endDate: string,
            messageText: string,
            messageTime: string,
            answerTime: string,
            chatLink: string,
            answerEnd: string,
            yourAnswer: string,
            curatorAnswer: string,
            quarter: string,
            urgently: string,
        },
        periodType: {
            urgent: string,
            quarter: string,
        },
        quarterType: {
            first: string,
            second: string,
            third: string,
            fourth: string
        },
        form: {
            headers: {
                invite: string
            },
            errors: {
                required: string,
                email: string,
                minLength: string,
                userRegistered: string,
            },
            help: {
                password: string,
                lessPassword: string,
            }
        },
        cabinetPages: {
            seller: {
                openTenders: string,
                offers: string,
                offersActive: string,
                offersChat: string,
                offersDraft: string,
                offerChat: string,
                offerActive: string,
            }
        },
        dialogs: {
            outPageQuestion: string,
            haveUnSaveChanges: string,
            updateTenderQuestion: string,
            unlockTenderQuestion: string,
            yes: string,
            no: string,
            repeatAfterError: string,
            successCreated: string,
            successUpdated: string,
            successDeleted: string,
            successDone: string,
            error: string,
            warning: string,
        },
        authErrors: {
            error: string,
            email: {
                invalidValue: string,
            },
            user: {
                userBanned: string,
            },
            sendEmail: {
                error: string,
            },
            nonFieldErrors: {
                captchaInvalid: string,
            },
            auth: {
                invalidValue: string,
                passwordValue: string;
            },
            uuid: {
                invalidValue: string,
            },
        },
        networkError: string,
        notFoundError: string,
        forbiddenError: string,
        formErrors: {
            invalidValue: string,
        },
        auditorPages: {
            invite: {
                warning: string;
                opened: string;
            }
        },
        statistics: {
            invites: string;
            proposals: string;
            questions: string;
        },
        settingsPage: {
            pageHeader: string,
            oldPassword: string,
            newPassword: string,
            repeatNewPassword: string,
            repeatError: string,
            saveError: string,
        },
        emptyList: string,
        noMessages: string,
    };
}
