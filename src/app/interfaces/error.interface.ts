export namespace IError {

    export interface IBackendError {
        [id: string]: IBackendErrorType[];
    }

    export interface IBackendTranslateError {
        [id: string]: {
            [id: string]: string
        };
    }

    export type IBackendErrorType =
        'DUPLICATED'
        | 'REQUIRED'
        | 'IS_NULL'
        | 'INVALID_VALUE'
        | 'USER_BANNED'
        | 'ERROR'
        | 'CAPTCHA_INVALID';

    export const BACKEND_ERROR_TYPES = {
        DUPLICATED: <IBackendErrorType>'DUPLICATED',
        REQUIRED: <IBackendErrorType>'REQUIRED',
        IS_NULL: <IBackendErrorType>'IS_NULL',
        INVALID_VALUE: <IBackendErrorType>'INVALID_VALUE',
        USER_BANNED: <IBackendErrorType>'USER_BANNED',
        ERROR: <IBackendErrorType>'ERROR',
        CAPTCHA_INVALID: <IBackendErrorType>'CAPTCHA_INVALID',
    };
}
