export namespace IPagination {

    export interface IPaginationResponse {
        beginPage: number;
        endPage: number;
        hasNextPage: boolean;
        hasPrevPage: boolean;
        limit: number;
        offset: number;
        totalItems: number;
        totalPages: number;
        currentPage?: number;
    }

    export interface IPaginationOnlyRequest {
        offset: number;
        limit: number;
    }

    export interface IPaginationConfig {
        defaultRequest: IPaginationOnlyRequest;
        limitValues: number[];
        perPageOption: boolean;
        seeMoreOption: boolean;
    }

    export interface IPaginationPages {
        label: string;
        value: number;
    }

    export interface ISortRequest {
        order_by: PaginationOrderByType;
        order_direction: PaginationOrderDirectionType;
    }

    export interface ISortConfig {
        defaultRequest: ISortRequest;
        orderByValues: PaginationOrderByType[];
    }

    export interface IPaginationRequest {
        offset: number;
        limit: number;
        order_by?: PaginationOrderByType;
        order_direction?: PaginationOrderDirectionType;
    }

    export interface IPatchPaginationRequest {
        offset?: number;
        limit?: number;
        order_by?: PaginationOrderByType;
        order_direction?: PaginationOrderDirectionType;
    }

    export interface IListResponse {
        data: any[];
        pageInfo: IPaginationResponse;
    }

    export const PAGINATION_ORDER_BY = {
        NAME: <PaginationOrderByType>'name',
        NAME_SORT: <PaginationOrderByType>'name_sort',
        DATE: <PaginationOrderByType>'date',
        USE_TOTAL: <PaginationOrderByType>'useTotal',
        PRICE: <PaginationOrderByType>'price',
    };

    export type PaginationOrderByType = 'name' | 'date' | 'useTotal' | 'name_sort' | 'price';

    export const PAGINATION_ORDER_DIRECTION = {
        ASC: <PaginationOrderDirectionType>'asc',
        DESC: <PaginationOrderDirectionType>'desc',
    };

    export type PaginationOrderDirectionType = 'asc' | 'desc';

    export interface IPaginationOrder {
        orderBy: string;
        orderDirection?: PaginationOrderDirectionType;
    }
}
