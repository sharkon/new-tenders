import { IError } from './error.interface';
import { IPagination } from './pagination.interface';

export namespace ICrud {

    export interface IModel {
        id: number;
    }

    export interface IList {
        data: IModel[];
        pageInfo?: IPagination.IPaginationResponse;
    }

    export interface ICloseModel extends IModel {
        close: boolean;
    }

    // CRUD REQUEST

    export interface ICRUDRequest {
        url: string;
        uid: string;
        notify?: boolean;
        deniedNotifySuccess?: boolean;
        mapMessage?: IError.IBackendTranslateError;
        options?: any;
    }

    export interface ICRUDPostRequest extends ICRUDRequest {
        item: any;
    }

    export interface ICRUDCreateRequest extends ICRUDRequest {
        item: IModel;
    }

    export interface ICRUDUpdateRequest extends ICRUDRequest {
        item: IModel | IModel[];
    }

    // CRUD RESPONSE

    export interface ICRUDResponse {
        uid: string;
    }

    export interface ICRUDCreateResponse extends ICRUDResponse {
        item: IModel;
    }

    export interface ICRUDUpdateResponse extends ICRUDResponse {
        item: IModel;
    }

    export interface ICRUDDeleteResponse extends ICRUDResponse {
        id: number;
    }

    // FOR ONE MODEL

    export interface ICRUDGetItemResponse extends ICRUDResponse {
        item: IModel;
    }

    // FOR LIST OF MODELS

    export interface IGetListResponse {
        data: IModel[];
        pageInfo?: IPagination.IPaginationResponse;
    }

    export interface ICRUDGetListResponse extends ICRUDResponse {
        data: IModel[];
        pageInfo?: IPagination.IPaginationResponse;
    }

    // FOR ERROR

    export interface ICRUDError extends ICRUDResponse {
        error: any;
    }

    export interface ICrudConfig {
        crudUrl: string;
        serviceUid: string;
    }

}
