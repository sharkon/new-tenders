import {Injectable, Injector} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from '../../services/auth/auth.service';
import {tap} from 'rxjs/operators';
import {NotifyService} from '../../services/notify/notify.service';
import {TranslateService} from '../../services/translate/translate.service';
import {Router} from '@angular/router';

@Injectable()
export class ErrorsInterceptor implements HttpInterceptor {

    constructor(private injector: Injector) {
    }

    private onError(err) {

        console.error(err);

        const notifyService = this.injector.get(NotifyService);
        const translateService = this.injector.get(TranslateService);

        notifyService.error({
            dialogTitle: translateService.data.T.networkError,
        });
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(req).pipe(tap(
            event => {

            },
            err => {

                if (err && err instanceof HttpErrorResponse) {

                    if (err.status === 401) {

                        const authService = this.injector.get(AuthService);
                        authService.logOut();

                    } else if (err.status === 403 || err.status === 404 || /[а-яё]+/i.test(req.url)) {

                        const router = this.injector.get(Router);
                        router.navigateByUrl('/', {replaceUrl: true});

                    } else if (err.status !== 400) {

                        this.onError(err);
                    }

                } else {

                    this.onError(err);
                }

            })
        );
    }
}
