import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class JSONInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (!req.headers.has('Content-Type') &&
            !(req.body instanceof FormData)) {

            req = req.clone({
                headers: req.headers.set('Content-Type', 'application/json')
            });
        }

        return next.handle(req);
    }
}
