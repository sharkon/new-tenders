import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth/auth.service';

@Injectable()
export class JWTInterceptor implements HttpInterceptor {

    constructor(private injector: Injector) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const authService = this.injector.get(AuthService);
        const authHeader = authService.getAuthorizationHeader();

        if (authHeader) {

            req = req.clone({
                headers: req.headers.set('Authorization', authHeader)
            });

        }

        return next.handle(req);
    }
}
