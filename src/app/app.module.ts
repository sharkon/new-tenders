import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {HeaderModule} from './modules/header/header.module';
import {FooterModule} from './modules/footer/footer.module';

import {TranslateService} from './services/translate/translate.service';
import {AuthService} from './services/auth/auth.service';

import {MetaReducer, StoreModule} from '@ngrx/store';
import {AppStore} from './store/app.store';
import {EffectsModule} from '@ngrx/effects';
import {AppRouterEffects} from './store/router/app-router.effects';
import {AppAuthEffects} from './store/auth/app-auth.effects';
import {RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';

import {WINDOW_PROVIDERS} from './providers/window.providers';

import {URLInterceptor} from './interceptors/url/url.interceptor';
import {JWTInterceptor} from './interceptors/jwt/jwt.interceptor';
import {ErrorsInterceptor} from './interceptors/error/error.interceptor';

import {NoAuthGuard} from './guards/no-auth/no-auth.guard';
import {RoleGuard} from './guards/role/role.guard';

import {IUser} from './interfaces/user.interface';

import {CustomRouterStateSerializer} from './providers/custom-router-state-serializer';
import {CrudServiceEffects} from './services/crud/store/crud-service.effects';

import {environment} from '../environments/environment';
import {DatePipe} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE} from 'ng-pick-datetime';
import {OwlMomentDateTimeModule} from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time.module';
import {AppConst} from './consts/app.const';
import {storeFreeze} from 'ngrx-store-freeze';
import {AppLangConst} from './consts/app-lang.const';
import {SellerInviteDataGuard} from './modules/seller/seller-invite/guards/seller-invite-data/seller-invite-data.guard';
import {AuditorInviteDataGuard} from './modules/auditor/auditor-invite/guards/auditor-invite-data/auditor-invite-data.guard';
import {AuthGuard} from './guards/auth/auth.guard';
import {JSONInterceptor} from './interceptors/json/json.interceptor';
import {NoCacheInterceptor} from './interceptors/no-cache/no-cache.interceptor';

export function setupTranslate(translateService: TranslateService): Function {
    return () => translateService.use(AppLangConst.DEFAULT_LANG);
}

export function setupUser(authService: AuthService): Function {
    return () => authService.setupUser();
}

export const metaReducers: MetaReducer<AppStore.IState>[] = !environment.production ? [storeFreeze] : [];

const providers: any[] = [
    {
        provide: RouterStateSerializer,
        useClass: CustomRouterStateSerializer
    },
    {
        provide: APP_INITIALIZER,
        useFactory: setupTranslate,
        deps: [TranslateService],
        multi: true
    },
    {
        provide: APP_INITIALIZER,
        useFactory: setupUser,
        deps: [AuthService],
        multi: true
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: URLInterceptor,
        multi: true,
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: JSONInterceptor,
        multi: true,
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: NoCacheInterceptor,
        multi: true,
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: JWTInterceptor,
        multi: true,
    },
    {
        provide: HTTP_INTERCEPTORS,
        useClass: ErrorsInterceptor,
        multi: true,
    },
    WINDOW_PROVIDERS,
    DatePipe,
    {
        provide: OWL_DATE_TIME_LOCALE,
        useValue: AppLangConst.DEFAULT_CALENDAR_LANG,
    },
    {
        provide: OWL_DATE_TIME_FORMATS,
        useValue: AppConst.MY_MOMENT_FORMATS,
    },
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        HeaderModule,
        FooterModule,
        RouterModule.forRoot([
            {
                path: '',
                loadChildren: './modules/public/public.module#PublicModule',
            },
            {
                path: 'login',
                loadChildren: './modules/auth/auth-root/auth-root.module#AuthRootModule',
                canActivate: [NoAuthGuard],
            },
            {
                path: 'settings',
                loadChildren: './modules/settings/settings.module#SettingsModule',
                canActivate: [AuthGuard],
            },
            {
                path: 'creator',
                loadChildren: './modules/creator/creator-root/creator-root.module#CreatorRootModule',
                canActivate: [RoleGuard],
                data: {
                    roleWhiteList: [IUser.USER_ROLES.CREATOR],
                }
            },
            {
                path: 'seller/invite/:inviteToken',
                loadChildren: './modules/seller/seller-invite/seller-invite.module#SellerInviteModule',
                canActivate: [SellerInviteDataGuard],
            },
            {
                path: 'seller',
                loadChildren: './modules/seller/seller-root/seller-root.module#SellerRootModule',
                canActivate: [RoleGuard],
                data: {
                    roleWhiteList: [IUser.USER_ROLES.SELLER],
                }
            },
            {
                path: 'auditor/invite/:inviteToken',
                loadChildren: './modules/auditor/auditor-invite/auditor-invite.module#AuditorInviteModule',
                canActivate: [AuditorInviteDataGuard],
            },
            {
                path: 'auditor',
                loadChildren: './modules/auditor/auditor-root/auditor-root.module#AuditorRootModule',
                canActivate: [RoleGuard],
                data: {
                    roleWhiteList: [IUser.USER_ROLES.AUDITOR],
                }
            },
            {
                path: 'curator',
                loadChildren: './modules/curator/curator-root/curator-root.module#CuratorRootModule',
                canActivate: [RoleGuard],
                data: {
                    roleWhiteList: [IUser.USER_ROLES.CURATOR, IUser.USER_ROLES.CURATOR_BOSS, IUser.USER_ROLES.BOSS],
                }
            },
            {
                path: 'admin',
                loadChildren: './modules/admin/admin-root/admin-root.module#AdminRootModule',
                canActivate: [RoleGuard],
                data: {
                    roleWhiteList: [IUser.USER_ROLES.SITE_ADMIN],
                }
            },
            {
                path: '**',
                redirectTo: ''
            }
        ]),
        StoreModule.forRoot(AppStore.mapReducers, {metaReducers}),
        EffectsModule.forRoot([
            AppRouterEffects,
            AppAuthEffects,
            CrudServiceEffects,
        ]),
        StoreRouterConnectingModule.forRoot(),
        StoreDevtoolsModule.instrument({maxAge: 50}),
        OwlMomentDateTimeModule,
    ],
    providers,
    bootstrap: [AppComponent]
})
export class AppModule {
}
