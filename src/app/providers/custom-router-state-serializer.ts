import { Params, RouterStateSnapshot } from '@angular/router';
import { RouterStateSerializer } from '@ngrx/router-store';
import { IMap } from '../interfaces/map.interface';

export interface RouterStateUrl extends RouterStateSnapshot {
    url: string;
    params: Params;
    queryParams: Params;
    data: IMap.IMapOfString;
    fragment: string;
}

export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateUrl> {
    serialize(routerState: RouterStateSnapshot): RouterStateUrl {
        let route = routerState.root;

        while (route.firstChild) {
            route = route.firstChild;
        }

        const { url, root: { queryParams, fragment } } = routerState;
        const { params, data } = route;

        // Only return an object including the URL, params and query params
        // instead of the entire snapshot
        return { url, params, queryParams, data, fragment, root: null };
    }
}
