import { InjectionToken } from '@angular/core';
import { IPagination } from '../../interfaces/pagination.interface';

export class PaginationStoreConfig {
    storeUid: string;
    moduleUid: string;
    config: IPagination.IPaginationConfig;
}

export const PAGINATION_STORE_CONFIG = new InjectionToken<PaginationStoreConfig>('pagination.store.config');
