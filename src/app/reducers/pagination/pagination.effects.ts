import {Injectable} from '@angular/core';
import {Actions, ofType} from '@ngrx/effects';
import {ROUTER_NAVIGATED, RouterNavigationAction} from '@ngrx/router-store';
import {Store} from '@ngrx/store';
import {PaginationActions} from './pagination.actions';
import {PaginationStoreConfig} from './pagination.store.config';
import {PaginationStore} from './pagination.reducer';
import {filter, map, mergeMap} from 'rxjs/operators';
import {RouterStateUrl} from '../../providers/custom-router-state-serializer';

@Injectable()
export class PaginationEffects {

    constructor(private storeConfig: PaginationStoreConfig,
                private store$: Store<PaginationStore.IState>,
                private actions$: Actions) {

        this.actions$
            .pipe(
                ofType(ROUTER_NAVIGATED),
                map((action: RouterNavigationAction<RouterStateUrl>) => action.payload),
                filter(payload => {
                    return payload.routerState.data.module === this.storeConfig.moduleUid;
                }),
                mergeMap(payload => {

                    const offset: number = parseFloat(payload.routerState.queryParams['offset']);
                    const limit: number = parseFloat(payload.routerState.queryParams['limit']);

                    return [
                        new PaginationActions.SetRequest(this.storeConfig.storeUid, {offset, limit}),
                        new PaginationActions.SetResponse(this.storeConfig.storeUid, null),
                    ];
                })
            )
            .subscribe(action => this.store$.dispatch(action));

        this.store$.dispatch(new PaginationActions.SetConfig(this.storeConfig.storeUid, this.storeConfig.config));
    }
}
