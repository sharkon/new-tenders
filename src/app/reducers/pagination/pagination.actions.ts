import {Action} from '@ngrx/store';
import { IPagination } from '../../interfaces/pagination.interface';

export namespace PaginationActionTypes {

    export const SET_CONFIG = '[Pagination] SET_CONFIG';
    export const SET_REQUEST = '[Pagination] SET_REQUEST';
    export const SET_RESPONSE = '[Pagination] SET_RESPONSE';
}

export namespace PaginationActions {

    export class SetConfig implements Action {

        readonly type = PaginationActionTypes.SET_CONFIG;

        constructor(public uid: string, public payload: IPagination.IPaginationConfig) {
        }
    }

    export class SetRequest implements Action {

        readonly type = PaginationActionTypes.SET_REQUEST;

        constructor(public uid: string, public payload: IPagination.IPaginationOnlyRequest) {
        }
    }

    export class SetResponse implements Action {

        readonly type = PaginationActionTypes.SET_RESPONSE;

        constructor(public uid: string, public payload: IPagination.IPaginationResponse) {
        }
    }

    export type All =
        SetConfig
        | SetRequest
        | SetResponse;
}
