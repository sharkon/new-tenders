import { AppStore } from '../../store/app.store';
import { PaginationActions, PaginationActionTypes } from './pagination.actions';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { IPagination } from '../../interfaces/pagination.interface';

export namespace PaginationStore {

    export interface IState {
        request: IPagination.IPaginationOnlyRequest;
        response: IPagination.IPaginationResponse;
        config: IPagination.IPaginationConfig;
    }

    export interface IExtendedState extends AppStore.IState {
        pagination: IState;
    }

    const initialState: IState = {
        request: null,
        response: null,
        config: {
            defaultRequest: {
                offset: 0,
                limit: 50,
            },
            limitValues: [10, 50, 100],
            perPageOption: false,
            seeMoreOption: false,
        },
    };

    export interface ISelects {
        request: (state: IState) => IPagination.IPaginationOnlyRequest;
        response: (state: IState) => IPagination.IPaginationResponse;
        config: (state: IState) => IPagination.IPaginationConfig;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Catalog2SearchProductsPagination implements ISelects {

            request = createSelector(
                selector,
                (state: IState) => state.request,
            );

            response = createSelector(
                selector,
                (state: IState) => state.response,
            );

            config = createSelector(
                selector,
                (state: IState) => state.config,
            );

        }

        return new Catalog2SearchProductsPagination();
    }

    export function createReducer(uid: string) {

        return function reducer(state = { ...initialState }, action: PaginationActions.All) {

            if (action.uid !== uid) {
                return state;
            }

            switch (action.type) {

                case PaginationActionTypes.SET_CONFIG: {

                    const config = action.payload ? { ...action.payload } : { ...state.config };
                    const request = { ...config.defaultRequest };

                    return {
                        ...state,
                        request,
                        config,
                    };
                }

                case PaginationActionTypes.SET_REQUEST: {

                    const request = action.payload ? { ...action.payload } : null;

                    let offset = request && !isNaN(request.offset) ? request.offset : state.config.defaultRequest.offset;
                    let limit = request && !isNaN(request.limit) ? request.limit : state.config.defaultRequest.limit;

                    if (offset < 0) {
                        offset = 0;
                    } else if (limit && offset % limit !== 0) {
                        // кратность нарушена
                        offset = 0;
                    }

                    const limitValues = state.config && state.config.limitValues;

                    if (limitValues && limitValues.length) {

                        const find = limitValues.find(val => val === limit);

                        if (!find) {
                            limit = limitValues[0];
                        }
                    }

                    return {
                        ...state,
                        request: {
                            offset,
                            limit,
                        },
                    };
                }

                case PaginationActionTypes.SET_RESPONSE: {

                    const response = action.payload ? { ...action.payload } : null;

                    return {
                        ...state,
                        response,
                    };
                }

                default: {

                    return state;
                }
            }
        };
    }

}
