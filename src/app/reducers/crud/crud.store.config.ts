import {InjectionToken} from '@angular/core';
import { ICrud } from '../../interfaces/crud.interface';

export class CrudStoreConfig {
    storeUid: string;
    config: ICrud.ICrudConfig;
}

export const CRUD_STORE_CONFIG = new InjectionToken<CrudStoreConfig>('crud.store.config');
