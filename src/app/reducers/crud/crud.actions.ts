import { Action } from '@ngrx/store';
import { ICrud } from '../../interfaces/crud.interface';

export namespace CrudActionTypes {

    export const IMPORT_MODELS = '[Crud] IMPORT_MODELS';

    export const UPDATE_MODEL = '[Crud] UPDATE_MODEL';
    export const UPDATE_MODEL_SUCCESS = '[Crud] UPDATE_MODEL_SUCCESS';
    export const UPDATE_MODEL_FAILED = '[Crud] UPDATE_MODEL_FAILED';

    export const DELETE_MODEL = '[Crud] DELETE_MODEL';
    export const DELETE_MODEL_SUCCESS = '[Crud] DELETE_MODEL_SUCCESS';
    export const DELETE_MODEL_FAILED = '[Crud] DELETE_MODEL_FAILED';

    export const DESTROY_STATE = '[Crud] DESTROY_STATE';
}

export namespace CrudActions {

    export class ImportModels implements Action {

        readonly type = CrudActionTypes.IMPORT_MODELS;

        constructor(public uid: string, public payload: ICrud.IModel[]) {
        }
    }

    export class UpdateModel implements Action {

        readonly type = CrudActionTypes.UPDATE_MODEL;

        constructor(public uid: string, public payload: ICrud.IModel) {
        }
    }

    export class UpdateModelSuccess implements Action {

        readonly type = CrudActionTypes.UPDATE_MODEL_SUCCESS;

        constructor(public uid: string, public payload: ICrud.IModel) {
        }
    }

    export class UpdateModelFailed implements Action {

        readonly type = CrudActionTypes.UPDATE_MODEL_FAILED;

        constructor(public uid: string) {
        }
    }

    export class DeleteModel implements Action {

        readonly type = CrudActionTypes.DELETE_MODEL;

        constructor(public uid: string, public payload: number) {
        }
    }

    export class DeleteModelSuccess implements Action {

        readonly type = CrudActionTypes.DELETE_MODEL_SUCCESS;

        constructor(public uid: string, public payload: number) {
        }
    }

    export class DeleteModelFailed implements Action {

        readonly type = CrudActionTypes.DELETE_MODEL_FAILED;

        constructor(public uid: string) {
        }
    }

    export class DestroyState implements Action {

        readonly type = CrudActionTypes.DESTROY_STATE;

        constructor(public uid: string) {
        }
    }

    export type All =
        ImportModels
        | UpdateModel
        | UpdateModelSuccess
        | UpdateModelFailed
        | DeleteModel
        | DeleteModelSuccess
        | DeleteModelFailed
        | DestroyState;

}
