import { Injectable } from '@angular/core';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { CrudStore } from './crud.reducer';
import { CrudStoreConfig } from './crud.store.config';
import { CrudActions, CrudActionTypes } from './crud.actions';
import { CrudServiceActions, CrudServiceActionTypes } from '../../services/crud/store/crud-service.actions';
import { Subscription } from 'rxjs';
import { filter, map, mergeMap } from 'rxjs/operators';
import { ICrud } from '../../interfaces/crud.interface';
import { AppErrorActions } from '../../store/error/app-error.actions';

@Injectable()
export class CurdEffects {

    constructor(private storeConfig: CrudStoreConfig,
        private store$: Store<CrudStore.IState>,
        private actions$: Actions) {

        const UPDATE_SERVICE_UID = `[Crud][Update]${storeConfig.config.serviceUid}`;
        const DELETE_SERVICE_UID = `[Crud][Delete]${storeConfig.config.serviceUid}`;

        const updateModel$: Subscription = this.actions$
            .pipe(
                ofType(CrudActionTypes.UPDATE_MODEL)
                , filter((action: CrudActions.UpdateModel) => action.uid === storeConfig.storeUid)
                , map((action: CrudActions.UpdateModel) => {

                    return new CrudServiceActions.Patch({
                        url: `${storeConfig.config.crudUrl}${action.payload.id}/`,
                        uid: UPDATE_SERVICE_UID,
                        item: action.payload,
                    });
                })
            )
            .subscribe(action => this.store$.dispatch(action));

        const updateModelSuccess$: Subscription = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.PATCH_SUCCESS)
                , map((action: CrudServiceActions.PatchSuccess) => action.payload)
                , filter(payload => payload.uid === UPDATE_SERVICE_UID)
                , map((payload: ICrud.ICRUDUpdateResponse) => {

                    return new CrudActions.UpdateModelSuccess(storeConfig.storeUid, payload.item);
                })
            )
            .subscribe(action => this.store$.dispatch(action));

        const updateModelFailed$: Subscription = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.PATCH_FAILED)
                , map((action: CrudServiceActions.PatchFailed) => action.payload)
                , filter(payload => payload.uid === UPDATE_SERVICE_UID)
                , mergeMap(payload => {

                    return [
                        new AppErrorActions.AddError(payload),
                        new CrudActions.UpdateModelFailed(storeConfig.storeUid),
                    ];
                })
            )
            .subscribe(action => this.store$.dispatch(action));

        const deleteModel$: Subscription = this.actions$
            .pipe(
                ofType(CrudActionTypes.DELETE_MODEL)
                , filter((action: CrudActions.DeleteModel) => action.uid === storeConfig.storeUid)
                , map((action: CrudActions.DeleteModel) => {

                    return new CrudServiceActions.Delete({
                        url: `${storeConfig.config.crudUrl}${action.payload}/`,
                        uid: DELETE_SERVICE_UID,
                    });
                })
            )
            .subscribe(action => this.store$.dispatch(action));

        const deleteModelSuccess$: Subscription = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.DELETE_SUCCESS)
                , map((action: CrudServiceActions.DeleteSuccess) => action.payload)
                , filter(payload => payload.uid === DELETE_SERVICE_UID)
                , map((payload: ICrud.ICRUDDeleteResponse) => {

                    return new CrudActions.DeleteModelSuccess(storeConfig.storeUid, payload.id);
                })
            )
            .subscribe(action => this.store$.dispatch(action));

        const deleteModelFailed$: Subscription = this.actions$
            .pipe(
                ofType(CrudServiceActionTypes.DELETE_FAILED)
                , map((action: CrudServiceActions.DeleteFailed) => action.payload)
                , filter(payload => payload.uid === DELETE_SERVICE_UID)
                , mergeMap(payload => {

                    return [
                        new AppErrorActions.AddError(payload),
                        new CrudActions.DeleteModelFailed(storeConfig.storeUid),
                    ];
                })
            )
            .subscribe(action => this.store$.dispatch(action));

        const destroyState$: Subscription = this.actions$
            .pipe(
                ofType(CrudActionTypes.DESTROY_STATE)
                , map(() => {
                    return new CrudServiceActions.CancelRequests([
                        UPDATE_SERVICE_UID,
                        DELETE_SERVICE_UID,
                    ]);
                })
            )
            .subscribe(action => this.store$.dispatch(action));
    }
}
