import { ICrud } from '../../interfaces/crud.interface';
import { AppStore } from '../../store/app.store';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { CrudActions, CrudActionTypes } from './crud.actions';

export namespace CrudStore {

    export interface IState {
        models: ICrud.IModel[];
        backupModel: ICrud.IModel;
        backupIndex: number;
    }

    export interface IExtendedState extends AppStore.IState {
        pagination: IState;
    }

    const initialState: IState = {
        models: null,
        backupModel: null,
        backupIndex: null,
    };

    export interface ISelects {
        models: (state: IState) => ICrud.IModel[];
        backupModel: (state: IState) => ICrud.IModel;
        backupIndex: (state: IState) => number;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Crud implements ISelects {

            models = createSelector(selector, (state: IState) => state.models);
            backupModel = createSelector(selector, (state: IState) => state.backupModel);
            backupIndex = createSelector(selector, (state: IState) => state.backupIndex);
        }

        return new Crud();
    }

    export function createReducer(uid: string) {

        return function reducer(state = { ...initialState }, action: CrudActions.All) {

            if (action.uid !== uid) {
                return state;
            }

            switch (action.type) {

                case CrudActionTypes.IMPORT_MODELS: {

                    const models = action.payload ? action.payload.slice() : null;

                    return {
                        ...state,
                        models,
                    };
                }

                case CrudActionTypes.UPDATE_MODEL: {

                    const models = state.models.slice();

                    const backupIndex = models.findIndex(r => r.id === action.payload.id);
                    const backupModel = { ...models[backupIndex] };

                    models.splice(backupIndex, 1, { ...action.payload });

                    return {
                        ...state,
                        models,
                        backupModel,
                        backupIndex,
                    };
                }

                case CrudActionTypes.UPDATE_MODEL_SUCCESS: {

                    if (state.backupIndex !== null) {

                        return {
                            ...state,
                            backupModel: null,
                            backupIndex: null,
                        };

                    } else {

                        // EXTERNAL UPDATE

                        const models = state.models.slice();

                        const backupIndex = models.findIndex(r => r.id === action.payload.id);

                        if (backupIndex > -1) {

                            models.splice(backupIndex, 1, { ...action.payload });
                        }

                        return {
                            ...state,
                            models,
                        };
                    }
                }

                case CrudActionTypes.DELETE_MODEL: {

                    const models = state.models.slice();

                    const backupIndex = models.findIndex(r => r.id === action.payload);
                    const backupModel = { ...models[backupIndex] };

                    models.splice(backupIndex, 1);

                    return {
                        ...state,
                        models,
                        backupModel,
                        backupIndex,
                    };
                }

                case CrudActionTypes.DELETE_MODEL_SUCCESS: {

                    if (state.backupIndex !== null) {

                        return {
                            ...state,
                            backupModel: null,
                            backupIndex: null,
                        };

                    } else {

                        // EXTERNAL DELETE

                        const models = state.models.slice();

                        const backupIndex = models.findIndex(r => r.id === action.payload);

                        if (backupIndex > -1) {

                            models.splice(backupIndex, 1);
                        }

                        return {
                            ...state,
                            models,
                        };
                    }
                }

                case CrudActionTypes.UPDATE_MODEL_FAILED:
                case CrudActionTypes.DELETE_MODEL_FAILED: {

                    if (state.backupIndex !== null) {

                        const models = state.models.slice();
                        const deleteCount = action.type === CrudActionTypes.UPDATE_MODEL_FAILED ? 1 : 0;

                        const model: ICrud.IModel = { ...state.backupModel };

                        models.splice(state.backupIndex, deleteCount, model);

                        return {
                            ...state,
                            models,
                            backupModel: null,
                            backupIndex: null,
                        };

                    } else {

                        return state;
                    }
                }

                case CrudActionTypes.DESTROY_STATE: {

                    return {
                        ...initialState,
                    };
                }

                default: {

                    return state;
                }
            }

        };

    }
}

