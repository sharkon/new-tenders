import { Action } from '@ngrx/store';
import { IPagination } from '../../interfaces/pagination.interface';

export namespace SortActionTypes {

    export const SET_CONFIG = '[Sort] SET_CONFIG';
    export const SET_REQUEST = '[Sort] SET_REQUEST';
}

export namespace SortActions {

    export class SetConfig implements Action {

        readonly type = SortActionTypes.SET_CONFIG;

        constructor(public uid: string, public payload: IPagination.ISortConfig) {
        }
    }

    export class SetRequest implements Action {

        readonly type = SortActionTypes.SET_REQUEST;

        constructor(public uid: string, public payload: IPagination.ISortRequest) {
        }
    }

    export type All =
        SetConfig
        | SetRequest;
}
