import {Injectable} from '@angular/core';
import {Actions, ofType} from '@ngrx/effects';
import {RouterStateUrl} from '../../providers/custom-router-state-serializer';
import {ROUTER_NAVIGATED, RouterNavigationAction} from '@ngrx/router-store';
import {Store} from '@ngrx/store';
import {SortActions} from './sort.actions';
import {SortStoreConfig} from './sort.store.config';
import {SortStore} from './sort.reducer';
import {filter, map} from 'rxjs/operators';

@Injectable()
export class SortEffects {

    constructor(private storeConfig: SortStoreConfig,
                private store$: Store<SortStore.IState>,
                private actions$: Actions) {

        this.actions$.pipe(
            ofType(ROUTER_NAVIGATED)
            , map((action: RouterNavigationAction<RouterStateUrl>) => action.payload)
            , filter(payload => payload.routerState.data.module === this.storeConfig.moduleUid)
            , map(payload => {

                const order_by = payload.routerState.queryParams['order_by'] || null;
                const order_direction = payload.routerState.queryParams['order_direction'] || null;

                this.store$.dispatch(new SortActions.SetRequest(this.storeConfig.storeUid, {
                    order_by,
                    order_direction
                }));
            })
        )
            .subscribe();

        this.store$.dispatch(new SortActions.SetConfig(this.storeConfig.storeUid, this.storeConfig.config));
    }
}
