import { InjectionToken } from '@angular/core';
import { IPagination } from '../../interfaces/pagination.interface';


export class SortStoreConfig {
    storeUid: string;
    moduleUid: string;
    config: IPagination.ISortConfig;
}

export const SORT_STORE_CONFIG = new InjectionToken<SortStoreConfig>('sort.store.config');
