import { AppStore } from '../../store/app.store';
import { SortActions, SortActionTypes } from './sort.actions';
import { createSelector, MemoizedSelector } from '@ngrx/store';
import { IPagination } from '../../interfaces/pagination.interface';
import { PaginationUtils } from '../../utils/pagination-utils';

export namespace SortStore {

    export interface IState {
        request: IPagination.ISortRequest;
        config: IPagination.ISortConfig;
    }

    export interface IExtendedState extends AppStore.IState {
        sort: IState;
    }

    const initialState: IState = {
        request: null,
        config: {
            defaultRequest: {
                order_by: IPagination.PAGINATION_ORDER_BY.NAME,
                order_direction: IPagination.PAGINATION_ORDER_DIRECTION.ASC,
            },
            orderByValues: [IPagination.PAGINATION_ORDER_BY.NAME],
        },
    };

    export interface ISelects {
        request: (state: IState) => IPagination.ISortRequest;
        config: (state: IState) => IPagination.ISortConfig;
    }

    export function createStoreSelector(selector: MemoizedSelector<object, IState>) {

        class Catalog2SearchProductsSort implements ISelects {

            request = createSelector(
                selector,
                (state: IState) => state.request,
            );

            config = createSelector(
                selector,
                (state: IState) => state.config,
            );

        }

        return new Catalog2SearchProductsSort();
    }

    export function createReducer(uid: string) {

        return function reducer(state = { ...initialState }, action: SortActions.All) {

            if (action.uid !== uid) {
                return state;
            }

            switch (action.type) {

                case SortActionTypes.SET_CONFIG: {

                    const config = action.payload ? { ...action.payload } : null;
                    const request = config ? { ...config.defaultRequest } : null;

                    return {
                        ...state,
                        config,
                        request,
                    };
                }

                case SortActionTypes.SET_REQUEST: {

                    const request = action.payload ? { ...action.payload } : null;

                    let order_direction = (request && request.order_direction) || state.config.defaultRequest.order_direction;
                    let order_by = (request && request.order_by) || state.config.defaultRequest.order_by;

                    order_direction = PaginationUtils.SortDirectionValidate(order_direction);

                    const orderByValues = state.config && state.config.orderByValues;

                    if (orderByValues && orderByValues.length) {

                        const find = orderByValues.find(val => val === order_by);

                        if (!find) {
                            order_by = orderByValues[0];
                        }
                    }

                    return {
                        ...state,
                        request: {
                            order_by,
                            order_direction,
                        },
                    };
                }

                default: {

                    return state;
                }
            }
        };
    }

}
