import { IPagination } from '../interfaces/pagination.interface';

export namespace PaginationUtils {

    export function revertOrderDirection(orderDirection: IPagination.PaginationOrderDirectionType) {

        if (orderDirection === IPagination.PAGINATION_ORDER_DIRECTION.ASC) {

            return IPagination.PAGINATION_ORDER_DIRECTION.DESC;
        } else {
            return IPagination.PAGINATION_ORDER_DIRECTION.ASC;
        }
    }

    export function GetPaginationUrlQueryString(request: IPagination.IPaginationOnlyRequest) {

        if (!request) {
            return '';
        }

        return `&offset=${request.offset}&limit=${request.limit}`;
    }

    export function GetSortUrlQueryString(request: IPagination.ISortRequest) {

        if (!request) {
            return '';
        }

        return `&order_by=${request.order_by}&order_direction=${request.order_direction}`;
    }

    export function SortDirectionValidate(dir: IPagination.PaginationOrderDirectionType) {

        if (dir === IPagination.PAGINATION_ORDER_DIRECTION.ASC || dir === IPagination.PAGINATION_ORDER_DIRECTION.DESC) {
            return dir;
        }

        return IPagination.PAGINATION_ORDER_DIRECTION.ASC;
    }

    export function PaginationCreatePageArray(instance: IPagination.IPaginationResponse,
        currentPage: number, paginationRange: number, itemsPerPage?: number) {

        const totalItems = instance.totalItems;
        const perPage = itemsPerPage || instance.limit;

        // paginationRange could be a string if passed from attribute, so cast to number.
        paginationRange = +paginationRange;

        const pages = [];
        const totalPages = Math.ceil(totalItems / perPage);
        const halfWay = Math.ceil(paginationRange / 2);
        const isStart = currentPage <= halfWay;
        const isEnd = totalPages - halfWay < currentPage;
        const isMiddle = !isStart && !isEnd;
        const ellipsesNeeded = paginationRange < totalPages;

        let i = 1;

        while (i <= totalPages && i <= paginationRange) {
            let label = void 0;
            const pageNumber = PaginationUtils.PaginationCalculatePageNumber(i, currentPage, paginationRange, totalPages);
            const openingEllipsesNeeded = (i === 2 && (isMiddle || isEnd));
            const closingEllipsesNeeded = (i === paginationRange - 1 && (isMiddle || isStart));

            if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
                label = '...';
            } else {
                label = pageNumber;
            }
            pages.push({
                label: label,
                value: pageNumber
            });
            i++;
        }

        return pages;
    }

    /**
     * Given the position in the sequence of pagination links [i],
     * figure out what page number corresponds to that position.
     */
    export function PaginationCalculatePageNumber(i, currentPage, paginationRange, totalPages) {
        const halfWay = Math.ceil(paginationRange / 2);
        if (i === paginationRange) {
            return totalPages;
        } else if (i === 1) {
            return i;
        } else if (paginationRange < totalPages) {
            if (totalPages - halfWay < currentPage) {
                return totalPages - paginationRange + i;
            } else if (halfWay < currentPage) {
                return currentPage - halfWay + i;
            } else {
                return i;
            }
        } else {
            return i;
        }
    }
}
