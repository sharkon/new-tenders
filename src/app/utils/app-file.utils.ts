export namespace AppFileUtils {

    export function downloadFile(url: string, document: HTMLDocument): boolean {

        if (!url) {
            return false;
        }

        const a: HTMLAnchorElement = document.createElement('A') as HTMLAnchorElement;

        a.href = url;
        a.download = url.substr(url.lastIndexOf('/') + 1);

        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);

        return true;
    }
}
