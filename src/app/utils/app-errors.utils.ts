import {IError} from '../interfaces/error.interface';
import {AppErrorsConst} from '../consts/app-errors.const';
import {TranslateService} from '../services/translate/translate.service';

export namespace AppErrorsUtils {

    export function parseErrors(errors: any | string | IError.IBackendError[], ts: TranslateService): string {

        if (!errors) {
            return null;
        }

        try {

            let errorMessage: string;

            if ((<any>errors).error) {
                errors = (<any>errors).error;
            }

            if (errors.forEach) {

                errors.forEach(error => {

                    Object.keys(error).forEach(key => {

                        const errorTypes = error[key];
                        const mapEM = AppErrorsConst.mapErrorMessage(ts);

                        if (!errorTypes.length || !mapEM[key]) {
                            return null;
                        }

                        errorMessage = mapEM[key][errorTypes[0]];

                    });
                });

            } else {

                errorMessage = 'Server Error';
            }

            return errorMessage;

        } catch (e) {
            return 'Server Error';
        }

    }

}
