import {FormArray} from '@angular/forms';

export namespace AppFormUtils {

    export function clearFormArray(formArray: FormArray) {
        while (formArray.length !== 0) {
            formArray.removeAt(0);
        }
    }
}
