import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { AppStore } from '../../store/app.store';
import { catchError, switchMap } from 'rxjs/operators';
import { IUser } from '../../interfaces/user.interface';

@Injectable({
    providedIn: 'root'
})
export class RoleGuard implements CanActivate {

    constructor(private router: Router,
        private store$: Store<AppStore.IState>) {
    }

    canActivate(next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> {

        return this.store$.pipe(
            select(AppStore.Selects.appAuth.user),
            switchMap((user: IUser.IUserResponse) => {

                if (!user) {

                    this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
                    return of(false);

                } else {

                    let checkRole = false;

                    const roleWhiteList = next.data.roleWhiteList;

                    if (roleWhiteList) {

                        const arrIntersection = user.role.filter(value => roleWhiteList.indexOf(value) > -1);
                        checkRole = arrIntersection.length > 0;
                    }

                    if (!checkRole) {
                        this.router.navigate(['/']);
                    }

                    return of(checkRole);
                }
            })
        );
    }
}
