import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppStore } from '../../store/app.store';
import { switchMap, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class NoAuthGuard implements CanActivate {

    constructor(private router: Router,
        private store$: Store<AppStore.IState>) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

        const noAuth = () => {
            return of(true);
        };

        return this.store$.pipe(
            select(AppStore.Selects.appAuth.authenticated),
            switchMap((authenticated: boolean) => {

                if (!authenticated) {
                    return noAuth();
                }

                this.router.navigate(['/']);
                return of(false);
            }),
            catchError((e) => noAuth())
        );
    }

}
