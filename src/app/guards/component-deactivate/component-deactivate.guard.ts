import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { IComponentDeactivateGuard } from './component-deactivate-guard.interface';

@Injectable({
    providedIn: 'root'
})
export class ComponentDeactivateGuard implements CanDeactivate<IComponentDeactivateGuard> {

    constructor() {
    }

    canDeactivate(component: IComponentDeactivateGuard): Observable<boolean> | Promise<boolean> | boolean {

        return component.canDeactivate();
    }
}
